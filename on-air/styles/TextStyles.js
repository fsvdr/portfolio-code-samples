import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { Color, Space, Font, FontSize, FontWeight } from './Settings';

export const Logo = styled(Link)`
	font-family: ${ Font.display };
	font-weight: ${ FontWeight.black };
	font-size: ${ FontSize.medium };
	color: ${ Color.white };
`;

export const NavLink = styled.li`
	font-family: ${ Font.regular };
	font-weight: ${ FontWeight.bold };
	font-size: ${ FontSize.regular };
	cursor: pointer;
`;

export const Small = styled.small`
	font-family: ${ Font.regular };
	font-weight: ${ FontWeight.thin };
	font-size: ${ FontSize.xSmall};
	margin: ${ Space.regular } 0;
	color: ${ Color.lightBlack };
`;

export const FeaturedTitle = styled.h1`
	font-family: ${ Font.display };
	font-weight: ${ FontWeight.black };
	font-size: ${ FontSize.large };
	letter-spacing: 1.4px;
`;
