import styled, { css } from 'styled-components';
import { Breakpoints } from './Settings';

export const Container = styled.div`
  width: 100%;
  min-height: 100vh;
`;

export const Flex = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: flex-start;
  align-items: flex-start;
  align-content: flex-start;

  ${props => props.row && css`flex-direction: row;`}
  ${props => props.column && css`flex-direction: column;`}
  ${props => props.reverse && props.column && css`flex-direction: column-reverse;`}
  ${props => props.reverse && !props.column && css`flex-direction: row-reverse;`}

  ${props => props.wrap && css`flex-wrap: wrap;`}
  ${props => props.nowrap && css`flex-wrap: nowrap;`}
  ${props => props.wrapRev && css`flex-wrap: wrap-reverse;`}

  ${props => props.justifyStart && css`justify-content: flex-start;`}
  ${props => props.justifyEnd && css`justify-content: flex-end;`}
  ${props => props.justifyCenter && css`justify-content: center;`}
  ${props => props.justifyBetween && css`justify-content: space-between;`}
  ${props => props.justifyAround && css`justify-content: space-around;`}
  ${props => props.justifyEvenly && css`justify-content: space-evenly;`}

  ${props => props.alignItemsStart && css`align-items: flex-start;`}
  ${props => props.alignItemsEnd && css`align-items: flex-end;`}
  ${props => props.alignItemsCenter && css`align-items: center;`}
  ${props => props.alignItemsStretch && css`align-items: stretch;`}
  ${props => props.alignItemsBaseline && css`align-items: baseline;`}

  ${props => props.alignContentStart && css`align-content: flex-start;`}
  ${props => props.alignContentEnd && css`align-content: flex-end;`}
  ${props => props.alignContentCenter && css`align-content: center;`}
  ${props => props.alignContentBetween && css`align-content: space-between;`}
  ${props => props.alignContentAround && css`align-content: space-around;`}
  ${props => props.alignContentStretch && css`align-content: stretch;`}
`;

const getColWidth = (size) => `${size * 100 / 12}%`;

export const FlexItem = styled.div`

	width: ${props => props.mob && getColWidth(props.mob)}

	@media screen and (min-width: ${ Breakpoints.tab }) {
		width: ${props => props.tab && getColWidth(props.tab)}
	}

	@media screen and (min-width: ${ Breakpoints.lap }) {
		width: ${props => props.lap && getColWidth(props.lap)}
	}
`;
