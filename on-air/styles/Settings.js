/**
 * This file contains application-wide style variables
 *
 * These only work for styled components, however, for css classes
 * this serves as a quick reference ;)
 */

export const Color = {
	primary: '#402E57',
	secondary: '#FC4358',
	darkPurple: '#322A3F',
	grey: '#F9F9F9',

	lightBlack: 'rgba(0, 0, 0, 0.53)',

	white: '#FFFFFF',
	black: '#000000',
};

export const Font = {
	display: 'Montserrat, sans-serif',
	regular: 'Lato, sans-serif',
	thin: 'Work Sans, sans-serif'
};

export const FontWeight = {
	thin: 100,
	regular: 400,
	bold: 700,
	black: 900
};

export const FontSize = {
	xxLarge: '3.286em',	// 46px
	xLarge: '2.429em', // 36px
	large: '1.571em', // 22px
	medium: '1.286em', // 18px
	regular: '1em', // 14px
	small: '0.857em', // 12px
	xSmall: '0.714em', // 10px
};

export const BorderRadius = {
	small: '0.5em',
	medium: '1em',
	large: '2em',
	circle: '50%'
};

export const Space = {
	small: '0.5em',
	regular: '1em',
	large: '2em',
	xLarge: '4em',
};

export const Breakpoints = {
	tab: '768px',
	lap: '1024px'
};

export const IconSize = {
	small: {
		w: '1.4em',
		h: '1.4em'
	},
	large: {
		w: '2em',
		h: '2em'
	},
	xLarge: {
		w: '3em',
		h: '3em'
	}
};
