import styled from 'styled-components';
import {
	Color,
	Space,
	Font,
	FontWeight,
	FontSize,
	IconSize
} from './Settings';

export const TextButton = styled.button`
	width: auto;
	max-width: 250px;
	font-family: ${ Font.display };
	font-weight: ${ FontWeight.bold };
	font-size: ${props => props.primary ? FontSize.regular : FontSize.small };
	letter-spacing: 1.4px;
	color: inherit;
	background-color: transparent;
	padding: ${ Space.regular } ${ Space.large };
	transition: all 0.3s;

	.icon {
		display: inline-block;
		width: ${ IconSize.large.w };
		height: ${ IconSize.large.h };
		color: ${props => props.primary ? Color.secondary : 'inherit' };
		margin-right: ${ Space.regular };
		vertical-align: middle;
	}

	:hover { opacity: 0.53; }
`;
