import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Container } from './styles';
import Header from './components/Header';
import Home from './containers/Home';
import Search from './containers/Search';
import PodcastOverview from './containers/PodcastOverview';
import PlayerWidget from './components/PlayerWidget';
import Player from './containers/Player';
import Studio from './containers/Studio';
import conf from './conf';

class App extends Component {

  constructor (props) {
    super (props);

    this.state = {
			isLoading: true,
      user: null,
			isPlaying: false,
			nowPlaying: null,
			queue: [],
			error: false,
    };

		this.audioSource = null;

		this.removeHashFromUrl = this.removeHashFromUrl.bind (this);
    this.handleLogout = this.handleLogout.bind (this);
		this.handleReproduction = this.handleReproduction.bind (this);
		this.handleAddToQueue = this.handleAddToQueue.bind (this);
  }

	removeHashFromUrl () {
		const url = window.location.toString ();
		if (url.includes ('#')) {
			const cleaned = url.substring (0, url.indexOf('#'));
			window.history.replaceState ({}, document.title, cleaned);
		}
	}

	componentWillMount () {
		const hash = window.location.hash.split('&')[0];
		const parsedHash = hash.substring (hash.indexOf ('=') + 1);

		this.removeHashFromUrl ();

		if (hash.includes ('access_token')) {
			localStorage.setItem ('access_token', parsedHash);
		}

		if (localStorage.getItem ('access_token')) {
			const config = {
				headers: {'Authorization': `Bearer ${localStorage.getItem ('access_token')}`}
			};

			fetch (`${conf.DOMAIN}:9999/user`, config)
				.then (res => res.json ())
				.then (json => {
					const user = json.principal;

					return {
						id: user.uuid,
						name: user.name,
						email: user.email,
						username: user.username,
						followers: user.followers
					}
				})
				.then (user => this.setState ({ user, isLoading: false, error: false }))
				.catch (err => {
					this.setState ({ isLoading: false, error: true });
					this.handleLogout ();
					console.log('An error occurred while fetching the authenticated user.', err)
				});
		}
	}

  handleLogout () {
    localStorage.removeItem ('access_token');
    this.setState ({ user: null });
  }

	handleReproduction (episode) {
		const { nowPlaying } = this.state;
		if (nowPlaying) {
			if (nowPlaying.id === episode.id) {
				// Pause current
				this.audioSource.pause ();
				this.setState ({ nowPlaying: episode, isPlaying: false });
			} else {
				// Reproduce new one
				this.audioSource.src = episode.file;
				this.audioSource.play ();
				this.setState ({ nowPlaying: episode, isPlaying: true });
			}
		} else {
			this.audioSource = new Audio (episode.file);
			this.audioSource.play ();
			this.setState ({ nowPlaying: episode, isPlaying: true });
		}


	}

	handleAddToQueue (episode) {
		// TODO: Implement queue logic
	}

  render() {
    const { user, nowPlaying, isPlaying } = this.state;

    return (
      <Container>
        <Router>
					<Fragment>
            <Header user={user} handleLogout={this.handleLogout} />

						<Route exact path="/" render={(props) =>  <Home user={user} /> } />
						<Route path="/search" component={ Search } />
						<Route path="/podcast/:id" render={(props) => user && <PodcastOverview {...props} handleReproduction={this.handleReproduction} user={user} /> } />
						<Route path="/player" render={ (props) => nowPlaying && <Player user={user} isPlaying={isPlaying} episode={nowPlaying} handleReproduction={this.handleReproduction}/> } />
						<Route path="/studio" render={(props) => user && <Studio {...props} handleReproduction={this.handleReproduction} user={user} />} />

						<Route path="/" render={(props) =>
							(props.location.pathname !== '/player')
							&& nowPlaying
							&& user && <PlayerWidget {...props} isPlaying={ isPlaying } episode={ nowPlaying } handleReproduction={this.handleReproduction} />} />
					</Fragment>
        </Router>
      </Container>
    );
  }
}

export default App;
