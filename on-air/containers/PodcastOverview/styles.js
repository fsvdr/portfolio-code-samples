import styled, { css } from 'styled-components';
import ReactLoading from 'react-loading';
import {
	Flex,
	Color,
	Space,
	Breakpoints,
	BorderRadius,
	Font,
	FontWeight,
	FontSize
} from '../../styles';

export const Loader = styled(ReactLoading)`
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
`;

export const Wrapper = Flex.extend`
	flex-flow: column nowrap;

	::before {
		content: '';
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 55vh;
		background-color: ${ Color.primary };
		box-shadow: inset 0 0 0 999px rgba(0, 0, 0, 0.32);
		z-index: -1;
	}

	${props => props.background && css`
			::before {
				background: url('${ props.background }') 50% 50%;
				background-size: cover;
			}
		`}
`;

export const Header = Flex.extend`
	flex-flow: column nowrap;
	align-items: stretch;
	width: 100%;
	padding: 24vh ${ Space.small } 0;

	@media screen and (min-width: ${ Breakpoints.lap }) {
		flex-flow: row nowrap;
		align-items: flex-end;
		height: 55vh;
		padding: ${ Space.regular } ${ Space.large };
	}
`;

export const PodcastDetails = Flex.extend`
	flex-flow: column nowrap;
	justify-content: flex-end;
	align-items: center;
	width: 100%;

	.cover {
		box-shadow: 0 5px 22px 0 rgba(0, 0, 0, 0.32);
	}

	@media screen and (min-width: ${ Breakpoints.lap }) {
		flex-flow: row nowrap;
		justify-content: flex-start;
		align-items: flex-end;
		min-height: initial;
		color: ${ Color.white };

		.cover {
			position: relative;
			top: 10vh;
			width: 24em;
			height: 24em;
		}
	}
`;

export const Actions = Flex.extend`
	flex-flow: column nowrap;
	justify-content: center;
	align-items: center;
	padding: ${ Space.regular } ${ Space.regular } 0;

	button { margin-top: ${ Space.small }; }

	.morphing {
		width: 100%;
		color: ${ Color.white };
		background-color: ${ Color.primary };
		border-radius: ${ BorderRadius.small };
	}

	@media screen and (min-width: ${ Breakpoints.lap }) {
		align-items: flex-end;
		width: 33%;

		button {
			text-align: right;
			color: ${ Color.white };
		}

		.morphing {
			width: auto;
			background-color: transparent;
		}
	}
`;

export const Body = Flex.extend`
	width: 100%;
	flex-flow: column nowrap;
	padding-top: 5vh;

	@media screen and (min-width: ${Breakpoints.lap} ) {
		flex-flow: row wrap;
		padding-top: 10vh;
	}
`;

export const PodcastDescription = styled.div`
	width: 100%;
	padding: ${ Space.small } ${ Space.large };

	p {
		font-family: ${ Font.regular };
		font-weight: ${ FontWeight.regular };
		font-size: ${ FontSize.regular };
		opacity: 0.53;
		letter-spacing: 1.4px;
		line-height: 1.8;
		text-align: justify;
		max-width: 75ch;
	}

	@media screen and (min-width: ${ Breakpoints.lap }) {
		width: 80%;

		p { padding-left: ${ Space.large }; }
	}
`;

export const PodcastStats = Flex.extend`
	width: 100%;
	flex-flow: row nowrap;
	justify-content: space-between;
	align-items: flex-start;
	padding: ${ Space.large };

	.stat {
		text-align: left;

		h1 {
			font-family: ${ Font.regular };
			font-weight: ${ FontWeight.bold };
			font-size: ${ FontSize.small };
			text-transform: uppercase;
			letter-spacing: 1.2px;
			margin-bottom: ${ Space.small };
		}

		span {
			font-family: ${ Font.thin };
			font-weight: ${ FontWeight.thin };
			font-size: ${ FontSize.xLarge };
		}
	}

	@media screen and (min-width: ${ Breakpoints.lap }) {
		width: 20%;
		flex-flow: column nowrap;
		justify-content: space-between;
		align-items: flex-end;
		padding-right: ${ Space.large };

		.stat {
			text-align: right;
			margin: ${ Space.regular } ${ Space.large };
		}
	}
`;

export const List = Flex.extend`
	flex-flow: row wrap;
	width: 100%;
	padding: ${ Space.large };
	margin-top: ${ Space.large };
	background-color: ${ Color.grey };

	@media screen and (min-width: ${ Breakpoints.lap }) {
		.episode { padding: ${ Space.regular }; }
	}
`;

export const ModalHeader = Flex.extend`
	flex-flow: row nowrap;
	align-items: stretch;
	position: relative;
	width: 100%;
	padding: ${ Space.regular }
	overflow: hidden;
	padding-top: ${ Space.xLarge };
	color: ${ Color.white };


	::before {
		content: '';
		position: absolute;
		top: 50%;
		left: 50%;
		width: 140%;
		height: 140%;
		transform: translate(-50%, -50%);
		filter: blur(1em);
		background-color: ${ Color.primary };
		z-index: -1;
	}

	${props => props.background && css`

			::before {
				background: url('${props.background}') 50% 50% no-repeat;
				background-size: cover;
				box-shadow: inset 0 0 0 999px rgba(0, 0, 0, 0.36);
			}
		`}

	@media screen and (min-width: ${ Breakpoints.lap }) {
		padding-right: 40%;
	}
`;

export const ModalBody = styled.div`
	padding: ${ Space.large };

	p {
		position: relative;
		opacity: 0.53;
		overflow: hidden;
		transition: all 0.3s;
		line-height: 1.8;
		text-align: justify;
	}
`
