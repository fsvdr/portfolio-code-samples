import React, { Component } from 'react';
import FeatherIcon from 'feather-icons-react';
import {
	Wrapper,
	Header,
	PodcastDetails,
	Actions,
	Body,
	PodcastDescription,
	PodcastStats,
	List,
	ModalHeader,
	ModalBody,
	Loader
} from './styles';
import { Cover, Header as Details } from '../../components/Podcast/styles';
import Episode, { EpisodeCover, EpisodeActions, EpisodeHeader, EpisodeDescription } from '../../components/Episode';
import Modal from '../../components/Modal';
import { Color, Flex, FlexItem, TextButton } from '../../styles';
import conf from '../../conf';

export default class PodcastOverview extends Component {

	constructor (props) {
		super (props);

		this.state = {
			isLoading: true,
			podcast: null,
			episodes: [],
			subscriptions: [],
			favorites: [],
			selectedEpisode: null,
			error: false
		};

		this.config = {
			headers: {'Authorization': `Bearer ${localStorage.getItem ('access_token')}`}
		};

		this.handleEpisodeSelection = this.handleEpisodeSelection.bind (this);
		this.handleEpisodeFav = this.handleEpisodeFav.bind (this);
		this.handleSubscription = this.handleSubscription.bind (this);
	}

	componentWillMount () {
		const user = this.props.user;

		if (!this.state.podcast || this.props.match.params.id !== this.state.podcast.id) {
			const id = this.props.match.params.id;
			const requests = [];

			this.setState ({ isLoading: true });

			// Fetch podcast data
			requests.push (new Promise ((resolve, reject) => {
				fetch (`${conf.API_URL}/podcast/${id}`, this.config)
					.then (res => res.json ())
					.then (json => {
						return {
							id: json.uuid,
							name: json.name,
							description: json.description,
							cover: json.cover,
							background: json.background,
							features: json.featured,
							author: json.author,
							creator: json.creatorId,
							category: json.category,
							publicationDate: json.publicationDate,
							subscribers: json.subscribers
						};
					})
					.then ((podcast) => {
						this.setState ({ podcast });
						resolve ();
					})
					.catch (e => {
						console.log ('Something went wrong while fetching the podcast', e);
						reject ();
					})
			}));

			// Fetch episodes
			requests.push (new Promise ((resolve, reject) => {
				fetch (`${conf.API_URL}/podcast/${id}/episodes`, this.config)
					.then (res => res.json ())
					.then (json => json.map ((e) => {
						return {
							id: e.uuid,
							title: e.title,
							description: e.description,
							cover: e.cover,
							season: e.season,
							file: e.file,
						};
					}))
					.then ((episodes) => {
						this.setState ({ episodes });
						resolve ();
					})
					.catch (e => {
						console.log ('Something went wrong while fetching the episodes', e);
						reject ();
					});
			}));

			// Fetch subscriptions
			requests.push (new Promise ((resolve, reject) => {
				fetch (`${conf.API_URL}/user/${user.id}/subscriptions`, this.config)
					.then (res => res.json ())
					.then (json => json.map (s => s.uuid))
					.then (subscriptions => {
						this.setState ({ subscriptions })
						resolve ();
					})
					.catch (e => {
						console.log ('Something went wrong while fetching the subscriptions', e);
						reject ();
					})
			}));

			// Fetch favs
			requests.push (new Promise ((resolve, reject) => {
				fetch (`${conf.API_URL}/user/${user.id}/favorites`, this.config)
					.then (res => res.json ())
					.then (json => json.map (f => f.uuid))
					.then (favorites => {
						this.setState ({ favorites });
						resolve ();
					})
					.catch (e => {
						console.log ('Something went wrong while fetching the favorites', e);
						reject ();
					})
			}));

			// Wait for all requests to fullfill
			Promise.all (requests)
				.then (() => {
					this.setState ({ isLoading: false, error: false });
				})
				.catch ((e) => {
					console.log (e);
					this.setState ({ isLoading: false, error: true });
				});
		}
	}

	handleSubscription () {
		const oldSubscriptions = this.state.subscriptions.slice ();
		const { podcast } = this.state;

		const config = {
			headers: {'Authorization': `Bearer ${localStorage.getItem ('access_token')}`}
		};

		if (oldSubscriptions.includes (podcast.id)) {
			// Remove subscription
			fetch (`${conf.API_URL}/podcast/unsubscribe/${podcast.id}`, Object.assign ({}, this.config, {method: 'PUT'}))
				.then (res => {
					const subscriptions = oldSubscriptions.filter (s => s !== podcast.id);
					this.setState ({ subscriptions });
				})
				.catch (e => {
					console.log ('An error occured while unsubscribing to podcast', e);
				});
		} else {
			// Add podcast to subscriptions
			fetch (`${conf.API_URL}/podcast/subscribe/${podcast.id}`, Object.assign ({}, this.config, {method: 'POST'}))
				.then (res => res.json ())
				.then (json => {
					const subscriptions = [...oldSubscriptions, podcast.id];
					this.setState ({ subscriptions });
				})
				.catch (e => {
					console.log ('An error occurred while subscribing to podcast', e);
				});
		}
	}

	handleEpisodeSelection (id) {
		const { episodes } = this.state;
		this.setState ({ selectedEpisode: episodes.find(e => e.id === id) });
	}

	handleEpisodeFav (id) {
		const { user } = this.props;
		const oldFavorites = this.state.favorites.slice();

		const config = {
			headers: {'Authorization': `Bearer ${localStorage.getItem ('access_token')}`}
		};

		if (oldFavorites.includes (id)) {
			// Remove from favorites
			fetch (`${conf.API_URL}/episode/unfavorite/${id}`, Object.assign ({}, this.config, {method: 'PUT'}))
				.then (res => {
					const favorites = oldFavorites.filter (f => f !== id);
					this.setState ({ favorites });
				})
				.catch (e => {
					console.log ('An error occured while removing episode from favorites', e);
				});
		} else {
			// Add to favorites
			fetch (`${conf.API_URL}/episode/favorite/${id}`, Object.assign ({}, this.config, {method: 'POST'}))
				.then (res => res.json ())
				.then (json => {
					const favorites = [...oldFavorites, id];
					this.setState ({ favorites });
				})
				.catch (e => {
					console.log ('An error occurred while adding episode to favorites', e)
				});
		}
	}

	render () {
		const { handleReproduction, isPlaying } = this.props;
		const { isLoading, podcast, episodes, subscriptions, favorites, selectedEpisode, error } = this.state;

		const body = isLoading || error
			? <Loader className="loader" type="bubbles" width={120} height={120} color={Color.primary} />
			: (
				<Wrapper background={!isLoading && podcast.background ? podcast.background : null}>

					{
						selectedEpisode
						? (
							<Modal handleClose={() => this.setState ({ selectedEpisode: null })}>
								<ModalHeader background={selectedEpisode.cover}>
									<EpisodeCover cover={selectedEpisode.cover} />
										<EpisodeActions
											className="actions"
											id={selectedEpisode.id}
											isPlaying={isPlaying}
											isFavorite={favorites.includes (selectedEpisode.id)}
											handleFav={this.handleEpisodeFav}
											handleReproduction={() => handleReproduction(selectedEpisode)} />
								</ModalHeader>
								<ModalBody>
									<EpisodeHeader title={selectedEpisode.title} publicationDate={selectedEpisode.publicationDate} />
									<EpisodeDescription description={selectedEpisode.description} />
								</ModalBody>
							</Modal>
						)
						: null
					}

					<Header>
						<PodcastDetails>
							<Cover className="cover" size="BIG"><img src={podcast.cover} alt="" /></Cover>

							<Details size="BIG">
								<h1>{podcast.name}</h1>
								<Flex justifyBetween alignItemsCenter>
									<span className="author">{podcast.author}</span>
									<span className="date">{podcast.publicationDate}</span>
								</Flex>
								<div className="subscribers">
									<FeatherIcon icon="users"/>
									<span>{podcast.subscribers}</span>
								</div>
							</Details>
						</PodcastDetails>

						<Actions>
							<TextButton onClick={() => handleReproduction (episodes[0])} primary><FeatherIcon className="icon" icon="play-circle" /> Play All</TextButton>
							<TextButton onClick={() => this.handleSubscription ()} className="morphing"><FeatherIcon className="icon" icon="award" /> {subscriptions.includes (podcast.id) ? 'Unsubscribe' : 'Subscribe'}</TextButton>
						</Actions>
					</Header>

					<Body>
						<PodcastDescription>
							<p>{podcast.description}</p>
						</PodcastDescription>

						<PodcastStats>
							<div className="stat">
								<h1>Category</h1>
								<span>{podcast.category}</span>
							</div>

							<div className="stat">
								<h1>Episodes</h1>
								<span>{episodes.length}</span>
							</div>
						</PodcastStats>

						<List>
							{
								episodes.map (e => (
									<FlexItem mob="12" lap="6" key={e.id}>
										<Episode {...e} className="episode"
											isFavorite={favorites.includes (e.id)}
											handleSelection={this.handleEpisodeSelection}
											handleFav={this.handleEpisodeFav}
											handleReproduction={() => handleReproduction (e)}/>
									</FlexItem>
								))
							}
						</List>
					</Body>
				</Wrapper>
			);

		return body;
	}
};
