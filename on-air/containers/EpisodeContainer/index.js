import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class EpisodeContainer extends Component {
  render() {
    return (
      <div>
        <h1>Hello to EpisodeContainer</h1>
        <Link to="/">Home</Link>
      </div>
    );
  }
}
