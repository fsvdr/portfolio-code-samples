import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import Slider from '../../components/Slider';
import Podcast from '../../components/Podcast';
import { Hero, Section, List } from './styles';
import { FlexItem } from '../../styles';
import conf from '../../conf';

export default class Home extends Component {
	constructor (props) {
		super (props);

		this.state = {
			isLoading: true,
			featured: [],
			error: false,
		};
	}

	componentWillMount () {
		const config = {
			headers: {'Authorization': `Bearer ${localStorage.getItem ('access_token')}`}
		};

		fetch (`${conf.API_URL}/podcast/featured`, config)
			.then (res => res.json ())
			.then (json => json.map (p => {
				return {
					id: p.uuid,
					name: p.name,
					description: p.description,
					cover: p.cover,
					background: p.background,
					features: p.featured,
					author: p.author,
					creator: p.creatorId,
					category: p.category,
					publicationDate: p.publicationDate,
					subscribers: p.subscribers
				}
			}))
			.then (podcasts => {
				this.setState ({ featured: podcasts, isLoading: false, error: false });
			})
			.catch (e => {
				this.setState ({ isLoading: false, error: true });
				console.log ('An error ocurred while fetching the featured podcasts', e)
			})
	}

  render () {
    const { isLoading, featured } = this.state;
		const { user } = this.props;

    return (
			<Fragment>

				{
					!user
					? (
						<Hero className="hero">
							<h1>Welcome to the podcast platform</h1>
							<p>The podcast addicts community where you can discover, share and create</p>
						</Hero>
					)
					: null
				}

				{
					!isLoading && featured.length >= 3
					? <Slider featured={featured} />
					: null
				}

				{
					user && (
						<Section>
							<h1>The newest</h1>

							<List>
								{
									featured.map ((f, i) => (
										<FlexItem className="item" mob="12" tab="6" lap="4" key={i}>
											<Podcast {...f} size="SMALL" />
										</FlexItem>
									))
								}
							</List>
						</Section>
					)
				}
			</Fragment>
    );
  }
}
