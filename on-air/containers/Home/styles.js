import styled from 'styled-components';
import {
	Font,
	FontWeight,
	FontSize,
	Space,
	Flex,
	Color
} from '../../styles';

export const Hero = Flex.extend`
	flex-flow: column nowrap;
	justify-content: center;
	with: 100vw;
	height: 100vh;
	position: relative;
	z-index: 0;
	color: ${ Color.white };
	overflow: hidden;
	padding: ${ Space.xLarge };

	::before {
		content: '';
		position: absolute;
		width: 120%;
		height: 120%;
		top: 50%;
		left: 50%;
		filter: blur(2em);
		box-shadow: inset 0 0 0 999px rgb(0, 0, 0, 0.74);
		background: url('./assets/hero-cover.jpg') 50% 50% no-repeat;
		background-size: cover;
		transform: translate(-50%, -50%);
		z-index: -1;
	}

	h1 {
		font-family: ${ Font.display };
		font-weight: ${ FontWeight.black };
		font-size: 4em;
		color: ${ Color.white };
	}

	p {
		font-weight: ${ FontWeight.bold };
		font-size: 1.6em;
		letter-spacing: 1.4px;
	}
`;

export const Section = styled.div`
	width: 100%;
	padding: ${ Space.large };

	& > h1 {
		font-family: ${ Font.thin };
		font-weight: ${ FontWeight.thin };
		font-size: ${ FontSize.xxLarge };
		letter-spacing: 1.4px;
		margin-bottom: ${ Space.regular };
	}
`;

export const List = Flex.extend`
	flex-flow: row wrap;
	justify-content: flex-start;
	align-items: center;
	padding-top: ${ Space.medium };

	.item { margin: ${ Space.regular } 0; }
`;
