import React from 'react';
import PropTypes from 'prop-types';
import FeatherIcon from 'feather-icons-react';
import {
	Wrapper,
	Cover,
	Header
} from './styles';

const Podcast = (props) => (
	<Wrapper to={`/podcast/${props.id}`} orientation={props.orientation}>
		<Cover size={props.size} className="cover">
			<img src={props.cover} alt="" />
		</Cover>

		<Header size={props.size}>
			<h1>{props.name}</h1>
			<span className="author">{props.author}</span>
			<span className="subscribers"><FeatherIcon icon="users" className="icon" /> {props.subscribers}</span>
		</Header>
	</Wrapper>
);

Podcast.propTypes = {
	id: PropTypes.string.isRequired,
	name: PropTypes.string.isRequired,
	description: PropTypes.string,
	cover: PropTypes.string.isRequired,
	background: PropTypes.string,
	featured: PropTypes.bool,
	author: PropTypes.string,
	category: PropTypes.string,
	publicationDate: PropTypes.string,
	subscribers: PropTypes.number,
	size: PropTypes.oneOf(['MINIMIZED', 'SMALL', 'REGULAR', 'BIG', 'GIANT']),
	orientation: PropTypes.oneOf(['HORIZONTAL', 'VERTICAL', 'RESPONSIVE'])
};

Podcast.defaultProps = {
	id: '1',
	name: 'The Habitat',
	description: 'On a remote mountain in Hawaii, there\'s a fake planet Mars. Six volunteers are secluded in an imitation Mars habitat where they will work as imitation astronauts for one very real year. The goal: to help NASA understand what life might be like on the red planet—and plan for the day when the dress rehearsals are over, and we blast off for real. Host Lynn Levy has been chronicling this experiment from the moment the crew set foot in their habitat, communicating with them through audio diaries that detail their discoveries, their frustrations, and their evolving and devolving relationships with each other. From those diaries, Gimlet Media has crafted an addictive serialized podcast: the true story of a fake planet.',
	cover: 'http://static.megaphone.fm/podcasts/806b466c-ef0c-11e6-b531-afa5d3e8b9e3/image/uploads_2F1522339695064-fiy1a2pf3tb-64aa58ac9b5bcdcf9e3145121e2a85eb_2FTheHabitat-final-cover.png',
	background: 'https://res-4.cloudinary.com/gimlet-media/image/upload/f_auto,q_auto:best/ewecvxdrglhdwtntfwsk',
	featured: true,
	author: 'Gimlet Media',
	category: 'science-fiction',
	publicationDate: 'Wednesday, April 18',
	subscribers: 0,
	size: 'REGULAR',
	orientation: 'RESPONSIVE'
}

export default Podcast;
