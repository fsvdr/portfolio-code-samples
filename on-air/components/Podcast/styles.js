import styled, { css } from 'styled-components';
import { Link } from 'react-router-dom';
import {
	Flex,
	Color,
	Space,
	BorderRadius,
	Breakpoints,
	Font,
	FontSize,
	FontWeight,
	IconSize
} from '../../styles';

export const Wrapper = Flex.withComponent(Link).extend`
	flex-flow: column nowrap;
	align-items: center;
	min-width: 16em;
	width: 100%;
	height: auto;

	${props => props.orientation === 'VERTICAL' && css`
		flex-flow: column nowrap;
	`}

	${props => props.orientation === 'HORIZONTAL' && css`
		flex-flow: row nowrap;
		justify-content: center;
		align-items: flex-end;
	`}

	${props => props.orientation === 'RESPONSIVE' && css`
		flex-flow: column nowrap;

		@media screen and (min-width: ${ Breakpoints.tab }) {
			flex-flow: row nowrap;
			justify-content: center;
			align-items: flex-end;
		}
	`}

	:hover .cover {
		box-shadow: 0 5px 22px 0 rgba(0, 0, 0, 0.32);
	}
`;

export const Cover = styled.div`
	width: 10em;
	height: 10em;
	border-radius: ${ BorderRadius.small };
	background-color: ${ Color.primary };
	overflow: hidden;
	margin: ${ Space.small } ${ Space.large };
	transition: all 0.3s;

	${props => props.size === 'MINIMIZED' && css`
			width: 3em;
			height: 3em;
		`}

	${props => props.size === 'SMALL' && css`
			width: 8em;
			height: 8em;
		`}

	${props => props.size === 'REGULAR' && css`
			width: 12em;
			height: 12em;
	`}

	${props => props.size === 'BIG' && css`
			width: 18em;
			height: 18em;
	`}

	${props => props.size === 'GIANT' && css`
			width: 24em;
			height: 24em;
	`}

	img {
		width: 100%;
		height: 100%;
		object-fit: cover;
		object-position: 50% 50%;
	}
`;

export const Header = Flex.extend`
	flex-flow: column nowrap;
	align-items: stretch;
	margin: ${ Space.small } 0;

	${props => props.size === 'MINIMIZED' && css`width: 5em;`}
	${props => props.size === 'SMALL' && css`width: 8em;`}
	${props => props.size === 'REGULAR' && css`width: 12em;`}
	${props => props.size === 'BIG' && css`width: 18em;`}
	${props => props.size === 'GIANT' && css`width: 24em;`}

	h1 {
		font-family: ${ Font.display };
		font-weight: ${ FontWeight.black };
		font-size: ${ FontSize.medium };
		letter-spacing: 1.4px;
		text-transform: uppercase;
		margin: 0;
	}

	.author,
	.date { opacity: 0.75; }

	.date { font-style: italic; }

	.icon {
		margin-right: ${ Space.small };
		width: ${ IconSize.small.w };
		height: ${ IconSize.small.h };
	}

	.subscribers {
		display: flex;
		flex-flow: row nowrap;
		justify-content: space-between;
		align-items: flex-end;
		font-weight: ${ FontWeight.bold };
		margin-top: ${ Space.small };
	}
`;
