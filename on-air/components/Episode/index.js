import React from 'react';
import PropTypes from 'prop-types';
import FeatherIcon from 'feather-icons-react';
import { Cover, Header } from '../Podcast/styles';
import { Flex } from '../../styles';
import { Wrapper, Media, Controls, Control, Body } from './styles';

export const EpisodeCover = (props) => (
	<Cover className="cover">
		<img src={props.cover} alt="" />
	</Cover>
);

export const EpisodeActions = (props) => (
	<Controls>
		<Control onClick={() => props.handleFav(props.id)} accent={props.isFavorite}><FeatherIcon icon="star" /></Control>
		<Control onClick={() => props.handleReproduction(props.id)}><FeatherIcon icon={`${props.isPlaying ? 'pause' : 'play'}`} /></Control>
	</Controls>
);

export const EpisodeHeader = (props) => (
	<Header>
		<h1>{props.title}</h1>
		<span className="date">{props.publicationDate}</span>
	</Header>
);

export const EpisodeDescription = (props) => (
	<p>{props.description}</p>
);

const Episode = (props) => (
	<Wrapper>
		<Media>
			<EpisodeCover cover={props.cover} />
			<EpisodeActions
				id={props.id}
				isFavorite={props.isFavorite}
				isPlaying={props.isPlaying}
				handleFav={props.handleFav}
				handleReproduction={props.handleReproduction} />
		</Media>

		<Flex column nowrap className="container">
			<EpisodeHeader title={props.title} publicationDate={props.publicationDate} />

			{
				props.description
				? (
					<Body>
						<EpisodeDescription description={props.description} />
						<button
							onClick={() => props.handleSelection(props.id)}>
							Show more
						</button>
					</Body>
				) : null
			}
		</Flex>
	</Wrapper>
);

Episode.propTypes = {
	id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
	title: PropTypes.string.isRequired,
	description: PropTypes.string,
	cover: PropTypes.string,
	season: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
	file: PropTypes.string.isRequired,
	isFavorite: PropTypes.bool,
	publicationDate: PropTypes.string,
	handleSelection: PropTypes.func,
	handleFav: PropTypes.func,
	handleReproduction: PropTypes.func,
};

Episode.defaultProps = {
	id: 1,
	title: 'Episode 1: This Is the Way Up',
	description: `
		The crew leaves Earth behind.

		The Habitat is a production of Gimlet Media. It’s produced by Lynn Levy, Peter Bresnan, and Megan Tan. Our editors are Alex Blumberg, Jorge Just, Caitlin Kenney, and Blythe Terrell. Music, sound design, and mixing by Haley Shaw. Music supervision by Matthew Boll. Additional music in this episode by Bobby Lord and Elliot Cole. Our credits music in this episode is performed by Ellen O, and written by David Bowie. Our fact checker is Michelle Harris. Special thanks to Kaitlin Roberts, Alexander Overington, and to Neil Scheibelhut. And a very special thanks to the HI-SEAS crew: Andrzej, Christiane, Cyprien, Carmel, Shey, and Tristan.

		To find a list of our sponsors and show-related promo codes, go to gimlet.media/OurAdvertisers.`,
	cover: 'http://static.megaphone.fm/podcasts/806b466c-ef0c-11e6-b531-afa5d3e8b9e3/image/uploads_2F1522339695064-fiy1a2pf3tb-64aa58ac9b5bcdcf9e3145121e2a85eb_2FTheHabitat-final-cover.png',
	season: 'Season One',
	file: 'https://traffic.megaphone.fm/GLT7178834586.mp3',
	isFavorite: true,
	publicationDate: 'April 18, 2018 2:30 AM',
	handleSelection: (id) => null,
	handleFav: (id) => null,
	handleReproduction: (id) => null
};

export default Episode;
