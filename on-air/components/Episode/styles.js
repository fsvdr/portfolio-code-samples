import styled from 'styled-components';
import {
	Flex,
	Color,
	Space,
	FontSize,
	FontWeight,
	Breakpoints
} from '../../styles';

export const Wrapper = Flex.extend`
	flex-flow: column nowrap;
	justify-content: flex-start;
	align-items: center;
	width: 100%;
	padding: ${ Space.regular };

	.container { flex: 1; }

	@media screen and (min-width: ${ Breakpoints.lap }) {
		flex-flow: row nowrap;
		align-items: flex-start;
	}
`;

export const Media = Flex.extend`
	width: 100%;
	flex-flow: row nowrap;
	justify-content: space-between;
	align-items: stretch;

	.cover { margin-left: 0; }

	@media screen and (min-width: ${ Breakpoints.lap }) {
		width: auto;
		flex-flow: column nowrap;
		justify-content: flex-start;
		align-items: center;

		.cover { margin-left: ${ Space.large }; }
	}
`;

export const Controls = Flex.extend`
	flex: 1;
	flex-flow: column nowrap;
	justify-content: space-between
	align-items: flex-end;
	padding: ${ Space.regular } 0;

	@media screen and (min-width: ${ Breakpoints.lap }) {
		width: 100%;
		flex-flow: row-reverse nowrap;
		align-items: stretch;
		padding: ${ Space.large };
	}
`;

export const Control = styled.button`
	height: 50%;
	width: 4em;
	color: ${props => props.accent ? Color.secondary : 'inherit'};
	transition: all 0.3s;

	:hover { opacity: 0.53; }

	@media screen and (min-width: ${ Breakpoints.lap }) {
		width: 2em;
		height: 2em;
	}
`;

export const Body = styled.div`
	padding: ${ Space.regular } 0;

	p {
		position: relative;
		max-height: 10em;
		opacity: 0.53;
		overflow: hidden;
		transition: all 0.3s;

		::before {
			content: '';
			position: absolute;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			background: linear-gradient(180deg, transparent 50%,  #ffffff);
		}
	}

	button {
		display: block;
		font-weight: ${ FontWeight.bold };
		font-size: ${ FontSize.xSmall };
		text-align: center;
		text-transform: uppercase;
		letter-spacing: 1.2px;
		margin: ${ Space.small } auto;
	}
`;
