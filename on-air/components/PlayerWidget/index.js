import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FeatherIcon from 'feather-icons-react';
import { Cover } from '../Podcast/styles';
import {
	Widget,
	Episode,
	Button
} from './styles';

export default class PlayerWidget extends Component {
	render () {
		const { episode, isPlaying, handleReproduction } = this.props;

		return (
			<Widget>
				<Episode to="/player">
					<Cover size="MINIMIZED" className="cover">
						<img src={episode.cover} alt="" />
					</Cover>

					<h1 className="title">{episode.title}</h1>
				</Episode>

				<Button onClick={() => handleReproduction (episode)}>
					{
						isPlaying
						? <FeatherIcon icon="pause-circle" className="icon"/>
						: <FeatherIcon icon="play-circle" className="icon"/>
					}
				</Button>
			</Widget>
		);
	}
}

PlayerWidget.propTypes = {
	episode: PropTypes.object,
	progress: PropTypes.number,
	isPlaying: PropTypes.bool,
	handleReproduction: PropTypes.func,
};

PlayerWidget.defaultProps = {
	progress: 0,
	isPlaying: false,
	handleReproduction: () => null
};
