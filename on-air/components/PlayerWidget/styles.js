import styled from 'styled-components';
import { Link } from 'react-router-dom';
import {
	Flex,
	Color,
	Space,
	Font,
	FontWeight,
	FontSize,
	IconSize
} from '../../styles';

export const Widget = Flex.extend`
	flex-flow: row nowrap;
	justify-content: space-between;
	align-items: center;
	position: fixed;
	bottom: ${ Space.regular };
	left: 50%;
	width: 80%;
	max-width: 500px;
	min-height: 5em;
	color: ${ Color.white };
	background-color: rgba(0, 0, 0, 0.90);
	padding: ${ Space.small };
	border-radius: 20em;
	transform: translateX(-50%);
	box-shadow: 0 5px 22px 0 rgba(0, 0, 0, 0.24);
`;

export const Episode = Flex.withComponent(Link).extend`
	flex-flow: row nowrap;
	align-items: center;
	width: 100%;
	max-width: 80%;
	padding-left: ${ Space.regular };

	.cover {
		margin: 0;
	}

	.title {
			max-width: 60%;
			font-family: ${ Font.display };
			font-weight: ${ FontWeight.black };
			font-size: ${ FontSize.small};
			color: ${ Color.white };
			white-space: nowrap;
			overflow: hidden;
			text-overflow: ellipsis;
			text-transform: uppercase;
			margin-left: ${ Space.regular };
	}
`;

export const Button = styled.button`
	width: 3em;
	height: 3em;
	text-align: center;
	color: ${ Color.secondary };
	transition: opacity 0.3s;
	margin: 0 ${ Space.small };

	.icon {
		width: ${ IconSize.large.w };
		height: ${ IconSize.large.h };
	}

	:hover {
		opacity: 0.53;
	}
`;
