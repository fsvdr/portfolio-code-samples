import {
	FlexItem ,
	Color,
	Space,
	Font,
	FontWeight,
	FontSize
} from '../../styles';

export const Field = FlexItem.extend`
	display: flex;
	flex-flow: column nowrap;
	padding: ${ Space.small } ${ Space.regular };
	margin-bottom: ${ Space.large };

	label {
		display: block;
		font-family: ${ Font.display };
		font-weight: ${ FontWeight.black };
		font-size: ${ FontSize.xSmall };
		letter-spacing: 1.8px;
		text-transform: uppercase;
		opacity: 0.46;
		margin-bottom: ${ Space.small };
	}

	input,
	select,
	textarea {
		width: 100%;
		border-bottom: 2px solid ${ Color.grey };
		padding: ${ Space.small };
		font-family: ${ Font.regular };
		font-weight: ${ FontWeight.bold };
		font-size: ${ FontSize.medium };
		transition: all 0.3s;

		:hover,
		:focus {
			border-color: ${ Color.black };
		}

		&.big {
			border: 0;
			font-family: ${ Font.display };
			font-weight: ${ FontWeight.black };
			font-size: ${ FontSize.xLarge };
		}

		textarea {
			height: 6em;
		}
	}
`;
