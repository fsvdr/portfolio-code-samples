import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Form extends Component {
	constructor (props) {
		super (props);

		this.state = this.props.state;
		this.onInputChange = this.onInputChange.bind (this);
		this.handleSubmit = this.handleSubmit.bind (this);
	}

	onInputChange (input) {
		const fields = this.state.fields;
		const fieldErrors = this.state.fieldErrors;

		const name = input.name;
		const value = input.value;
		const error = input.validate ? input.validate (value) : false;

		fields[name] = value;
		fieldErrors[name] = error;

		this.setState ({ fields, fieldErrors });
	}

	handleSubmit (e) {
		e.preventDefault ();
		const { fields, fieldErrors } = this.state;

		const errorKeys = Object.keys (fieldErrors);
		const errors = errorKeys.filter (k => fieldErrors[k] !== false);

		if (errors.length === 0) {
			this.props.handleSubmit (fields);
		}
	}

	render () {
		return (
			<form className={this.props.className}>
				{ this.props.children (this.state.fields, this.onInputChange, this.handleSubmit)}
			</form>
		);
	}
}

Form.propTypes = {
	state: PropTypes.shape ({
		fields: PropTypes.object,
		fieldErrors: PropTypes.object
	}).isRequired,
	handleSubmit: PropTypes.func.isRequired,
	children: PropTypes.func.isRequired
};
