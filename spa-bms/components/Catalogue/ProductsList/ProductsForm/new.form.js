import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import './index.scss';
import InputField from '../../../Forms/InputField';
import FileField from '../../../Forms/FileField';
import TextareaField from '../../../Forms/TextareaField';
import SelectField from '../../../Forms/SelectField';

class NewProductsForm extends Component{

  constructor(props) {
    super(props);

    this.state = {
      fields: {
        name: '',
        description: '',
        image: '',
        cost: '',
        price: '',
        warningLimit: '',
        stock: '',
        bonus: '',
        provider: ''
      },
      errors: {}
    };

    this.onChange = this.onChange.bind(this);
    this.handlePost = this.handlePost.bind(this);
    this.handlePostSuccess = this.handlePostSuccess.bind(this);
  }

  componentWillMount() {
    this.props.requestProviders();
  }

  componentWillUnmount() {
    const fields = {
      name: '',
      description: '',
      image: '',
      cost: '',
      price: '',
      warningLimit: '',
      stock: '',
      bonus: '',
      provider: ''
    };

    const errors = {};

    this.setState({fields, errors});
  }

  onChange(input) {
    const fields = this.state.fields;
    const errors = this.state.errors;

    fields[input.name] = input.value;
    errors[input.name] = input.error;

    this.setState({fields, errors});
  }

  handlePost(event) {
    event.preventDefault();
    const { handlePost } = this.props;
    const { fields, errors } = this.state;
    const errorMessages = Object.keys(errors).filter((k) => errors[k]);

    // TODO: Check for individual required fields
    if (errorMessages.length === 0) handlePost(fields, this.handlePostSuccess);
  }

  handlePostSuccess() {
    this.props.requestDataReload();
    this.props.route.history.goBack();
  }

  render() {
    const { providers, route } = this.props;
    const { fields } = this.state;

    return (
      <div className={`c-catalogue-list__form`}>
        <h2 className="t2">Agrega un nuevo producto</h2>
        <p className="o-catalogue-description">Por favor llena los siguientes campos con la información del producto a agregar.</p>
          <form className="c-catalogue-form c-form" action="/" method="post">
          {/* SECTION */}
            <div className="c-catalogue-form__section o-grid o-grid-axis-align--start o-grid-cross-align--stretch">
              <h2 className="o-section-title t1 o-grid__child u-12of12">Información General</h2>

              <div className="c-input-field [ o-grid__child u-12of12 u-2of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                <label htmlFor="image" className="c-input-field__label">Foto</label>
                <FileField
                  name="image"
                  value={fields.image}
                  placeholder="Fotografía"
                  required={true}
                  validate={(value) => value ? false : true}
                  onChange={this.onChange}
                  id="image"/>
              </div>

              <div className="c-input-field--small [ o-grid__child u-8of12 u-6of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                <label htmlFor="name" className="c-input-field__label">Nombre</label>
                <InputField
                  type="text"
                  name="name"
                  value={fields.name}
                  placeholder="Nombre"
                  required={true}
                  validate={(value) => value.length <= 4}
                  onChange={this.onChange}
                  id="name"
                  classNames="c-input-field__field"
                  />
              </div>

              <div className="c-input-field--small [ o-grid__child u-4of12 u-2of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                <label htmlFor="price" className="c-input-field__label">Precio</label>
                <InputField
                  type="number"
                  name="price"
                  value={fields.price}
                  min={0}
                  placeholder="0.00"
                  required={true}
                  validate={(value) => value <= 0}
                  onChange={this.onChange}
                  id="price"
                  classNames="c-input-field__field"
                  />
              </div>

              <div className="c-input-field--small [ o-grid__child u-12of12 u-6of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                <label htmlFor="description" className="c-input-field__label">Descripción</label>
                <TextareaField
                  name="description"
                  value={fields.description}
                  placeholder="Descripción"
                  required={true}
                  validate={(value) => value.length <= 4}
                  onChange={this.onChange}
                  id="description"
                  classNames="c-input-field__field"
                  />
              </div>
            </div>

            {/* SECTION */}
            <div className="c-catalogue-form__section o-grid o-grid-axis-align--start o-grid-cross-align--stretch">
              <h2 className="o-section-title t1 o-grid__child u-12of12">Información del proveedor</h2>

              <div className="c-input-field--small [ o-grid__child u-12of12 u-6of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                <label htmlFor="provider" className="c-input-field__label">Proveedor</label>
                {
                  !providers.isLoading
                  ? <SelectField
                    name="provider"
                    options={[{value: 1, name: 'Ninguno'}, ...providers.items.map((p) => {return {value: p.id, name: p.name}})]}
                    value={fields.provider}
                    onChange={this.onChange}
                    required={true}
                    classNames={"c-input-field__field"}
                    id="provider"
                    />
                  : <p className="s1 o-loading-indicator">Cargando...</p>
                }
              </div>

              <div className="c-input-field--small [ o-grid__child u-4of12 u-2of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                <label htmlFor="cost" className="c-input-field__label">Costo</label>
                <InputField
                  type="number"
                  name="cost"
                  value={fields.cost}
                  min={0}
                  placeholder="0.00"
                  required={true}
                  validate={(value) => value <= 0}
                  onChange={this.onChange}
                  id="cost"
                  classNames="c-input-field__field"
                  />
              </div>

            </div>

            {/* SECTION */}
            <div className="c-catalogue-form__section o-grid o-grid-axis-align--start o-grid-cross-align--stretch">
              <h2 className="o-section-title t1 o-grid__child u-12of12">Información de almacén</h2>

              <div className="c-input-field--small [ o-grid__child u-4of12 u-2of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                <label htmlFor="stock" className="c-input-field__label">Inicial</label>
                <InputField
                  type="number"
                  name="stock"
                  value={fields.stock}
                  min={0}
                  placeholder="0.00"
                  required={true}
                  validate={(value) => value <= 0}
                  onChange={this.onChange}
                  id="stock"
                  classNames="c-input-field__field"
                  />
              </div>

              <div className="c-input-field--small [ o-grid__child u-4of12 u-2of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                <label htmlFor="warningLimit" className="c-input-field__label">Límite</label>
                <InputField
                  type="number"
                  name="warningLimit"
                  value={fields.warningLimit}
                  min={0}
                  placeholder="0.00"
                  required={true}
                  validate={(value) => value <= 0}
                  onChange={this.onChange}
                  id="warningLimit"
                  classNames="c-input-field__field"
                  />
              </div>

            </div>

            {/* SECTION */}
            <div className="c-catalogue-form__section o-grid o-grid-axis-align--start o-grid-cross-align--stretch">
              <h2 className="o-section-title t1 o-grid__child u-12of12">Información de venta</h2>

              <div className="c-input-field--small [ o-grid__child u-4of12 u-2of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                <label htmlFor="bonus" className="c-input-field__label">Comisión por venta</label>
                <InputField
                  type="number"
                  name="bonus"
                  value={fields.bonus}
                  min={0}
                  placeholder="0.00"
                  required={true}
                  validate={(value) => value <= 0}
                  onChange={this.onChange}
                  id="bonus"
                  classNames="c-input-field__field"
                  />
              </div>

            </div>

            <div className="c-buttons-field">
              <button className="c-buttons-field__button" onClick={() => route.history.goBack()}>Cancelar</button>
              <button className="c-buttons-field__button--primary" onClick={(e) => this.handlePost(e)} type="submit">Agregar</button>
            </div>
          </form>
      </div>
    );
  }
}

NewProductsForm.propTypes = {
  providers: PropTypes.object.isRequired,
  requestProviders: PropTypes.func.isRequired,
  requestDataReload: PropTypes.func,
  handlePost: PropTypes.func.isRequired,
  route: PropTypes.object.isRequired
};

export default NewProductsForm;
