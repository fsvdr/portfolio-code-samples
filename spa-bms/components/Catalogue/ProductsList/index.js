import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { BeatLoader } from 'react-spinners';
import { Route, NavLink } from 'react-router-dom';
import NewProductsForm from './ProductsForm/new.form';
import EditProductsForm from './ProductsForm/edit.form';
import { API_URL } from '../../../constants';
import { toMoney } from '../../../helpers/money-format';

class ProductsList extends Component {

  componentWillMount() {
    this.props.api.fetch();
  }

  postProduct() {
    this.api.post();
  }

  render() {
    const { products, providers, api, requestProviders, addToCart, route } = this.props;

    return (
      <div className="c-catalogue-list">
        <div className="c-catalogue-list__header">
          <h1 className="t3">Listado de Productos</h1>
          <p className="o-catalogue-description">Consulta, edita y crea productos a la venta. También puedes agregar artículos al carrito de compra.</p>
          <div className="c-catalogue-list__filters">
            <NavLink to="/catalogo/productos/nuevo" className="o-filter">Agregar nuevo producto</NavLink>
          </div>
        </div>

        {/* NEW PRODUCT FORM */}
        <Route path="/catalogo/productos/nuevo" render={(match) => (
          <NewProductsForm
            providers={providers}
            requestProviders={requestProviders}
            requestDataReload={api.fetch}
            handlePost={api.post}
            route={match} />
        )}/>


        {/* EDIT PRODUCT FORM */}
        <Route path="/catalogo/productos/editar/:id" render={(match) => (
          <EditProductsForm
            isLoading={products.isLoading}
            product={products.item}
            providers={providers}
            requestProviders={requestProviders}
            requestDataReload={api.fetch}
            handleGet={api.get}
            handlePost={api.post}
            handleDelete={api.delete}
            route={match} />
        )}/>

        {/* PRODUCTS LIST */}
        <Route exact path="/catalogo/productos" render={() => (
          <div className="c-catalogue-list__body">
            {
              products.isLoading
              ? <BeatLoader className="c-catalogue-list__spinner" color={'#ffffff'} loading={true} />
              : (
                  <div className="c-items-list o-grid o-grid-cross-align--stretch">
                    {
                      products.items.map((i) => (
                        <div className="[ o-grid__child u-12of12 u-6of12@tab u-4of12@lap ]" key={i.id}>
                          <div className="c-item o-grid o-grid-axis-align--between o-grid-cross-align--center">
                            <div className="o-grid__child u-margin-m--bottom">
                              <img className="c-item__photo" src={`${API_URL}/${i.image}`} alt=""/>
                            </div>
                            <div className="o-grid__child u-margin-m--bottom">
                              <button onClick={() => addToCart(i)} className="o-primary-action"><i className="o-icon o-icon-cart--small"></i></button>
                            </div>
                            <div className="c-item__data [ o-grid__child u-12of12 ]">
                              <h2 className="o-item-name t2 u-text-truncate u-margin-c"><NavLink to={`${route.match.url}/editar/${i.id}`}>{i.name}</NavLink></h2>
                              <p className="o-item-description u-margin-s--bottom u-text-left">{i.description}</p>
                              <div className="c-item__numbers [ o-grid o-grid-axis-align--between o-grid-cross-align--center o-grid-wrap--no ]">
                                <span className="o-item-number">En stock: <strong>{i.stock}</strong></span>
                                <span className="o-item-number u-text-right">Precio: <strong>{toMoney(i.price)}</strong></span>
                              </div>
                            </div>
                          </div>
                        </div>
                      ))
                    }
                  </div>
                )
            }
          </div>
        )}/>
      </div>
    );
  }
}

ProductsList.propTypes = {
  products: PropTypes.object.isRequired,
  providers: PropTypes.object.isRequired,
  api: PropTypes.object.isRequired,
  requestProviders: PropTypes.func.isRequired,
  addToCart: PropTypes.func.isRequired,
  route: PropTypes.object.isRequired
};

export default ProductsList;
