import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import InputField from '../../../Forms/InputField';
import FileField from '../../../Forms/FileField';
import TextareaField from '../../../Forms/TextareaField';
import SelectField from '../../../Forms/SelectField';
import { BeatLoader } from 'react-spinners';
import CheckboxField from '../../../Forms/CheckboxField';

class EditTreatmentsForm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      fields: {
        id: this.props.treatment.id,
        name: this.props.treatment.name,
        image: this.props.treatment.image,
        description: this.props.treatment.description,
        price: this.props.treatment.price,
        sessions: this.props.treatment.sessions,
        comments: this.props.treatment.comments,
        gift: this.props.treatment.gift ? this.props.treatment.gift.id : 1,
        trial: this.props.treatment.trial,
        equivalents: this.props.treatment.equivalents ? this.props.treatment.equivalents.map((e) => e.id) : []
      },
      errors: {},
      controls: {
        equivalence: this.props.treatments.length ? this.props.treatments[0].id : 0
      }
    };

    this.onChange = this.onChange.bind(this);
    this.onControlChange = this.onControlChange.bind(this);
    this.handlePost = this.handlePost.bind(this);
    this.handlePostSuccess = this.handlePostSuccess.bind(this);
    this.handleDeleteSuccess = this.handleDeleteSuccess.bind(this);
    this.handleEquivalence = this.handleEquivalence.bind(this);
    this.handleEquivalenceRemoval = this.handleEquivalenceRemoval.bind(this);
  }

  componentWillMount() {
    const id = this.props.route.match.params.id;
    this.props.handleGet(id);
    this.props.requestProducts();
  }

  componentWillReceiveProps(update) {
    if (update.treatment.id) {
      const fields = {
        id: update.treatment.id,
        name: update.treatment.name,
        image: update.treatment.image,
        description: update.treatment.description,
        price: update.treatment.price,
        sessions: update.treatment.sessions,
        comments: update.treatment.comments,
        gift: update.treatment.gift ? update.treatment.gift.id : 1,
        trial: update.treatment.trial,
        equivalents: update.treatment.equivalents.map((e) => e.id) || []
      };

      const errors = {};

      this.setState({fields, errors});
    }
  }

  componentWillUnmount() {
    const fields = {
      id: '',
      name: '',
      image: '',
      description: '',
      price: '',
      sessions: '',
      comments: '',
      gift: '',
      trial: '',
      equivalents: []
    }

    const errors = {};

    this.setState({fields, errors});
  }

  onChange(input) {
    const fields = this.state.fields;
    const errors = this.state.errors;

    fields[input.name] = input.value;
    errors[input.name] = input.error;

    this.setState({fields, errors});
  }

  onControlChange(input) {
    const controls = this.state.controls;

    controls[input.name] = input.value;

    this.setState({controls});
  }

  handlePost(event) {
    event.preventDefault();

    const { handlePost } = this.props;
    const { fields, errors } = this.state;
    const errorMessages = Object.keys(errors).filter((k) => errors[k]);

    // TODO: Check for individual required fields
    if (errorMessages.length === 0) handlePost(fields, this.handlePostSuccess);
  }

  handlePostSuccess() {
    this.props.handleGet(this.state.fields.id);
    this.props.requestDataReload();
  }

  handleDeleteSuccess() {
    this.props.requestDataReload();
    this.props.route.history.goBack()
  }

  handleEquivalence(event) {
    event.preventDefault();

    const fields = this.state.fields;
    const controls = this.state.controls;
    const value = Number(controls.equivalence);

    if (!fields['equivalents'].includes(value)) fields['equivalents'].push(value);

    this.setState({fields, controls});
  }

  handleEquivalenceRemoval(event, id) {
    event.preventDefault();

    let fields = this.state.fields;
    const index = fields['equivalents'].findIndex((i) => i === id);
    console.log(index);

    if (index >= 0) {
      fields['equivalents'] = [...fields['equivalents'].slice(0, index), ...fields['equivalents'].slice(index + 1)];
      this.setState({fields});
    }
  }

  render() {
    const { isLoading, products, treatments, handleDelete, route } = this.props;
    const { fields, controls } = this.state;

    const equivalentsControl = !isLoading
      ? <div className="c-control [ o-grid__child u-12of12 ] [ o-grid o-grid-axis-align--between o-grid-cross-align--center o-grid-wrap--no ]">
          <SelectField
          name="equivalence"
          value={controls.equivalence}
          onChange={(input) => this.onControlChange(input)}
          options={treatments.map((p) => {return {value: p.id, name: p.name}})}
          classNames={"c-input-field__field o-grid__child u-8of12"}
          id="equivalence"
          />

          <button
            onClick={(e) => this.handleEquivalence(e)}
            className="c-control__btn o-primary-action o-grid__child u-margin-l--left">
            <i className="o-icon o-icon-tick--small"></i>
          </button>
        </div>
      : <p className="s1 o-loading-indicator">Cargando...</p>;

    return (
      <div className={`c-catalogue-list__form`}>
        <h2 className="t2">Editar un tratamiento existente</h2>
        <p className="o-catalogue-description">Modifica los campos deseados del tratamiento seleccionado.</p>
          {
            isLoading
            ? (
              <div className="c-catalogue-list__spinner u-text-center">
                <BeatLoader color={'#FC5185'} loading={true} />
              </div>
            ) : (
              <form className="c-catalogue-form c-form" action="/" method="post">
              {/* SECTION */}
                <div className="c-catalogue-form__section o-grid o-grid-axis-align--start o-grid-cross-align--stretch">
                  <h2 className="o-section-title t1 o-grid__child u-12of12">Información General</h2>

                  <div className="c-input-field [ o-grid__child u-12of12 u-2of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                    <label htmlFor="image" className="c-input-field__label">Foto</label>
                    <FileField
                      name="image"
                      value={fields.image}
                      placeholder="Fotografía"
                      required={true}
                      validate={(value) => value ? false : true}
                      onChange={this.onChange}
                      id="image"/>
                  </div>

                  <div className="c-input-field--small [ o-grid__child u-8of12 u-6of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                    <label htmlFor="name" className="c-input-field__label">Nombre</label>
                    <InputField
                      type="text"
                      name="name"
                      value={fields.name}
                      placeholder="Nombre"
                      required={true}
                      validate={(value) => value.length <= 4}
                      onChange={this.onChange}
                      id="name"
                      classNames="c-input-field__field"
                      />
                  </div>

                  <div className="c-input-field--small [ o-grid__child u-4of12 u-2of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                    <label htmlFor="price" className="c-input-field__label">Precio</label>
                    <InputField
                      type="number"
                      name="price"
                      value={fields.price}
                      min={0}
                      placeholder="0.00"
                      required={true}
                      validate={(value) => value <= 0}
                      onChange={this.onChange}
                      id="price"
                      classNames="c-input-field__field"
                      />
                  </div>

                  <div className="c-input-field--small [ o-grid__child u-12of12 u-6of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                    <label htmlFor="description" className="c-input-field__label">Descripción</label>
                    <TextareaField
                      name="description"
                      value={fields.description}
                      placeholder="Descripción"
                      required={true}
                      validate={(value) => value.length <= 4}
                      onChange={this.onChange}
                      id="description"
                      classNames="c-input-field__field"
                      />
                  </div>
                </div>

                {/* SECTION */}
                <div className="c-catalogue-form__section o-grid o-grid-axis-align--start o-grid-cross-align--stretch">
                  <h2 className="o-section-title t1 o-grid__child u-12of12">Información sobre la aplicación del tratamiento</h2>

                  <div className="c-input-field--small [ o-grid__child u-4of12 u-2of12@lap ] [ o-grid--vertical o-grid-axis-align--between ]">
                    <label htmlFor="sessions" className="c-input-field__label">No. de Sesiones</label>
                    <InputField
                      type="number"
                      name="sessions"
                      value={fields.sessions}
                      min={0}
                      placeholder="0"
                      required={true}
                      validate={(value) => value <= 0}
                      onChange={this.onChange}
                      id="sessions"
                      classNames="c-input-field__field"
                      />
                  </div>

                  <div className="c-input-field--small [ o-grid__child u-12of12 u-6of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                    <label htmlFor="comments" className="c-input-field__label">Protocolo</label>
                    <TextareaField
                      name="comments"
                      value={fields.comments}
                      placeholder="Protocolo"
                      required={true}
                      validate={(value) => value.length <= 4}
                      onChange={this.onChange}
                      id="comments"
                      classNames="c-input-field__field"
                      />
                  </div>

                  <div className="c-input-field--small [ o-grid__child u-12of12 u-3of12@lap ] [ o-grid--vertical o-grid-axis-align--between ]">
                    <label htmlFor="gift" className="c-input-field__label">Regalo</label>
                    {
                      !products.isLoading
                      ? <SelectField
                        name="gift"
                        value={fields.gift}
                        onChange={this.onChange}
                        options={[{value: 1, name: 'Ninguno'}, ...products.items.map((p) => {return {value: p.id, name: p.name}})]}
                        classNames={"c-input-field__field"}
                        id="gift"
                        />
                      : <p className="s1 o-loading-indicator">Cargando...</p>
                    }
                  </div>

                    <div className="c-input-field--small [ o-grid__child u-8of12 u-6of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                      <label className="c-input-field__label">¿Se ofrece de prueba?</label>
                      <CheckboxField
                        name="trial"
                        checked={fields.trial ? true : false}
                        value={1}
                        label="Se ofrece de prueba"
                        required={true}
                        onChange={this.onChange}
                        id="trial"
                        classNames="c-checkbox-field"
                        />
                    </div>

                  <div className="c-input-field--small [ o-grid__child u-12of12 u-6of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                    <label htmlFor="equivalence" className="c-input-field__label">Equivalentes</label>
                    { equivalentsControl }
                    <div className="c-dynamic-list o-grid_child u-12of12">
                      { fields.equivalents.map((e) => {
                        const treatment = treatments.find((t) => t.id == e); //eslint-disable-line
                        return (
                          <div className="c-dynamic-list__item o-grid o-grid-axis-align--between o-grid-cross-align--center u-12of12" key={treatment.id}>
                            <span className="o-dynamic-item-title o-grid__child u-6of12">{treatment.name}</span>
                            <button onClick={(e) => this.handleEquivalenceRemoval(e, treatment.id)} className="o-dynamic-item-remove-btn o-primary-action"><i className="o-icon o-icon-trash--small"></i></button>
                          </div>
                        );
                      })}
                    </div>
                  </div>

                </div>
                <div className="c-buttons-field">
                  <button className="c-buttons-field__button" onClick={() => route.history.goBack()}>Cancelar</button>
                  <button className="c-buttons-field__button" onClick={() => handleDelete(fields.id, this.handleDeleteSuccess)}>Desactivar</button>
                  <button className="c-buttons-field__button--primary" onClick={(e) => this.handlePost(e)} type="submit">Editar</button>
                </div>
              </form>
            )
          }
      </div>
    );
  }
}

EditTreatmentsForm.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  treatment: PropTypes.object.isRequired,
  treatments: PropTypes.arrayOf(PropTypes.object).isRequired,
  products: PropTypes.object.isRequired,
  requestDataReload: PropTypes.func,
  requestProducts: PropTypes.func.isRequired,
  handleGet: PropTypes.func.isRequired,
  handlePost: PropTypes.func.isRequired,
  handleDelete: PropTypes.func.isRequired,
  route: PropTypes.object.isRequired
};

export default EditTreatmentsForm;
