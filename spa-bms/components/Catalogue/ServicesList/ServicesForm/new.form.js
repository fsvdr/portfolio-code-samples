import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import InputField from '../../../Forms/InputField';
import FileField from '../../../Forms/FileField';
import TextareaField from '../../../Forms/TextareaField';
import SelectField from '../../../Forms/SelectField';

class NewServicesForm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      fields: {
        name: '',
        image: '',
        description: '',
        price: '',
        sessions: '',
        comments: '',
        bonus: '',
        gift: '',
        treatments: []
      },
      errors: {},
      controls: {
        treatments: this.props.treatments.items.length ? this.props.treatments.items[0].id : 0
      }
    };

    this.onChange = this.onChange.bind(this);
    this.onControlChange = this.onControlChange.bind(this);
    this.handlePost = this.handlePost.bind(this);
    this.handlePostSuccess = this.handlePostSuccess.bind(this);
    this.handleTreatments = this.handleTreatments.bind(this);
    this.handleTreatmentsRemoval = this.handleTreatmentsRemoval.bind(this);
  }

  componentWillMount() {
    this.props.requestProducts();
    this.props.requestTreatments();
  }

  componentWillUnMount() {
    const fields = {
      name: '',
      image: '',
      description: '',
      price: '',
      sessions: '',
      comments: '',
      bonus: '',
      gift: '',
      treatments: []
    };

    const errors = {};

    this.setState({fields, errors});
  }

  onChange(input) {
    const fields = this.state.fields;
    const errors = this.state.errors;

    fields[input.name] = input.value;
    errors[input.name] = input.error;

    this.setState({fields, errors});
  }

  onControlChange(input) {
    const controls = this.state.controls;

    controls[input.name] = input.value;

    this.setState({controls});
  }

  handlePost(event) {
    event.preventDefault();

    const { handlePost } = this.props;
    const { fields, errors } = this.state;
    const errorMessages = Object.keys(errors).filter((k) => errors[k]);

    // TODO: Check for individual required fields
    if (errorMessages.length === 0) handlePost(fields, this.handlePostSuccess);
  }

  handlePostSuccess() {
    this.props.requestDataReload();
    this.props.route.history.goBack();
  }

  handleTreatments(event) {
    event.preventDefault();

    const fields = this.state.fields;
    const controls = this.state.controls;
    const value = Number(controls.treatments);

    if (!fields['treatments'].includes(value)) fields['treatments'].push(value);

    this.setState({fields, controls});
  }

  handleTreatmentsRemoval(event, id) {
    event.preventDefault();

    let fields = this.state.fields;
    const index = fields['treatments'].findIndex((i) => i === id);

    if (index >= 0) {
      fields['treatments'] = [...fields['treatments'].slice(0, index), ...fields['treatments'].slice(index + 1)];
      this.setState({fields});
    }
  }

  render() {
    const { products, treatments, route } = this.props;
    const { fields, controls } = this.state;

    const treatmentsControl = !treatments.isLoading
      ? <div className="c-control [ o-grid__child u-12of12 ] [ o-grid o-grid-axis-align--between o-grid-cross-align--center o-grid-wrap--no ]">
          <SelectField
          name="treatments"
          value={controls.equivalence}
          onChange={(input) => this.onControlChange(input)}
          options={treatments.items.map((p) => {return {value: p.id, name: p.name}})}
          classNames={"c-input-field__field o-grid__child u-8of12"}
          id="treatments"
          />

          <button
            onClick={(e) => this.handleTreatments(e)}
            className="c-control__btn o-primary-action o-grid__child u-margin-l--left">
            <i className="o-icon o-icon-tick--small"></i>
          </button>
        </div>
      : <p className="s1 o-loading-indicator">Cargando...</p>;

    return (
      <div className={`c-catalogue-list__form`}>
        <h2 className="t2">Agrega un nuevo paquete</h2>
        <p className="o-catalogue-description">Por favor llena los siguientes campos con la información del paquete a agregar.</p>
          <form className="c-catalogue-form c-form" action="/" method="post">
          {/* SECTION */}
            <div className="c-catalogue-form__section o-grid o-grid-axis-align--start o-grid-cross-align--stretch">
              <h2 className="o-section-title t1 o-grid__child u-12of12">Información General</h2>

              <div className="c-input-field [ o-grid__child u-12of12 u-2of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                <label htmlFor="image" className="c-input-field__label">Foto</label>
                <FileField
                  name="image"
                  value={fields.image}
                  placeholder="Fotografía"
                  required={true}
                  validate={(value) => value ? false : true}
                  onChange={this.onChange}
                  id="image"/>
              </div>

              <div className="c-input-field--small [ o-grid__child u-8of12 u-6of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                <label htmlFor="name" className="c-input-field__label">Nombre</label>
                <InputField
                  type="text"
                  name="name"
                  value={fields.name}
                  placeholder="Nombre"
                  required={true}
                  validate={(value) => value.length <= 4}
                  onChange={this.onChange}
                  id="name"
                  classNames="c-input-field__field"
                  />
              </div>

              <div className="c-input-field--small [ o-grid__child u-4of12 u-2of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                <label htmlFor="price" className="c-input-field__label">Precio</label>
                <InputField
                  type="number"
                  name="price"
                  value={fields.price}
                  min={0}
                  placeholder="0.00"
                  required={true}
                  validate={(value) => value <= 0}
                  onChange={this.onChange}
                  id="price"
                  classNames="c-input-field__field"
                  />
              </div>

              <div className="c-input-field--small [ o-grid__child u-12of12 u-6of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                <label htmlFor="description" className="c-input-field__label">Descripción</label>
                <TextareaField
                  name="description"
                  value={fields.description}
                  placeholder="Descripción"
                  required={true}
                  validate={(value) => value.length <= 4}
                  onChange={this.onChange}
                  id="description"
                  classNames="c-input-field__field"
                  />
              </div>
            </div>

            {/* SECTION */}
            <div className="c-catalogue-form__section o-grid o-grid-axis-align--start o-grid-cross-align--stretch">
              <h2 className="o-section-title t1 o-grid__child u-12of12">Información sobre el contenido</h2>

              <div className="c-input-field--small [ o-grid__child u-4of12 u-2of12@lap ] [ o-grid--vertical o-grid-axis-align--between ]">
                <label htmlFor="sessions" className="c-input-field__label">No. de Sesiones</label>
                <InputField
                  type="number"
                  name="sessions"
                  value={fields.sessions}
                  min={0}
                  placeholder="0"
                  required={true}
                  validate={(value) => value <= 0}
                  onChange={this.onChange}
                  id="sessions"
                  classNames="c-input-field__field"
                  />
              </div>

              <div className="c-input-field--small [ o-grid__child u-12of12 u-3of12@lap ] [ o-grid--vertical o-grid-axis-align--between ]">
                <label htmlFor="gift" className="c-input-field__label">Regalo</label>
                {
                  !products.isLoading
                  ? <SelectField
                    name="gift"
                    value={fields.gift}
                    onChange={this.onChange}
                    options={[{value: 1, name: 'Ninguno'}, ...products.items.map((p) => {return {value: p.id, name: p.name}})]}
                    classNames={"c-input-field__field"}
                    id="gift"
                    />
                  : <p className="s1 o-loading-indicator">Cargando...</p>
                }
              </div>

              <div className="c-input-field--small [ o-grid__child u-12of12 u-5of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                <label htmlFor="comments" className="c-input-field__label">Protocolo</label>
                <TextareaField
                  name="comments"
                  value={fields.comments}
                  placeholder="Protocolo"
                  required={true}
                  validate={(value) => value.length <= 4}
                  onChange={this.onChange}
                  id="comments"
                  classNames="c-input-field__field"
                  />
              </div>

              <div className="c-input-field--small [ o-grid__child u-12of12 u-6of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                <label htmlFor="treatments" className="c-input-field__label">Tratamientos</label>
                { treatmentsControl }
                <div className="c-dynamic-list o-grid_child u-12of12">
                  { fields.treatments.map((i) => {
                      const treatment = treatments.items.find((t) =>t.id == i) //eslint-disable-line
                      return (
                        <div className="c-dynamic-list__item o-grid o-grid-axis-align--between o-grid-cross-align--center u-12of12" key={treatment.id}>
                          <span className="o-dynamic-item-title o-grid__child u-6of12">{treatment.name}</span>
                          <button onClick={(e) => this.handleTreatmentsRemoval(e, treatment.id)} className="o-dynamic-item-remove-btn o-primary-action"><i className="o-icon o-icon-trash--small"></i></button>
                        </div>
                      );
                  })}
                </div>
              </div>

            </div>
            <div className="c-buttons-field">
              <button className="c-buttons-field__button" onClick={() => route.history.goBack()}>Cancelar</button>
              <button className="c-buttons-field__button--primary" onClick={(e) => this.handlePost(e)} type="submit">Agregar</button>
            </div>
          </form>
      </div>
    );
  }
}

NewServicesForm.propTypes = {
  services: PropTypes.object.isRequired,
  products: PropTypes.object.isRequired,
  treatments: PropTypes.object.isRequired,
  handlePost: PropTypes.func.isRequired,
  requestDataReload: PropTypes.func,
  requestProducts: PropTypes.func.isRequired,
  requestTreatments: PropTypes.func.isRequired,
  route: PropTypes.object.isRequired
};

export default NewServicesForm;
