import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { BeatLoader } from 'react-spinners';
import { Route, NavLink } from 'react-router-dom';
import { API_URL } from '../../../constants';
import NewServicesForm from './ServicesForm/new.form';
import EditServicesForm from './ServicesForm/edit.form';
import { toMoney } from '../../../helpers/money-format';

class ServicesList extends Component {

  componentWillMount() {
    this.props.api.fetch();
  }

  render() {
    const { services, products, treatments, api, requestProducts, requestTreatments, addToCart, route } = this.props;

    return (
      <div className="c-catalogue-list">
        <div className="c-catalogue-list__header">
          <h1 className="t3">Listado de Paquetes</h1>
          <p className="o-catalogue-description">Consulta, edita y crea paquetes. También puedes agregar artículos al carrito de compra.</p>
          <div className="c-catalogue-list__filters">
            <NavLink to="/catalogo/paquetes/nuevo" className="o-filter">Agregar nuevo paquete</NavLink>
          </div>
        </div>

        {/* NEW SERVICE FORM */}
        <Route path="/catalogo/paquetes/nuevo" render={(match) => (
          <NewServicesForm
            services={services}
            products={products}
            treatments={treatments}
            handlePost={api.post}
            requestDataReload={api.fetch}
            requestProducts={requestProducts}
            requestTreatments={requestTreatments}
            route={match} />
        )}/>


        {/* EDIT SERVICE FORM */}
        <Route path="/catalogo/paquetes/editar/:id" render={(match) => (
          <EditServicesForm
            isLoading={services.isLoading}
            service={services.item}
            products={products}
            treatments={treatments}
            handleGet={api.get}
            handlePost={api.post}
            handleDelete={api.delete}
            requestDataReload={api.fetch}
            requestProducts={requestProducts}
            requestTreatments={requestTreatments}
            route={match} />
        )}/>

        {/* SERVICES LIST */}
        <Route exact path="/catalogo/paquetes" render={() => (
          <div className="c-catalogue-list__body">
            {
              services.isLoading
              ? <BeatLoader className="c-catalogue-list__spinner" color={'#ffffff'} loading={true} />
              : (
                  <div className="c-items-list o-grid o-grid-cross-align--stretch">
                    {
                      services.items.map((i) => (
                        <div className="[ o-grid__child u-12of12 u-6of12@tab u-4of12@lap ]" key={i.id}>
                          <div className="c-item o-grid o-grid-axis-align--between o-grid-cross-align--center">
                            <div className="o-grid__child u-margin-m--bottom">
                              <img className="c-item__photo" src={`${API_URL}/${i.image}`} alt=""/>
                            </div>
                            <div className="o-grid__child u-margin-m--bottom">
                              <button onClick={() => addToCart(i)} className="o-primary-action"><i className="o-icon o-icon-cart--small"></i></button>
                            </div>
                            <div className="c-item__data [ o-grid__child u-12of12 ]">
                              <h2 className="o-item-name t2 u-text-truncate u-margin-c"><NavLink to={`${route.match.url}/editar/${i.id}`}>{i.name}</NavLink></h2>
                              <p className="o-item-description u-margin-s--bottom u-text-left">{i.description}</p>
                              <div className="c-item__numbers [ o-grid o-grid-axis-align--between o-grid-cross-align--center o-grid-wrap--no ]">
                                <span className="o-item-number">Sesiones: <strong>{i.sessions}</strong></span>
                                <span className="o-item-number u-text-right">Precio: <strong>{toMoney(i.price)}</strong></span>
                              </div>
                            </div>
                          </div>
                        </div>
                      ))
                    }
                  </div>
                )
            }
          </div>
        )}/>
      </div>
    );
  }
}

ServicesList.propTypes = {
  services: PropTypes.object.isRequired,
  products: PropTypes.object.isRequired,
  treatments: PropTypes.object.isRequired,
  api: PropTypes.object.isRequired,
  requestProducts: PropTypes.func.isRequired,
  requestTreatments: PropTypes.func.isRequired,
  addToCart: PropTypes.func.isRequired,
  route: PropTypes.object.isRequired
};

export default ServicesList;
