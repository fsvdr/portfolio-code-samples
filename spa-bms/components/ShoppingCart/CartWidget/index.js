import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { toMoney } from '../../../helpers/money-format';

class CartWidget extends Component {

  constructor(props) {
    super(props);

    this.state = {
      quantity: 0
    };
  }

  componentWillReceiveProps(update) {
    const items = [...update.cart.products, ...update.cart.treatments, ...update.cart.services];
    const quantity = items.reduce((acc, i) => acc + i.quantity, 0);

    this.setState({quantity});
  }

  render() {
    const { cart, handleCartClear } = this.props;
    const { quantity } = this.state;

    return (
      <div className="c-cart-widget [ o-grid__child u-6of12 u-12of12@lap ] [ o-grid o-grid-axis-align--between o-grid-cross-align--center ]">
        <div className="o-grid__child u-12of12 u-4of12@lap [ o-grid o-grid-cross-align--between o-grid-axis-align--between ]">
          <div className="o-grid__child u-12of12@lap u-margin-s--bottom">
            <NavLink to="/carrito" className="o-primary-action"><i className="o-icon o-icon-tick--small"></i></NavLink>
          </div>
          <div className="o-grid__child u-12of12@lap u-margin-s--bottom">
            <button onClick={() => handleCartClear()} className="o-primary-action"><i className="o-icon o-icon-trash--small"></i></button>
          </div>
        </div>

        <div className="o-grid__child u-12of12 u-8of12@lap">
        <h2 className="c-cart-widget__total t2 u-text-truncate u-margin-c">{toMoney(cart.total)}</h2>
        <span className="c-cart-widget__quantity o-grid__child u-12of12">Artículos: <strong>{quantity}</strong></span>
        </div>
      </div>
    );
  }
}

CartWidget.propTypes = {
  cart: PropTypes.object.isRequired,
  handleCartClear: PropTypes.func.isRequired,
};

export default CartWidget;
