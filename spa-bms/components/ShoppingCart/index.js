import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import InputField from '../Forms/InputField';
import SelectField from '../Forms/SelectField';
import CheckboxField from '../Forms/CheckboxField';
import {PAYMENT_METHODS} from '../../constants';
import PromotionToggler from './PromotionToggler';
import { toMoney } from '../../helpers/money-format';

class ShoppingCart extends Component {

  constructor(props) {
    super(props);

    this.state = {
      fields: {
        client: this.props.cart.client,
        staff: this.props.cart.staff,
        prospection: this.props.cart.prospection,
      },
      errors: {},
      controls: {
        payment: PAYMENT_METHODS[0].id,
        paymentQuantity: ''
      }
    };

    this.onChange = this.onChange.bind(this);
    this.onControlChange = this.onControlChange.bind(this);
    this.handlePost = this.handlePost.bind(this);
    this.handlePostSuccess = this.handlePostSuccess.bind(this);
    this.addPromotionToCart = this.addPromotionToCart.bind(this);
    this.removePromotionFromCart = this.removePromotionFromCart.bind(this);
  }

  componentWillMount() {
    this.props.requestClients();
    this.props.requestStaffs();
  }

  componentWillReceiveProps(update) {
    let fields = this.state.fields

    if (update.cart.client === '' && !update.clients.isLoading && update.clients.items.length) {
      fields['client'] = update.clients.items[0].id;
      this.props.api.setClientToCart(fields.client);
    }

    fields['staff'] = update.cart.staff;
    fields['prospection'] = update.cart.prospection;

    this.setState({fields});
  }

  componentWillUnmount() {
    const fields = {
      client: '',
      staff: '',
      prospection: '',
      payments: [],
      products: [],
      treatments: [],
      services: [],
      promotions: []
    };

    const errors = {};

    this.setState({fields, errors});
  }

  onChange(input) {
    const fields = this.state.fields;
    const errors = this.state.errors;

    fields[input.name] = input.value;
    errors[input.name] = input.error;

    if (input.name === 'client') this.props.api.setClientToCart(input.value);
    if (input.name === 'staff') this.props.api.setStaffToCart(input.value);
    if (input.name === 'prospection') this.props.api.setProspectionToCart(input.value);

    this.setState({fields, errors});
  }

  onControlChange(input) {
    const controls = this.state.controls;

    controls[input.name] = input.value;

    this.setState({controls});
  }

  handlePost(event) {
    event.preventDefault();
    const { cart, handlePost } = this.props;
    const { errors } = this.state;
    const errorMessages = Object.keys(errors).filter((k) => errors[k]);

    // TODO: Check for individual required fields
    if (errorMessages.length === 0) handlePost(cart, this.handlePostSuccess);
  }

  handlePostSuccess() {
    this.props.api.clearCart();
    let controls = this.state.controls;
    controls['payment'] = PAYMENT_METHODS[0].id;
    controls['paymentQuantity'] = '';

    this.setState({controls});
  }

  addPromotionToCart(promotion) {
    this.props.api.addPromotionToCart(promotion);
  }

  removePromotionFromCart(id) {
    this.props.api.removePromotionFromCart(id);
  }

  handlePayment(event) {
    event.preventDefault();

    const controls = this.state.controls;
    const method = controls.payment;
    const quantity = controls.paymentQuantity;

    if ( method !== '' && quantity > 0) {
      this.props.api.addPaymentToCart({method, quantity});
    }
  }

  render() {
    const { cart, clients, staffs, api, route } = this.props;
    const { fields, controls } = this.state;

    const productsInCart = cart.products.map((i, index) => (
      <div className="c-dynamic-list__item o-grid o-grid-axis-align--between o-grid-cross-align--center u-12of12" key={index}>
        <span className="o-dynamic-item-title o-grid__child u-8of12">{i.name} ({i.quantity}) x {toMoney(i.price)}</span>
        <span className="o-dynamic-item-title o-grid__child u-2of12">{toMoney(i.quantity * i.price)}</span>
        <button onClick={() => api.removeProductFromCart(index)} className="o-dynamic-item-remove-btn o-primary-action"><i className="o-icon o-icon-trash--small"></i></button>
      </div>
    ));

    const treatmentsInCart = cart.treatments.map((i, index) => (
      <div className="c-dynamic-list__item o-grid o-grid-axis-align--between o-grid-cross-align--center u-12of12" key={index}>
        <span className="o-dynamic-item-title o-grid__child u-8of12">{i.name} ({i.quantity}) x {toMoney(i.price)}</span>
        <span className="o-dynamic-item-title o-grid__child u-2of12">{toMoney(i.quantity * i.price)}</span>
        <button onClick={() => api.removeTreatmentFromCart(index)} className="o-dynamic-item-remove-btn o-primary-action"><i className="o-icon o-icon-trash--small"></i></button>
      </div>
    ));

    const servicesInCart = cart.services.map((i, index) => (
      <div className="c-dynamic-list__item o-grid o-grid-axis-align--between o-grid-cross-align--center u-12of12" key={index}>
        <span className="o-dynamic-item-title o-grid__child u-8of12">{i.name} ({i.quantity}) x {toMoney(i.price)}</span>
        <span className="o-dynamic-item-title o-grid__child u-2of12">{toMoney(i.quantity * i.price)}</span>
        <button onClick={() => api.removeServiceFromCart(index)} className="o-dynamic-item-remove-btn o-primary-action"><i className="o-icon o-icon-trash--small"></i></button>
      </div>
    ));

    const paymentsControl = (
      <div className="c-control [ o-grid__child u-12of12 ] [ o-grid o-grid-axis-align--between o-grid-cross-align--center o-grid-wrap--no ]">
          <div className="[ o-grid__child u-10of12 ] [ o-grid o-grid-axis-align--between o-grid-cross-align--center ]">
            <div className="o-grid__child u-12of12 u-8of12@lap u-margin-s--bottom">
              <SelectField
              name="payment"
              value={controls.payment}
              onChange={this.onControlChange}
              options={PAYMENT_METHODS.map((p) => {return {value: p.id, name: p.name}})}
              classNames={"c-input-field__field"}
              id="payment"
              />
            </div>
            <div className="o-grid__child u-4of12 u-4of12@lap u-margin-s--bottom">
              <InputField
                type="number"
                name="paymentQuantity"
                value={controls.paymentQuantity}
                min={0}
                placeholder="0.00"
                required={true}
                validate={(value) => value <= 0}
                onChange={this.onControlChange}
                id="paymentQuantity"
                classNames="c-input-field__field"
                />
            </div>
          </div>

          <button
            onClick={(e) => this.handlePayment(e)}
            className="c-control__btn o-primary-action o-grid__child u-margin-l--left">
            <i className="o-icon o-icon-tick--small"></i>
          </button>
        </div>
    );

    const paymentsInCart = cart.payments.map((i, index) => (
      <div className="c-dynamic-list__item o-grid o-grid-axis-align--between o-grid-cross-align--center u-12of12" key={index}>
        <span className="o-dynamic-item-title o-grid__child u-8of12">{i.method}</span>
        <span className="o-dynamic-item-title o-grid__child u-2of12">{toMoney(i.quantity)}</span>
        <button onClick={() => api.removePaymentFromCart(index)} className="o-dynamic-item-remove-btn o-primary-action"><i className="o-icon o-icon-trash--small"></i></button>
      </div>
    ));

    const items = [...cart.products, ...cart.treatments, ...cart.services];
    const promotionsOffer = items.reduce((acc, i) => {
      if (i.promotions.length) { acc = [...acc, ...i.promotions] };
      return acc;
    }, []);

    return (
      <div className="c-catalogue-list">
        <div className="c-catalogue-list__header">
          <h1 className="t3">Carrito de Compra</h1>
          <p className="o-catalogue-description">Verifica y completa la información sobre la venta actual.</p>
          <div className="c-catalogue-list__filters">
            <button onClick={() => api.clearCart()} className="o-filter">Limpiar el carrito</button>
          </div>
        </div>

        <div className={`c-catalogue-list__form`}>
            <form className="c-catalogue-form c-form" action="/" method="post">

            {/* SECTION */}
              <div className="c-catalogue-form__section o-grid o-grid-axis-align--start o-grid-cross-align--stretch">
                <h2 className="o-section-title t1 o-grid__child u-12of12">Información General</h2>

                <div className="[ o-grid__child u-12of12 u-6of12@lap ]">
                  <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--around ]">
                    <label htmlFor="client" className="c-input-field__label">Cliente que realiza la compra</label>
                    {
                      !clients.isLoading
                      ? <SelectField
                        name="client"
                        options={clients.items.map((p) => {return {value: p.id, name: p.name}})}
                        value={fields.client}
                        onChange={this.onChange}
                        required={true}
                        classNames={"c-input-field__field"}
                        id="client"
                        />
                      : <p className="s1 o-loading-indicator">Cargando...</p>
                    }
                  </div>
                </div>

                <div className="[ o-grid__child u-12of12 u-6of12@lap ]">
                  <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--around ]">
                    <label htmlFor="staff" className="c-input-field__label">Empleado que realiza la venta</label>
                    {
                      !staffs.isLoading
                      ? <SelectField
                        name="staff"
                        options={[{value: 1, name: 'Diruma Body Spa'}, ...staffs.items.map((p) => {return {value: p.id, name: p.name}})]}
                        value={fields.staff}
                        onChange={this.onChange}
                        required={true}
                        classNames={"c-input-field__field"}
                        id="staff"
                        />
                      : <p className="s1 o-loading-indicator">Cargando...</p>
                    }
                  </div>
                </div>

                <div className="[ o-grid__child u-8of12 u-6of12@lap ]">
                  <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--around ]">
                    <label className="c-input-field__label">¿La venta es prospecto?</label>
                    <CheckboxField
                      name="prospection"
                      checked={fields.prospection ? true : false}
                      value={1}
                      label="Es prospecto"
                      required={true}
                      onChange={this.onChange}
                      id="prospection"
                      classNames="c-checkbox-field"
                      />
                  </div>
                </div>
              </div>

              {/* SECTION */}
              <div className="c-catalogue-form__section o-grid o-grid-axis-align--start o-grid-cross-align--stretch">
                <h2 className="o-section-title t1 o-grid__child u-12of12">Contenido de la venta</h2>

                <div className="c-input-field--small u-margin-c u-margin-l--bottom [ o-grid__child u-12of12 u-8of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                  <label className="c-input-field__label">Productos incluidos</label>
                  <div className="c-dynamic-list o-grid_child u-12of12">
                    { productsInCart }
                  </div>
                </div>

                <div className="c-input-field--small u-margin-c u-margin-l--bottom [ o-grid__child u-12of12 u-8of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                  <label className="c-input-field__label">Tratamientos incluidos</label>
                  <div className="c-dynamic-list o-grid_child u-12of12">
                    { treatmentsInCart }
                  </div>
                </div>

                <div className="c-input-field--small u-margin-c u-margin-l--bottom [ o-grid__child u-12of12 u-8of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                  <label className="c-input-field__label">Paquetes incluidos</label>
                  <div className="c-dynamic-list o-grid_child u-12of12">
                    { servicesInCart }
                  </div>
                </div>

              </div>

              {/* SECTION */}
              <div className="c-catalogue-form__section o-grid o-grid-axis-align--start o-grid-cross-align--stretch">
                <h2 className="o-section-title t1 o-grid__child u-12of12">Promociones aplicables</h2>

                {
                  promotionsOffer.map((i) => (
                    <div className="o-grid__child u-6of12 u-4of12@lap" key={i.id}>
                      <PromotionToggler
                        promotion={i}
                        requestToggleOn={this.addPromotionToCart}
                        requestToggleOff={this.removePromotionFromCart}
                        />
                    </div>
                  ))
                }

              </div>

              {/* SECTION */}
              <div className="c-catalogue-form__section o-grid o-grid-axis-align--start o-grid-cross-align--stretch">
                <h2 className="o-section-title t1 o-grid__child u-12of12">Información del pago</h2>

                <div className="c-input-field--small [ o-grid__child u-4of12 u-12of12@lap ] [ o-grid--vertical o-grid-axis-align--between ]">
                  <label className="c-input-field__label">Costo total</label>
                  <span className="o-big-number">{toMoney(cart.total)}</span>
                </div>

                <div className="c-input-field--small [ o-grid__child u-12of12 u-6of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                  <label htmlFor="payment" className="c-input-field__label">Forma de pago</label>
                  { paymentsControl }
                  <div className="c-dynamic-list o-grid_child u-12of12">
                    { paymentsInCart }
                  </div>
                </div>

              </div>

              <div className="c-buttons-field">
                <button className="c-buttons-field__button" onClick={() => route.history.goBack()}>Cancelar</button>
                <button className="c-buttons-field__button--primary" onClick={(e) => this.handlePost(e)} type="submit">Agregar</button>
              </div>
            </form>
        </div>

      </div>
    );
  }
}

ShoppingCart.propTypes = {
  cart: PropTypes.object.isRequired,
  clients: PropTypes.object.isRequired,
  staffs: PropTypes.object.isRequired,
  api: PropTypes.object.isRequired,
  handlePost: PropTypes.func.isRequired,
  requestClients: PropTypes.func.isRequired,
  requestStaffs: PropTypes.func.isRequired,
  route: PropTypes.object.isRequired
};

export default ShoppingCart;
