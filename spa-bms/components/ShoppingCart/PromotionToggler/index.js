import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './index.scss';

class PromotionToggler extends Component {
  constructor(props) {
    super(props);

    this.state = {
      toggled: false
    };

    this.toggleAction = this.toggleAction.bind(this);
  }

  toggleAction() {
    const { toggled } = this.state;
    const { promotion, requestToggleOn, requestToggleOff } = this.props;
    if (toggled) { requestToggleOff(promotion.id) }
    else { requestToggleOn(promotion) }

    this.setState({toggled: !this.state.toggled});
  }

  render() {
    const { promotion } = this.props;
    const { toggled } = this.state;

    return (
      <div
        onClick={() => this.toggleAction()}
        className={`c-promotion ${toggled ? 'is-active' : ''}`}>
          <h2 className="t2 u-text-center u-text-truncate u-margin-s--bottom">{promotion.name}</h2>
          <p className="u-margin-c u-text-center">{promotion.description}</p>
      </div>
    );
  }
}

PromotionToggler.propTypes = {
  promotion: PropTypes.object.isRequired,
  requestToggleOn: PropTypes.func.isRequired,
  requestToggleOff: PropTypes.func.isRequired
};

export default PromotionToggler;
