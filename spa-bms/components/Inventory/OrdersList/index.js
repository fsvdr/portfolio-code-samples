import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { BeatLoader } from 'react-spinners';
import { Route, NavLink } from 'react-router-dom';
import Moment from 'react-moment';
import 'moment/locale/es';
import NewOrdersForm from './OrdersForm/new.form';
import EditOrdersForm from './OrdersForm/edit.form';
import { toMoney } from '../../../helpers/money-format';

class OrdersList extends Component {

  constructor(props) {
    super(props);

    this.completeOrder = this.completeOrder.bind(this);
  }

  componentWillMount() {
    this.props.api.fetch();
  }

  completeOrder(id, status) {
    if (status !== 'Completada') {
      this.props.api.complete(id, this.props.api.fetch);
    }
  }

  render() {
    const { orders, products, api, requestProducts, route } = this.props;

    return (
      <div className="c-catalogue-list">
        <div className="c-catalogue-list__header">
          <h1 className="t3">Listado de Pedidos</h1>
          <p className="o-catalogue-description">Consulta, edita y crea pedidos a proveedor.</p>
          <div className="c-catalogue-list__filters">
            <NavLink to="/inventario/pedidos/nuevo" className="o-filter">Agregar nuevo pedido</NavLink>
          </div>
        </div>

        {/* NEW ORDER FORM */}
        <Route path="/inventario/pedidos/nuevo" render={(match) => (
          <NewOrdersForm
            products={products}
            handlePost={api.post}
            requestDataReload={api.fetch}
            requestProducts={requestProducts}
            route={match} />
        )}/>


        {/* EDIT ORDER FORM */}
        <Route path="/inventario/pedidos/editar/:id" render={(match) => (
          <EditOrdersForm
            isLoading={orders.isLoading}
            order={orders.item}
            products={products}
            handleGet={api.get}
            handlePost={api.post}
            handleDelete={api.delete}
            requestDataReload={api.fetch}
            requestProducts={requestProducts}
            route={match} />
        )}/>

        {/* ORDERS LIST */}
        <Route exact path="/inventario/pedidos" render={() => (
          <div className="c-catalogue-list__body">
            {
              orders.isLoading
              ? <BeatLoader className="c-catalogue-list__spinner" color={'#ffffff'} loading={true} />
              : (
                  <div className="c-items-list o-grid o-grid-cross-align--stretch">
                    {
                      orders.items.map((i) => {
                        const title = i.products.map((p) => p.name).join(', ');
                        return (
                          <div className="[ o-grid__child u-12of12 u-6of12@tab u-4of12@lap ]" key={i.id}>
                            <div className="c-item o-grid o-grid-axis-align--between o-grid-cross-align--center">
                              <div className="o-grid__child u-margin-m--bottom">
                                <button onClick={() => {this.completeOrder(i.id, i.status)}} className="o-primary-action">
                                  <i className={`o-icon o-icon-${i.status !== 'Completada' ? 'clock' : 'tick'}--small`}></i>
                                </button>
                              </div>
                              <div className="c-item__data [ o-grid__child u-12of12 ]">
                                <h2 className="o-item-name t2 u-text-truncate u-margin-c">
                                  <NavLink to={`${route.match.url}/editar/${i.id}`}>{title}</NavLink>
                                </h2>
                                <p className="o-item-description u-margin-s--bottom u-text-left"><Moment locale="es" format="MMMM D YYYY">{i.date}</Moment></p>
                                <div className="c-item__numbers [ o-grid o-grid-axis-align--between o-grid-cross-align--center o-grid-wrap--no ]">
                                  <span className="o-item-number">Costo: <strong>{toMoney(i.cost)}</strong></span>
                                  <span className="o-item-number u-text-right">Pagado: <strong>{toMoney(i.paid)}</strong></span>
                                </div>
                              </div>
                            </div>
                          </div>
                        )
                      })
                    }
                  </div>
                )
            }
          </div>
        )}/>
      </div>
    );
  }
}

OrdersList.propTypes = {
  orders: PropTypes.object.isRequired,
  products: PropTypes.object.isRequired,
  api: PropTypes.object.isRequired,
  requestProducts: PropTypes.func.isRequired,
  route: PropTypes.object.isRequired
};

export default OrdersList;
