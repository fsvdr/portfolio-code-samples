import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import InputField from '../../../Forms/InputField';
import SelectField from '../../../Forms/SelectField';
import { PAYMENT_METHODS } from '../../../../constants';

class NewOrdersForm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      fields: {
        products: [],
        payment: '',
        paid: '',
        status: ''
      },
      errors: {},
      controls: {
        product: this.props.products.items.length ? this.props.products.items[0].id : 0,
        productQuantity: '',
      }
    };

    this.onChange = this.onChange.bind(this);
    this.onControlChange = this.onControlChange.bind(this);
    this.handlePost = this.handlePost.bind(this);
    this.handlePostSuccess = this.handlePostSuccess.bind(this);
    this.handleProduct = this.handleProduct.bind(this);
    this.handleProductRemoval = this.handleProductRemoval.bind(this);
  }

  componentWillMount() {
    this.props.requestProducts();
  }

  componentWillUnMount() {
    const fields = {
      products: [],
      payment: '',
      paid: '',
      status: ''
    };

    const errors = {};

    this.setState({fields, errors});
  }

  onChange(input) {
    const fields = this.state.fields;
    const errors = this.state.errors;

    fields[input.name] = input.value;
    errors[input.name] = input.error;

    this.setState({fields, errors});
  }

  onControlChange(input) {
    const controls = this.state.controls;

    controls[input.name] = input.value;

    this.setState({controls});
  }

  handlePost(event) {
    event.preventDefault();

    const { handlePost } = this.props;
    const { fields, errors } = this.state;
    const errorMessages = Object.keys(errors).filter((k) => errors[k]);

    // TODO: Check for individual required fields
    if (errorMessages.length === 0) handlePost(fields, this.handlePostSuccess);
  }

  handlePostSuccess() {
    this.props.requestDataReload();
    this.props.route.history.goBack();
  }

  handleProduct(event) {
    event.preventDefault();

    const controls = this.state.controls;
    const id = controls.product;
    const quantity = controls.productQuantity;

    if ( id !== '' && quantity> 0) {
      let fields = this.state.fields;
      let productsField = fields.products;

      // If selected product already in order, ignore
      const matchId = productsField.findIndex((p) => p.id == id); // eslint-disable-line
      if (matchId >= 0) return;

      // If selection not in order, add it
      productsField.push({id, quantity});
      fields['products'] = productsField;

      this.setState({fields});
    }
  }

  handleProductRemoval(event, id) {
    event.preventDefault();

    let fields = this.state.fields;
    let productsField = fields['products'];

    const matchId = productsField.findIndex((p) => p.id == id); // eslint-disable-line
    if (matchId < 0) return;

    const products = [...productsField.slice(0, matchId), ...productsField.slice(matchId + 1)];

    fields['products'] = products;
    this.setState({fields});
  }


  render() {
    const { products, route } = this.props;
    const { fields, controls } = this.state;

    const productsControl = !products.isLoading
      ? <div className="c-control [ o-grid__child u-12of12 ] [ o-grid o-grid-axis-align--between o-grid-cross-align--center o-grid-wrap--no ]">
          <div className="[ o-grid__child u-10of12 ] [ o-grid o-grid-axis-align--between o-grid-cross-align--center ]">
            <div className="o-grid__child u-12of12 u-8of12@lap u-margin-s--bottom">
              <SelectField
              name="product"
              value={controls.product}
              onChange={this.onControlChange}
              options={products.items.map((p) => {return {value: p.id, name: p.name}})}
              classNames={"c-input-field__field"}
              id="product"
              />
            </div>
            <div className="o-grid__child u-4of12 u-4of12@lap u-margin-s--bottom">
              <InputField
                type="number"
                name="productQuantity"
                value={controls.productQuantity}
                min={0}
                placeholder="0"
                required={true}
                validate={(value) => value <= 0}
                onChange={this.onControlChange}
                id="productQuantity"
                classNames="c-input-field__field"
                />
            </div>
          </div>

          <button
            onClick={(e) => this.handleProduct(e)}
            className="c-control__btn o-primary-action o-grid__child u-margin-l--left">
            <i className="o-icon o-icon-tick--small"></i>
          </button>
        </div>
      : <p className="s1 o-loading-indicator">Cargando...</p>;

    let orderTotal = 0;

    const orderProducts = fields.products.map((p) => {
        const item = products.items.find((i) => i.id == p.id) //eslint-disable-line
        orderTotal += p.quantity * item.cost;
        return (
          <div className="c-dynamic-list__item o-grid o-grid-axis-align--between o-grid-cross-align--center u-12of12" key={item.id}>
            <span className="o-dynamic-item-title o-grid__child u-10of12">{item.name} ({p.quantity}) x ${item.cost}</span>
            <button onClick={(e) => this.handleProductRemoval(e, item.id)} className="o-dynamic-item-remove-btn o-primary-action"><i className="o-icon o-icon-trash--small"></i></button>
          </div>
        );
    })

    return (
      <div className={`c-catalogue-list__form`}>
        <h2 className="t2">Agrega un nuevo pedido</h2>
        <p className="o-catalogue-description">Por favor llena los siguientes campos con la información del pedido a agregar.</p>
          <form className="c-catalogue-form c-form" action="/" method="post">

            {/* SECTION */}
            <div className="c-catalogue-form__section o-grid o-grid-axis-align--start o-grid-cross-align--stretch">
              <h2 className="o-section-title t1 o-grid__child u-12of12">Información sobre el pedido</h2>

              <div className="c-input-field--small u-margin-c [ o-grid__child u-12of12 u-6of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                <label htmlFor="products" className="c-input-field__label">Productos incluidos</label>
                { productsControl }
                <div className="c-dynamic-list o-grid_child u-12of12">
                  { orderProducts }
                </div>
              </div>

            </div>

            {/* SECTION */}
            <div className="c-catalogue-form__section o-grid o-grid-axis-align--start o-grid-cross-align--stretch">
              <h2 className="o-section-title t1 o-grid__child u-12of12">Información sobre el pago</h2>

              <div className="c-input-field--small [ o-grid__child u-4of12 u-2of12@lap ] [ o-grid--vertical o-grid-axis-align--between ]">
                <label htmlFor="cost" className="c-input-field__label">Costo total</label>
                <span className="o-big-number">${ orderTotal }</span>
              </div>

              <div className="c-input-field--small [ o-grid__child u-4of12 u-2of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                <label htmlFor="paid" className="c-input-field__label">Cantidad pagada</label>
                <InputField
                  type="number"
                  name="paid"
                  value={fields.paid}
                  min={0}
                  placeholder="0.00"
                  required={true}
                  validate={(value) => value <= 0}
                  onChange={this.onChange}
                  id="paid"
                  classNames="c-input-field__field"
                  />
              </div>

              <div className="c-input-field--small [ o-grid__child u-12of12 u-4of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                <label htmlFor="payment" className="c-input-field__label">Método de pago</label>
                <SelectField
                  name="payment"
                  value={fields.payment}
                  onChange={this.onChange}
                  options={PAYMENT_METHODS.map((p) => {return {value: p.id, name: p.name}})}
                  classNames={"c-input-field__field o-grid__child u-8of12"}
                  id="payment"
                  />
              </div>

              <div className="c-input-field--small [ o-grid__child u-12of12 u-4of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                <label htmlFor="status" className="c-input-field__label">Status del pedido</label>
                <SelectField
                  name="status"
                  value={fields.status}
                  onChange={this.onChange}
                  options={[{value: 'Pendiente', name: 'Pendiente'}, {value: 'Recibida', name: 'Recibido'}]}
                  classNames={"c-input-field__field o-grid__child u-8of12"}
                  id="status"
                  />
              </div>

            </div>

            <div className="c-buttons-field">
              <button className="c-buttons-field__button" onClick={() => route.history.goBack()}>Cancelar</button>
              <button className="c-buttons-field__button--primary" onClick={(e) => this.handlePost(e)} type="submit">Agregar</button>
            </div>
          </form>
      </div>
    );
  }
}

NewOrdersForm.propTypes = {
  products: PropTypes.object.isRequired,
  handlePost: PropTypes.func.isRequired,
  requestDataReload: PropTypes.func,
  requestProducts: PropTypes.func.isRequired,
  route: PropTypes.object.isRequired
};

export default NewOrdersForm;
