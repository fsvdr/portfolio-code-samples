import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { BeatLoader } from 'react-spinners';
import { Route, NavLink } from 'react-router-dom';
import NewProvidersForm from './ProvidersForm/new.form';
import EditProvidersForm from './ProvidersForm/edit.form';

class ProvidersList extends Component {

  componentWillMount() {
    this.props.api.fetch();
  }

  render() {
    const { providers, api, route } = this.props;

    return (
      <div className="c-catalogue-list">
        <div className="c-catalogue-list__header">
          <h1 className="t3">Listado de Proveedores</h1>
          <p className="o-catalogue-description">Consulta, edita y agrega proveedores.</p>
          <div className="c-catalogue-list__filters">
            <NavLink to="/inventario/proveedores/nuevo" className="o-filter">Agregar nuevo proveedor</NavLink>
          </div>
        </div>

        {/* NEW PROVIDER FORM */}
        <Route path="/inventario/proveedores/nuevo" render={(match) => (
          <NewProvidersForm
            handlePost={api.post}
            requestDataReload={api.fetch}
            route={match} />
        )}/>

        {/* EDIT PROVIDER FORM */}
        <Route path="/inventario/proveedores/editar/:id" render={(match) => (
          <EditProvidersForm
            isLoading={providers.isLoading}
            provider={providers.item}
            handleGet={api.get}
            handlePost={api.post}
            handleDelete={api.delete}
            requestDataReload={api.fetch}
            route={match} />
        )}/>

        {/* PROVIDERS LIST */}
        <Route exact path="/inventario/proveedores" render={() => (
          <div className="c-catalogue-list__body">
            {
              providers.isLoading
              ? <BeatLoader className="c-catalogue-list__spinner" color={'#ffffff'} loading={true} />
              : (
                  <div className="c-items-list o-grid o-grid-cross-align--stretch">
                    {
                      providers.items.map((i) => (
                        <div className="[ o-grid__child u-12of12 u-6of12@tab u-4of12@lap ]" key={i.id}>
                          <div className="c-item o-grid o-grid-axis-align--between o-grid-cross-align--center">
                            <div className="o-grid__child u-margin-m--bottom">
                              {i.phone ? (<a href={`tel:${i.phone}`} className="o-primary-action"><i className="o-icon o-icon-phone--small"></i></a>) : null}
                            </div>
                            <div className="o-grid__child u-margin-m--bottom">
                              {i.website ? (<a href={i.website} target="_blank" className="o-primary-action"><i className="o-icon o-icon-link--small"></i></a>) : null}
                            </div>
                            <div className="c-item__data [ o-grid__child u-12of12 ]">
                              <h2 className="o-item-name t2 u-text-truncate u-margin-c"><NavLink to={`${route.match.url}/editar/${i.id}`}>{i.name}</NavLink></h2>
                              <p className="o-item-description u-margin-s--bottom u-text-left">{i.address}</p>
                              <div className="c-item__numbers [ o-grid o-grid-axis-align--between o-grid-cross-align--center o-grid-wrap--no ]">
                                {i.email ? (<a href={`mailto:${i.email}`} className="o-item-number">Correo electrónico: <strong>{i.email}</strong></a>) : null}
                              </div>
                            </div>
                          </div>
                        </div>
                      ))
                    }
                  </div>
                )
            }
          </div>
        )}/>
      </div>
    );
  }
}

ProvidersList.propTypes = {
  providers: PropTypes.object.isRequired,
  api: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired
};

export default ProvidersList;
