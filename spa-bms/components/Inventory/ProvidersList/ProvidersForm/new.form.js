import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import InputField from '../../../Forms/InputField';
import TextareaField from '../../../Forms/TextareaField';

class NewProvidersForm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      fields: {
        name: '',
        phone: '',
        cellphone: '',
        address: '',
        website: '',
        contactName: '',
        email: '',
        rfc: ''
      },
      errors: {},
    };

    this.onChange = this.onChange.bind(this);
    this.handlePost = this.handlePost.bind(this);
    this.handlePostSuccess = this.handlePostSuccess.bind(this);
  }

  componentWillUnMount() {
    const fields = {
      name: '',
      phone: '',
      cellphone: '',
      address: '',
      website: '',
      contactName: '',
      email: '',
      rfc: ''
    };

    const errors = {};

    this.setState({fields, errors});
  }

  onChange(input) {
    const fields = this.state.fields;
    const errors = this.state.errors;

    fields[input.name] = input.value;
    errors[input.name] = input.error;

    this.setState({fields, errors});
  }

  handlePost(event) {
    event.preventDefault();

    const { handlePost } = this.props;
    const { fields, errors } = this.state;
    const errorMessages = Object.keys(errors).filter((k) => errors[k]);

    // TODO: Check for individual required fields
    if (errorMessages.length === 0) handlePost(fields, this.handlePostSuccess);
  }

  handlePostSuccess() {
    this.props.requestDataReload();
    this.props.route.history.goBack();
  }

  render() {
    const { route } = this.props;
    const { fields } = this.state;

    return (
      <div className={`c-catalogue-list__form`}>
        <h2 className="t2">Agrega un nuevo proveedor</h2>
        <p className="o-catalogue-description">Por favor llena los siguientes campos con la información del proveedor a agregar.</p>
          <form className="c-catalogue-form c-form" action="/" method="post">
          {/* SECTION */}
            <div className="c-catalogue-form__section o-grid o-grid-axis-align--start o-grid-cross-align--stretch">
              <h2 className="o-section-title t1 o-grid__child u-12of12">Información General</h2>

              <div className="[ o-grid__child u-8of12 u-6of12@lap ]">
                <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--around ]">
                  <label htmlFor="name" className="c-input-field__label">Nombre</label>
                  <InputField
                    type="text"
                    name="name"
                    value={fields.name}
                    placeholder="Nombre"
                    required={true}
                    validate={(value) => value.length <= 4}
                    onChange={this.onChange}
                    id="name"
                    classNames="c-input-field__field"
                    />
                </div>
              </div>

              <div className="[ o-grid__child u-8of12 u-6of12@lap ]">
                <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--around ]">
                  <label htmlFor="website" className="c-input-field__label">Sitio web</label>
                  <InputField
                    type="text"
                    name="website"
                    value={fields.website}
                    placeholder="Sitio web"
                    required={true}
                    validate={(value) => value.length <= 4}
                    onChange={this.onChange}
                    id="website"
                    classNames="c-input-field__field"
                    />
                </div>
              </div>

              <div className="[ o-grid__child u-8of12 u-6of12@lap ]">
                <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--between ]">
                  <label htmlFor="rfc" className="c-input-field__label">RFC</label>
                  <InputField
                    type="text"
                    name="rfc"
                    value={fields.rfc}
                    placeholder="RFC"
                    required={true}
                    validate={(value) => value.length <= 4}
                    onChange={this.onChange}
                    id="rfc"
                    classNames="c-input-field__field"
                    />
                </div>
              </div>

              <div className="[ o-grid__child u-8of12 u-6of12@lap ]">
                <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--around ]">
                  <label htmlFor="address" className="c-input-field__label">Dirección</label>
                  <TextareaField
                    name="address"
                    value={fields.address}
                    placeholder="Dirección"
                    required={true}
                    validate={(value) => value.length <= 4}
                    onChange={this.onChange}
                    id="address"
                    classNames="c-input-field__field"
                    />
                </div>
              </div>
            </div>

            {/* SECTION */}
            <div className="c-catalogue-form__section o-grid o-grid-axis-align--start o-grid-cross-align--stretch">
              <h2 className="o-section-title t1 o-grid__child u-12of12">Información de contacto</h2>

              <div className="[ o-grid__child u-8of12 u-6of12@lap ]">
                <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--around ]">
                  <label htmlFor="contactName" className="c-input-field__label">Nombre de contacto</label>
                  <InputField
                    type="text"
                    name="contactName"
                    value={fields.contactName}
                    placeholder="Nombre de contacto"
                    required={true}
                    validate={(value) => value.length <= 4}
                    onChange={this.onChange}
                    id="contactName"
                    classNames="c-input-field__field"
                    />
                </div>
              </div>

              <div className="[ o-grid__child u-8of12 u-6of12@lap ]">
                <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--around ]">
                  <label htmlFor="phone" className="c-input-field__label">Teléfono fijo</label>
                  <InputField
                    type="text"
                    name="phone"
                    value={fields.phone}
                    placeholder="Teléfono fijo"
                    required={true}
                    validate={(value) => value.length <= 4}
                    onChange={this.onChange}
                    id="phone"
                    classNames="c-input-field__field"
                    />
                </div>
              </div>

              <div className="[ o-grid__child u-8of12 u-6of12@lap ]">
                <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--around ]">
                  <label htmlFor="cellphone" className="c-input-field__label">Teléfono celular</label>
                  <InputField
                    type="text"
                    name="cellphone"
                    value={fields.cellphone}
                    placeholder="Teléfono celular"
                    required={true}
                    validate={(value) => value.length <= 4}
                    onChange={this.onChange}
                    id="cellphone"
                    classNames="c-input-field__field"
                    />
                </div>
              </div>

              <div className="[ o-grid__child u-8of12 u-6of12@lap ]">
                <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--around ]">
                  <label htmlFor="email" className="c-input-field__label">Correo electrónico</label>
                  <InputField
                    type="text"
                    name="email"
                    value={fields.email}
                    placeholder="Correo electrónico"
                    required={true}
                    validate={(value) => value.length <= 4}
                    onChange={this.onChange}
                    id="email"
                    classNames="c-input-field__field"
                    />
                </div>
              </div>

            </div>
            <div className="c-buttons-field">
              <button className="c-buttons-field__button" onClick={() => route.history.goBack()}>Cancelar</button>
              <button className="c-buttons-field__button--primary" onClick={(e) => this.handlePost(e)} type="submit">Agregar</button>
            </div>
          </form>
      </div>
    );
  }
}

NewProvidersForm.propTypes = {
  handlePost: PropTypes.func.isRequired,
  requestDataReload: PropTypes.func,
  route: PropTypes.object.isRequired
};

export default NewProvidersForm;
