import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { BeatLoader } from 'react-spinners';
import { Route, NavLink } from 'react-router-dom';
import Moment from 'react-moment';
import { API_URL } from '../../../constants';
import NewPromotionsForm from './PromotionsForm/new.form';
import EditPromotionsForm from './PromotionsForm/edit.form';

class PromotionsList extends Component {

  componentWillMount() {
    this.props.api.fetch();
  }

  render() {
    const { promotions, products, treatments, services, api, requestProducts, requestTreatments, requestServices, route } = this.props;

    return (
      <div className="c-catalogue-list">
        <div className="c-catalogue-list__header">
          <h1 className="t3">Listado de Promociones</h1>
          <p className="o-catalogue-description">Consulta, edita y crea promociones.</p>
          <div className="c-catalogue-list__filters">
            <NavLink to="/inventario/promociones/nueva" className="o-filter">Agregar nueva promocion</NavLink>
          </div>
        </div>

        {/* NEW PROMOTION FORM */}
        <Route path="/inventario/promociones/nueva" render={(match) => (
          <NewPromotionsForm
            products={products}
            treatments={treatments}
            services={services}
            handlePost={api.post}
            requestDataReload={api.fetch}
            requestProducts={requestProducts}
            requestTreatments={requestTreatments}
            requestServices={requestServices}
            route={match} />
        )}/>


        {/* EDIT PROMOTION FORM */}
        <Route path="/inventario/promociones/editar/:id" render={(match) => (
          <EditPromotionsForm
            isLoading={promotions.isLoading}
            promotion={promotions.item}
            products={products}
            treatments={treatments}
            services={services}
            handleGet={api.get}
            handlePost={api.post}
            handleDelete={api.delete}
            requestDataReload={api.fetch}
            requestProducts={requestProducts}
            requestTreatments={requestTreatments}
            requestServices={requestServices}
            route={match} />
        )}/>


        {/* PROMOTIONS LIST */}
        <Route exact path="/inventario/promociones" render={() => (
          <div className="c-catalogue-list__body">
            {
              promotions.isLoading
              ? <BeatLoader className="c-catalogue-list__spinner" color={'#ffffff'} loading={true} />
              : (
                  <div className="c-items-list o-grid o-grid-cross-align--stretch">
                    {
                      promotions.items.map((i) => (
                        <div className="[ o-grid__child u-12of12 u-6of12@tab u-4of12@lap ]" key={i.id}>
                          <div className="c-item o-grid o-grid-axis-align--between o-grid-cross-align--center">
                            <div className="o-grid__child u-margin-m--bottom">
                              <img className="c-item__photo" src={`${API_URL}/${i.image}`} alt=""/>
                            </div>
                            <div className="c-item__data [ o-grid__child u-12of12 ]">
                              <h2 className="o-item-name t2 u-text-truncate u-margin-c"><NavLink to={`${route.match.url}/editar/${i.id}`}>{i.name}</NavLink></h2>
                              <p className="o-item-description u-margin-s--bottom u-text-left">{i.description}</p>
                              <div className="c-item__numbers [ o-grid o-grid-axis-align--between o-grid-cross-align--center o-grid-wrap--no ]">
                                <span className="o-item-number">Desde: <strong><Moment locale="es" format="MMM D YYYY">{i.validStart}</Moment></strong></span>
                                <span className="o-item-number u-text-right">Hasta: <strong><Moment locale="es" format="MMM D YYYY">{i.validEnd}</Moment></strong></span>
                              </div>
                            </div>
                          </div>
                        </div>
                      ))
                    }
                  </div>
                )
            }
          </div>
        )}/>
      </div>
    );
  }
}

PromotionsList.propTypes = {
  promotions: PropTypes.object.isRequired,
  products: PropTypes.object.isRequired,
  treatments: PropTypes.object.isRequired,
  services: PropTypes.object.isRequired,
  api: PropTypes.object.isRequired,
  requestProducts: PropTypes.func.isRequired,
  requestTreatments: PropTypes.func.isRequired,
  requestServices: PropTypes.func.isRequired,
  route: PropTypes.object.isRequired
};

export default PromotionsList;
