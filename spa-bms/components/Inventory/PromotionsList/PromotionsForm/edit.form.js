import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import InputField from '../../../Forms/InputField';
import FileField from '../../../Forms/FileField';
import TextareaField from '../../../Forms/TextareaField';
import SelectField from '../../../Forms/SelectField';
import { PROMOTION_TYPES } from '../../../../constants';
import { BeatLoader } from 'react-spinners';

class EditPromotionsForm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      fields: {
        id: '',
        validStart: '',
        validEnd: '',
        name: '',
        image: '',
        description: '',
        type: '',
        value: '',
        itemType: '',
        itemId: ''
      },
      errors: {},
    };

    this.onChange = this.onChange.bind(this);
    this.handlePost = this.handlePost.bind(this);
    this.handlePostSuccess = this.handlePostSuccess.bind(this);
    this.handleDeleteSuccess = this.handleDeleteSuccess.bind(this);
  }

  componentWillMount() {
    const id = this.props.route.match.params.id;
    this.props.handleGet(id);
    this.props.requestProducts();
    this.props.requestTreatments();
    this.props.requestServices();
  }

  componentWillReceiveProps(update) {
    if (update.promotion.id) {
      const fields = {
        id: update.promotion.id,
        validStart: update.promotion.validStart,
        validEnd: update.promotion.validEnd,
        name: update.promotion.name,
        image: update.promotion.image,
        description: update.promotion.description,
        type: update.promotion.type,
        value: update.promotion.value,
        itemType: update.promotion.itemType,
        itemId: update.promotion.itemId,
      };

      const errors = {};

      this.setState({fields, errors});
    }
  }

  componentWillUnmount() {
    const fields = {
      validStart: '',
      validEnd: '',
      name: '',
      image: '',
      description: '',
      type: '',
      value: '',
      itemType: 'Product',
      itemId: ''
    };

    const errors = {};

    this.setState({fields, errors});
  }

  onChange(input) {
    const fields = this.state.fields;
    const errors = this.state.errors;

    fields[input.name] = input.value;
    errors[input.name] = input.error;

    this.setState({fields, errors});
  }

  handlePost(event) {
    event.preventDefault();

    const { handlePost } = this.props;
    const { fields, errors } = this.state;
    const errorMessages = Object.keys(errors).filter((k) => errors[k]);

    // TODO: Check for individual required fields
    if (errorMessages.length === 0) handlePost(fields, this.handlePostSuccess);
  }

  handlePostSuccess() {
    this.props.requestDataReload();
    this.props.route.history.goBack();
  }

  handleDeleteSuccess() {
    this.props.requestDataReload();
    this.props.route.history.goBack()
  }

  render() {
    const { isLoading, products, treatments, services, handleDelete, route } = this.props;
    const { fields } = this.state;

    let itemsSelect;

    if (fields.itemType === 'Product') {
      itemsSelect = !products.isLoading
        ? (
          <SelectField
            name="itemId"
            value={fields.itemId}
            onChange={this.onChange}
            options={products.items.map((i) => {return {value: i.id, name: i.name}})}
            classNames={"c-input-field__field"}
            id="itemId"
            />
        ) : (
          <p className="s1 o-loading-indicator">Cargando...</p>
        )
    } else if (fields.itemType === 'Treatment') {
      itemsSelect = !treatments.isLoading
        ? (
          <SelectField
            name="itemId"
            value={fields.itemId}
            onChange={this.onChange}
            options={treatments.items.map((i) => {return {value: i.id, name: i.name}})}
            classNames={"c-input-field__field"}
            id="itemId"
            />
        ) : (
          <p className="s1 o-loading-indicator">Cargando...</p>
        )
    } else if (fields.itemType === 'Service') {
      itemsSelect = !services.isLoading
        ? (
          <SelectField
            name="itemId"
            value={fields.itemId}
            onChange={this.onChange}
            options={services.items.map((i) => {return {value: i.id, name: i.name}})}
            classNames={"c-input-field__field"}
            id="itemId"
            />
        ) : (
          <p className="s1 o-loading-indicator">Cargando...</p>
        )
    }

    return (
      <div className={`c-catalogue-list__form`}>
        <h2 className="t2">Edita una promoción existente</h2>
        <p className="o-catalogue-description">Por favor modifica los campos deseados con la información de la promoción.</p>
          {
            isLoading
            ? (
              <div className="c-catalogue-list__spinner u-text-center">
                <BeatLoader color={'#FC5185'} loading={true} />
              </div>
            ) : (
              <form className="c-catalogue-form c-form" action="/" method="post">
              {/* SECTION */}
                <div className="c-catalogue-form__section o-grid o-grid-axis-align--start o-grid-cross-align--stretch">
                  <h2 className="o-section-title t1 o-grid__child u-12of12">Información General</h2>

                  <div className="c-input-field [ o-grid__child u-12of12 u-2of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                    <label htmlFor="image" className="c-input-field__label">Foto</label>
                    <FileField
                      name="image"
                      value={fields.image}
                      placeholder="Fotografía"
                      required={true}
                      validate={(value) => value ? false : true}
                      onChange={this.onChange}
                      id="image"/>
                  </div>

                  <div className="c-input-field--small [ o-grid__child u-8of12 u-4of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                    <label htmlFor="name" className="c-input-field__label">Nombre</label>
                    <InputField
                      type="text"
                      name="name"
                      value={fields.name}
                      placeholder="Nombre"
                      required={true}
                      validate={(value) => value.length <= 4}
                      onChange={this.onChange}
                      id="name"
                      classNames="c-input-field__field"
                      />
                  </div>

                  <div className="c-input-field--small [ o-grid__child u-12of12 u-5of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                    <label htmlFor="description" className="c-input-field__label">Descripción</label>
                    <TextareaField
                      name="description"
                      value={fields.description}
                      placeholder="Descripción"
                      required={true}
                      validate={(value) => value.length <= 4}
                      onChange={this.onChange}
                      id="description"
                      classNames="c-input-field__field"
                      />
                  </div>

                  <div className="[ o-grid__child u-12of12 u-4of12@lap ]">
                    <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--around ]">
                      <label htmlFor="validStart" className="c-input-field__label">Valida desde</label>
                      <InputField
                        type="date"
                        name="validStart"
                        value={fields.validStart}
                        placeholder="dd/mm/aaaa"
                        required={true}
                        validate={(value) => value ? false : true}
                        onChange={this.onChange}
                        id="validStart"
                        classNames="c-input-field__field"
                        />
                    </div>
                  </div>

                  <div className="[ o-grid__child u-12of12 u-4of12@lap ]">
                    <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--around ]">
                      <label htmlFor="validEnd" className="c-input-field__label">Valida hasta</label>
                      <InputField
                        type="date"
                        name="validEnd"
                        value={fields.validEnd}
                        placeholder="dd/mm/aaaa"
                        required={true}
                        validate={(value) => value ? false : true}
                        onChange={this.onChange}
                        id="validEnd"
                        classNames="c-input-field__field"
                        />
                    </div>
                  </div>
                </div>

                {/* SECTION */}
                <div className="c-catalogue-form__section o-grid o-grid-axis-align--start o-grid-cross-align--stretch">
                  <h2 className="o-section-title t1 o-grid__child u-12of12">Información la aplicación</h2>

                  <div className="[ o-grid__child u-12of12 u-6of12@lap ]">
                    <div className="c-input-field--small u-margin-c u-margin-m--bottom [ o-grid--vertical o-grid-axis-align--between ]">
                      <label htmlFor="type" className="c-input-field__label">Tipo de promoción</label>
                      <SelectField
                        name="type"
                        value={fields.type}
                        onChange={this.onChange}
                        options={PROMOTION_TYPES.map((i) => {return {value: i.id, name: i.name}})}
                        classNames={"c-input-field__field"}
                        id="type"
                        />
                    </div>
                  </div>

                  <div className="[ o-grid__child u-12of12 u-6of12@lap ]">
                    <div className="c-input-field--small u-margin-c u-margin-m--bottom  [ o-grid--vertical o-grid-axis-align--around ]">
                      <label htmlFor="value" className="c-input-field__label">Valor</label>
                      <InputField
                        type="number"
                        name="value"
                        value={fields.value}
                        min={0}
                        placeholder="0.00"
                        required={true}
                        validate={(value) => value <= 0}
                        onChange={this.onChange}
                        id="value"
                        classNames="c-input-field__field"
                        />
                    </div>
                  </div>

                  <div className="[ o-grid__child u-12of12 u-6of12@lap ]">
                    <div className="c-input-field--small u-margin-c u-margin-m--bottom  [ o-grid--vertical o-grid-axis-align--between ]">
                      <label htmlFor="itemType" className="c-input-field__label">Tipo de item al que se aplica</label>
                      <SelectField
                        name="itemType"
                        value={fields.itemType}
                        onChange={this.onChange}
                        options={[{value: 'Product', name: 'Producto'}, {value: 'Treatment', name: 'Tratamientos'}, {value: 'Service', name: 'Servicio'}]}
                        classNames={"c-input-field__field"}
                        id="itemType"
                        />
                    </div>
                  </div>

                  <div className="[ o-grid__child u-12of12 u-6of12@lap ]">
                    <div className="c-input-field--small u-margin-c  [ o-grid--vertical o-grid-axis-align--between ]">
                      <label htmlFor="itemId" className="c-input-field__label">Item al que se aplica</label>
                      { itemsSelect }
                    </div>
                  </div>

                </div>
                <div className="c-buttons-field">
                  <button className="c-buttons-field__button" onClick={() => route.history.goBack()}>Cancelar</button>
                  <button className="c-buttons-field__button" onClick={() => handleDelete(fields.id, this.handleDeleteSuccess)}>Desactivar</button>
                  <button className="c-buttons-field__button--primary" onClick={(e) => this.handlePost(e)} type="submit">Editar</button>
                </div>
              </form>
            )
          }
      </div>
    );
  }
}

EditPromotionsForm.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  promotion: PropTypes.object.isRequired,
  products: PropTypes.object.isRequired,
  treatments: PropTypes.object.isRequired,
  services: PropTypes.object.isRequired,
  requestDataReload: PropTypes.func,
  requestProducts: PropTypes.func.isRequired,
  requestTreatments: PropTypes.func.isRequired,
  requestServices: PropTypes.func.isRequired,
  handleGet: PropTypes.func.isRequired,
  handlePost: PropTypes.func.isRequired,
  handleDelete: PropTypes.func.isRequired,
  route: PropTypes.object.isRequired
};

export default EditPromotionsForm;
