import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { BeatLoader } from 'react-spinners';
import { Route, NavLink } from 'react-router-dom';
import { API_URL } from '../../../constants';
import NewSuppliesForm from './SuppliesForm/new.form';
import EditSuppliesForm from './SuppliesForm/edit.form';
import { toMoney } from '../../../helpers/money-format';

class SuppliesList extends Component {

  componentWillMount() {
    this.props.api.fetch();
  }

  render() {
    const { supplies, providers, api, requestProviders, route } = this.props;

    return (
      <div className="c-catalogue-list">
        <div className="c-catalogue-list__header">
          <h1 className="t3">Listado de Insumos</h1>
          <p className="o-catalogue-description">Consulta, edita y crea insumos disponibles.</p>
          <div className="c-catalogue-list__filters">
            <NavLink to="/inventario/insumos/nuevo" className="o-filter">Agregar nuevo insumo</NavLink>
          </div>
        </div>

        {/* NEW SUPPLY FORM */}
        <Route path="/inventario/insumos/nuevo" render={(match) => (
          <NewSuppliesForm
            providers={providers}
            handlePost={api.post}
            requestDataReload={api.fetch}
            requestProviders={requestProviders}
            route={match} />
        )}/>


        {/* EDIT SUPPLY FORM */}
        <Route path="/inventario/insumos/editar/:id" render={(match) => (
          <EditSuppliesForm
            isLoading={supplies.isLoading}
            supply={supplies.item}
            providers={providers}
            handleGet={api.get}
            handlePost={api.post}
            handleDelete={api.delete}
            requestDataReload={api.fetch}
            requestProviders={requestProviders}
            route={match} />
        )}/>

        {/* SUPPLIES LIST */}
        <Route exact path="/inventario/insumos" render={() => (
          <div className="c-catalogue-list__body">
            {
              supplies.isLoading
              ? <BeatLoader className="c-catalogue-list__spinner" color={'#ffffff'} loading={true} />
              : (
                  <div className="c-items-list o-grid o-grid-cross-align--stretch">
                    {
                      supplies.items.map((i) => (
                        <div className="[ o-grid__child u-12of12 u-6of12@tab u-4of12@lap ]" key={i.id}>
                          <div className="c-item o-grid o-grid-axis-align--between o-grid-cross-align--center">
                            <div className="o-grid__child u-margin-m--bottom">
                              <img className="c-item__photo" src={`${API_URL}/${i.image}`} alt=""/>
                            </div>
                            <div className="c-item__data [ o-grid__child u-12of12 ]">
                              <h2 className="o-item-name t2 u-text-truncate u-margin-c"><NavLink to={`${route.match.url}/editar/${i.id}`}>{i.name}</NavLink></h2>
                              <p className="o-item-description u-margin-s--bottom u-text-left">{i.description}</p>
                              <div className="c-item__numbers [ o-grid o-grid-axis-align--between o-grid-cross-align--center o-grid-wrap--no ]">
                                <span className="o-item-number">En stock: <strong>{i.stock}</strong></span>
                                <span className="o-item-number u-text-right">Costo: <strong>{toMoney(i.cost)}</strong></span>
                              </div>
                            </div>
                          </div>
                        </div>
                      ))
                    }
                  </div>
                )
            }
          </div>
        )}/>
      </div>
    );
  }
}

SuppliesList.propTypes = {
  supplies: PropTypes.object.isRequired,
  providers: PropTypes.object.isRequired,
  api: PropTypes.object.isRequired,
  requestProviders: PropTypes.func.isRequired,
  route: PropTypes.object.isRequired
};

export default SuppliesList;
