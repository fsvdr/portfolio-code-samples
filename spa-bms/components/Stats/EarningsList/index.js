import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { BeatLoader } from 'react-spinners';
import { Route, NavLink } from 'react-router-dom';
import EarningsForm from './EarningsForm';
import { toMoney } from '../../../helpers/money-format';

class EarningsList extends Component {

  componentWillMount() {
    this.props.api.fetch();
  }

  render() {
    const { earnings, api } = this.props;

    return (
      <div className="c-catalogue-list">
        <div className="c-catalogue-list__header">
          <h1 className="t3">Contabilidad de ganancias</h1>
          <p className="o-catalogue-description">Consulta la contabilidad de las ganancias del negocio. También puedes restablecer las ganancias.</p>
          <div className="c-catalogue-list__filters">
            <NavLink to="/contabilidad/ganancias/editar" className="o-filter">Reestablecer las ganancias</NavLink>
          </div>
        </div>


        {/* EDIT EARNINGS FORM */}
        <Route path="/contabilidad/ganancias/editar/" render={(match) => (
          <EarningsForm
            earnings={earnings.item}
            requestDataReload={api.fetch}
            handlePost={api.post}
            route={match} />
        )}/>

        {/* EARNINGS LIST */}
        <Route exact path="/contabilidad/ganancias" render={() => (
          <div className="c-catalogue-list__body">
            {
              earnings.isLoading
              ? <BeatLoader className="c-catalogue-list__spinner" color={'#ffffff'} loading={true} />
              : (
                  <div className="c-items-list o-grid o-grid-cross-align--stretch">
                    <div className="[ o-grid__child u-12of12 u-6of12@tab u-4of12@lap ]">
                      <div className="c-item c-number-stat o-grid o-grid-axis-align--center o-grid-cross-align--center">
                        <div className="o-grid__child o-grid-self-align--center u-margin-l--bottom">
                          <span className="o-big-number c-number-stat__number">{earnings.item.money ? toMoney(earnings.item.money) : toMoney(0)}</span>
                        </div>
                        <div className="c-item__data [ o-grid__child u-12of12 ]">
                          <h2 className="o-item-name c-number-stat__name t2 u-text-truncate u-margin-c">Ganancias Totales</h2>
                        </div>
                      </div>
                    </div>

                    <div className="[ o-grid__child u-12of12 u-6of12@tab u-4of12@lap ]">
                      <div className="c-item c-number-stat o-grid o-grid-axis-align--center o-grid-cross-align--center">
                        <div className="o-grid__child o-grid-self-align--center u-margin-l--bottom">
                          <span className="o-big-number c-number-stat__number">{earnings.item.real ? toMoney(earnings.item.real) : toMoney(0)}</span>
                        </div>
                        <div className="c-item__data [ o-grid__child u-12of12 ]">
                          <h2 className="o-item-name c-number-stat__name t2 u-text-truncate u-margin-c">Ganancias Reales</h2>
                        </div>
                      </div>
                    </div>

                    <div className="[ o-grid__child u-12of12 u-6of12@tab u-4of12@lap ]">
                      <div className="c-item c-number-stat o-grid o-grid-axis-align--center o-grid-cross-align--center">
                        <div className="o-grid__child o-grid-self-align--center u-margin-l--bottom">
                          <span className="o-big-number c-number-stat__number">{earnings.item.pending ? toMoney(earnings.item.pending) : toMoney(0)}</span>
                        </div>
                        <div className="c-item__data [ o-grid__child u-12of12 ]">
                          <h2 className="o-item-name c-number-stat__name t2 u-text-truncate u-margin-c">Ganancias Pendientes</h2>
                        </div>
                      </div>
                    </div>
                  </div>
                )
            }
          </div>
        )}/>
      </div>
    );
  }
}

EarningsList.propTypes = {
  earnings: PropTypes.object.isRequired,
  api: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired
};

export default EarningsList;
