import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import InputField from '../../../Forms/InputField';

class EarningsForm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      fields: {
        id: this.props.earnings.id,
        money: this.props.earnings.money,
        real: this.props.earnings.real,
        pending: this.props.earnings.pending
      },
      errors: {}
    };

    this.onChange = this.onChange.bind(this);
    this.handlePost = this.handlePost.bind(this);
    this.handlePostSuccess = this.handlePostSuccess.bind(this);
  }

  componentWillReceiveProps(update) {
    if (update.earnings.id) {
      const fields = {
        id: update.earnings.id,
        money: update.earnings.money,
        real: update.earnings.real,
        pending: update.earnings.pending
      };

      const errors = {}

      this.setState({fields, errors});
    }
  }

  componentWillUnmount() {
    const fields = {
      id: '',
      money: '',
      real: '',
      pending: ''
    }

    const errors = {};

    this.setState({fields, errors});
  }

  onChange(input) {
    const fields = this.state.fields;
    const errors = this.state.errors;

    fields[input.name] = input.value;
    errors[input.name] = input.error;

    this.setState({fields, errors});
  }

  handlePost(event) {
    event.preventDefault();
    const { handlePost } = this.props;
    const { fields, errors } = this.state;
    const errorMessages = Object.keys(errors).filter((k) => errors[k]);

    // TODO: Check for individual required fields
    if (errorMessages.length === 0) handlePost(fields, this.handlePostSuccess);
  }

  handlePostSuccess() {
    this.props.handleGet(this.state.fields.id);
    this.props.requestDataReload();
  }

  render() {
    const { route } = this.props;
    const { fields } = this.state;

    return (
      <div className={`c-catalogue-list__form`}>
      <h2 className="t2">Restablece las ganancias</h2>
      <p className="o-catalogue-description">Modifica los campos de acuerdo a los datos reales.</p>
        <form className="c-catalogue-form c-form">
        {/* SECTION */}
          <div className="c-catalogue-form__section o-grid o-grid-axis-align--start o-grid-cross-align--stretch">
            <h2 className="o-section-title t1 o-grid__child u-12of12">Información General</h2>

            <div className="[ o-grid__child u-12of12 u-4of12@lap ]">
              <div className="c-input-field--small u-margin-c u-margin-m--bottom [ o-grid--vertical o-grid-axis-align--around ]">
                <label htmlFor="money" className="c-input-field__label">Ganancias Totales</label>
                <InputField
                  type="number"
                  name="money"
                  value={fields.money}
                  min={0}
                  placeholder="0.00"
                  required={true}
                  validate={(value) => value <= 0}
                  onChange={this.onChange}
                  id="money"
                  classNames="c-input-field__field"
                  />
              </div>
            </div>

            <div className="[ o-grid__child u-12of12 u-4of12@lap ]">
              <div className="c-input-field--small u-margin-c u-margin-m--bottom [ o-grid--vertical o-grid-axis-align--around ]">
                <label htmlFor="real" className="c-input-field__label">Ganancias Reales</label>
                <InputField
                  type="number"
                  name="real"
                  value={fields.real}
                  min={0}
                  placeholder="0.00"
                  required={true}
                  validate={(value) => value <= 0}
                  onChange={this.onChange}
                  id="real"
                  classNames="c-input-field__field"
                  />
              </div>
            </div>

            <div className="[ o-grid__child u-12of12 u-4of12@lap ]">
              <div className="c-input-field--small u-margin-c u-margin-m--bottom [ o-grid--vertical o-grid-axis-align--around ]">
                <label htmlFor="pending" className="c-input-field__label">Ganancias Pendientes</label>
                <InputField
                  type="number"
                  name="pending"
                  value={fields.pending}
                  min={0}
                  placeholder="0.00"
                  required={true}
                  validate={(value) => value <= 0}
                  onChange={this.onChange}
                  id="pending"
                  classNames="c-input-field__field"
                  />
              </div>
            </div>

          </div>

          <div className="c-buttons-field">
            <button className="c-buttons-field__button" onClick={() => route.history.goBack()}>Cancelar</button>
            <button className="c-buttons-field__button--primary" onClick={(e) => this.handlePost(e)} type="submit">Restablecer</button>
          </div>
        </form>
      </div>
    );
  }
}

EarningsForm.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  earnings: PropTypes.object.isRequired,
  requestDataReload: PropTypes.func,
  handlePost: PropTypes.func.isRequired,
  route: PropTypes.object.isRequired
};

export default EarningsForm;
