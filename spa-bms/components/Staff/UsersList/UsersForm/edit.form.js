import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import InputField from '../../../Forms/InputField';
import SelectField from '../../../Forms/SelectField';
import { ACCESS_CLEARANCE } from '../../../../constants';
import { BeatLoader } from 'react-spinners';

class EditUsersForm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      fields: {
        id: this.props.user.id,
        email: this.props.user.email,
        clearance: this.props.user.clearance,
        staff: this.props.user.staff ? this.props.user.staff.id : 0
      },
      errors: {},
    };

    this.onChange = this.onChange.bind(this);
    this.handlePost = this.handlePost.bind(this);
    this.handlePostSuccess = this.handlePostSuccess.bind(this);
    this.handleDeleteSuccess = this.handleDeleteSuccess.bind(this);
  }

  componentWillMount() {
    const id = this.props.route.match.params.id;
    this.props.handleGet(id);
    this.props.requestStaffs();
  }

  componentWillReceiveProps(update) {
    if (update.user.id) {
      const fields = {
        id: update.user.id,
        email: update.user.email,
        clearance: update.user.clearance,
        staff: update.user.staff.id
      };

      const errors = {};

      this.setState({fields, errors});
    }
  }

  componentWillUnMount() {
    const fields = {
      id: '',
      email: '',
      clearance: '',
      staff: ''
    };

    const errors = {};

    this.setState({fields, errors});
  }

  onChange(input) {
    const fields = this.state.fields;
    const errors = this.state.errors;

    fields[input.name] = input.value;
    errors[input.name] = input.error;

    this.setState({fields, errors});
  }

  handlePost(event) {
    event.preventDefault();

    const { handlePost } = this.props;
    const { fields, errors } = this.state;
    const errorMessages = Object.keys(errors).filter((k) => errors[k]);

    // TODO: Check for individual required fields
    if (errorMessages.length === 0) handlePost(fields, this.handlePostSuccess);
  }

  handlePostSuccess() {
    this.props.requestDataReload();
    this.props.route.history.goBack();
  }

  handleDeleteSuccess() {
    this.props.requestDataReload();
    this.props.route.history.goBack()
  }

  render() {
    const { isLoading, staffs, handleDelete, route } = this.props;
    const { fields } = this.state;

    return (
      <div className={`c-catalogue-list__form`}>
        <h2 className="t2">Edita un usuario existente</h2>
        <p className="o-catalogue-description">Modifica los campos deseados del usuario seleccionado.</p>

          {
            isLoading
            ? (
              <div className="c-catalogue-list__spinner u-text-center">
                <BeatLoader color={'#FC5185'} loading={true} />
              </div>
            ) : (
              <form className="c-catalogue-form c-form" action="/" method="post">
              {/* SECTION */}
                <div className="c-catalogue-form__section o-grid o-grid-axis-align--start o-grid-cross-align--stretch">
                  <h2 className="o-section-title t1 o-grid__child u-12of12">Credenciales</h2>

                  <div className="[ o-grid__child u-8of12 u-6of12@lap ]">
                    <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--around ]">
                      <label htmlFor="email" className="c-input-field__label">Correo electrónico</label>
                      <InputField
                        type="email"
                        name="email"
                        value={fields.email}
                        placeholder="Correo electrónico"
                        required={true}
                        validate={(value) => value.length <= 4}
                        onChange={this.onChange}
                        id="email"
                        classNames="c-input-field__field"
                        />
                    </div>
                  </div>

                </div>

                {/* SECTION */}
                <div className="c-catalogue-form__section o-grid o-grid-axis-align--start o-grid-cross-align--stretch">
                  <h2 className="o-section-title t1 o-grid__child u-12of12">Información del acceso</h2>

                  <div className="c-input-field--small [ o-grid__child u-12of12 u-4of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                    <label htmlFor="clearance" className="c-input-field__label">Nivel de acceso</label>
                    <SelectField
                      name="clearance"
                      value={fields.clearance}
                      onChange={this.onChange}
                      options={ACCESS_CLEARANCE.map((i) => {return {value: i.id, name: i.name}})}
                      classNames={"c-input-field__field o-grid__child u-8of12"}
                      id="clearance"
                      />
                  </div>

                  <div className="c-input-field--small [ o-grid__child u-12of12 u-4of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                    <label htmlFor="staff" className="c-input-field__label">Empleado asignado</label>
                    {
                      !staffs.isLoading
                      ? <SelectField
                        name="staff"
                        options={staffs.items.map((p) => {return {value: p.id, name: p.name}})}
                        value={fields.staff}
                        onChange={this.onChange}
                        required={true}
                        classNames={"c-input-field__field"}
                        id="staff"
                        />
                      : <p className="s1 o-loading-indicator">Cargando...</p>
                    }
                  </div>

                </div>
                <div className="c-buttons-field">
                  <button className="c-buttons-field__button" onClick={() => route.history.goBack()}>Cancelar</button>
                  <button className="c-buttons-field__button" onClick={() => handleDelete(fields.id, this.handleDeleteSuccess)}>Desactivar</button>
                  <button className="c-buttons-field__button--primary" onClick={(e) => this.handlePost(e)} type="submit">Editar</button>
                </div>
              </form>
            )
          }
      </div>
    );
  }
}

EditUsersForm.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  user: PropTypes.object.isRequired,
  staffs: PropTypes.object.isRequired,
  handleGet: PropTypes.func.isRequired,
  handlePost: PropTypes.func.isRequired,
  handleDelete: PropTypes.func.isRequired,
  requestDataReload: PropTypes.func,
  requestStaffs: PropTypes.func,
  route: PropTypes.object.isRequired
};

export default EditUsersForm;
