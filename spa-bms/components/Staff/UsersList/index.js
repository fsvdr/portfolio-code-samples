import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { BeatLoader } from 'react-spinners';
import { Route, NavLink } from 'react-router-dom';
import NewUsersForm from './UsersForm/new.form';
import EditUsersForm from './UsersForm/edit.form';
import { ACCESS_CLEARANCE } from '../../../constants';

const mapClearance = (id) => {
  const match = ACCESS_CLEARANCE.find((i) => i.id == id); // eslint-disable-line
  return match.name;
}

class UsersList extends Component {

  componentWillMount() {
    this.props.api.fetch();
  }

  render() {
    const { users, staffs, api, requestStaffs, route } = this.props;

    return (
      <div className="c-catalogue-list">
        <div className="c-catalogue-list__header">
          <h1 className="t3">Listado de Usuarios</h1>
          <p className="o-catalogue-description">Consulta, edita y agrega usuarios del sistema.</p>
          <div className="c-catalogue-list__filters">
            <NavLink to="/personal/usuarios/nuevo" className="o-filter">Agregar nuevo usuario</NavLink>
          </div>
        </div>

        {/* NEW PROVIDER FORM */}
        <Route path="/personal/usuarios/nuevo" render={(match) => (
          <NewUsersForm
            staffs={staffs}
            handlePost={api.post}
            requestDataReload={api.fetch}
            requestStaffs={requestStaffs}
            route={match} />
        )}/>

        {/* EDIT PROVIDER FORM */}
        <Route path="/personal/usuarios/editar/:id" render={(match) => (
          <EditUsersForm
            isLoading={users.isLoading}
            user={users.item}
            staffs={staffs}
            handleGet={api.get}
            handlePost={api.post}
            handleDelete={api.delete}
            requestDataReload={api.fetch}
            requestStaffs={requestStaffs}
            route={match} />
        )}/>

        {/* PROVIDERS LIST */}
        <Route exact path="/personal/usuarios" render={() => (
          <div className="c-catalogue-list__body">
            {
              users.isLoading
              ? <BeatLoader className="c-catalogue-list__spinner" color={'#ffffff'} loading={true} />
              : (
                  <div className="c-items-list o-grid o-grid-cross-align--stretch">
                    {
                      users.items.map((i) => (
                        <div className="[ o-grid__child u-12of12 u-6of12@tab u-4of12@lap ]" key={i.id}>
                          <div className="c-item o-grid o-grid-axis-align--between o-grid-cross-align--center">
                            <div className="c-item__data [ o-grid__child u-12of12 ]">
                              <h2 className="o-item-name t2 u-text-truncate u-margin-c"><NavLink to={`${route.match.url}/editar/${i.id}`}>{i.email}</NavLink></h2>
                              <p className="o-item-description u-margin-s--bottom u-text-left">{i.staff.name}</p>
                              <div className="c-item__numbers u-margin-l--top [ o-grid o-grid-axis-align--between o-grid-cross-align--center o-grid-wrap--no ]">
                                <span className="o-item-number">Acceso: <strong>{mapClearance(i.clearance)}</strong></span>
                              </div>
                            </div>
                          </div>
                        </div>
                      ))
                    }
                  </div>
                )
            }
          </div>
        )}/>
      </div>
    );
  }
}

UsersList.propTypes = {
  users: PropTypes.object.isRequired,
  staffs: PropTypes.object.isRequired,
  api: PropTypes.object.isRequired,
  requestStaffs: PropTypes.func.isRequired,
  route: PropTypes.object.isRequired
};

export default UsersList;
