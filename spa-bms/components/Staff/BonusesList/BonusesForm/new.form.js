import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import InputField from '../../../Forms/InputField';
import SelectField from '../../../Forms/SelectField';
import { FREQUENCIES } from '../../../../constants';

class NewBonusesForm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      fields: {
        concept: '',
        frequency: '',
        quantity: ''
      },
      errors: {},
    };

    this.onChange = this.onChange.bind(this);
    this.handlePost = this.handlePost.bind(this);
    this.handlePostSuccess = this.handlePostSuccess.bind(this);
  }

  componentWillUnMount() {
    const fields = {
      concept: '',
      frequency: '',
      quantity: ''
    };

    const errors = {};

    this.setState({fields, errors});
  }

  onChange(input) {
    const fields = this.state.fields;
    const errors = this.state.errors;

    fields[input.name] = input.value;
    errors[input.name] = input.error;

    this.setState({fields, errors});
  }

  handlePost(event) {
    event.preventDefault();

    const { handlePost } = this.props;
    const { fields, errors } = this.state;
    const errorMessages = Object.keys(errors).filter((k) => errors[k]);

    // TODO: Check for individual required fields
    if (errorMessages.length === 0) handlePost(fields, this.handlePostSuccess);
  }

  handlePostSuccess() {
    this.props.requestDataReload();
    this.props.route.history.goBack();
  }

  render() {
    const { route } = this.props;
    const { fields } = this.state;

    return (
      <div className={`c-catalogue-list__form`}>
        <h2 className="t2">Agrega una nueva prestación</h2>
        <p className="o-catalogue-description">Por favor llena los siguientes campos con los datos de la prestación a agregar.</p>
          <form className="c-catalogue-form c-form" action="/" method="post">
          {/* SECTION */}
            <div className="c-catalogue-form__section o-grid o-grid-axis-align--start o-grid-cross-align--stretch">
              <h2 className="o-section-title t1 o-grid__child u-12of12">Datos de la prestación</h2>

              <div className="[ o-grid__child u-8of12 u-6of12@lap ]">
                <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--around ]">
                  <label htmlFor="concept" className="c-input-field__label">Concepto</label>
                  <InputField
                    type="text"
                    name="concept"
                    value={fields.concept}
                    placeholder="Concepto"
                    required={true}
                    validate={(value) => value.length <= 4}
                    onChange={this.onChange}
                    id="concept"
                    classNames="c-input-field__field"
                    />
                </div>
              </div>

              <div className="c-input-field--small [ o-grid__child u-12of12 u-4of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                <label htmlFor="frequency" className="c-input-field__label">Frecuencia</label>
                <SelectField
                  name="frequency"
                  value={fields.frequency}
                  onChange={this.onChange}
                  options={FREQUENCIES.map((i) => {return {value: i.id, name: i.name}})}
                  classNames={"c-input-field__field o-grid__child u-8of12"}
                  id="frequency"
                  />
              </div>

              <div className="c-input-field--small [ o-grid__child u-4of12 ] [ o-grid--vertical o-grid-axis-align--around ]">
                <label htmlFor="quantity" className="c-input-field__label">Cantidad</label>
                <InputField
                  type="number"
                  name="quantity"
                  value={fields.quantity}
                  min={0}
                  placeholder="0.00"
                  required={true}
                  validate={(value) => value <= 0}
                  onChange={this.onChange}
                  id="quantity"
                  classNames="c-input-field__field"
                  />
              </div>

            </div>

            <div className="c-buttons-field">
              <button className="c-buttons-field__button" onClick={() => route.history.goBack()}>Cancelar</button>
              <button className="c-buttons-field__button--primary" onClick={(e) => this.handlePost(e)} type="submit">Agregar</button>
            </div>
          </form>
      </div>
    );
  }
}

NewBonusesForm.propTypes = {
  bonus: PropTypes.object.isRequired,
  handlePost: PropTypes.func.isRequired,
  requestDataReload: PropTypes.func,
  route: PropTypes.object.isRequired
};

export default NewBonusesForm;
