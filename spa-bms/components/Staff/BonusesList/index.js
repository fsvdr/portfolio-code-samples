import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { BeatLoader } from 'react-spinners';
import { Route, NavLink } from 'react-router-dom';
import NewBonusesForm from './BonusesForm/new.form';
import EditBonusesForm from './BonusesForm/edit.form';
import { toMoney } from '../../../helpers/money-format';

class BonusesList extends Component {

  componentWillMount() {
    this.props.api.fetch();
  }

  render() {
    const { bonuses, api, route } = this.props;

    return (
      <div className="c-catalogue-list">
        <div className="c-catalogue-list__header">
          <h1 className="t3">Listado de Prestaciones</h1>
          <p className="o-catalogue-description">Consulta, edita y agrega prestaciones.</p>
          <div className="c-catalogue-list__filters">
            <NavLink to="/personal/prestaciones/nueva" className="o-filter">Agregar nueva prestación</NavLink>
          </div>
        </div>

        {/* NEW BONUS FORM */}
        <Route path="/personal/prestaciones/nueva" render={(match) => (
          <NewBonusesForm
            handlePost={api.post}
            requestDataReload={api.fetch}
            route={match} />
        )}/>

        {/* EDIT BONUS FORM */}
        <Route path="/personal/prestaciones/editar/:id" render={(match) => (
          <EditBonusesForm
            isLoading={bonuses.isLoading}
            bonus={bonuses.item}
            handleGet={api.get}
            handlePost={api.post}
            handleDelete={api.delete}
            requestDataReload={api.fetch}
            route={match} />
        )}/>

        {/* BONUSES LIST */}
        <Route exact path="/personal/prestaciones" render={() => (
          <div className="c-catalogue-list__body">
            {
              bonuses.isLoading
              ? <BeatLoader className="c-catalogue-list__spinner" color={'#ffffff'} loading={true} />
              : (
                  <div className="c-items-list o-grid o-grid-cross-align--stretch">
                    {
                      bonuses.items.map((i) => (
                        <div className="[ o-grid__child u-12of12 u-6of12@tab u-4of12@lap ]" key={i.id}>
                          <div className="c-item o-grid o-grid-axis-align--between o-grid-cross-align--center">
                            <div className="c-item__data [ o-grid__child u-12of12 ]">
                              <h2 className="o-item-name t2 u-text-truncate u-margin-c"><NavLink to={`${route.match.url}/editar/${i.id}`}>{i.concept}</NavLink></h2>
                              <div className="c-item__numbers u-margin-l--top [ o-grid o-grid-axis-align--between o-grid-cross-align--center o-grid-wrap--no ]">
                                <span className="o-item-number">Cantidad: <strong>{toMoney(i.quantity)}</strong></span>
                              </div>
                            </div>
                          </div>
                        </div>
                      ))
                    }
                  </div>
                )
            }
          </div>
        )}/>
      </div>
    );
  }
}

BonusesList.propTypes = {
  bonuses: PropTypes.object.isRequired,
  api: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired
};

export default BonusesList;
