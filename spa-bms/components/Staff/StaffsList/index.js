import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { BeatLoader } from 'react-spinners';
import { Route, NavLink } from 'react-router-dom';
import NewStaffsForm from './StaffsForm/new.form';
import EditStaffsForm from './StaffsForm/edit.form';
import { toMoney } from '../../../helpers/money-format';

class StaffsList extends Component {

  componentWillMount() {
    this.props.api.fetch();
  }

  render() {
    const { staffs, bonuses, api, requestBonuses, route } = this.props;

    return (
      <div className="c-catalogue-list">
        <div className="c-catalogue-list__header">
          <h1 className="t3">Listado de Empleados</h1>
          <p className="o-catalogue-description">Consulta, edita y agrega empleados.</p>
          <div className="c-catalogue-list__filters">
            <NavLink to="/personal/empleados/nuevo" className="o-filter">Agregar nuevo empleado</NavLink>
          </div>
        </div>

        {/* NEW STAFF FORM */}
        <Route path="/personal/empleados/nuevo" render={(match) => (
          <NewStaffsForm
            bonuses={bonuses}
            handlePost={api.post}
            requestDataReload={api.fetch}
            requestBonuses={requestBonuses}
            route={match} />
        )}/>

        {/* EDIT STAFF FORM */}
        <Route path="/personal/empleados/editar/:id" render={(match) => (
          <EditStaffsForm
            isLoading={staffs.isLoading}
            staff={staffs.item}
            bonuses={bonuses}
            handleGet={api.get}
            handlePost={api.post}
            handleDelete={api.delete}
            requestDataReload={api.fetch}
            requestBonuses={requestBonuses}
            route={match} />
        )}/>

        {/* STAFFS LIST */}
        <Route exact path="/personal/empleados" render={() => (
          <div className="c-catalogue-list__body">
            {
              staffs.isLoading
              ? <BeatLoader className="c-catalogue-list__spinner" color={'#ffffff'} loading={true} />
              : (
                  <div className="c-items-list o-grid o-grid-cross-align--stretch">
                    {
                      staffs.items.map((i) => (
                        <div className="[ o-grid__child u-12of12 u-6of12@tab u-4of12@lap ]" key={i.id}>
                          <div className="c-item o-grid o-grid-axis-align--between o-grid-cross-align--center">
                            <div className="o-grid__child u-margin-m--bottom">
                              {i.phone ? (<a href={`tel:${i.phone}`} className="o-primary-action"><i className="o-icon o-icon-phone--small"></i></a>) : null}
                            </div>
                            <div className="o-grid__child u-margin-m--bottom">
                              {i.email ? (<a href={`mailto:${i.email}`} className="o-primary-action"><i className="o-icon o-icon-link--small"></i></a>) : null}
                            </div>
                            <div className="c-item__data [ o-grid__child u-12of12 ]">
                              <h2 className="o-item-name t2 u-text-truncate u-margin-c"><NavLink to={`${route.match.url}/editar/${i.id}`}>{i.name}</NavLink></h2>
                              <p className="o-item-description u-margin-s--bottom u-text-left">{i.title}</p>
                              <div className="c-item__numbers [ o-grid o-grid-axis-align--between o-grid-cross-align--center o-grid-wrap--no ]">
                                <span className="o-item-number">Salario: <strong>{toMoney(i.salary)}</strong></span>
                              </div>
                            </div>
                          </div>
                        </div>
                      ))
                    }
                  </div>
                )
            }
          </div>
        )}/>
      </div>
    );
  }
}

StaffsList.propTypes = {
  staffs: PropTypes.object.isRequired,
  bonuses: PropTypes.object.isRequired,
  api: PropTypes.object.isRequired,
  requestBonuses: PropTypes.func.isRequired,
  route: PropTypes.object.isRequired
};

export default StaffsList;
