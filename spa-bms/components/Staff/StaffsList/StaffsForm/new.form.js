import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import InputField from '../../../Forms/InputField';
import TextareaField from '../../../Forms/TextareaField';
import SelectField from '../../../Forms/SelectField';

class NewStaffsForm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      fields: {
        name: '',
        salary: '',
        address: '',
        phone: '',
        cellphone: '',
        rfc: '',
        imss: '',
        curp: '',
        email: '',
        title: '',
        objective: '',
        objectiveBonus: '',
        bonus: []
      },
      errors: {},
      controls: {
        bonus: this.props.bonuses.items.length ? this.props.bonuses.items[0].id : 0,
        bonusQuantity: ''
      }
    };

    this.onChange = this.onChange.bind(this);
    this.onControlChange = this.onControlChange.bind(this);
    this.handlePost = this.handlePost.bind(this);
    this.handlePostSuccess = this.handlePostSuccess.bind(this);
    this.handleBonuses = this.handleBonuses.bind(this);
    this.handleBonusesRemoval = this.handleBonusesRemoval.bind(this);
  }

  componentWillMount() {
    this.props.requestBonuses();
  }

  componentWillUnMount() {
    const fields = {
      name: '',
      salary: '',
      address: '',
      phone: '',
      cellphone: '',
      rfc: '',
      imss: '',
      curp: '',
      email: '',
      title: '',
      objective: '',
      objectiveBonus: '',
      bonus: []
    };

    const errors = {};

    this.setState({fields, errors});
  }

  onChange(input) {
    const fields = this.state.fields;
    const errors = this.state.errors;

    fields[input.name] = input.value;
    errors[input.name] = input.error;

    this.setState({fields, errors});
  }

  onControlChange(input) {
    const controls = this.state.controls;

    controls[input.name] = input.value;

    this.setState({controls});
  }

  handlePost(event) {
    event.preventDefault();

    const { handlePost } = this.props;
    const { fields, errors } = this.state;
    const errorMessages = Object.keys(errors).filter((k) => errors[k]);

    // TODO: Check for individual required fields
    if (errorMessages.length === 0) handlePost(fields, this.handlePostSuccess);
  }

  handlePostSuccess() {
    this.props.requestDataReload();
    this.props.route.history.goBack();
  }

  handleBonuses(event) {
    event.preventDefault();

    const controls = this.state.controls;
    const id = controls.bonus;
    const quantity = controls.bonusQuantity;

    if (id !== '') {
      let fields = this.state.fields;
      let bonusField = fields['bonus'];

      // Check if selected bonus is already in fields
      // if it is, ignore
      const matchIndex = bonusField.findIndex((i) => i.bonus == id); // eslint-disable-line
      if (matchIndex >= 0) return;

      // If selection is not in fields, add it
      const { bonuses } = this.props;
      const bonus = bonuses.items.find((i) => i.id == id); // eslint-disable-line
      bonusField.push({bonus: id, quantity: quantity ? quantity : bonus.quantity})

      fields['bonus'] = bonusField;
      this.setState({fields});
    }
  }

  handleBonusesRemoval(event, id) {
    event.preventDefault();

    let fields = this.state.fields;
    let bonusField = fields['bonus'];

    const matchId = bonusField.findIndex((p) => p.bonus == id); // eslint-disable-line
    if (matchId < 0) return;

    const bonus = [...bonusField.slice(0, matchId), ...bonusField.slice(matchId + 1)];

    fields['bonus'] = bonus;
    this.setState({fields});
  }

  render() {
    const { bonuses, route } = this.props;
    const { fields, controls } = this.state;

    const bonusesControl = !bonuses.isLoading
      ? (
        <div className="c-control [ o-grid__child u-12of12 ] [ o-grid o-grid-axis-align--between o-grid-cross-align--center o-grid-wrap--no ]">
            <div className="[ o-grid__child u-10of12 ] [ o-grid o-grid-axis-align--between o-grid-cross-align--center ]">
              <div className="o-grid__child u-12of12 u-8of12@lap u-margin-s--bottom">
                <SelectField
                  name="bonus"
                  value={controls.bonus}
                  onChange={this.onControlChange}
                  options={bonuses.items.map((b) => {return {value: b.id, name: b.concept}})}
                  classNames={"c-input-field__field"}
                  id="bonus"
                  />
              </div>

              <div className="o-grid__child u-4of12 u-4of12@lap u-margin-s--bottom">
                <InputField
                  type="number"
                  name="bonusQuantity"
                  value={controls.bonusQuantity}
                  min={0}
                  placeholder="0.00"
                  required={true}
                  validate={(value) => value <= 0}
                  onChange={this.onControlChange}
                  id="bonusQuantity"
                  classNames="c-input-field__field"
                  />
              </div>
            </div>

            <button
              onClick={(e) => this.handleBonuses(e)}
              className="c-control__btn o-primary-action o-grid__child u-margin-l--left">
              <i className="o-icon o-icon-tick--small"></i>
            </button>
          </div>
      ) : <p className="s1 o-loading-indicator">Cargando...</p>;

    return (
      <div className={`c-catalogue-list__form`}>
        <h2 className="t2">Agrega un nuevo empleado</h2>
        <p className="o-catalogue-description">Por favor llena los siguientes campos con la información del empleado a agregar.</p>
          <form className="c-catalogue-form c-form" action="/" method="post">
          {/* SECTION */}
            <div className="c-catalogue-form__section o-grid o-grid-axis-align--start o-grid-cross-align--stretch">
              <h2 className="o-section-title t1 o-grid__child u-12of12">Información General</h2>

              <div className="[ o-grid__child u-8of12 u-6of12@lap ]">
                <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--around ]">
                  <label htmlFor="name" className="c-input-field__label">Nombre</label>
                  <InputField
                    type="text"
                    name="name"
                    value={fields.name}
                    placeholder="Nombre"
                    required={true}
                    validate={(value) => value.length <= 4}
                    onChange={this.onChange}
                    id="name"
                    classNames="c-input-field__field"
                    />
                </div>
              </div>

              <div className="[ o-grid__child u-8of12 u-6of12@lap ]">
                <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--between ]">
                  <label htmlFor="rfc" className="c-input-field__label">RFC</label>
                  <InputField
                    type="text"
                    name="rfc"
                    value={fields.rfc}
                    placeholder="RFC"
                    required={true}
                    validate={(value) => value.length <= 4}
                    onChange={this.onChange}
                    id="rfc"
                    classNames="c-input-field__field"
                    />
                </div>
              </div>

              <div className="[ o-grid__child u-8of12 u-6of12@lap ]">
                <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--between ]">
                  <label htmlFor="imss" className="c-input-field__label">IMSS</label>
                  <InputField
                    type="text"
                    name="imss"
                    value={fields.imss}
                    placeholder="IMSS"
                    required={true}
                    validate={(value) => value.length <= 4}
                    onChange={this.onChange}
                    id="imss"
                    classNames="c-input-field__field"
                    />
                </div>
              </div>

              <div className="[ o-grid__child u-8of12 u-6of12@lap ]">
                <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--between ]">
                  <label htmlFor="curp" className="c-input-field__label">CUPR</label>
                  <InputField
                    type="text"
                    name="curp"
                    value={fields.curp}
                    placeholder="CURP"
                    required={true}
                    validate={(value) => value.length <= 4}
                    onChange={this.onChange}
                    id="curp"
                    classNames="c-input-field__field"
                    />
                </div>
              </div>

            </div>

            {/* SECTION */}
            <div className="c-catalogue-form__section o-grid o-grid-axis-align--start o-grid-cross-align--stretch">
              <h2 className="o-section-title t1 o-grid__child u-12of12">Información de contacto</h2>

              <div className="[ o-grid__child u-8of12 u-6of12@lap ]">
                <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--around ]">
                  <label htmlFor="phone" className="c-input-field__label">Teléfono fijo</label>
                  <InputField
                    type="text"
                    name="phone"
                    value={fields.phone}
                    placeholder="Teléfono fijo"
                    required={true}
                    validate={(value) => value.length <= 4}
                    onChange={this.onChange}
                    id="phone"
                    classNames="c-input-field__field"
                    />
                </div>
              </div>

              <div className="[ o-grid__child u-8of12 u-6of12@lap ]">
                <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--around ]">
                  <label htmlFor="cellphone" className="c-input-field__label">Teléfono celular</label>
                  <InputField
                    type="text"
                    name="cellphone"
                    value={fields.cellphone}
                    placeholder="Teléfono celular"
                    required={true}
                    validate={(value) => value.length <= 4}
                    onChange={this.onChange}
                    id="cellphone"
                    classNames="c-input-field__field"
                    />
                </div>
              </div>

              <div className="[ o-grid__child u-8of12 u-6of12@lap ]">
                <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--around ]">
                  <label htmlFor="email" className="c-input-field__label">Correo electrónico</label>
                  <InputField
                    type="text"
                    name="email"
                    value={fields.email}
                    placeholder="Correo electrónico"
                    required={true}
                    validate={(value) => value.length <= 4}
                    onChange={this.onChange}
                    id="email"
                    classNames="c-input-field__field"
                    />
                </div>
              </div>

              <div className="[ o-grid__child u-8of12 u-6of12@lap ]">
                <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--around ]">
                  <label htmlFor="address" className="c-input-field__label">Dirección</label>
                  <TextareaField
                    name="address"
                    value={fields.address}
                    placeholder="Dirección"
                    required={true}
                    validate={(value) => value.length <= 4}
                    onChange={this.onChange}
                    id="address"
                    classNames="c-input-field__field"
                    />
                </div>
              </div>

            </div>

            {/* SECTION */}
            <div className="c-catalogue-form__section o-grid o-grid-axis-align--start o-grid-cross-align--stretch">
              <h2 className="o-section-title t1 o-grid__child u-12of12">Información del título</h2>

              <div className="[ o-grid__child u-8of12 u-6of12@lap ]">
                <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--around ]">
                  <label htmlFor="title" className="c-input-field__label">Título</label>
                  <InputField
                    type="text"
                    name="title"
                    value={fields.title}
                    placeholder="Título"
                    required={true}
                    validate={(value) => value.length <= 4}
                    onChange={this.onChange}
                    id="title"
                    classNames="c-input-field__field"
                    />
                </div>
              </div>

              <div className="c-input-field--small [ o-grid__child u-4of12 ] [ o-grid--vertical o-grid-axis-align--around ]">
                <label htmlFor="salary" className="c-input-field__label">Salario</label>
                <InputField
                  type="number"
                  name="salary"
                  value={fields.salary}
                  min={0}
                  placeholder="0.00"
                  required={true}
                  validate={(value) => value <= 0}
                  onChange={this.onChange}
                  id="salary"
                  classNames="c-input-field__field"
                  />
              </div>

              <div className="c-input-field--small [ o-grid__child u-4of12 u-4of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                <label htmlFor="objective" className="c-input-field__label">Objetivo de venta</label>
                <InputField
                  type="number"
                  name="objective"
                  value={fields.objective}
                  min={0}
                  placeholder="0.00"
                  required={true}
                  validate={(value) => value <= 0}
                  onChange={this.onChange}
                  id="objective"
                  classNames="c-input-field__field"
                  />
              </div>

              <div className="c-input-field--small [ o-grid__child u-12of12 u-4of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                <label htmlFor="objectiveBonus" className="c-input-field__label">Bonus por objetivo de venta</label>
                <InputField
                  type="number"
                  name="objectiveBonus"
                  value={fields.objectiveBonus}
                  min={0}
                  placeholder="0%"
                  required={true}
                  validate={(value) => value <= 0}
                  onChange={this.onChange}
                  id="objectiveBonus"
                  classNames="c-input-field__field"
                  />
              </div>

              <div className="c-input-field--small [ o-grid__child u-12of12 u-6of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                <label htmlFor="bonus" className="c-input-field__label">Prestaciones</label>
                { bonusesControl }
                <div className="c-dynamic-list o-grid_child u-12of12">
                  { fields.bonus.map((i) => {
                      const item = bonuses.items.find((b) => b.id == i.bonus) //eslint-disable-line
                      return item ? (
                        <div className="c-dynamic-list__item o-grid o-grid-axis-align--between o-grid-cross-align--center u-12of12" key={item.id}>
                          <span className="o-dynamic-item-title o-grid__child u-6of12">{item.concept} -- ${i.quantity}</span>
                          <button onClick={(e) => this.handleBonusesRemoval(e, item.id)} className="o-dynamic-item-remove-btn o-primary-action"><i className="o-icon o-icon-trash--small"></i></button>
                        </div>
                      ) : null;
                  })}
                </div>
              </div>

            </div>

            <div className="c-buttons-field">
              <button className="c-buttons-field__button" onClick={() => route.history.goBack()}>Cancelar</button>
              <button className="c-buttons-field__button--primary" onClick={(e) => this.handlePost(e)} type="submit">Agregar</button>
            </div>
          </form>
      </div>
    );
  }
}

NewStaffsForm.propTypes = {
  handlePost: PropTypes.func.isRequired,
  requestDataReload: PropTypes.func,
  requestBonuses: PropTypes.func.isRequired,
  route: PropTypes.object.isRequired
};

export default NewStaffsForm;
