import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import './index.scss';
import {API_URL} from '../../../constants';

export default class FileField extends Component {
  constructor(props) {
    super(props);

    this.state ={
      value: this.props.value,
      url: `${API_URL}/${this.props.value}`,
      error: false
    };

    this.onChange = this.onChange.bind(this);
  }

  componentWillReceiveProps(update){
    this.setState({value: update.value});
  }

  onChange(e) {
    const { name, validate, onChange } = this.props;
    const value = e.target.files[0];
    const error = validate(value);

    const url = URL.createObjectURL(value);

    this.setState({value, url, error});
    onChange({name, value, error});
  }

  render() {
    const { name, placeholder, required, classNames, id } = this.props;
    const { url } = this.state;

    return (
      <div className={`c-file-field ${classNames ? classNames : ''}`}>
        <img className="c-file-field__preview" src={url} alt=""/>
        <input
          type="file"
          name={name}
          placeholder={placeholder}
          required={required}
          onChange={(e) => this.onChange(e)}
          className="c-file-field__input"
          id={id}
          />
      </div>
    );
  }
}

FileField.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.any,
  paceholder: PropTypes.string,
  required: PropTypes.bool,
  validate: PropTypes.func,
  onChange: PropTypes.func,
  id: PropTypes.string,
  classNames: PropTypes.string
};
