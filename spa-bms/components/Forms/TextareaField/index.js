import React, { Component } from 'react';
import { PropTypes } from 'prop-types';

export default class TextareaField extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: this.props.value,
      error: false
    };

    this.onChange = this.onChange.bind(this);
  }

  componentWillReceiveProps(update){
    this.setState({value: update.value});
  }

  onChange(e) {
    const { name, validate, onChange } = this.props;
    const value = e.target.value;
    const error = validate(value);

    this.setState({value, error});
    onChange({name, value, error});
  }

  render() {
    const { name, placeholder, required, id, classNames } = this.props;
    const { value } = this.state;

    return (
      <textarea
        name={name}
        value={value}
        onChange={(e) => this.onChange(e)}
        placeholder={placeholder}
        required={required}
        className={classNames}
        id={id}></textarea>
    );
  }
}

TextareaField.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.PropTypes.string,
  paceholder: PropTypes.string,
  required: PropTypes.bool,
  validate: PropTypes.func,
  onChange: PropTypes.func,
  id: PropTypes.string,
  classNames: PropTypes.string
};
