import React, { Component } from 'react';
import { PropTypes } from 'prop-types';

export default class CheckboxField extends Component {
  constructor(props) {
    super(props);

    this.state = {
      checked: this.props.checked,
    };

    this.onChange = this.onChange.bind(this);
  }

  componentWillReceiveProps(update){
    this.setState({value: update.value});
  }

  onChange(e) {
    const { name, value, onChange } = this.props;
    const checked = e.target.checked;
    const newValue = checked ? value : '';

    this.setState({checked});
    onChange({name, value: newValue});
  }

  render() {
    const { name, value, label, required, id, classNames } = this.props;
    const { checked } = this.state;

    return (
      <div className={`o-grid o-grid-axis-align--start o-grid-cross-align--center ${classNames ? classNames : ''}`}>

        <div className={`o-checkbox ${checked ? 'is-checked' : ''}`}>
          <input
            type="checkbox"
            name={name}
            value={value}
            checked={checked}
            required={required}
            onChange={(e) => this.onChange(e)}
            id={id}
            />
          </div>
        <label className="u-margin-m--left" htmlFor={id}>{label}</label>
      </div>
    );
  }
}

CheckboxField.propTypes = {
  name: PropTypes.string.isRequired,
  checked: PropTypes.bool.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  label: PropTypes.string,
  required: PropTypes.bool,
  onChange: PropTypes.func,
  id: PropTypes.string,
  classNames: PropTypes.string
};
