import React, { Component } from 'react';
import { PropTypes } from 'prop-types';

export default class SelectField extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: this.props.value,
      error: false
    };

    this.onChange = this.onChange.bind(this);
  }

  onChange(e) {
    const { name, validate, onChange } = this.props;
    const value = e.target.value;
    const error = validate ? validate(value) : false;

    this.setState({value, error});
    onChange({name, value, error})
  }

  render() {
    const { name, options, required, disabled, classNames, id } = this.props;
    const { value } = this.state;

    return (
      <select
        name={name}
        value={value}
        required={required}
        disabled={disabled}
        onChange={(e) => this.onChange(e)}
        className={`${classNames} ${disabled ? 'is-disabled' : ''}`}
        id={id}>
        {options.map((o, i) => (
          <option value={o.value} key={o.value}>{o.name}</option>
        ))}
      </select>
    );
  }
}

SelectField.propTypes = {
  name: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(PropTypes.object).isRequired,
  disabled: PropTypes.bool,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onChange: PropTypes.func.isRequired,
  required: PropTypes.bool,
  validate: PropTypes.func,
  classNames: PropTypes.string,
  id: PropTypes.string
};
