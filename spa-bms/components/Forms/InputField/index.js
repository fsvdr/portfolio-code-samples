import React, { Component } from 'react';
import { PropTypes } from 'prop-types';

export default class InputField extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: this.props.value,
      error: false
    };

    this.onChange = this.onChange.bind(this);
  }

  componentWillReceiveProps(update){
    this.setState({value: update.value});
  }

  onChange(e) {
    const { name, validate, onChange } = this.props;
    const value = e.target.value;
    const error = validate(value);

    this.setState({value, error});
    onChange({name, value, error});
  }

  render() {
    const { type, name, min, max, placeholder, required, id, classNames } = this.props;
    const { value } = this.state;

    return (
      <input
        type={type}
        name={name}
        value={value}
        min={min}
        max={max}
        placeholder={placeholder}
        required={required}
        onChange={(e) => this.onChange(e)}
        className={classNames}
        id={id}
        />
    );
  }
}

InputField.propTypes = {
  type: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  min: PropTypes.number,
  max: PropTypes.number,
  paceholder: PropTypes.string,
  required: PropTypes.bool,
  validate: PropTypes.func,
  onChange: PropTypes.func,
  id: PropTypes.string,
  classNames: PropTypes.string
};
