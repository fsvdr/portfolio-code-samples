import React from 'react';
import { PropTypes } from 'prop-types';
import './index.scss';

export const Notification = (props) => (
  <div className="c-notification">
    <div className={`c-notification__toast
      ${props.show ? 'is-visible': ''}
      ${props.state === 'WARNING' ? 'has-warn' : props.state === 'ERROR' ? 'has-error' : ''}
      `}>
      <span className="c-notification__msg">{props.msg}</span>
    </div>
  </div>
);

Notification.propTypes = {
  show: PropTypes.bool.isRequired,
  msg: PropTypes.string.isRequired,
  state: PropTypes.oneOf(['SUCCESS', 'WARNING', 'ERROR'])
}
