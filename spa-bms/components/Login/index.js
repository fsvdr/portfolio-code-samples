import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import './index.scss';

export class Login extends Component {

  constructor(props) {
    super(props);

    this.state = {
      user: '',
      password: ''
    };

    this.handleOnChange = this.handleOnChange.bind(this);
    this.submitForm = this.submitForm.bind(this);
  }

  handleOnChange(e) {
    let state = this.state;
    state[e.target.name] = e.target.value;

    this.setState(state);
  }

  submitForm(e) {
    e.preventDefault();

    if (this.state.user && this.state.password) this.props.doLogin(this.state);
  }

  render() {
    let { user, password } = this.state;

    return (
      <div className="c-login o-app-background">
        <div className="c-login__panel o-grid--vertical o-grid-wrap--no">
          <div className="c-panel-header o-grid__child">
            <h1 className="t4 u-margin-s--bottom">Diruma Body Spa</h1>
            <span className="o-app-subtitle">Bienestar y salud para ti</span>
          </div>
          <div className="c-panel-body o-grid__child">
            <p className="c-panel-body__text">Bienvenidos al sistema administrador de negocio. Por favor ingresa tus credenciales para acceder</p>
          </div>
          <form className="c-panel-form c-form o-grid__child--auto o-grid--vertical o-grid-axis-align--around">
            <div className="c-input-field">
              <label className="c-input-field__label" htmlFor="name">Ingresa tu usuario</label>
              <input className="c-input-field__field" type="text" name="user" placeholder="Usuario" required={true} value={user} onChange={(e) => this.handleOnChange(e)} id="name" />
            </div>
            <div className="c-input-field">
              <label className="c-input-field__label" htmlFor="password">Ingresa tu contraseña</label>
              <input className="c-input-field__field" type="password" name="password" placeholder="Contraseña" required={true} value={password} onChange={(e) => this.handleOnChange(e)} id="password" />
            </div>
            <div className="c-buttons-field--single">
              <button className="c-buttons-field__button" onClick={(e) => this.submitForm(e)} type="submit">Ingresar</button>
            </div>
          </form>
        </div>

        <div className="c-login__overlay o-grid--vertical o-grid-axis-align--end">
          <h1 className="u-color-white o-grid__child">Diruma Body Spa</h1>
          <span className="o-app-subtitle o-grid__child">Bienestar y salud para ti</span>
        </div>
      </div>
    );
  }
}

Login.propTypes = {
  doLogin: PropTypes.func.isRequired
};
