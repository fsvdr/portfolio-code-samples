import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import InputField from '../../../Forms/InputField';
import CheckboxField from '../../../Forms/CheckboxField';
import TextareaField from '../../../Forms/TextareaField';
import SelectField from '../../../Forms/SelectField';

class EditClientsForm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      fields: {
        id: this.props.client.id,
        name: this.props.client.name,
        birthday: this.props.client.birthday,
        email: this.props.client.email,
        phone: this.props.client.phone,
        cellphone: this.props.client.cellphone,
        rfc: this.props.client.rfc,
        address: this.props.client.address,
        media: this.props.client.media,
        prospect: this.props.client.prospect,
        referer: this.props.client.referer,
      },
      errors: {},
    };

    this.onChange = this.onChange.bind(this);
    this.handlePost = this.handlePost.bind(this);
    this.handlePostSuccess = this.handlePostSuccess.bind(this);
    this.handleDeleteSuccess = this.handleDeleteSuccess.bind(this);
  }

  componentWillMount() {
    const id = this.props.route.match.params.id;
    this.props.handleGet(id);
  }

  componentWillReceiveProps(update) {
    if (update.client.id) {
      const fields = {
        id: update.client.id,
        name: update.client.name,
        birthday: update.client.birthday,
        email: update.client.email,
        phone: update.client.phone,
        cellphone: update.client.cellphone,
        rfc: update.client.rfc,
        address: update.client.address,
        media: update.client.media,
        prospect: update.client.prospect,
        referer: update.client.referer,
      };

      const errors = {};

      this.setState({fields, errors});
    }
  }

  componentWillUnMount() {
    const fields = {
      id: '',
      name: '',
      birthday: '',
      email: '',
      phone: '',
      cellphone: '',
      rfc: '',
      address: '',
      media: '',
      prospect: '',
      referer: ''
    };

    const errors = {};

    this.setState({fields, errors});
  }

  onChange(input) {
    const fields = this.state.fields;
    const errors = this.state.errors;

    fields[input.name] = input.value;
    errors[input.name] = input.error;

    this.setState({fields, errors});
  }

  handlePost(event) {
    event.preventDefault();

    const { handlePost } = this.props;
    const { fields, errors } = this.state;
    const errorMessages = Object.keys(errors).filter((k) => errors[k]);

    // TODO: Check for individual required fields
    if (errorMessages.length === 0) handlePost(fields, this.handlePostSuccess);
  }

  handlePostSuccess() {
    this.props.requestDataReload();
    this.props.route.history.goBack();
  }

  handleDeleteSuccess() {
    this.props.requestDataReload();
    this.props.route.history.goBack()
  }

  render() {
    const { clients, handleDelete, route } = this.props;
    const { fields } = this.state;

    return (
      <div className={`c-catalogue-list__form`}>
        <h2 className="t2">Edita los datos del cliente seleccionado</h2>
        <p className="o-catalogue-description">Por favor modifica los datos del cliente que desees.</p>
          <form className="c-catalogue-form c-form" action="/" method="post">
          {/* SECTION */}
            <div className="c-catalogue-form__section o-grid o-grid-axis-align--start o-grid-cross-align--stretch">
              <h2 className="o-section-title t1 o-grid__child u-12of12">Información General</h2>

              <div className="[ o-grid__child u-8of12 u-6of12@lap ]">
                <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--around ]">
                  <label htmlFor="name" className="c-input-field__label">Nombre</label>
                  <InputField
                    type="text"
                    name="name"
                    value={fields.name}
                    placeholder="Nombre"
                    required={true}
                    validate={(value) => value.length <= 4}
                    onChange={this.onChange}
                    id="name"
                    classNames="c-input-field__field"
                    />
                </div>
              </div>

              <div className="[ o-grid__child u-8of12 u-6of12@lap ]">
                <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--around ]">
                  <label htmlFor="birthday" className="c-input-field__label">Fecha de nacimiento</label>
                  <InputField
                    type="date"
                    name="birthday"
                    value={fields.birthday}
                    placeholder="dd/mm/aaaa"
                    required={true}
                    validate={(value) => value.length <= 4}
                    onChange={this.onChange}
                    id="birthday"
                    classNames="c-input-field__field"
                    />
                </div>
              </div>

              <div className="[ o-grid__child u-8of12 u-6of12@lap ]">
                <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--between ]">
                  <label htmlFor="rfc" className="c-input-field__label">RFC</label>
                  <InputField
                    type="text"
                    name="rfc"
                    value={fields.rfc}
                    placeholder="RFC"
                    required={true}
                    validate={(value) => value.length <= 4}
                    onChange={this.onChange}
                    id="rfc"
                    classNames="c-input-field__field"
                    />
                </div>
              </div>

              <div className="[ o-grid__child u-8of12 u-6of12@lap ]">
                <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--around ]">
                  <label htmlFor="address" className="c-input-field__label">Dirección</label>
                  <TextareaField
                    name="address"
                    value={fields.address}
                    placeholder="Dirección"
                    required={true}
                    validate={(value) => value.length <= 4}
                    onChange={this.onChange}
                    id="address"
                    classNames="c-input-field__field"
                    />
                </div>
              </div>
            </div>

            {/* SECTION */}
            <div className="c-catalogue-form__section o-grid o-grid-axis-align--start o-grid-cross-align--stretch">
              <h2 className="o-section-title t1 o-grid__child u-12of12">Información de contacto</h2>

              <div className="[ o-grid__child u-8of12 u-6of12@lap ]">
                <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--around ]">
                  <label htmlFor="phone" className="c-input-field__label">Teléfono fijo</label>
                  <InputField
                    type="text"
                    name="phone"
                    value={fields.phone}
                    placeholder="Teléfono fijo"
                    required={true}
                    validate={(value) => value.length <= 4}
                    onChange={this.onChange}
                    id="phone"
                    classNames="c-input-field__field"
                    />
                </div>
              </div>

              <div className="[ o-grid__child u-8of12 u-6of12@lap ]">
                <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--around ]">
                  <label htmlFor="cellphone" className="c-input-field__label">Teléfono celular</label>
                  <InputField
                    type="text"
                    name="cellphone"
                    value={fields.cellphone}
                    placeholder="Teléfono celular"
                    required={true}
                    validate={(value) => value.length <= 4}
                    onChange={this.onChange}
                    id="cellphone"
                    classNames="c-input-field__field"
                    />
                </div>
              </div>

              <div className="[ o-grid__child u-8of12 u-6of12@lap ]">
                <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--around ]">
                  <label htmlFor="email" className="c-input-field__label">Correo electrónico</label>
                  <InputField
                    type="text"
                    name="email"
                    value={fields.email}
                    placeholder="Correo electrónico"
                    required={true}
                    validate={(value) => value.length <= 4}
                    onChange={this.onChange}
                    id="email"
                    classNames="c-input-field__field"
                    />
                </div>
              </div>

            </div>

            {/* SECTION */}
            <div className="c-catalogue-form__section o-grid o-grid-axis-align--start o-grid-cross-align--stretch">
              <h2 className="o-section-title t1 o-grid__child u-12of12">Información de captación</h2>

              <div className="[ o-grid__child u-8of12 u-6of12@lap ]">
                <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--around ]">
                  <label htmlFor="media" className="c-input-field__label">Medio de captación</label>
                  <InputField
                    type="text"
                    name="media"
                    value={fields.media}
                    placeholder="Medio de captación"
                    required={true}
                    validate={(value) => value.length <= 4}
                    onChange={this.onChange}
                    id="media"
                    classNames="c-input-field__field"
                    />
                </div>
              </div>

              <div className="c-input-field--small [ o-grid__child u-12of12 u-6of12@lap ] [ o-grid--vertical o-grid-axis-align--around ]">
                <label htmlFor="referer" className="c-input-field__label">Recomendado por</label>
                <SelectField
                  name="referer"
                  value={fields.referer}
                  onChange={this.onChange}
                  options={[{value: '', name: 'Sin selección'}, ...clients.items.map((i) => {return {value: i.id, name: i.name}})]}
                  classNames={"c-input-field__field o-grid__child u-8of12"}
                  id="referer"
                  />
              </div>

              <div className="[ o-grid__child u-8of12 u-6of12@lap ]">
                <div className="c-input-field--small [ o-grid--vertical o-grid-axis-align--around ]">
                  <label className="c-input-field__label">¿Es prospecto?</label>
                  <CheckboxField
                    name="prospect"
                    checked={fields.prospect ? true : false}
                    value={1}
                    label="Es prospecto"
                    required={true}
                    onChange={this.onChange}
                    id="prospect"
                    classNames="c-checkbox-field"
                    />
                </div>
              </div>

            </div>

            <div className="c-buttons-field">
            <button className="c-buttons-field__button" onClick={() => route.history.goBack()}>Cancelar</button>
            <button className="c-buttons-field__button" onClick={() => handleDelete(fields.id, this.handleDeleteSuccess)}>Desactivar</button>
            <button className="c-buttons-field__button--primary" onClick={(e) => this.handlePost(e)} type="submit">Editar</button>
            </div>
          </form>
      </div>
    );
  }
}

EditClientsForm.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  client: PropTypes.object.isRequired,
  clients: PropTypes.object.isRequired,
  handleGet: PropTypes.func.isRequired,
  handlePost: PropTypes.func.isRequired,
  handleDelete: PropTypes.func.isRequired,
  requestDataReload: PropTypes.func,
  route: PropTypes.object.isRequired
};

export default EditClientsForm;
