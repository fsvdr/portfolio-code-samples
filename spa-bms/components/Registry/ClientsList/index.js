import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { BeatLoader } from 'react-spinners';
import Moment from 'react-moment';
import { Route, NavLink } from 'react-router-dom';
import NewClientsForm from './ClientsForm/new.form';
import EditClientsForm from './ClientsForm/edit.form';

class ClientsList extends Component {

  componentWillMount() {
    this.props.api.fetch();
  }

  render() {
    const { clients, api, route } = this.props;

    return (
      <div className="c-catalogue-list">
        <div className="c-catalogue-list__header">
          <h1 className="t3">Listado de Clientes</h1>
          <p className="o-catalogue-description">Consulta, edita y agrega clientes y prospectos.</p>
          <div className="c-catalogue-list__filters">
            <NavLink to="/registro/clientes/nuevo" className="o-filter">Agregar nuevo cliente</NavLink>
          </div>
        </div>

        {/* NEW CLIENT FORM */}
        <Route path="/registro/clientes/nuevo" render={(match) => (
          <NewClientsForm
            clients={clients}
            handlePost={api.post}
            requestDataReload={api.fetch}
            route={match} />
        )}/>

        {/* EDIT CLIENT FORM */}
        <Route path="/registro/clientes/editar/:id" render={(match) => (
          <EditClientsForm
            isLoading={clients.isLoading}
            client={clients.item}
            clients={clients}
            handleGet={api.get}
            handlePost={api.post}
            handleDelete={api.delete}
            requestDataReload={api.fetch}
            route={match} />
        )}/>

        {/* CLIENTS LIST */}
        <Route exact path="/registro/clientes" render={() => (
          <div className="c-catalogue-list__body">
            {
              clients.isLoading
              ? <BeatLoader className="c-catalogue-list__spinner" color={'#ffffff'} loading={true} />
              : (
                  <div className="c-items-list o-grid o-grid-cross-align--stretch">
                    {
                      clients.items.map((i) => (
                        <div className="[ o-grid__child u-12of12 u-6of12@tab u-4of12@lap ]" key={i.id}>
                          <div className="c-item o-grid o-grid-axis-align--between o-grid-cross-align--center">
                            <div className="o-grid__child u-margin-m--bottom">
                              {i.phone ? (<a href={`tel:${i.phone}`} className="o-primary-action"><i className="o-icon o-icon-phone--small"></i></a>) : null}
                            </div>
                            <div className="o-grid__child u-margin-m--bottom">
                              {i.email ? (<a href={i.email} target="_blank" className="o-primary-action"><i className="o-icon o-icon-link--small"></i></a>) : null}
                            </div>
                            <div className="c-item__data [ o-grid__child u-12of12 ]">
                              <h2 className="o-item-name t2 u-text-truncate u-margin-c"><NavLink to={`${route.match.url}/editar/${i.id}`}>{i.name}</NavLink></h2>
                              <p className="o-item-description u-margin-s--bottom u-text-left">{i.prospect ? 'Prospecto' : 'Recurrente'}</p>
                              <div className="c-item__numbers [ o-grid o-grid-axis-align--between o-grid-cross-align--center o-grid-wrap--no ]">
                                {i.birthday ? (<span className="o-item-number">Cumpleaños: <strong><Moment locale="es" format="MMMM D YYYY">{i.birthday}</Moment></strong></span>) : null}
                              </div>
                            </div>
                          </div>
                        </div>
                      ))
                    }
                  </div>
                )
            }
          </div>
        )}/>
      </div>
    );
  }
}

ClientsList.propTypes = {
  clients: PropTypes.object.isRequired,
  api: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired
};

export default ClientsList;
