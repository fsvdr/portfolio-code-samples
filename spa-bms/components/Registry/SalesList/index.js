import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { BeatLoader } from 'react-spinners';
import { Route, NavLink } from 'react-router-dom';
import EditSalesForm from './SalesForm/edit.form';
import { toMoney } from '../../../helpers/money-format';

class SalesList extends Component {

  componentWillMount() {
    this.props.api.fetch();
  }

  render() {
    const { sales, api, route } = this.props;

    return (
      <div className="c-catalogue-list">
        <div className="c-catalogue-list__header">
          <h1 className="t3">Listado de Ventas</h1>
          <p className="o-catalogue-description">Consulta y edita ventas realizadas.</p>
        </div>

        {/* EDIT SALE FORM */}


        {/* SALES LIST */}
        <Route exact path="/registro/ventas" render={() => (
          <div className="c-catalogue-list__body">
            {
              sales.isLoading
              ? <BeatLoader className="c-catalogue-list__spinner" color={'#ffffff'} loading={true} />
              : (
                  <div className="c-items-list o-grid o-grid-cross-align--stretch">
                    {
                      sales.items.map((i) => {
                        const rawItems = [...i.products, ...i.treatments, ...i.services];
                        const items = rawItems.map((i) => i.name);
                        const content = items.join(', ');
                        return (
                          <div className="[ o-grid__child u-12of12 u-6of12@tab u-4of12@lap ]" key={i.id}>
                            <div className="c-item o-grid o-grid-axis-align--between o-grid-cross-align--center">
                              <div className="o-grid__child u-margin-m--bottom">
                                <span className="o-primary-action"><i className={`o-icon o-icon-${toMoney(i.price) === toMoney(i.paid) ? 'tick' : 'clock'}--small`}></i></span>
                              </div>
                              <div className="c-item__data [ o-grid__child u-12of12 ]">
                                <h2 className="o-item-name t2 u-text-truncate u-margin-c"><NavLink to={`${route.match.url}/editar/${i.id}`}>{i.client.name}</NavLink></h2>
                                <p className="o-item-description u-margin-s--bottom u-text-left">{content}</p>
                                <div className="c-item__numbers [ o-grid o-grid-axis-align--between o-grid-cross-align--center o-grid-wrap--no ]">
                                  <span className="o-item-number">Total: <strong>{toMoney(i.price)}</strong></span>
                                  <span className="o-item-number">Pagado: <strong>{toMoney(i.paid)}</strong></span>
                                </div>
                              </div>
                            </div>
                          </div>
                        );
                      })
                    }
                  </div>
                )
            }
          </div>
        )}/>
      </div>
    );
  }
}

SalesList.propTypes = {
  sales: PropTypes.object.isRequired,
  api: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired
};

export default SalesList;
