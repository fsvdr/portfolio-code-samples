import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux'
import { createStore , applyMiddleware } from 'redux'
import { rootReducer } from './reducers';

import createSagaMiddleware from 'redux-saga'
import { rootSaga } from './sagas';

import { BrowserRouter as Router, Route } from 'react-router-dom'

import './index.scss';
import App from './containers/App';
import registerServiceWorker from './registerServiceWorker';

// Create the saga middleware
const sagaMiddleware = createSagaMiddleware();

// Create Redux Store
const store = createStore(
  rootReducer,
  applyMiddleware(sagaMiddleware)
);

// Run the saga
sagaMiddleware.run(rootSaga);

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Route path="/" component={App} />
    </Router>
  </Provider>,
  document.getElementById('root'));
registerServiceWorker();
