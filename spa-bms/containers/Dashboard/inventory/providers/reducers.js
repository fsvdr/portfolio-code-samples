import {
  RESOLVE_PROVIDERS_FETCH,
  REJECT_PROVIDERS_FETCH,

  RESOLVE_PROVIDER_GET,
  REJECT_PROVIDER_GET,

  RESOLVE_PROVIDER_POST,
  REJECT_PROVIDER_POST,

  RESOLVE_PROVIDER_DELETE,
  REJECT_PROVIDER_DELETE
} from './constants';
import {
  REQUEST_API,
  RESOLVE_REQUEST,
  REJECT_REQUEST,
} from '../../constants';

const initialSatate = {
  items: [],
  item: {},
  isLoading: false,
  error: false
};

export const providers = (
  state = initialSatate,
  action
) => {
  switch (action.type) {
    case REQUEST_API:
      return Object.assign({}, state, {isLoading: true, error: false});

    case RESOLVE_REQUEST:
      return Object.assign({}, state, {isLoading: false});

    case REJECT_REQUEST:
    case REJECT_PROVIDERS_FETCH:
    case REJECT_PROVIDER_GET:
    case REJECT_PROVIDER_POST:
    case REJECT_PROVIDER_DELETE:
      return Object.assign({}, state, {isLoading: false, error: true});

    case RESOLVE_PROVIDERS_FETCH:
      return Object.assign({}, state, {items: action.payload, isLoading: false});

    case RESOLVE_PROVIDER_GET:
      return Object.assign({}, state, {item: action.payload, isLoading: false});

    case RESOLVE_PROVIDER_POST:
    case RESOLVE_PROVIDER_DELETE:
      return Object.assign({}, state, {isLoading: false});

    default:
      return state;
  }
};
