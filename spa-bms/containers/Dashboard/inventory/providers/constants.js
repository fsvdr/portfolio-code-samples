export const DO_FETCH_PROVIDERS = 'DO_FETCH_PROVIDERS';
export const RESOLVE_PROVIDERS_FETCH = 'RESOLVE_PROVIDERS_FETCH';
export const REJECT_PROVIDERS_FETCH = 'REJECT_PROVIDERS_FETCH';

export const DO_GET_PROVIDER = 'DO_GET_PROVIDER';
export const RESOLVE_PROVIDER_GET = 'RESOLVE_PROVIDER_GET';
export const REJECT_PROVIDER_GET = 'REJECT_PROVIDER_GET';

export const DO_POST_PROVIDER = 'DO_POST_PROVIDER';
export const RESOLVE_PROVIDER_POST = 'RESOLVE_PROVIDER_POST';
export const REJECT_PROVIDER_POST = 'REJECT_PROVIDER_POST';

export const DO_DELETE_PROVIDER = 'DO_DELETE_PROVIDER';
export const RESOLVE_PROVIDER_DELETE = 'RESOLVE_PROVIDER_DELETE';
export const REJECT_PROVIDER_DELETE = 'REJECT_PROVIDER_DELETE';
