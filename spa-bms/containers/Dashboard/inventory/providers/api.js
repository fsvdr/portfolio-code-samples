import { API_URL } from '../../constants';

export const fetchProviders = () => {
  return fetch(`${API_URL}/providers`, {
    method: 'GET',
    credentials: 'include',
    mode: 'cors'
  });
};

export const getProvider = (id) => {
  return fetch(`${API_URL}/provider/${id}`, {
    method: 'GET',
    credentials: 'include'
  });
};

export const postProvider = (body) => {
  return fetch(`${API_URL}/provider`, {
    method: 'POST',
    credentials: 'include',
    body
  });
};

export const deleteProvider = (id) => {
  return fetch(`${API_URL}/provider/deactivate/${id}`, {
    method: 'POST',
    credentials: 'include'
  });
};
