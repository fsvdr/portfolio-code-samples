import {
  DO_FETCH_PROVIDERS,
  RESOLVE_PROVIDERS_FETCH,
  REJECT_PROVIDERS_FETCH,

  DO_GET_PROVIDER,
  RESOLVE_PROVIDER_GET,
  REJECT_PROVIDER_GET,

  DO_POST_PROVIDER,
  RESOLVE_PROVIDER_POST,
  REJECT_PROVIDER_POST,

  DO_DELETE_PROVIDER,
  RESOLVE_PROVIDER_DELETE,
  REJECT_PROVIDER_DELETE
} from './constants';

/**************************************
 * LIST PROVIDERS
 **************************************/

export const doFetchProviders = () => {
  return {
    type: DO_FETCH_PROVIDERS
  };
};

export const resolveProvidersFetch = (providers) => {
  return {
    type: RESOLVE_PROVIDERS_FETCH,
    payload: providers
  };
};

export const rejectProvidersFetch = () => {
  return {
    type: REJECT_PROVIDERS_FETCH
  };
};

/**************************************
 * GET PROVIDER
 **************************************/

export const doGetProvider = (id) => {
  return {
    type: DO_GET_PROVIDER,
    payload: id
  };
};

export const resolveProviderGet = (provider) => {
  return {
    type: RESOLVE_PROVIDER_GET,
    payload: provider
  };
};

export const rejectProviderGet = () => {
  return {
    type: REJECT_PROVIDER_GET
  };
};

/**************************************
 * POST PROVIDER
 **************************************/

 export const doPostProvider = (provider, callback) => {
   return {
     type: DO_POST_PROVIDER,
     payload: provider,
     callback
   };
 };

 export const resolveProviderPost = () => {
   return {
     type: RESOLVE_PROVIDER_POST
   };
 };

 export const rejectProviderPost = () => {
   return {
     type: REJECT_PROVIDER_POST
   };
 };

/**************************************
 * DELETE PROVIDER
 **************************************/

 export const doDeleteProvider = (id, callback) => {
   return {
     type: DO_DELETE_PROVIDER,
     payload: id,
     callback
   };
 };

 export const resolveProviderDelete = () => {
   return {
     type: RESOLVE_PROVIDER_DELETE
   };
 };

 export const rejectProviderDelete = () => {
   return {
     type: REJECT_PROVIDER_DELETE
   };
 };
