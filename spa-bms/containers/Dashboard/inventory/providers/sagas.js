import { call, put, takeEvery } from 'redux-saga/effects'
import { doTriggerNotification, doDeAuthenticate } from '../../../../actions';
import { mapCodeToMessage } from '../../../../helpers/api-codes';
import { fetchProviders, getProvider, postProvider, deleteProvider } from './api';
import {
  DO_FETCH_PROVIDERS,
  DO_GET_PROVIDER,
  DO_POST_PROVIDER,
  DO_DELETE_PROVIDER
} from './constants';
import {
  resolveProvidersFetch, rejectProvidersFetch,
  resolveProviderGet, rejectProviderGet,
  resolveProviderPost, rejectProviderPost,
  resolveProviderDelete, rejectProviderDelete
} from './actions';
import {
  requestApi, resolveRequest, rejectRequest
} from '../../actions';

/**************************************
 * LIST PROVIDERS
 **************************************/

function *handleProvidersFetch(action) {
  // Inform the UI that a request is beign handled
  yield put(requestApi());
  try {
    // Do API request
    const response = yield call(fetchProviders);

    if (response.status === 200) {
      // Resolve successful API request
      const data = yield response.json();
      yield put(resolveProvidersFetch(data));
    } else if (response.status === 401) {
      // Resolve unsuccessful API request due to authorization
      // Trigger a notification and de authenticate
      yield put(rejectProvidersFetch());
      yield put(doTriggerNotification('Sesión terminada'));
      yield put(doDeAuthenticate());
    } else {
      // Reject unsuccessful API request and trigger a notification
      const notify = mapCodeToMessage(response.status);
      yield put(rejectProvidersFetch());
      yield put(doTriggerNotification(notify.msg, notify.state));
    }

    // Resolve correct request
    yield put(resolveRequest());
  } catch (e) {
    // Resolve unsuccessful request
    yield put(rejectRequest());
  }
}

/**************************************
 * GET PROVIDER
 **************************************/

function *handleProviderGet(action) {
  // Inform the UI that a request is beign handled
  yield put(requestApi());

  try {
    // Do API request
    const response = yield call(getProvider, action.payload);

    if (response.status === 200) {
      // Resolve successful API request
      const data = yield(response.json());
      yield put(resolveProviderGet(data));
    } else if (response.status === 401) {
      // Resolve unsuccessful API request due to authorization
      // Trigger a notification and de authenticate
      yield put(rejectProviderGet());
      yield put(doTriggerNotification('Sesión terminada'));
      yield put(doDeAuthenticate());
    } else {
      // Reject unsuccessful API request and trigger a notification
      const notify = mapCodeToMessage(response.status);
      yield put(rejectProviderGet());
      yield put(doTriggerNotification(notify.msg, notify.state));
    }

    // Resolve correct request
    yield put(resolveRequest());
  } catch (e) {
    // Resolve unsuccessful request
    yield put(rejectRequest());
  }
}

/**************************************
 * POST PROVIDER
 **************************************/

function *handleProviderPost(action) {
 // Inform the UI that a request is beign handled
 yield put(requestApi());

 // Format data for API request's body
 const data = {
   id: action.payload.id,
   name: action.payload.name,
   phone: action.payload.phone,
   cellphone: action.payload.cellphone,
   address: action.payload.address,
   website: action.payload.website,
   contactName: action.payload.contactName,
   email: action.payload.email,
   rfc: action.payload.rfc,
 }
 let body = new FormData();
 body.append('provider', JSON.stringify(data));

const notifyMsg = action.payload.id ? 'actualizó' : 'creó';

 try {
   // Do API request
   const response = yield call(postProvider, body);

   if (response.status === 200) {
     // Resolve successful API request
     yield put(resolveProviderPost());
     yield put(doTriggerNotification(`El proveedor se ${notifyMsg} exitosamente`));
     if (action.callback) action.callback();
   } else if (response.status === 401) {
     // Resolve unsuccessful API request due to authorization
     // Trigger a notification and de authenticate
     yield put(rejectProviderPost());
     yield put(doTriggerNotification('Sesión terminada'));
     yield put(doDeAuthenticate());
   } else {
     // Reject unsuccessful API request and trigger a notification
     const notify = mapCodeToMessage(response.status);
     yield put(rejectProviderPost());
     yield put(doTriggerNotification(notify.msg, notify.state));
   }

   // Resolve correct request
   yield put(resolveRequest());
 } catch (e) {
   // Resolve unsuccessful request
   yield put(rejectRequest());
 }
}

 /**************************************
  * DELETE PROVIDER
  **************************************/

  function *handleProviderDelete(action) {
    // Inform the UI that a request is beign handled
    yield put(requestApi());

    try {
      // Do API request
      const response = yield call(deleteProvider, action.payload);

      if (response.status === 200) {
        // Resolve successful API request
        yield put(resolveProviderDelete());
        yield put(doTriggerNotification('El proveedor se eliminó exitosamente'));
        if (action.callback) action.callback();
      } else if (response.status === 401) {
        // Resolve unsuccessful API request due to authorization
        // Trigger a notification and de authenticate
        yield put(rejectProviderDelete());
        yield put(doTriggerNotification('Sesión terminada'));
        yield put(doDeAuthenticate());
      } else {
        // Reject unsuccessful API request and trigger a notification
        const notify = mapCodeToMessage(response.status);
        yield put(rejectProviderDelete());
        yield put(doTriggerNotification(notify.msg, notify.state));
      }

      // Resolve correct request
      yield put(resolveRequest());
    } catch (e) {
      // Resolve unsuccessful request
      yield put(rejectRequest());
    }
  }





  export const providersSaga = [
    takeEvery(DO_FETCH_PROVIDERS, handleProvidersFetch),
    takeEvery(DO_GET_PROVIDER, handleProviderGet),
    takeEvery(DO_POST_PROVIDER, handleProviderPost),
    takeEvery(DO_DELETE_PROVIDER, handleProviderDelete)
  ];
