import {
  DO_FETCH_ORDERS,
  RESOLVE_ORDERS_FETCH,
  REJECT_ORDERS_FETCH,

  DO_GET_ORDER,
  RESOLVE_ORDER_GET,
  REJECT_ORDER_GET,

  DO_POST_ORDER,
  RESOLVE_ORDER_POST,
  REJECT_ORDER_POST,

  DO_COMPLETE_ORDER,
  RESOLVE_ORDER_COMPLETE,
  REJECT_ORDER_COMPLETE,

  DO_DELETE_ORDER,
  RESOLVE_ORDER_DELETE,
  REJECT_ORDER_DELETE
} from './constants';

/**************************************
 * LIST ORDERS
 **************************************/

export const doFetchOrders = () => {
  return {
    type: DO_FETCH_ORDERS
  };
};

export const resolveOrdersFetch = (orders) => {
  return {
    type: RESOLVE_ORDERS_FETCH,
    payload: orders
  };
};

export const rejectOrdersFetch = () => {
  return {
    type: REJECT_ORDERS_FETCH
  };
};

/**************************************
 * GET ORDER
 **************************************/

export const doGetOrder = (id) => {
  return {
    type: DO_GET_ORDER,
    payload: id
  };
};

export const resolveOrderGet = (order) => {
  return {
    type: RESOLVE_ORDER_GET,
    payload: order
  };
};

export const rejectOrderGet = () => {
  return {
    type: REJECT_ORDER_GET
  };
};

/**************************************
 * POST ORDER
 **************************************/

 export const doPostOrder = (order, callback) => {
   return {
     type: DO_POST_ORDER,
     payload: order,
     callback
   };
 };

 export const resolveOrderPost = () => {
   return {
     type: RESOLVE_ORDER_POST
   };
 };

 export const rejectOrderPost = () => {
   return {
     type: REJECT_ORDER_POST
   };
 };

 /**************************************
  * COMPLETE ORDER
  **************************************/

  export const doCompleteOrder = (id, callback) => {
    return {
      type: DO_COMPLETE_ORDER,
      payload: id,
      callback
    };
  };

  export const resolveOrderComplete = () => {
    return {
      type: RESOLVE_ORDER_COMPLETE
    };
  };

  export const rejectOrderComplete = () => {
    return {
      type: REJECT_ORDER_COMPLETE
    };
  };

/**************************************
 * DELETE ORDER
 **************************************/

 export const doDeleteOrder = (id, callback) => {
   return {
     type: DO_DELETE_ORDER,
     payload: id,
     callback
   };
 };

 export const resolveOrderDelete = () => {
   return {
     type: RESOLVE_ORDER_DELETE
   };
 };

 export const rejectOrderDelete = () => {
   return {
     type: REJECT_ORDER_DELETE
   };
 };
