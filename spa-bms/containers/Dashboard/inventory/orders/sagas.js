import { call, put, takeEvery } from 'redux-saga/effects'
import { doTriggerNotification, doDeAuthenticate } from '../../../../actions';
import { mapCodeToMessage } from '../../../../helpers/api-codes';
import { fetchOrders, getOrder, postOrder, completeOrder, deleteOrder } from './api';
import {
  DO_FETCH_ORDERS,
  DO_GET_ORDER,
  DO_POST_ORDER,
  DO_COMPLETE_ORDER,
  DO_DELETE_ORDER
} from './constants';
import {
  resolveOrdersFetch, rejectOrdersFetch,
  resolveOrderGet, rejectOrderGet,
  resolveOrderPost, rejectOrderPost,
  resolveOrderComplete, rejectOrderComplete,
  resolveOrderDelete, rejectOrderDelete
} from './actions';
import {
  requestApi, resolveRequest, rejectRequest
} from '../../actions';

/**************************************
 * LIST ORDERS
 **************************************/

function *handleOrdersFetch(action) {
  // Inform the UI that a request is beign handled
  yield put(requestApi());
  try {
    // Do API request
    const response = yield call(fetchOrders);

    if (response.status === 200) {
      // Resolve successful API request
      const data = yield response.json();
      yield put(resolveOrdersFetch(data));
    } else if (response.status === 401) {
      // Resolve unsuccessful API request due to authorization
      // Trigger a notification and de authenticate
      yield put(rejectOrdersFetch());
      yield put(doTriggerNotification('Sesión terminada'));
      yield put(doDeAuthenticate());
    } else {
      // Reject unsuccessful API request and trigger a notification
      const notify = mapCodeToMessage(response.status);
      yield put(rejectOrdersFetch());
      yield put(doTriggerNotification(notify.msg, notify.state));
    }

    // Resolve correct request
    yield put(resolveRequest());
  } catch (e) {
    // Resolve unsuccessful request
    yield put(rejectRequest());
  }
}

/**************************************
 * GET ORDER
 **************************************/

function *handleOrderGet(action) {
  // Inform the UI that a request is beign handled
  yield put(requestApi());

  try {
    // Do API request
    const response = yield call(getOrder, action.payload);

    if (response.status === 200) {
      // Resolve successful API request
      const data = yield(response.json());
      yield put(resolveOrderGet(data));
    } else if (response.status === 401) {
      // Resolve unsuccessful API request due to authorization
      // Trigger a notification and de authenticate
      yield put(rejectOrderGet());
      yield put(doTriggerNotification('Sesión terminada'));
      yield put(doDeAuthenticate());
    } else {
      // Reject unsuccessful API request and trigger a notification
      const notify = mapCodeToMessage(response.status);
      yield put(rejectOrderGet());
      yield put(doTriggerNotification(notify.msg, notify.state));
    }

    // Resolve correct request
    yield put(resolveRequest());
  } catch (e) {
    // Resolve unsuccessful request
    yield put(rejectRequest());
  }
}

/**************************************
 * POST ORDER
 **************************************/

function *handleOrderPost(action) {
 // Inform the UI that a request is beign handled
 yield put(requestApi());

 // Format data for API request's body
 const data = {
   id: action.payload.id,
   payment: action.payload.payment,
   paid: action.payload.paid,
   status: action.payload.status,
   products: action.payload.products
 }
 let body = new FormData();
 body.append('order', JSON.stringify(data));

const notifyMsg = action.payload.id ? 'actualizó' : 'creó';

 try {
   // Do API request
   const response = yield call(postOrder, body);

   if (response.status === 200) {
     // Resolve successful API request
     yield put(resolveOrderPost());
     yield put(doTriggerNotification(`La orden se ${notifyMsg} exitosamente`));
     if (action.callback) action.callback();
   } else if (response.status === 401) {
     // Resolve unsuccessful API request due to authorization
     // Trigger a notification and de authenticate
     yield put(rejectOrderPost());
     yield put(doTriggerNotification('Sesión terminada'));
     yield put(doDeAuthenticate());
   } else {
     // Reject unsuccessful API request and trigger a notification
     const notify = mapCodeToMessage(response.status);
     yield put(rejectOrderPost());
     yield put(doTriggerNotification(notify.msg, notify.state));
   }

   // Resolve correct request
   yield put(resolveRequest());
 } catch (e) {
   // Resolve unsuccessful request
   yield put(rejectRequest());
 }
}

/**************************************
 * COMPLETE ORDER
 **************************************/

function *handleOrderComplete(action) {
 // Inform the UI that a request is beign handled
 yield put(requestApi());

 try {
   // Do API request
   const response = yield call(completeOrder, action.payload);

   if (response.status === 200) {
     // Resolve successful API request
     yield put(resolveOrderComplete());
     yield put(doTriggerNotification(`La orden se marcó como completada`));
     if (action.callback) action.callback();
   } else if (response.status === 401) {
     // Resolve unsuccessful API request due to authorization
     // Trigger a notification and de authenticate
     yield put(rejectOrderComplete());
     yield put(doTriggerNotification('Sesión terminada'));
     yield put(doDeAuthenticate());
   } else {
     // Reject unsuccessful API request and trigger a notification
     const notify = mapCodeToMessage(response.status);
     yield put(rejectOrderComplete());
     yield put(doTriggerNotification(notify.msg, notify.state));
   }

   // Resolve correct request
   yield put(resolveRequest());
 } catch (e) {
   // Resolve unsuccessful request
   yield put(rejectRequest());
 }
}

 /**************************************
  * DELETE ORDER
  **************************************/

  function *handleOrderDelete(action) {
    // Inform the UI that a request is beign handled
    yield put(requestApi());

    try {
      // Do API request
      const response = yield call(deleteOrder, action.payload);

      if (response.status === 200) {
        // Resolve successful API request
        yield put(resolveOrderDelete());
        yield put(doTriggerNotification('La orden se eliminó exitosamente'));
        if (action.callback) action.callback();
      } else if (response.status === 401) {
        // Resolve unsuccessful API request due to authorization
        // Trigger a notification and de authenticate
        yield put(rejectOrderDelete());
        yield put(doTriggerNotification('Sesión terminada'));
        yield put(doDeAuthenticate());
      } else {
        // Reject unsuccessful API request and trigger a notification
        const notify = mapCodeToMessage(response.status);
        yield put(rejectOrderDelete());
        yield put(doTriggerNotification(notify.msg, notify.state));
      }

      // Resolve correct request
      yield put(resolveRequest());
    } catch (e) {
      // Resolve unsuccessful request
      yield put(rejectRequest());
    }
  }





  export const ordersSaga = [
    takeEvery(DO_FETCH_ORDERS, handleOrdersFetch),
    takeEvery(DO_GET_ORDER, handleOrderGet),
    takeEvery(DO_POST_ORDER, handleOrderPost),
    takeEvery(DO_COMPLETE_ORDER, handleOrderComplete),
    takeEvery(DO_DELETE_ORDER, handleOrderDelete)
  ];
