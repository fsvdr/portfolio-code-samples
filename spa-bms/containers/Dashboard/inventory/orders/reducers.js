import {
  RESOLVE_ORDERS_FETCH,
  REJECT_ORDERS_FETCH,

  RESOLVE_ORDER_GET,
  REJECT_ORDER_GET,

  RESOLVE_ORDER_POST,
  REJECT_ORDER_POST,

  RESOLVE_ORDER_COMPLETE,
  REJECT_ORDER_COMPLETE,

  RESOLVE_ORDER_DELETE,
  REJECT_ORDER_DELETE
} from './constants';
import {
  REQUEST_API,
  RESOLVE_REQUEST,
  REJECT_REQUEST,
} from '../../constants';

const initialSatate = {
  items: [],
  item: {},
  isLoading: false,
  error: false
};

export const orders = (
  state = initialSatate,
  action
) => {
  switch (action.type) {
    case REQUEST_API:
      return Object.assign({}, state, {isLoading: true, error: false});

    case RESOLVE_REQUEST:
      return Object.assign({}, state, {isLoading: false});

    case REJECT_REQUEST:
    case REJECT_ORDERS_FETCH:
    case REJECT_ORDER_GET:
    case REJECT_ORDER_POST:
    case REJECT_ORDER_COMPLETE:
    case REJECT_ORDER_DELETE:
      return Object.assign({}, state, {isLoading: false, error: true});

    case RESOLVE_ORDERS_FETCH:
      return Object.assign({}, state, {items: action.payload, isLoading: false});

    case RESOLVE_ORDER_GET:
      return Object.assign({}, state, {item: action.payload, isLoading: false});

    case RESOLVE_ORDER_POST:
    case RESOLVE_ORDER_COMPLETE:
    case RESOLVE_ORDER_DELETE:
      return Object.assign({}, state, {isLoading: false});

    default:
      return state;
  }
};
