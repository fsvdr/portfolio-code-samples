import { API_URL } from '../../constants';

export const fetchOrders = () => {
  return fetch(`${API_URL}/orders`, {
    method: 'GET',
    credentials: 'include',
    mode: 'cors'
  });
};

export const getOrder = (id) => {
  return fetch(`${API_URL}/order/${id}`, {
    method: 'GET',
    credentials: 'include'
  });
};

export const postOrder = (body) => {
  return fetch(`${API_URL}/order`, {
    method: 'POST',
    credentials: 'include',
    body
  });
};

export const completeOrder = (id) => {
  return fetch(`${API_URL}/order/complete/${id}`, {
    method: 'POST',
    credentials: 'include',
  });
};

export const deleteOrder = (id) => {
  return fetch(`${API_URL}/order/deactivate/${id}`, {
    method: 'POST',
    credentials: 'include'
  })
}
