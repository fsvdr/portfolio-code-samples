import { call, put, takeEvery } from 'redux-saga/effects'
import { doTriggerNotification, doDeAuthenticate } from '../../../../actions';
import { mapCodeToMessage } from '../../../../helpers/api-codes';
import {
  fetchPromotions, fetchProductPromotions, fetchTreatmentPromotions,
  fetchServicePromotions, getPromotion, postPromotion,
  deletePromotion
} from './api';
import {
  DO_FETCH_PROMOTIONS,
  DO_FETCH_PRODUCT_PROMOTIONS,
  DO_FETCH_TREATMENT_PROMOTIONS,
  DO_FETCH_SERVICE_PROMOTIONS,
  DO_GET_PROMOTION,
  DO_POST_PROMOTION,
  DO_DELETE_PROMOTION
} from './constants';
import {
  resolvePromotionsFetch, rejectPromotionsFetch,
  resolveProductPromotionsFetch, rejectProductPromotionsFetch,
  resolveTreatmentPromotionsFetch, rejectTreatmentPromotionsFetch,
  resolveServicePromotionsFetch, rejectServicePromotionsFetch,
  resolvePromotionGet, rejectPromotionGet,
  resolvePromotionPost, rejectPromotionPost,
  resolvePromotionDelete, rejectPromotionDelete
} from './actions';
import {
  requestApi, resolveRequest, rejectRequest
} from '../../actions';

/**************************************
 * LIST PROMOTIONS
 **************************************/

function *handlePromotionsFetch(action) {
  // Inform the UI that a request is beign handled
  yield put(requestApi());
  try {
    // Do API request
    const response = yield call(fetchPromotions);

    if (response.status === 200) {
      // Resolve successful API request
      const data = yield response.json();
      yield put(resolvePromotionsFetch(data));
    } else if (response.status === 401) {
      // Resolve unsuccessful API request due to authorization
      // Trigger a notification and de authenticate
      yield put(rejectPromotionsFetch());
      yield put(doTriggerNotification('Sesión terminada'));
      yield put(doDeAuthenticate());
    } else {
      // Reject unsuccessful API request and trigger a notification
      const notify = mapCodeToMessage(response.status);
      yield put(rejectPromotionsFetch());
      yield put(doTriggerNotification(notify.msg, notify.state));
    }

    // Resolve correct request
    yield put(resolveRequest());
  } catch (e) {
    // Resolve unsuccessful request
    yield put(rejectRequest());
  }
}

function *handleProductPromotionsFetch(action) {
  // Inform the UI that a request is beign handled
  yield put(requestApi());
  try {
    // Do API request
    const response = yield call(fetchProductPromotions);

    if (response.status === 200) {
      // Resolve successful API request
      const data = yield response.json();
      yield put(resolveProductPromotionsFetch(data));
    } else if (response.status === 401) {
      // Resolve unsuccessful API request due to authorization
      // Trigger a notification and de authenticate
      yield put(rejectProductPromotionsFetch());
      yield put(doTriggerNotification('Sesión terminada'));
      yield put(doDeAuthenticate());
    } else {
      // Reject unsuccessful API request and trigger a notification
      const notify = mapCodeToMessage(response.status);
      yield put(rejectProductPromotionsFetch());
      yield put(doTriggerNotification(notify.msg, notify.state));
    }

    // Resolve correct request
    yield put(resolveRequest());
  } catch (e) {
    // Resolve unsuccessful request
    yield put(rejectRequest());
  }
}

function *handleTreatmentPromotionsFetch(action) {
  // Inform the UI that a request is beign handled
  yield put(requestApi());
  try {
    // Do API request
    const response = yield call(fetchTreatmentPromotions);

    if (response.status === 200) {
      // Resolve successful API request
      const data = yield response.json();
      yield put(resolveTreatmentPromotionsFetch(data));
    } else if (response.status === 401) {
      // Resolve unsuccessful API request due to authorization
      // Trigger a notification and de authenticate
      yield put(rejectTreatmentPromotionsFetch());
      yield put(doTriggerNotification('Sesión terminada'));
      yield put(doDeAuthenticate());
    } else {
      // Reject unsuccessful API request and trigger a notification
      const notify = mapCodeToMessage(response.status);
      yield put(rejectTreatmentPromotionsFetch());
      yield put(doTriggerNotification(notify.msg, notify.state));
    }

    // Resolve correct request
    yield put(resolveRequest());
  } catch (e) {
    // Resolve unsuccessful request
    yield put(rejectRequest());
  }
}

function *handleServicePromotionsFetch(action) {
  // Inform the UI that a request is beign handled
  yield put(requestApi());
  try {
    // Do API request
    const response = yield call(fetchServicePromotions);

    if (response.status === 200) {
      // Resolve successful API request
      const data = yield response.json();
      yield put(resolveServicePromotionsFetch(data));
    } else if (response.status === 401) {
      // Resolve unsuccessful API request due to authorization
      // Trigger a notification and de authenticate
      yield put(rejectServicePromotionsFetch());
      yield put(doTriggerNotification('Sesión terminada'));
      yield put(doDeAuthenticate());
    } else {
      // Reject unsuccessful API request and trigger a notification
      const notify = mapCodeToMessage(response.status);
      yield put(rejectServicePromotionsFetch());
      yield put(doTriggerNotification(notify.msg, notify.state));
    }

    // Resolve correct request
    yield put(resolveRequest());
  } catch (e) {
    // Resolve unsuccessful request
    yield put(rejectRequest());
  }
}

/**************************************
 * GET PROMOTION
 **************************************/

function *handlePromotionGet(action) {
  // Inform the UI that a request is beign handled
  yield put(requestApi());

  try {
    // Do API request
    const response = yield call(getPromotion, action.payload);

    if (response.status === 200) {
      // Resolve successful API request
      const data = yield(response.json());
      yield put(resolvePromotionGet(data));
    } else if (response.status === 401) {
      // Resolve unsuccessful API request due to authorization
      // Trigger a notification and de authenticate
      yield put(rejectPromotionGet());
      yield put(doTriggerNotification('Sesión terminada'));
      yield put(doDeAuthenticate());
    } else {
      // Reject unsuccessful API request and trigger a notification
      const notify = mapCodeToMessage(response.status);
      yield put(rejectPromotionGet());
      yield put(doTriggerNotification(notify.msg, notify.state));
    }

    // Resolve correct request
    yield put(resolveRequest());
  } catch (e) {
    // Resolve unsuccessful request
    yield put(rejectRequest());
  }
}

/**************************************
 * POST PROMOTION
 **************************************/

function *handlePromotionPost(action) {
 // Inform the UI that a request is beign handled
 yield put(requestApi());

 // Format data for API request's body
 const data = {
   id: action.payload.id,
   validStart: action.payload.validStart,
   validEnd: action.payload.validEnd,
   name: action.payload.name,
   description: action.payload.description,
   type: action.payload.type,
   value: action.payload.value,
   itemType: action.payload.itemType,
   itemId: action.payload.itemId
 }
 let body = new FormData();
 body.append('promotion', JSON.stringify(data));
 body.append('image', action.payload.image)

const notifyMsg = action.payload.id ? 'actualizó' : 'creó';

 try {
   // Do API request
   const response = yield call(postPromotion, body);

   if (response.status === 200) {
     // Resolve successful API request
     yield put(resolvePromotionPost());
     yield put(doTriggerNotification(`La promoción se ${notifyMsg} exitosamente`));
     if (action.callback) action.callback();
   } else if (response.status === 401) {
     // Resolve unsuccessful API request due to authorization
     // Trigger a notification and de authenticate
     yield put(rejectPromotionPost());
     yield put(doTriggerNotification('Sesión terminada'));
     yield put(doDeAuthenticate());
   } else {
     // Reject unsuccessful API request and trigger a notification
     const notify = mapCodeToMessage(response.status);
     yield put(rejectPromotionPost());
     yield put(doTriggerNotification(notify.msg, notify.state));
   }

   // Resolve correct request
   yield put(resolveRequest());
 } catch (e) {
   // Resolve unsuccessful request
   yield put(rejectRequest());
 }
}

 /**************************************
  * DELETE PROMOTION
  **************************************/

  function *handlePromotionDelete(action) {
    // Inform the UI that a request is beign handled
    yield put(requestApi());

    try {
      // Do API request
      const response = yield call(deletePromotion, action.payload);

      if (response.status === 200) {
        // Resolve successful API request
        yield put(resolvePromotionDelete());
        yield put(doTriggerNotification('La promoción se eliminó exitosamente'));
        if (action.callback) action.callback();
      } else if (response.status === 401) {
        // Resolve unsuccessful API request due to authorization
        // Trigger a notification and de authenticate
        yield put(rejectPromotionDelete());
        yield put(doTriggerNotification('Sesión terminada'));
        yield put(doDeAuthenticate());
      } else {
        // Reject unsuccessful API request and trigger a notification
        const notify = mapCodeToMessage(response.status);
        yield put(rejectPromotionDelete());
        yield put(doTriggerNotification(notify.msg, notify.state));
      }

      // Resolve correct request
      yield put(resolveRequest());
    } catch (e) {
      // Resolve unsuccessful request
      yield put(rejectRequest());
    }
  }





  export const promotionsSaga = [
    takeEvery(DO_FETCH_PROMOTIONS, handlePromotionsFetch),
    takeEvery(DO_FETCH_PRODUCT_PROMOTIONS, handleProductPromotionsFetch),
    takeEvery(DO_FETCH_TREATMENT_PROMOTIONS, handleTreatmentPromotionsFetch),
    takeEvery(DO_FETCH_SERVICE_PROMOTIONS, handleServicePromotionsFetch),
    takeEvery(DO_GET_PROMOTION, handlePromotionGet),
    takeEvery(DO_POST_PROMOTION, handlePromotionPost),
    takeEvery(DO_DELETE_PROMOTION, handlePromotionDelete)
  ];
