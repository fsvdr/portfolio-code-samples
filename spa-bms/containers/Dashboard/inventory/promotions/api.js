import { API_URL } from '../../constants';

export const fetchPromotions = () => {
  return fetch(`${API_URL}/promotions`, {
    method: 'GET',
    credentials: 'include',
    mode: 'cors'
  });
};

export const fetchProductPromotions = () => {
  return fetch(`${API_URL}/promotions/product-deals`, {
    method: 'GET',
    credentials: 'include',
    mode: 'cors'
  });
};

export const fetchTreatmentPromotions = () => {
  return fetch(`${API_URL}/promotions/treatment-deals`, {
    method: 'GET',
    credentials: 'include',
    mode: 'cors'
  });
};

export const fetchServicePromotions = () => {
  return fetch(`${API_URL}/promotions/service-deals`, {
    method: 'GET',
    credentials: 'include',
    mode: 'cors'
  });
};

export const getPromotion = (id) => {
  return fetch(`${API_URL}/promotion/${id}`, {
    method: 'GET',
    credentials: 'include'
  });
};

export const postPromotion = (body) => {
  return fetch(`${API_URL}/promotion`, {
    method: 'POST',
    credentials: 'include',
    body
  });
};

export const deletePromotion = (id) => {
  return fetch(`${API_URL}/promotion/deactivate/${id}`, {
    method: 'POST',
    credentials: 'include'
  });
};
