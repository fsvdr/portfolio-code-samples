import {
  DO_FETCH_PROMOTIONS,
  RESOLVE_PROMOTIONS_FETCH,
  REJECT_PROMOTIONS_FETCH,

  DO_FETCH_PRODUCT_PROMOTIONS,
  RESOLVE_PRODUCT_PROMOTIONS_FETCH,
  REJECT_PRODUCT_PROMOTIONS_FETCH,

  DO_FETCH_TREATMENT_PROMOTIONS,
  RESOLVE_TREATMENT_PROMOTIONS_FETCH,
  REJECT_TREATMENT_PROMOTIONS_FETCH,

  DO_FETCH_SERVICE_PROMOTIONS,
  RESOLVE_SERVICE_PROMOTIONS_FETCH,
  REJECT_SERVICE_PROMOTIONS_FETCH,

  DO_GET_PROMOTION,
  RESOLVE_PROMOTION_GET,
  REJECT_PROMOTION_GET,

  DO_POST_PROMOTION,
  RESOLVE_PROMOTION_POST,
  REJECT_PROMOTION_POST,

  DO_DELETE_PROMOTION,
  RESOLVE_PROMOTION_DELETE,
  REJECT_PROMOTION_DELETE
} from './constants';

/**************************************
 * LIST PROMOTIONS
 **************************************/

export const doFetchPromotions = () => {
  return {
    type: DO_FETCH_PROMOTIONS
  };
};

export const resolvePromotionsFetch = (promotions) => {
  return {
    type: RESOLVE_PROMOTIONS_FETCH,
    payload: promotions
  };
};

export const rejectPromotionsFetch = () => {
  return {
    type: REJECT_PROMOTIONS_FETCH
  };
};

export const doFetchProductPromotions = () => {
  return {
    type: DO_FETCH_PRODUCT_PROMOTIONS
  };
};

export const resolveProductPromotionsFetch = (promotions) => {
  return {
    type: RESOLVE_PRODUCT_PROMOTIONS_FETCH,
    payload: promotions
  };
};

export const rejectProductPromotionsFetch = () => {
  return {
    type: REJECT_PRODUCT_PROMOTIONS_FETCH
  };
};

export const doFetchTreatmentPromotions = () => {
  return {
    type: DO_FETCH_TREATMENT_PROMOTIONS
  };
};

export const resolveTreatmentPromotionsFetch = (promotions) => {
  return {
    type: RESOLVE_TREATMENT_PROMOTIONS_FETCH,
    payload: promotions
  };
};

export const rejectTreatmentPromotionsFetch = () => {
  return {
    type: REJECT_TREATMENT_PROMOTIONS_FETCH
  };
};

export const doFetchServicePromotions = () => {
  return {
    type: DO_FETCH_SERVICE_PROMOTIONS
  };
};

export const resolveServicePromotionsFetch = (promotions) => {
  return {
    type: RESOLVE_SERVICE_PROMOTIONS_FETCH,
    payload: promotions
  };
};

export const rejectServicePromotionsFetch = () => {
  return {
    type: REJECT_SERVICE_PROMOTIONS_FETCH
  };
};

/**************************************
 * GET PROMOTION
 **************************************/

export const doGetPromotion = (id) => {
  return {
    type: DO_GET_PROMOTION,
    payload: id
  };
};

export const resolvePromotionGet = (promotion) => {
  return {
    type: RESOLVE_PROMOTION_GET,
    payload: promotion
  };
};

export const rejectPromotionGet = () => {
  return {
    type: REJECT_PROMOTION_GET
  };
};

/**************************************
 * POST PROMOTION
 **************************************/

 export const doPostPromotion = (promotion, callback) => {
   return {
     type: DO_POST_PROMOTION,
     payload: promotion,
     callback
   };
 };

 export const resolvePromotionPost = () => {
   return {
     type: RESOLVE_PROMOTION_POST
   };
 };

 export const rejectPromotionPost = () => {
   return {
     type: REJECT_PROMOTION_POST
   };
 };

/**************************************
 * DELETE PROMOTION
 **************************************/

 export const doDeletePromotion = (id, callback) => {
   return {
     type: DO_DELETE_PROMOTION,
     payload: id,
     callback
   };
 };

 export const resolvePromotionDelete = () => {
   return {
     type: RESOLVE_PROMOTION_DELETE
   };
 };

 export const rejectPromotionDelete = () => {
   return {
     type: REJECT_PROMOTION_DELETE
   };
 };
