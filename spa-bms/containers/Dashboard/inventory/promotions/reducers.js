import {
  RESOLVE_PROMOTIONS_FETCH,
  REJECT_PROMOTIONS_FETCH,

  RESOLVE_PRODUCT_PROMOTIONS_FETCH,
  REJECT_PRODUCT_PROMOTIONS_FETCH,

  RESOLVE_TREATMENT_PROMOTIONS_FETCH,
  REJECT_TREATMENT_PROMOTIONS_FETCH,

  RESOLVE_SERVICE_PROMOTIONS_FETCH,
  REJECT_SERVICE_PROMOTIONS_FETCH,

  RESOLVE_PROMOTION_GET,
  REJECT_PROMOTION_GET,

  RESOLVE_PROMOTION_POST,
  REJECT_PROMOTION_POST,

  RESOLVE_PROMOTION_DELETE,
  REJECT_PROMOTION_DELETE
} from './constants';
import {
  REQUEST_API,
  RESOLVE_REQUEST,
  REJECT_REQUEST,
} from '../../constants';

const initialSatate = {
  items: [],
  products: [],
  treatments: [],
  services: [],
  item: {},
  isLoading: false,
  error: false
};

export const promotions = (
  state = initialSatate,
  action
) => {
  switch (action.type) {
    case REQUEST_API:
      return Object.assign({}, state, {isLoading: true, error: false});

    case RESOLVE_REQUEST:
      return Object.assign({}, state, {isLoading: false});

    case REJECT_REQUEST:
    case REJECT_PROMOTIONS_FETCH:
    case REJECT_PRODUCT_PROMOTIONS_FETCH:
    case REJECT_TREATMENT_PROMOTIONS_FETCH:
    case REJECT_SERVICE_PROMOTIONS_FETCH:
    case REJECT_PROMOTION_GET:
    case REJECT_PROMOTION_POST:
    case REJECT_PROMOTION_DELETE:
      return Object.assign({}, state, {isLoading: false, error: true});

    case RESOLVE_PROMOTIONS_FETCH:
      return Object.assign({}, state, {items: action.payload, isLoading: false});

    case RESOLVE_PRODUCT_PROMOTIONS_FETCH:
      return Object.assign({}, state, {products: action.payload, isLoading: false});

    case RESOLVE_TREATMENT_PROMOTIONS_FETCH:
      return Object.assign({}, state, {treatments: action.payload, isLoading: false});

    case RESOLVE_SERVICE_PROMOTIONS_FETCH:
      return Object.assign({}, state, {services: action.payload, isLoading: false});

    case RESOLVE_PROMOTION_GET:
      return Object.assign({}, state, {item: action.payload, isLoading: false});

    case RESOLVE_PROMOTION_POST:
    case RESOLVE_PROMOTION_DELETE:
      return Object.assign({}, state, {isLoading: false});

    default:
      return state;
  }
};
