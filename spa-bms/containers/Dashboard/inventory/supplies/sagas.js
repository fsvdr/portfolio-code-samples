import { call, put, takeEvery } from 'redux-saga/effects';
import { doTriggerNotification, doDeAuthenticate } from '../../../../actions';
import { mapCodeToMessage } from '../../../../helpers/api-codes';
import { fetchSupplies, getSupply, postSupply, deleteSupply } from './api';
import {
  DO_FETCH_SUPPLIES,
  DO_GET_SUPPLY,
  DO_POST_SUPPLY,
  DO_DELETE_SUPPLY
} from './constants';
import {
  resolveSuppliesFetch, rejectSuppliesFetch,
  resolveSupplyGet, rejectSupplyGet,
  resolveSupplyPost, rejectSupplyPost,
  resolveSupplyDelete, rejectSupplyDelete
} from './actions';
import {
  requestApi, resolveRequest, rejectRequest
} from '../../actions';

/**************************************
 * LIST SUPPLIES
 **************************************/

 function *handleSuppliesFetch(action) {
   // Inform the UI that a request is beign handled
   yield put(requestApi());
   try {
     // Do API request
     const response = yield call(fetchSupplies);

     if (response.status === 200) {
       // Resolve successful API request
       const data = yield response.json();
       yield put(resolveSuppliesFetch(data));
     } else if (response.status === 401) {
       // Resolve unsuccessful API request due to authorization
       // Trigger a notification and de authenticate
       yield put(rejectSuppliesFetch());
       yield put(doTriggerNotification('Sesión terminada'));
       yield put(doDeAuthenticate());
     } else {
       // Reject unsuccessful API request and trigger a notification
       const notify = mapCodeToMessage(response.status);
       yield put(rejectSuppliesFetch());
       yield put(doTriggerNotification(notify.msg, notify.state));

       // Resolve correct request
       yield put(resolveRequest());
     }
   } catch (e) {
     // Resolve unsuccessful request
     yield put(rejectRequest())
   }
 }

 /**************************************
  * GET SUPPLY
  **************************************/

function *handleSupplyGet(action) {
  // Inform the UI that a request is beign handled
  yield put(requestApi());

  try {
    // Do API request
    const response = yield call(getSupply, action.payload);

    if (response.status === 200) {
      // Resolve successful API request
      const data = yield(response.json());
      yield put(resolveSupplyGet(data));
    } else if (response.status === 401) {
      // Resolve unsuccessful API request due to authorization
      // Trigger a notification and de authenticate
      yield put(rejectSupplyGet());
      yield put(doTriggerNotification('Sesión terminada'));
      yield put(doDeAuthenticate());
    } else {
      // Reject unsuccessful API request and trigger a notification
      const notify = mapCodeToMessage(response.status);
      yield put(rejectSupplyGet());
      yield put(doTriggerNotification(notify.msg, notify.state));
    }

    // Resolve correct request
    yield put(resolveRequest());
  } catch (e) {
    // Resolve unsuccessful request
    yield put(rejectRequest());
  }
}

/**************************************
 * POST SUPPLY
 **************************************/

 function *handleSupplyPost(action) {
   // Inform the UI that a request is beign handled
   yield put(requestApi());

   // Format data for API request's body
   const data = {
     id: action.payload.id,
     name: action.payload.name,
     description: action.payload.description,
     provider: action.payload.provider,
     cost: action.payload.cost,
     price: action.payload.price,
     warningLimit: action.payload.warningLimit,
     stock: action.payload.stock,
     bonus: action.payload.bonus,
     onSale: 0 // Supplies are never on sale
   }
   let body = new FormData();
   body.append('product', JSON.stringify(data));
   body.append('image', action.payload.image);

   const notifyMsg = action.payload.id ? 'actualizó' : 'creó';

   try {
     // Do API request
     const response = yield call(postSupply, body);

     if (response.status === 200) {
       // Resolve successful API request
       yield put(resolveSupplyPost());
       yield put(doTriggerNotification(`El insumo se ${notifyMsg} exitosamente`));
       if (action.callback) action.callback();
     } else if (response.status === 401) {
       // Resolve unsuccessful API request due to authorization
       // Trigger a notification and de authenticate
       yield put(rejectSupplyPost());
       yield put(doTriggerNotification('Sesión terminada'));
       yield put(doDeAuthenticate());
     } else {
       // Reject unsuccessful API request and trigger a notification
       const notify = mapCodeToMessage(response.status);
       yield put(rejectSupplyPost());
       yield put(doTriggerNotification(notify.msg, notify.state));
     }

     // Resolve correct request
     yield put(resolveRequest());
   } catch(e) {
     // Resolve unsuccessful request
     yield put(rejectRequest());
   }
 }

 /**************************************
  * DELETE PRODUCT
  **************************************/

  function *handleSupplyDelete(action) {
    // Inform the UI that a request is beign handled
    yield put(requestApi());

    try {
      // Do API request
      const response = yield call(deleteSupply, action.payload);

      if (response.status === 200) {
        // Resolve successful API request
        yield put(resolveSupplyDelete());
        yield put(doTriggerNotification('El insumo se eliminó exitosamente'));
        if (action.callback) action.callback();
      } else if (response.status === 401) {
        // Resolve unsuccessful API request due to authorization
        // Trigger a notification and de authenticate
        yield put(rejectSupplyDelete());
        yield put(doTriggerNotification('Sesión terminada'));
        yield put(doDeAuthenticate());
      } else {
        // Reject unsuccessful API request and trigger a notification
        const notify = mapCodeToMessage(response.status);
        yield put(rejectSupplyDelete());
        yield put(doTriggerNotification(notify.msg, notify.state));
      }

      // Resolve correct request
      yield put(resolveRequest());
    } catch (e) {
      // Resolve unsuccessful request
      yield put(rejectRequest());
    }
  }





 export const suppliesSaga = [
   takeEvery(DO_FETCH_SUPPLIES, handleSuppliesFetch),
   takeEvery(DO_GET_SUPPLY, handleSupplyGet),
   takeEvery(DO_POST_SUPPLY, handleSupplyPost),
   takeEvery(DO_DELETE_SUPPLY, handleSupplyDelete)
 ];
