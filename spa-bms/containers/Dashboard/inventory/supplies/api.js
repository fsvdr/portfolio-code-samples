import { API_URL } from '../../constants';

export const fetchSupplies = () => {
  return fetch(`${API_URL}/supplies`, {
    method: 'GET',
    credentials: 'include',
    mode: 'cors'
  });
};

export const getSupply = (id) => {
  return fetch(`${API_URL}/product/${id}`, {
    method: 'GET',
    credentials: 'include'
  });
};

export const postSupply = (body) => {
  return fetch(`${API_URL}/product`, {
    method: 'POST',
    credentials: 'include',
    body
  });
};

export const deleteSupply = (id) => {
  return fetch(`${API_URL}/product/deactivate/${id}`, {
    method: 'POST',
    credentials: 'include'
  });
};
