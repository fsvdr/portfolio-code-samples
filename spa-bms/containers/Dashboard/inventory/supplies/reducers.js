import {
  RESOLVE_SUPPLIES_FETCH,
  REJECT_SUPPLIES_FETCH,

  RESOLVE_SUPPLY_GET,
  REJECT_SUPPLY_GET,

  RESOLVE_SUPPLY_POST,
  REJECT_SUPPLY_POST,

  RESOLVE_SUPPLY_DELETE,
  REJECT_SUPPLY_DELETE
} from './constants';
import {
  REQUEST_API,
  RESOLVE_REQUEST,
  REJECT_REQUEST,
} from '../../constants';

const initialSatate = {
  items: [],
  item: {},
  isLoading: false,
  error: false
};

export const supplies = (
  state = initialSatate,
  action
) => {
  switch (action.type) {
    case REQUEST_API:
      return Object.assign({}, state, {isLoading: true, error: false});

    case RESOLVE_REQUEST:
      return Object.assign({}, state, {isLoading: false});

    case REJECT_REQUEST:
    case REJECT_SUPPLIES_FETCH:
    case REJECT_SUPPLY_GET:
    case REJECT_SUPPLY_POST:
    case REJECT_SUPPLY_DELETE:
      return Object.assign({}, state, {isLoading: false, error: true});

    case RESOLVE_SUPPLIES_FETCH:
      return Object.assign({}, state, {items: action.payload, isLoading: false});

    case RESOLVE_SUPPLY_GET:
      return Object.assign({}, state, {item: action.payload, isLoading: false});

    case RESOLVE_SUPPLY_POST:
    case RESOLVE_SUPPLY_DELETE:
      return Object.assign({}, state, {isLoading: false});

    default:
      return state;
  }
};
