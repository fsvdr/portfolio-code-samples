import {
  DO_FETCH_SUPPLIES,
  RESOLVE_SUPPLIES_FETCH,
  REJECT_SUPPLIES_FETCH,

  DO_GET_SUPPLY,
  RESOLVE_SUPPLY_GET,
  REJECT_SUPPLY_GET,

  DO_POST_SUPPLY,
  RESOLVE_SUPPLY_POST,
  REJECT_SUPPLY_POST,

  DO_DELETE_SUPPLY,
  RESOLVE_SUPPLY_DELETE,
  REJECT_SUPPLY_DELETE
} from './constants';

/**************************************
 * LIST SUPPLIES
 **************************************/

export const doFetchSupplies = () => {
  return {
    type: DO_FETCH_SUPPLIES
  };
};

export const resolveSuppliesFetch = (supplies) => {
  return {
    type: RESOLVE_SUPPLIES_FETCH,
    payload: supplies
  };
};

export const rejectSuppliesFetch = () => {
  return {
    type: REJECT_SUPPLIES_FETCH
  };
};

/**************************************
 * GET SUPPLY
 **************************************/

export const doGetSupply = (id) => {
  return {
    type: DO_GET_SUPPLY,
    payload: id
  };
};

export const resolveSupplyGet = (supply) => {
  return {
    type: RESOLVE_SUPPLY_GET,
    payload: supply
  };
};

export const rejectSupplyGet = () => {
  return {
    type: REJECT_SUPPLY_GET
  };
};

/**************************************
 * POST SUPPLY
 **************************************/

 export const doPostSupply = (supply, callback) => {
   return {
     type: DO_POST_SUPPLY,
     payload: supply,
     callback
   };
 };

 export const resolveSupplyPost = () => {
   return {
     type: RESOLVE_SUPPLY_POST
   };
 };

 export const rejectSupplyPost = () => {
   return {
     type: REJECT_SUPPLY_POST
   };
 };

/**************************************
 * DELETE SUPPLY
 **************************************/

 export const doDeleteSupply = (id, callback) => {
   return {
     type: DO_DELETE_SUPPLY,
     payload: id,
     callback
   };
 };

 export const resolveSupplyDelete = () => {
   return {
     type: RESOLVE_SUPPLY_DELETE
   };
 };

 export const rejectSupplyDelete = () => {
   return {
     type: REJECT_SUPPLY_DELETE
   };
 };
