import {
  DO_FETCH_TREATMENTS,
  RESOLVE_TREATMENTS_FETCH,
  REJECT_TREATMENTS_FETCH,

  DO_GET_TREATMENT,
  RESOLVE_TREATMENT_GET,
  REJECT_TREATMENT_GET,

  DO_POST_TREATMENT,
  RESOLVE_TREATMENT_POST,
  REJECT_TREATMENT_POST,

  DO_DELETE_TREATMENT,
  RESOLVE_TREATMENT_DELETE,
  REJECT_TREATMENT_DELETE
} from './constants';

/**************************************
 * LIST TREATMENTS
 **************************************/

export const doFetchTreatments = () => {
  return {
    type: DO_FETCH_TREATMENTS
  };
};

export const resolveTreatmentsFetch = (treatments) => {
  return {
    type: RESOLVE_TREATMENTS_FETCH,
    payload: treatments
  };
};

export const rejectTreatmentsFetch = () => {
  return {
    type: REJECT_TREATMENTS_FETCH
  };
};

/**************************************
 * GET TREATMENT
 **************************************/

export const doGetTreatment = (id) => {
  return {
    type: DO_GET_TREATMENT,
    payload: id
  };
};

export const resolveTreatmentGet = (treatment) => {
  return {
    type: RESOLVE_TREATMENT_GET,
    payload: treatment
  };
};

export const rejectTreatmentGet = () => {
  return {
    type: REJECT_TREATMENT_GET
  };
};

/**************************************
 * POST TREATMENT
 **************************************/

 export const doPostTreatment = (treatment, callback) => {
   return {
     type: DO_POST_TREATMENT,
     payload: treatment,
     callback
   };
 };

 export const resolveTreatmentPost = () => {
   return {
     type: RESOLVE_TREATMENT_POST
   };
 };

 export const rejectTreatmentPost = () => {
   return {
     type: REJECT_TREATMENT_POST
   };
 };

/**************************************
 * DELETE TREATMENT
 **************************************/

 export const doDeleteTreatment = (id, callback) => {
   return {
     type: DO_DELETE_TREATMENT,
     payload: id,
     callback
   };
 };

 export const resolveTreatmentDelete = () => {
   return {
     type: RESOLVE_TREATMENT_DELETE
   };
 };

 export const rejectTreatmentDelete = () => {
   return {
     type: REJECT_TREATMENT_DELETE
   };
 };
