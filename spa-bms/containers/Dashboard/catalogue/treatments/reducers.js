import {
  RESOLVE_TREATMENTS_FETCH,
  REJECT_TREATMENTS_FETCH,

  RESOLVE_TREATMENT_GET,
  REJECT_TREATMENT_GET,

  RESOLVE_TREATMENT_POST,
  REJECT_TREATMENT_POST,

  RESOLVE_TREATMENT_DELETE,
  REJECT_TREATMENT_DELETE
} from './constants';
import {
  REQUEST_API,
  RESOLVE_REQUEST,
  REJECT_REQUEST,
} from '../../constants';

const initialSatate = {
  items: [],
  item: {},
  isLoading: false,
  error: false
};

export const treatments = (
  state = initialSatate,
  action
) => {
  switch (action.type) {
    case REQUEST_API:
      return Object.assign({}, state, {isLoading: true, error: false});

    case RESOLVE_REQUEST:
      return Object.assign({}, state, {isLoading: false});

    case REJECT_REQUEST:
    case REJECT_TREATMENTS_FETCH:
    case REJECT_TREATMENT_GET:
    case REJECT_TREATMENT_POST:
    case REJECT_TREATMENT_DELETE:
      return Object.assign({}, state, {isLoading: false, error: true});

    case RESOLVE_TREATMENTS_FETCH:
      return Object.assign({}, state, {items: action.payload, isLoading: false});

    case RESOLVE_TREATMENT_GET:
      return Object.assign({}, state, {item: action.payload, isLoading: false});

    case RESOLVE_TREATMENT_POST:
    case RESOLVE_TREATMENT_DELETE:
      return Object.assign({}, state, {isLoading: false});

    default:
      return state;
  }
};
