import { API_URL } from '../../constants';

export const fetchTreatments = () => {
  return fetch(`${API_URL}/treatments`, {
    method: 'GET',
    credentials: 'include'
  });
};

export const getTreatment = (id) => {
return fetch(`${API_URL}/treatment/${id}`, {
  method: 'GET',
  credentials: 'include'
});
};

export const postTreatment = (body) => {
return fetch(`${API_URL}/treatment`, {
  method: 'POST',
  credentials: 'include',
  body
});
};

export const deleteTreatment = (id) => {
return fetch(`${API_URL}/treatment/deactivate/${id}`, {
  method: 'POST',
  credentials: 'include'
});
};
