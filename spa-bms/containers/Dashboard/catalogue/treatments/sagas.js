import { call, put, takeEvery } from 'redux-saga/effects'
import { doTriggerNotification, doDeAuthenticate } from '../../../../actions';
import { mapCodeToMessage } from '../../../../helpers/api-codes';
import { fetchTreatments, getTreatment, postTreatment, deleteTreatment } from './api';
import {
  DO_FETCH_TREATMENTS,
  DO_GET_TREATMENT,
  DO_POST_TREATMENT,
  DO_DELETE_TREATMENT
} from './constants';
import {
  resolveTreatmentsFetch, rejectTreatmentsFetch,
  resolveTreatmentGet, rejectTreatmentGet,
  resolveTreatmentPost, rejectTreatmentPost,
  resolveTreatmentDelete, rejectTreatmentDelete
} from './actions';
import {
  requestApi, resolveRequest, rejectRequest
} from '../../actions';

/**************************************
 * LIST TREATMENTS
 **************************************/

function *handleTreatmentsFetch(action) {
  // Inform the UI that a request is beign handled
  yield put(requestApi());

  try {
    // Do API request
    const response = yield call(fetchTreatments);

    if (response.status === 200) {
      // Resolve successful API request
      const data = yield response.json();
      yield put(resolveTreatmentsFetch(data));
    } else if (response.status === 401) {
      // Resolve unsuccessful API request due to authorization
      // Trigger a notification and de authenticate
      yield put(rejectTreatmentsFetch());
      yield put(doTriggerNotification('Sesión terminada'));
      yield put(doDeAuthenticate());
    } else {
      // Reject unsuccessful API request and trigger a notification
      const notify = mapCodeToMessage(response.status);
      yield put(rejectTreatmentsFetch());
      yield put(doTriggerNotification(notify.msg, notify.state));
    }

    // Resolve correct request
    yield put(resolveRequest());
  } catch (e) {
    // Resolve unsuccessful request
    yield put(rejectRequest());
  }
}

/**************************************
 * GET TREATMENT
 **************************************/

function *handleTreatmentGet(action) {
  // Inform the UI that a request is beign handled
  yield put(requestApi());

  try {
    // Do API request
    const response = yield call(getTreatment, action.payload);

    if (response.status === 200) {
      // Resolve successful API request
      const data = yield(response.json());
      yield put(resolveTreatmentGet(data));
    } else if (response.status === 401) {
      // Resolve unsuccessful API request due to authorization
      // Trigger a notification and de authenticate
      yield put(rejectTreatmentGet());
      yield put(doTriggerNotification('Sesión terminada'));
      yield put(doDeAuthenticate());
    } else {
      // Reject unsuccessful API request and trigger a notification
      const notify = mapCodeToMessage(response.status);
      yield put(rejectTreatmentGet());
      yield put(doTriggerNotification(notify.msg, notify.state));
    }

    // Resolve correct request
    yield put(resolveRequest());
  } catch (e) {
    // Resolve unsuccessful request
    yield put(rejectRequest());
  }
}

/**************************************
 * POST TREATMENT
 **************************************/

function *handleTreatmentPost(action) {
 // Inform the UI that a request is beign handled
 yield put(requestApi());

 // Format data for API request's body
 let data = {
   id: action.payload.id,
   name: action.payload.name,
   description: action.payload.description,
   price: action.payload.price,
   sessions: action.payload.sessions,
   comments: action.payload.comments,
   gift: action.payload.gift,
   trial: action.payload.trial,
   equivalents: action.payload.equivalents,
 }

 let body = new FormData();
 body.append('treatment', JSON.stringify(data));
 body.append('image', action.payload.image);

const notifyMsg = action.payload.id ? 'actualizó' : 'creó';

 try {
   // Do API request
   const response = yield call(postTreatment, body);

   if (response.status === 200) {
     // Resolve successful API request
     yield put(resolveTreatmentPost());
     yield put(doTriggerNotification(`El tratamiento se ${notifyMsg} exitosamente`));
     if (action.callback) action.callback();
   } else if (response.status === 401) {
     // Resolve unsuccessful API request due to authorization
     // Trigger a notification and de authenticate
     yield put(rejectTreatmentPost());
     yield put(doTriggerNotification('Sesión terminada'));
     yield put(doDeAuthenticate());
   } else {
     // Reject unsuccessful API request and trigger a notification
     const notify = mapCodeToMessage(response.status);
     yield put(rejectTreatmentPost());
     yield put(doTriggerNotification(notify.msg, notify.state));
   }

   // Resolve correct request
   yield put(resolveRequest());
 } catch (e) {
   // Resolve unsuccessful request
   yield put(rejectRequest());
 }
}

 /**************************************
  * DELETE TREATMENT
  **************************************/

  function *handleTreatmentDelete(action) {
    // Inform the UI that a request is beign handled
    yield put(requestApi());

    try {
      // Do API request
      const response = yield call(deleteTreatment, action.payload);

      if (response.status === 200) {
        // Resolve successful API request
        yield put(resolveTreatmentDelete());
        yield put(doTriggerNotification('El tratamiento se eliminó exitosamente'));
        if (action.callback) action.callback();
      } else if (response.status === 401) {
        // Resolve unsuccessful API request due to authorization
        // Trigger a notification and de authenticate
        yield put(rejectTreatmentDelete());
        yield put(doTriggerNotification('Sesión terminada'));
        yield put(doDeAuthenticate());
      } else {
        // Reject unsuccessful API request and trigger a notification
        const notify = mapCodeToMessage(response.status);
        yield put(rejectTreatmentDelete());
        yield put(doTriggerNotification(notify.msg, notify.state));
      }

      // Resolve correct request
      yield put(resolveRequest());
    } catch (e) {
      // Resolve unsuccessful request
      yield put(rejectRequest());
    }
  }





  export const treatmentsSaga = [
    takeEvery(DO_FETCH_TREATMENTS, handleTreatmentsFetch),
    takeEvery(DO_GET_TREATMENT, handleTreatmentGet),
    takeEvery(DO_POST_TREATMENT, handleTreatmentPost),
    takeEvery(DO_DELETE_TREATMENT, handleTreatmentDelete)
  ];
