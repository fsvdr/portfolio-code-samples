import {
  DO_FETCH_PRODUCTS,
  RESOLVE_PRODUCTS_FETCH,
  REJECT_PRODUCTS_FETCH,

  DO_GET_PRODUCT,
  RESOLVE_PRODUCT_GET,
  REJECT_PRODUCT_GET,

  DO_POST_PRODUCT,
  RESOLVE_PRODUCT_POST,
  REJECT_PRODUCT_POST,

  DO_DELETE_PRODUCT,
  RESOLVE_PRODUCT_DELETE,
  REJECT_PRODUCT_DELETE
} from './constants';

/**************************************
 * LIST PRODUCTS
 **************************************/

export const doFetchProducts = () => {
  return {
    type: DO_FETCH_PRODUCTS
  };
};

export const resolveProductsFetch = (products) => {
  return {
    type: RESOLVE_PRODUCTS_FETCH,
    payload: products
  };
};

export const rejectProductsFetch = () => {
  return {
    type: REJECT_PRODUCTS_FETCH
  };
};

/**************************************
 * GET PRODUCT
 **************************************/

export const doGetProduct = (id) => {
  return {
    type: DO_GET_PRODUCT,
    payload: id
  };
};

export const resolveProductGet = (product) => {
  return {
    type: RESOLVE_PRODUCT_GET,
    payload: product
  };
};

export const rejectProductGet = () => {
  return {
    type: REJECT_PRODUCT_GET
  };
};

/**************************************
 * POST PRODUCT
 **************************************/

 export const doPostProduct = (product, callback) => {
   return {
     type: DO_POST_PRODUCT,
     payload: product,
     callback
   };
 };

 export const resolveProductPost = () => {
   return {
     type: RESOLVE_PRODUCT_POST
   };
 };

 export const rejectProductPost = () => {
   return {
     type: REJECT_PRODUCT_POST
   };
 };

/**************************************
 * DELETE PRODUCT
 **************************************/

 export const doDeleteProduct = (id, callback) => {
   return {
     type: DO_DELETE_PRODUCT,
     payload: id,
     callback: callback
   };
 };

 export const resolveProductDelete = () => {
   return {
     type: RESOLVE_PRODUCT_DELETE
   };
 };

 export const rejectProductDelete = () => {
   return {
     type: REJECT_PRODUCT_DELETE
   };
 };
