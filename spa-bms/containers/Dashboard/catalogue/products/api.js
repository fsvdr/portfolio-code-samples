import { API_URL } from '../../constants';

export const fetchProducts = () => {
  return fetch(`${API_URL}/products`, {
    method: 'GET',
    credentials: 'include',
    mode: 'cors'
  });
};

export const getProduct = (id) => {
  return fetch(`${API_URL}/product/${id}`, {
    method: 'GET',
    credentials: 'include'
  });
};

export const postProduct = (body) => {
  return fetch(`${API_URL}/product`, {
    method: 'POST',
    credentials: 'include',
    body
  });
};

export const deleteProduct = (id) => {
  return fetch(`${API_URL}/product/deactivate/${id}`, {
    method: 'POST',
    credentials: 'include'
  });
};
