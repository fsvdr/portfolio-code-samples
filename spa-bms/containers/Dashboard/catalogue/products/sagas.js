import { call, put, takeEvery } from 'redux-saga/effects'
import { doTriggerNotification, doDeAuthenticate } from '../../../../actions';
import { mapCodeToMessage } from '../../../../helpers/api-codes';
import { fetchProducts, getProduct, postProduct, deleteProduct } from './api';
import {
  DO_FETCH_PRODUCTS,
  DO_GET_PRODUCT,
  DO_POST_PRODUCT,
  DO_DELETE_PRODUCT
} from './constants';
import {
  resolveProductsFetch, rejectProductsFetch,
  resolveProductGet, rejectProductGet,
  resolveProductPost, rejectProductPost,
  resolveProductDelete, rejectProductDelete
} from './actions';
import {
  requestApi, resolveRequest, rejectRequest
} from '../../actions';

/**************************************
 * LIST PRODUCTS
 **************************************/

function *handleProductsFetch(action) {
  // Inform the UI that a request is beign handled
  yield put(requestApi());
  try {
    // Do API request
    const response = yield call(fetchProducts);

    if (response.status === 200) {
      // Resolve successful API request
      const data = yield response.json();
      yield put(resolveProductsFetch(data));
    } else if (response.status === 401) {
      // Resolve unsuccessful API request due to authorization
      // Trigger a notification and de authenticate
      yield put(rejectProductsFetch());
      yield put(doTriggerNotification('Sesión terminada'));
      yield put(doDeAuthenticate());
    } else {
      // Reject unsuccessful API request and trigger a notification
      const notify = mapCodeToMessage(response.status);
      yield put(rejectProductsFetch());
      yield put(doTriggerNotification(notify.msg, notify.state));
    }

    // Resolve correct request
    yield put(resolveRequest());
  } catch (e) {
    // Resolve unsuccessful request
    yield put(rejectRequest());
  }
}

/**************************************
 * GET PRODUCT
 **************************************/

function *handleProductGet(action) {
  // Inform the UI that a request is beign handled
  yield put(requestApi());

  try {
    // Do API request
    const response = yield call(getProduct, action.payload);

    if (response.status === 200) {
      // Resolve successful API request
      const data = yield(response.json());
      yield put(resolveProductGet(data));
    } else if (response.status === 401) {
      // Resolve unsuccessful API request due to authorization
      // Trigger a notification and de authenticate
      yield put(rejectProductGet());
      yield put(doTriggerNotification('Sesión terminada'));
      yield put(doDeAuthenticate());
    } else {
      // Reject unsuccessful API request and trigger a notification
      const notify = mapCodeToMessage(response.status);
      yield put(rejectProductGet());
      yield put(doTriggerNotification(notify.msg, notify.state));
    }

    // Resolve correct request
    yield put(resolveRequest());
  } catch (e) {
    // Resolve unsuccessful request
    yield put(rejectRequest());
  }
}

/**************************************
 * POST PRODUCT
 **************************************/

function *handleProductPost(action) {
 // Inform the UI that a request is beign handled
 yield put(requestApi());

 // Format data for API request's body
 const data = {
   id: action.payload.id,
   name: action.payload.name,
   description: action.payload.description,
   provider: action.payload.provider,
   cost: action.payload.cost,
   price: action.payload.price,
   warningLimit: action.payload.warningLimit,
   stock: action.payload.stock,
   bonus: action.payload.bonus,
   onSale: 1 // Products are always on sale
 }
 let body = new FormData();
 body.append('product', JSON.stringify(data));
 body.append('image', action.payload.image);

const notifyMsg = action.payload.id ? 'actualizó' : 'creó';

 try {
   // Do API request
   const response = yield call(postProduct, body);

   if (response.status === 200) {
     // Resolve successful API request
     yield put(resolveProductPost());
     yield put(doTriggerNotification(`El producto se ${notifyMsg} exitosamente`));
     if (action.callback) action.callback();
   } else if (response.status === 401) {
     // Resolve unsuccessful API request due to authorization
     // Trigger a notification and de authenticate
     yield put(rejectProductPost());
     yield put(doTriggerNotification('Sesión terminada'));
     yield put(doDeAuthenticate());
   } else {
     // Reject unsuccessful API request and trigger a notification
     const notify = mapCodeToMessage(response.status);
     yield put(rejectProductPost());
     yield put(doTriggerNotification(notify.msg, notify.state));
   }

   // Resolve correct request
   yield put(resolveRequest());
 } catch (e) {
   // Resolve unsuccessful request
   yield put(rejectRequest());
 }
}

 /**************************************
  * DELETE PRODUCT
  **************************************/

  function *handleProductDelete(action) {
    // Inform the UI that a request is beign handled
    yield put(requestApi());

    try {
      // Do API request
      const response = yield call(deleteProduct, action.payload);

      if (response.status === 200) {
        // Resolve successful API request
        yield put(resolveProductDelete());
        yield put(doTriggerNotification('El producto se eliminó exitosamente'));
        if (action.callback) action.callback();
      } else if (response.status === 401) {
        // Resolve unsuccessful API request due to authorization
        // Trigger a notification and de authenticate
        yield put(rejectProductDelete());
        yield put(doTriggerNotification('Sesión terminada'));
        yield put(doDeAuthenticate());
      } else {
        // Reject unsuccessful API request and trigger a notification
        const notify = mapCodeToMessage(response.status);
        yield put(rejectProductDelete());
        yield put(doTriggerNotification(notify.msg, notify.state));
      }

      // Resolve correct request
      yield put(resolveRequest());
    } catch (e) {
      // Resolve unsuccessful request
      yield put(rejectRequest());
    }
  }





  export const productsSaga = [
    takeEvery(DO_FETCH_PRODUCTS, handleProductsFetch),
    takeEvery(DO_GET_PRODUCT, handleProductGet),
    takeEvery(DO_POST_PRODUCT, handleProductPost),
    takeEvery(DO_DELETE_PRODUCT, handleProductDelete)
  ];
