import {
  RESOLVE_PRODUCTS_FETCH,
  REJECT_PRODUCTS_FETCH,

  RESOLVE_PRODUCT_GET,
  REJECT_PRODUCT_GET,

  RESOLVE_PRODUCT_POST,
  REJECT_PRODUCT_POST,

  RESOLVE_PRODUCT_DELETE,
  REJECT_PRODUCT_DELETE
} from './constants';
import {
  REQUEST_API,
  RESOLVE_REQUEST,
  REJECT_REQUEST,
} from '../../constants';

const initialSatate = {
  items: [],
  item: {},
  isLoading: false,
  error: false
};

export const products = (
  state = initialSatate,
  action
) => {
  switch (action.type) {
    case REQUEST_API:
      return Object.assign({}, state, {isLoading: true, error: false});

    case RESOLVE_REQUEST:
      return Object.assign({}, state, {isLoading: false});

    case REJECT_REQUEST:
    case REJECT_PRODUCTS_FETCH:
    case REJECT_PRODUCT_GET:
    case REJECT_PRODUCT_POST:
    case REJECT_PRODUCT_DELETE:
      return Object.assign({}, state, {isLoading: false, error: true});

    case RESOLVE_PRODUCTS_FETCH:
      return Object.assign({}, state, {items: action.payload, isLoading: false});

    case RESOLVE_PRODUCT_GET:
      return Object.assign({}, state, {item: action.payload, isLoading: false});

    case RESOLVE_PRODUCT_POST:
    case RESOLVE_PRODUCT_DELETE:
      return Object.assign({}, state, {isLoading: false});

    default:
      return state;
  }
};
