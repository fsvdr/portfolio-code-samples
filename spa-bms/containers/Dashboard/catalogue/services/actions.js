import {
  DO_FETCH_SERVICES,
  RESOLVE_SERVICES_FETCH,
  REJECT_SERVICES_FETCH,

  DO_GET_SERVICE,
  RESOLVE_SERVICE_GET,
  REJECT_SERVICE_GET,

  DO_POST_SERVICE,
  RESOLVE_SERVICE_POST,
  REJECT_SERVICE_POST,

  DO_DELETE_SERVICE,
  RESOLVE_SERVICE_DELETE,
  REJECT_SERVICE_DELETE
} from './constants';

/**************************************
 * LIST SERVICES
 **************************************/

export const doFetchServices = () => {
  return {
    type: DO_FETCH_SERVICES
  };
};

export const resolveServicesFetch = (services) => {
  return {
    type: RESOLVE_SERVICES_FETCH,
    payload: services
  };
};

export const rejectServicesFetch = () => {
  return {
    type: REJECT_SERVICES_FETCH
  };
};

/**************************************
 * GET SERVICE
 **************************************/

export const doGetService = (id) => {
  return {
    type: DO_GET_SERVICE,
    payload: id
  };
};

export const resolveServiceGet = (service) => {
  return {
    type: RESOLVE_SERVICE_GET,
    payload: service
  };
};

export const rejectServiceGet = () => {
  return {
    type: REJECT_SERVICE_GET
  };
};

/**************************************
 * POST SERVICE
 **************************************/

 export const doPostService = (service, callback) => {
   return {
     type: DO_POST_SERVICE,
     payload: service,
     callback
   };
 };

 export const resolveServicePost = () => {
   return {
     type: RESOLVE_SERVICE_POST
   };
 };

 export const rejectServicePost = () => {
   return {
     type: REJECT_SERVICE_POST
   };
 };

/**************************************
 * DELETE SERVICE
 **************************************/

 export const doDeleteService = (id, callback) => {
   return {
     type: DO_DELETE_SERVICE,
     payload: id,
     callback
   };
 };

 export const resolveServiceDelete = () => {
   return {
     type: RESOLVE_SERVICE_DELETE
   };
 };

 export const rejectServiceDelete = () => {
   return {
     type: REJECT_SERVICE_DELETE
   };
 };
