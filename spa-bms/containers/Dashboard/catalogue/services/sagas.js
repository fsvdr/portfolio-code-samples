import { call, put, takeEvery } from 'redux-saga/effects'
import { doTriggerNotification, doDeAuthenticate } from '../../../../actions';
import { mapCodeToMessage } from '../../../../helpers/api-codes';
import { fetchServices, getService, postService, deleteService } from './api';
import {
  DO_FETCH_SERVICES,
  DO_GET_SERVICE,
  DO_POST_SERVICE,
  DO_DELETE_SERVICE
} from './constants';
import {
  resolveServicesFetch, rejectServicesFetch,
  resolveServiceGet, rejectServiceGet,
  resolveServicePost, rejectServicePost,
  resolveServiceDelete, rejectServiceDelete
} from './actions';
import {
  requestApi, resolveRequest, rejectRequest
} from '../../actions';

/**************************************
 * LIST SERVICES
 **************************************/

function *handleServicesFetch(action) {
  // Inform the UI that a request is beign handled
  yield put(requestApi());

  try {
    // Do API request
    const response = yield call(fetchServices);

    if (response.status === 200) {
      // Resolve successful API request
      const data = yield response.json();
      yield put(resolveServicesFetch(data));
    } else if (response.status === 401) {
      // Resolve unsuccessful API request due to authorization
      // Trigger a notification and de authenticate
      yield put(rejectServicesFetch());
      yield put(doTriggerNotification('Sesión terminada'));
      yield put(doDeAuthenticate());
    } else {
      // Reject unsuccessful API request and trigger a notification
      const notify = mapCodeToMessage(response.status);
      yield put(rejectServicesFetch());
      yield put(doTriggerNotification(notify.msg, notify.state));
    }

    // Resolve correct request
    yield put(resolveRequest());
  } catch (e) {
    // Resolve unsuccessful request
    yield put(rejectRequest());
  }
}

/**************************************
 * GET SERVICE
 **************************************/

function *handleServiceGet(action) {
  // Inform the UI that a request is beign handled
  yield put(requestApi());

  try {
    // Do API request
    const response = yield call(getService, action.payload);

    if (response.status === 200) {
      // Resolve successful API request
      const data = yield(response.json());
      yield put(resolveServiceGet(data));
    } else if (response.status === 401) {
      // Resolve unsuccessful API request due to authorization
      // Trigger a notification and de authenticate
      yield put(rejectServiceGet());
      yield put(doTriggerNotification('Sesión terminada'));
      yield put(doDeAuthenticate());
    } else {
      // Reject unsuccessful API request and trigger a notification
      const notify = mapCodeToMessage(response.status);
      yield put(rejectServiceGet());
      yield put(doTriggerNotification(notify.msg, notify.state));
    }

    // Resolve correct request
    yield put(resolveRequest());
  } catch (e) {
    // Resolve unsuccessful request
    yield put(rejectRequest());
  }
}

/**************************************
 * POST SERVICE
 **************************************/

function *handleServicePost(action) {
 // Inform the UI that a request is beign handled
 yield put(requestApi());

 // Format data for API request's body
 const data = {
   id: action.payload.id,
   name: action.payload.name,
   description: action.payload.description,
   price: action.payload.price,
   sessions: action.payload.sessions,
   bonus: action.payload.bonus,
   comments: action.payload.comments,
   gift: action.payload.gift,
   treatments: action.payload.treatments,
 }
 let body = new FormData();
 body.append('service', JSON.stringify(data));
 body.append('image', action.payload.image);

const notifyMsg = action.payload.id ? 'actualizó' : 'creó';

 try {
   // Do API request
   const response = yield call(postService, body);

   if (response.status === 200) {
     // Resolve successful API request
     yield put(resolveServicePost());
     yield put(doTriggerNotification(`El paquete se ${notifyMsg} exitosamente`));
     if (action.callback) action.callback();
   } else if (response.status === 401) {
     // Resolve unsuccessful API request due to authorization
     // Trigger a notification and de authenticate
     yield put(rejectServicePost());
     yield put(doTriggerNotification('Sesión terminada'));
     yield put(doDeAuthenticate());
   } else {
     // Reject unsuccessful API request and trigger a notification
     const notify = mapCodeToMessage(response.status);
     yield put(rejectServicePost());
     yield put(doTriggerNotification(notify.msg, notify.state));
   }

   // Resolve correct request
   yield put(resolveRequest());
 } catch (e) {
   // Resolve unsuccessful request
   yield put(rejectRequest());
 }
}

 /**************************************
  * DELETE SERVICE
  **************************************/

  function *handleServiceDelete(action) {
    // Inform the UI that a request is beign handled
    yield put(requestApi());

    try {
      // Do API request
      const response = yield call(deleteService, action.payload);

      if (response.status === 200) {
        // Resolve successful API request
        yield put(resolveServiceDelete());
        yield put(doTriggerNotification('El paquete se eliminó exitosamente'));
        if (action.callback) action.callback();
      } else if (response.status === 401) {
        // Resolve unsuccessful API request due to authorization
        // Trigger a notification and de authenticate
        yield put(rejectServiceDelete());
        yield put(doTriggerNotification('Sesión terminada'));
        yield put(doDeAuthenticate());
      } else {
        // Reject unsuccessful API request and trigger a notification
        const notify = mapCodeToMessage(response.status);
        yield put(rejectServiceDelete());
        yield put(doTriggerNotification(notify.msg, notify.state));
      }

      // Resolve correct request
      yield put(resolveRequest());
    } catch (e) {
      // Resolve unsuccessful request
      yield put(rejectRequest());
    }
  }





  export const servicesSaga = [
    takeEvery(DO_FETCH_SERVICES, handleServicesFetch),
    takeEvery(DO_GET_SERVICE, handleServiceGet),
    takeEvery(DO_POST_SERVICE, handleServicePost),
    takeEvery(DO_DELETE_SERVICE, handleServiceDelete)
  ];
