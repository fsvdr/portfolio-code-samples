import { API_URL } from '../../constants';

export const fetchServices = () => {
  return fetch(`${API_URL}/services`, {
    method: 'GET',
    credentials: 'include'
  });
};

export const getService = (id) => {
  return fetch(`${API_URL}/service/${id}`, {
    method: 'GET',
    credentials: 'include'
  });
};

export const postService = (body) => {
  return fetch(`${API_URL}/service`, {
    method: 'POST',
    credentials: 'include',
    body
  })
};

export const deleteService = (id) => {
  return fetch(`${API_URL}/service/deactivate/${id}`, {
    method: 'POST',
    credentials: 'include'
  });
};
