import {
  RESOLVE_SERVICES_FETCH,
  REJECT_SERVICES_FETCH,

  RESOLVE_SERVICE_GET,
  REJECT_SERVICE_GET,

  RESOLVE_SERVICE_POST,
  REJECT_SERVICE_POST,

  RESOLVE_SERVICE_DELETE,
  REJECT_SERVICE_DELETE
} from './constants';
import {
  REQUEST_API,
  RESOLVE_REQUEST,
  REJECT_REQUEST,
} from '../../constants';

const initialSatate = {
  items: [],
  item: {},
  isLoading: false,
  error: false
};

export const services = (
  state = initialSatate,
  action
) => {
  switch (action.type) {
    case REQUEST_API:
      return Object.assign({}, state, {isLoading: true, error: false});

    case RESOLVE_REQUEST:
      return Object.assign({}, state, {isLoading: false});

    case REJECT_REQUEST:
    case REJECT_SERVICES_FETCH:
    case REJECT_SERVICE_GET:
    case REJECT_SERVICE_POST:
    case REJECT_SERVICE_DELETE:
      return Object.assign({}, state, {isLoading: false, error: true});

    case RESOLVE_SERVICES_FETCH:
      return Object.assign({}, state, {items: action.payload, isLoading: false});

    case RESOLVE_SERVICE_GET:
      return Object.assign({}, state, {item: action.payload, isLoading: false});

    case RESOLVE_SERVICE_POST:
    case RESOLVE_SERVICE_DELETE:
      return Object.assign({}, state, {isLoading: false});

    default:
      return state;
  }
};
