export * from './catalogue/products/sagas';
export * from './catalogue/treatments/sagas';
export * from './catalogue/services/sagas';

export * from './inventory/supplies/sagas';
export * from './inventory/orders/sagas';
export * from './inventory/promotions/sagas';
export * from './inventory/providers/sagas';

export * from './staff/staffs/sagas';
export * from './staff/users/sagas';
export * from './staff/bonuses/sagas';

export * from './registry/clients/sagas';
export * from './registry/sales/sagas';

export * from './stats/earnings/sagas';

export * from './shopping-cart/sagas';
