export * from './catalogue/products/reducers';
export * from './catalogue/treatments/reducers';
export * from './catalogue/services/reducers';

export * from './inventory/supplies/reducers';
export * from './inventory/orders/reducers';
export * from './inventory/promotions/reducers';
export * from './inventory/providers/reducers';

export * from './staff/staffs/reducers';
export * from './staff/users/reducers';
export * from './staff/bonuses/reducers';

export * from './registry/clients/reducers';
export * from './registry/sales/reducers';

export * from './stats/earnings/reducers';

export * from './shopping-cart/reducers';
