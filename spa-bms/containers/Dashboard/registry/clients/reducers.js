import {
  RESOLVE_CLIENTS_FETCH,
  REJECT_CLIENTS_FETCH,

  RESOLVE_CLIENT_GET,
  REJECT_CLIENT_GET,

  RESOLVE_CLIENT_POST,
  REJECT_CLIENT_POST,

  RESOLVE_CLIENT_DELETE,
  REJECT_CLIENT_DELETE
} from './constants';
import {
  REQUEST_API,
  RESOLVE_REQUEST,
  REJECT_REQUEST,
} from '../../constants';

const initialSatate = {
  items: [],
  item: {},
  isLoading: false,
  error: false
};

export const clients = (
  state = initialSatate,
  action
) => {
  switch (action.type) {
    case REQUEST_API:
      return Object.assign({}, state, {isLoading: true, error: false});

    case RESOLVE_REQUEST:
      return Object.assign({}, state, {isLoading: false});

    case REJECT_REQUEST:
    case REJECT_CLIENTS_FETCH:
    case REJECT_CLIENT_GET:
    case REJECT_CLIENT_POST:
    case REJECT_CLIENT_DELETE:
      return Object.assign({}, state, {isLoading: false, error: true});

    case RESOLVE_CLIENTS_FETCH:
      return Object.assign({}, state, {items: action.payload, isLoading: false});

    case RESOLVE_CLIENT_GET:
      return Object.assign({}, state, {item: action.payload, isLoading: false});

    case RESOLVE_CLIENT_POST:
    case RESOLVE_CLIENT_DELETE:
      return Object.assign({}, state, {isLoading: false});

    default:
      return state;
  }
};
