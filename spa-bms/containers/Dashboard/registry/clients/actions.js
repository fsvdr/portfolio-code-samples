import {
  DO_FETCH_CLIENTS,
  RESOLVE_CLIENTS_FETCH,
  REJECT_CLIENTS_FETCH,

  DO_GET_CLIENT,
  RESOLVE_CLIENT_GET,
  REJECT_CLIENT_GET,

  DO_POST_CLIENT,
  RESOLVE_CLIENT_POST,
  REJECT_CLIENT_POST,

  DO_DELETE_CLIENT,
  RESOLVE_CLIENT_DELETE,
  REJECT_CLIENT_DELETE
} from './constants';

/**************************************
 * LIST CLIENTS
 **************************************/

export const doFetchClients = () => {
  return {
    type: DO_FETCH_CLIENTS
  };
};

export const resolveClientsFetch = (clients) => {
  return {
    type: RESOLVE_CLIENTS_FETCH,
    payload: clients
  };
};

export const rejectClientsFetch = () => {
  return {
    type: REJECT_CLIENTS_FETCH
  };
};

/**************************************
 * GET CLIENT
 **************************************/

export const doGetClient = (id) => {
  return {
    type: DO_GET_CLIENT,
    payload: id
  };
};

export const resolveClientGet = (client) => {
  return {
    type: RESOLVE_CLIENT_GET,
    payload: client
  };
};

export const rejectClientGet = () => {
  return {
    type: REJECT_CLIENT_GET
  };
};

/**************************************
 * POST CLIENT
 **************************************/

 export const doPostClient = (client, callback) => {
   return {
     type: DO_POST_CLIENT,
     payload: client,
     callback
   };
 };

 export const resolveClientPost = () => {
   return {
     type: RESOLVE_CLIENT_POST
   };
 };

 export const rejectClientPost = () => {
   return {
     type: REJECT_CLIENT_POST
   };
 };

/**************************************
 * DELETE CLIENT
 **************************************/

 export const doDeleteClient = (id, callback) => {
   return {
     type: DO_DELETE_CLIENT,
     payload: id,
     callback: callback
   };
 };

 export const resolveClientDelete = () => {
   return {
     type: RESOLVE_CLIENT_DELETE
   };
 };

 export const rejectClientDelete = () => {
   return {
     type: REJECT_CLIENT_DELETE
   };
 };
