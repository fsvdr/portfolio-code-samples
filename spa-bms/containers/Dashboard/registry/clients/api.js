import { API_URL } from '../../constants';

export const fetchClients = () => {
  return fetch(`${API_URL}/clients`, {
    method: 'GET',
    credentials: 'include',
    mode: 'cors'
  });
};

export const getClient = (id) => {
  return fetch(`${API_URL}/client/${id}`, {
    method: 'GET',
    credentials: 'include'
  });
};

export const postClient = (body) => {
  return fetch(`${API_URL}/client`, {
    method: 'POST',
    credentials: 'include',
    body
  });
};

export const deleteClient = (id) => {
  return fetch(`${API_URL}/client/deactivate/${id}`, {
    method: 'POST',
    credentials: 'include'
  });
};
