import { call, put, takeEvery } from 'redux-saga/effects'
import { doTriggerNotification, doDeAuthenticate } from '../../../../actions';
import { mapCodeToMessage } from '../../../../helpers/api-codes';
import { fetchClients, getClient, postClient, deleteClient } from './api';
import {
  DO_FETCH_CLIENTS,
  DO_GET_CLIENT,
  DO_POST_CLIENT,
  DO_DELETE_CLIENT
} from './constants';
import {
  resolveClientsFetch, rejectClientsFetch,
  resolveClientGet, rejectClientGet,
  resolveClientPost, rejectClientPost,
  resolveClientDelete, rejectClientDelete
} from './actions';
import {
  requestApi, resolveRequest, rejectRequest
} from '../../actions';

/**************************************
 * LIST CLIENTS
 **************************************/

function *handleClientsFetch(action) {
  // Inform the UI that a request is beign handled
  yield put(requestApi());
  try {
    // Do API request
    const response = yield call(fetchClients);

    if (response.status === 200) {
      // Resolve successful API request
      const data = yield response.json();
      yield put(resolveClientsFetch(data));
    } else if (response.status === 401) {
      // Resolve unsuccessful API request due to authorization
      // Trigger a notification and de authenticate
      yield put(rejectClientsFetch());
      yield put(doTriggerNotification('Sesión terminada'));
      yield put(doDeAuthenticate());
    } else {
      // Reject unsuccessful API request and trigger a notification
      const notify = mapCodeToMessage(response.status);
      yield put(rejectClientsFetch());
      yield put(doTriggerNotification(notify.msg, notify.state));
    }

    // Resolve correct request
    yield put(resolveRequest());
  } catch (e) {
    // Resolve unsuccessful request
    yield put(rejectRequest());
  }
}

/**************************************
 * GET CLIENT
 **************************************/

function *handleClientGet(action) {
  // Inform the UI that a request is beign handled
  yield put(requestApi());

  try {
    // Do API request
    const response = yield call(getClient, action.payload);

    if (response.status === 200) {
      // Resolve successful API request
      const data = yield(response.json());
      yield put(resolveClientGet(data));
    } else if (response.status === 401) {
      // Resolve unsuccessful API request due to authorization
      // Trigger a notification and de authenticate
      yield put(rejectClientGet());
      yield put(doTriggerNotification('Sesión terminada'));
      yield put(doDeAuthenticate());
    } else {
      // Reject unsuccessful API request and trigger a notification
      const notify = mapCodeToMessage(response.status);
      yield put(rejectClientGet());
      yield put(doTriggerNotification(notify.msg, notify.state));
    }

    // Resolve correct request
    yield put(resolveRequest());
  } catch (e) {
    // Resolve unsuccessful request
    yield put(rejectRequest());
  }
}

/**************************************
 * POST CLIENT
 **************************************/

function *handleClientPost(action) {
 // Inform the UI that a request is beign handled
 yield put(requestApi());

 // Format data for API request's body
 const data = {
   id: action.payload.id,
   name: action.payload.name,
   birthday: action.payload.birthday,
   email: action.payload.email,
   phone: action.payload.phone,
   cellphone: action.payload.cellphone,
   rfc: action.payload.rfc,
   address: action.payload.address,
   media: action.payload.media,
   prospect: action.payload.prospect,
   referer: action.payload.referer
 }
 let body = new FormData();
 body.append('client', JSON.stringify(data));

const notifyMsg = action.payload.id ? 'actualizó' : 'creó';

 try {
   // Do API request
   const response = yield call(postClient, body);

   if (response.status === 200) {
     // Resolve successful API request
     yield put(resolveClientPost());
     yield put(doTriggerNotification(`El cliente se ${notifyMsg} exitosamente`));
     if (action.callback) action.callback();
   } else if (response.status === 401) {
     // Resolve unsuccessful API request due to authorization
     // Trigger a notification and de authenticate
     yield put(rejectClientPost());
     yield put(doTriggerNotification('Sesión terminada'));
     yield put(doDeAuthenticate());
   } else {
     // Reject unsuccessful API request and trigger a notification
     const notify = mapCodeToMessage(response.status);
     yield put(rejectClientPost());
     yield put(doTriggerNotification(notify.msg, notify.state));
   }

   // Resolve correct request
   yield put(resolveRequest());
 } catch (e) {
   // Resolve unsuccessful request
   yield put(rejectRequest());
 }
}

 /**************************************
  * DELETE CLIENT
  **************************************/

  function *handleClientDelete(action) {
    // Inform the UI that a request is beign handled
    yield put(requestApi());

    try {
      // Do API request
      const response = yield call(deleteClient, action.payload);

      if (response.status === 200) {
        // Resolve successful API request
        yield put(resolveClientDelete());
        yield put(doTriggerNotification('El cliente se eliminó exitosamente'));
        if (action.callback) action.callback();
      } else if (response.status === 401) {
        // Resolve unsuccessful API request due to authorization
        // Trigger a notification and de authenticate
        yield put(rejectClientDelete());
        yield put(doTriggerNotification('Sesión terminada'));
        yield put(doDeAuthenticate());
      } else {
        // Reject unsuccessful API request and trigger a notification
        const notify = mapCodeToMessage(response.status);
        yield put(rejectClientDelete());
        yield put(doTriggerNotification(notify.msg, notify.state));
      }

      // Resolve correct request
      yield put(resolveRequest());
    } catch (e) {
      // Resolve unsuccessful request
      yield put(rejectRequest());
    }
  }





  export const clientsSaga = [
    takeEvery(DO_FETCH_CLIENTS, handleClientsFetch),
    takeEvery(DO_GET_CLIENT, handleClientGet),
    takeEvery(DO_POST_CLIENT, handleClientPost),
    takeEvery(DO_DELETE_CLIENT, handleClientDelete)
  ];
