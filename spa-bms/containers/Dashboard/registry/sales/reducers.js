import {
  RESOLVE_SALES_FETCH,
  REJECT_SALES_FETCH,

  RESOLVE_SALE_GET,
  REJECT_SALE_GET,

  RESOLVE_SALE_POST,
  REJECT_SALE_POST,

  RESOLVE_SALE_DELETE,
  REJECT_SALE_DELETE
} from './constants';
import {
  REQUEST_API,
  RESOLVE_REQUEST,
  REJECT_REQUEST,
} from '../../constants';

const initialSatate = {
  items: [],
  item: {},
  isLoading: false,
  error: false
};

export const sales = (
  state = initialSatate,
  action
) => {
  switch (action.type) {
    case REQUEST_API:
      return Object.assign({}, state, {isLoading: true, error: false});

    case RESOLVE_REQUEST:
      return Object.assign({}, state, {isLoading: false});

    case REJECT_REQUEST:
    case REJECT_SALES_FETCH:
    case REJECT_SALE_GET:
    case REJECT_SALE_POST:
    case REJECT_SALE_DELETE:
      return Object.assign({}, state, {isLoading: false, error: true});

    case RESOLVE_SALES_FETCH:
      return Object.assign({}, state, {items: action.payload, isLoading: false});

    case RESOLVE_SALE_GET:
      return Object.assign({}, state, {item: action.payload, isLoading: false});

    case RESOLVE_SALE_POST:
    case RESOLVE_SALE_DELETE:
      return Object.assign({}, state, {isLoading: false});

    default:
      return state;
  }
};
