import { API_URL } from '../../constants';

export const fetchSales = (from, to) => {
  return fetch(`${API_URL}/sells/from/${from}/to/${to}`, {
    method: 'GET',
    credentials: 'include',
    mode: 'cors'
  });
};

export const getSale = (id) => {
  return fetch(`${API_URL}/sell/${id}`, {
    method: 'GET',
    credentials: 'include'
  });
};

export const postSale = (body) => {
  return fetch(`${API_URL}/sell`, {
    method: 'POST',
    credentials: 'include',
    body
  });
};

export const deleteSale = (id) => {
  return fetch(`${API_URL}/sell/deactivate/${id}`, {
    method: 'POST',
    credentials: 'include'
  });
};
