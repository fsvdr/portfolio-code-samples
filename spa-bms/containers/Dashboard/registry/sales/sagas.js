import { call, put, takeEvery } from 'redux-saga/effects'
import { doTriggerNotification, doDeAuthenticate } from '../../../../actions';
import { mapCodeToMessage } from '../../../../helpers/api-codes';
import { fetchSales, getSale, postSale, deleteSale } from './api';
import {
  DO_FETCH_SALES,
  DO_GET_SALE,
  DO_POST_SALE,
  DO_DELETE_SALE
} from './constants';
import {
  resolveSalesFetch, rejectSalesFetch,
  resolveSaleGet, rejectSaleGet,
  resolveSalePost, rejectSalePost,
  resolveSaleDelete, rejectSaleDelete
} from './actions';
import {
  requestApi, resolveRequest, rejectRequest
} from '../../actions';

/**************************************
 * LIST SALES
 **************************************/

function *handleSalesFetch(action) {
  // Inform the UI that a request is beign handled
  yield put(requestApi());
  try {
    // Do API request
    const response = yield call(fetchSales, action.from, action.to);

    if (response.status === 200) {
      // Resolve successful API request
      const data = yield response.json();
      yield put(resolveSalesFetch(data));
    } else if (response.status === 401) {
      // Resolve unsuccessful API request due to authorization
      // Trigger a notification and de authenticate
      yield put(rejectSalesFetch());
      yield put(doTriggerNotification('Sesión terminada'));
      yield put(doDeAuthenticate());
    } else {
      // Reject unsuccessful API request and trigger a notification
      const notify = mapCodeToMessage(response.status);
      yield put(rejectSalesFetch());
      yield put(doTriggerNotification(notify.msg, notify.state));
    }

    // Resolve correct request
    yield put(resolveRequest());
  } catch (e) {
    // Resolve unsuccessful request
    yield put(rejectRequest());
  }
}

/**************************************
 * GET SALE
 **************************************/

function *handleSaleGet(action) {
  // Inform the UI that a request is beign handled
  yield put(requestApi());

  try {
    // Do API request
    const response = yield call(getSale, action.payload);

    if (response.status === 200) {
      // Resolve successful API request
      const data = yield(response.json());
      yield put(resolveSaleGet(data));
    } else if (response.status === 401) {
      // Resolve unsuccessful API request due to authorization
      // Trigger a notification and de authenticate
      yield put(rejectSaleGet());
      yield put(doTriggerNotification('Sesión terminada'));
      yield put(doDeAuthenticate());
    } else {
      // Reject unsuccessful API request and trigger a notification
      const notify = mapCodeToMessage(response.status);
      yield put(rejectSaleGet());
      yield put(doTriggerNotification(notify.msg, notify.state));
    }

    // Resolve correct request
    yield put(resolveRequest());
  } catch (e) {
    // Resolve unsuccessful request
    yield put(rejectRequest());
  }
}

/**************************************
 * POST SALE
 **************************************/

function *handleSalePost(action) {
 // Inform the UI that a request is beign handled
 yield put(requestApi());

 // Format data for API request's body
 const data = {
   id: action.payload.id,
   client: action.payload.client,
   staff: action.payload.staff,
   prospection: action.payload.prospection,
   payments: action.payload.payments,
   products: action.payload.products.map((i) => {return {id: i.id, name: i.name, quantity: i.quantity}}),
   treatments: action.payload.treatments.map((i) => {return {id: i.id, name: i.name, quantity: i.quantity}}),
   services: action.payload.services.map((i) => {return {id: i.id, name: i.name, quantity: i.quantity}}),
   promotions: action.payload.promotions.map((i) => i.id)
 }
 let body = new FormData();
 body.append('sell', JSON.stringify(data));

const notifyMsg = action.payload.id ? 'actualizó' : 'creó';

 try {
   // Do API request
   const response = yield call(postSale, body);

   if (response.status === 200) {
     // Resolve successful API request
     yield put(resolveSalePost());
     yield put(doTriggerNotification(`La venta se ${notifyMsg} exitosamente`));
     if (action.callback) action.callback();
   } else if (response.status === 401) {
     // Resolve unsuccessful API request due to authorization
     // Trigger a notification and de authenticate
     yield put(rejectSalePost());
     yield put(doTriggerNotification('Sesión terminada'));
     yield put(doDeAuthenticate());
   } else {
     // Reject unsuccessful API request and trigger a notification
     const notify = mapCodeToMessage(response.status);
     yield put(rejectSalePost());
     yield put(doTriggerNotification(notify.msg, notify.state));
   }

   // Resolve correct request
   yield put(resolveRequest());
 } catch (e) {
   // Resolve unsuccessful request
   yield put(rejectRequest());
 }
}

 /**************************************
  * DELETE SALE
  **************************************/

  function *handleSaleDelete(action) {
    // Inform the UI that a request is beign handled
    yield put(requestApi());

    try {
      // Do API request
      const response = yield call(deleteSale, action.payload);

      if (response.status === 200) {
        // Resolve successful API request
        yield put(resolveSaleDelete());
        yield put(doTriggerNotification('La venta se eliminó exitosamente'));
        if (action.callback) action.callback();
      } else if (response.status === 401) {
        // Resolve unsuccessful API request due to authorization
        // Trigger a notification and de authenticate
        yield put(rejectSaleDelete());
        yield put(doTriggerNotification('Sesión terminada'));
        yield put(doDeAuthenticate());
      } else {
        // Reject unsuccessful API request and trigger a notification
        const notify = mapCodeToMessage(response.status);
        yield put(rejectSaleDelete());
        yield put(doTriggerNotification(notify.msg, notify.state));
      }

      // Resolve correct request
      yield put(resolveRequest());
    } catch (e) {
      // Resolve unsuccessful request
      yield put(rejectRequest());
    }
  }





  export const salesSaga = [
    takeEvery(DO_FETCH_SALES, handleSalesFetch),
    takeEvery(DO_GET_SALE, handleSaleGet),
    takeEvery(DO_POST_SALE, handleSalePost),
    takeEvery(DO_DELETE_SALE, handleSaleDelete)
  ];
