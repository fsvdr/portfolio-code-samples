import {
  DO_FETCH_SALES,
  RESOLVE_SALES_FETCH,
  REJECT_SALES_FETCH,

  DO_GET_SALE,
  RESOLVE_SALE_GET,
  REJECT_SALE_GET,

  DO_POST_SALE,
  RESOLVE_SALE_POST,
  REJECT_SALE_POST,

  DO_DELETE_SALE,
  RESOLVE_SALE_DELETE,
  REJECT_SALE_DELETE,

  SYSTEM_INIT_DATE
} from './constants';

/**************************************
 * LIST SALES
 **************************************/

export const doFetchSales = (from = SYSTEM_INIT_DATE, to = new Date().toISOString().split('T')[0]) => {
  return {
    type: DO_FETCH_SALES,
    from,
    to
  };
};

export const resolveSalesFetch = (sales) => {
  return {
    type: RESOLVE_SALES_FETCH,
    payload: sales
  };
};

export const rejectSalesFetch = () => {
  return {
    type: REJECT_SALES_FETCH
  };
};

/**************************************
 * GET SALE
 **************************************/

export const doGetSale = (id) => {
  return {
    type: DO_GET_SALE,
    payload: id
  };
};

export const resolveSaleGet = (sale) => {
  return {
    type: RESOLVE_SALE_GET,
    payload: sale
  };
};

export const rejectSaleGet = () => {
  return {
    type: REJECT_SALE_GET
  };
};

/**************************************
 * POST SALE
 **************************************/

 export const doPostSale = (sale, callback) => {
   return {
     type: DO_POST_SALE,
     payload: sale,
     callback
   };
 };

 export const resolveSalePost = () => {
   return {
     type: RESOLVE_SALE_POST
   };
 };

 export const rejectSalePost = () => {
   return {
     type: REJECT_SALE_POST
   };
 };

/**************************************
 * DELETE SALE
 **************************************/

 export const doDeleteSale = (id, callback) => {
   return {
     type: DO_DELETE_SALE,
     payload: id,
     callback: callback
   };
 };

 export const resolveSaleDelete = () => {
   return {
     type: RESOLVE_SALE_DELETE
   };
 };

 export const rejectSaleDelete = () => {
   return {
     type: REJECT_SALE_DELETE
   };
 };
