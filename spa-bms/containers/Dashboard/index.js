import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import './index.scss';
import { navLinks } from './data';
import { Route, NavLink, withRouter } from 'react-router-dom';

/**************************************
 * CATALOGUE
 **************************************/

import ProductsList from '../../components/Catalogue/ProductsList';
import TreatmentsList from '../../components/Catalogue/TreatmentsList';
import ServicesList from '../../components/Catalogue/ServicesList';

import {
  doFetchProducts, doGetProduct, doPostProduct, doDeleteProduct
} from './catalogue/products/actions';
import {
  doFetchTreatments, doGetTreatment, doPostTreatment, doDeleteTreatment
} from './catalogue/treatments/actions';
import {
  doFetchServices, doGetService, doPostService, doDeleteService
} from './catalogue/services/actions';

/**************************************
 * INVENTORY
 **************************************/

import SuppliesList from '../../components/Inventory/SuppliesList';
import OrdersList from '../../components/Inventory/OrdersList';
import PromotionsList from '../../components/Inventory/PromotionsList';
import ProvidersList from '../../components/Inventory/ProvidersList';

import {
  doFetchSupplies, doGetSupply, doPostSupply, doDeleteSupply
} from './inventory/supplies/actions';
import {
  doFetchOrders, doGetOrder, doPostOrder, doCompleteOrder, doDeleteOrder
} from './inventory/orders/actions';
import {
  doFetchPromotions, doFetchProductPromotions, doFetchTreatmentPromotions,
  doFetchServicePromotions, doGetPromotion, doPostPromotion, doDeletePromotion
} from './inventory/promotions/actions';
import {
  doFetchProviders, doGetProvider, doPostProvider, doDeleteProvider
} from './inventory/providers/actions';

/**************************************
 * STAFF
 **************************************/

import StaffsList from '../../components/Staff/StaffsList';
import UsersList from '../../components/Staff/UsersList';
import BonusesList from '../../components/Staff/BonusesList';

 import {
   doFetchStaffs, doGetStaff, doPostStaff, doDeleteStaff
 } from './staff/staffs/actions';
 import {
   doFetchUsers, doGetUser, doPostUser, doDeleteUser
 } from './staff/users/actions';
 import {
   doFetchBonuses, doGetBonus, doPostBonus, doDeleteBonus
 } from './staff/bonuses/actions';

 /**************************************
  * REGISTRY
  **************************************/

import ClientsList from '../../components/Registry/ClientsList';
import SalesList from '../../components/Registry/SalesList';

import {
  doFetchClients, doGetClient, doPostClient, doDeleteClient
} from './registry/clients/actions';
import {
  doFetchSales, doGetSale, doPostSale, doDeleteSale
} from './registry/sales/actions';

/**************************************
 * STATS
 **************************************/

import EarningsList from '../../components/Stats/EarningsList';

import {
  doFetchEarnings, doPostEarning
} from './stats/earnings/actions';

/**************************************
 * SHOPPING CART
 **************************************/

import CartWidget from '../../components/ShoppingCart/CartWidget';
import ShoppingCart from '../../components/ShoppingCart';

 import {
   doClearCart, doSetClient, doSetStaff, doSetProspection,
   doAddPayment, doRemovePayment, doAddProduct, doRemoveProduct,
   doAddTreatment, doRemoveTreatment, doAddService, doRemoveService,
   doAddPromotion, doRemovePromotion,
   doRequestTotal
 } from './shopping-cart/actions';

class Dashboard extends Component {

  constructor(props) {
    super(props);

    this.state = {
      menu: {
        open: false,
        toggle: 0,
        section: 0,
        link: 0
      },
    };

    this.toggleMenu = this.toggleMenu.bind(this);
    this.toggleSection = this.toggleSection.bind(this);
  }

  componentWillMount() {
    this.props.history.push('/catalogo/productos');
    this.unlisten = this.props.history.listen((location, action) => {
      const menu = this.state.menu;
      const path = location.pathname.split('/'); // ['', 'section', 'link']

      if (path[1] !== 'carrito') {
        const section = navLinks.find((s) => s.route === path[1]);
        const link = section.links.find((l) => l.route === path[2]);

        menu['section'] = section.id;
        menu['link'] = link.id;
        this.setState({menu});
      }
    });
  }

  componentWillUnmount() {
    this.unlisten();
  }

  toggleMenu() {
    const menu = this.state.menu;
    menu['open'] = !menu.open;

    this.setState({menu});
  }

  toggleSection(section) {
    const menu = this.state.menu;
    menu['toggle'] = section;

    this.setState({menu});
  }

  render() {
    const { menu } = this.state;
    const { catalogue, inventory, staff, registry, stats, cart } = this.props;

    return (
      <div className="c-dashboard o-app-background">
        <div className="c-dashboard__panel o-grid--vertical o-grid-wrap--no">
          <nav className={`c-dashboard-navigation o-grid__child u-12of12
            ${menu.open ? 'is-open' : ''}`}>
            <ul className="c-dashboard-navigation__list">
              { /* MENU SECTIONING */
                navLinks.map((s) => (
                    <li className={`c-dashboard-navigation__section
                      ${menu.toggle === s.id ? 'is-toggled' : ''}
                      ${menu.section === s.id ? 'is-active' : ''}`}
                      key={s.id}>
                      <span
                        onClick={() => this.toggleSection(s.id)}
                        className="o-nav-section-title">{s.name}</span>
                      <ul className="c-dashboard-navigation__content">
                      { /* SECTION LINKS */
                        s.links.map((l) => (
                          <li className={`o-nav-section-item
                            ${menu.section === s.id && menu.link === l.id ? 'is-active' : ''}`}
                            key={l.id}>
                            <NavLink to={`/${s.route}/${l.route}`}>{l.name}</NavLink>
                          </li>
                        ))
                      }
                      </ul>
                    </li>
                ))
              }
              <li className={`c-dashboard-navigation__section is-toggled`}>
                <ul className="c-dashboard-navigation__content">
                  <li className={`o-nav-section-item`}>
                    <button onClick={this.props.doLogout} className="c-dashboard-navigation__action-btn">Cerrar sesión</button>
                  </li>
                </ul>
              </li>
            </ul>
            <button onClick={() => this.toggleMenu()} className="c-dashboard-navigation__toggler"><i className={`${menu.open ? 'o-icon-chevron-up--small' : 'o-icon-chevron-down--small'}`}></i></button>
          </nav>

          <CartWidget
            cart={cart}
            handleCartClear={this.props.apiCart.clearCart}
            />

        </div>

        <div className="c-dashboard__main">

          {/****************
            * CATALOGUE
            ****************/}

          <Route path="/catalogo/productos" render={(match) => (
            <ProductsList
              products={catalogue.products}
              providers={inventory.providers}
              api={this.props.apiProducts}
              requestProviders={this.props.apiProviders.fetch}
              addToCart={this.props.apiCart.addProductToCart}
              route={match}/>
          )}/>

          <Route path="/catalogo/tratamientos" render={(match) => (
            <TreatmentsList
              treatments={catalogue.treatments}
              products={catalogue.products}
              api={this.props.apiTreatments}
              requestProducts={this.props.apiProducts.fetch}
              addToCart={this.props.apiCart.addTreatmentToCart}
              route={match}/>
          )}/>

          <Route path="/catalogo/paquetes" render={(match) => (
            <ServicesList
              services={catalogue.services}
              products={catalogue.products}
              treatments={catalogue.treatments}
              api={this.props.apiServices}
              requestProducts={this.props.apiProducts.fetch}
              requestTreatments={this.props.apiTreatments.fetch}
              addToCart={this.props.apiCart.addServiceToCart}
              route={match} />
          )}/>

          {/****************
            * INVENTORY
            ****************/}

          <Route path="/inventario/insumos" render={(match) => (
            <SuppliesList
              supplies={inventory.supplies}
              providers={inventory.providers}
              api={this.props.apiSupplies}
              requestProviders={this.props.apiProviders.fetch}
              route={match}/>
          )}/>

          <Route path="/inventario/pedidos" render={(match) => (
            <OrdersList
              orders={inventory.orders}
              products={catalogue.products}
              api={this.props.apiOrders}
              requestProducts={this.props.apiProducts.fetch}
              route={match}/>
          )}/>

          <Route path="/inventario/promociones" render={(match) => (
            <PromotionsList
              promotions={inventory.promotions}
              products={catalogue.products}
              treatments={catalogue.treatments}
              services={catalogue.services}
              api={this.props.apiPromotions}
              requestProducts={this.props.apiProducts.fetch}
              requestTreatments={this.props.apiTreatments.fetch}
              requestServices={this.props.apiServices.fetch}
              route={match}/>
          )}/>

          <Route path="/inventario/proveedores" render={(match) => (
            <ProvidersList
              providers={inventory.providers}
              api={this.props.apiProviders}
              route={match}/>
          )}/>

          {/****************
            * STAFF
            ****************/}

          <Route path="/personal/empleados" render={(match) => (
            <StaffsList
              staffs={staff.staffs}
              bonuses={staff.bonuses}
              api={this.props.apiStaffs}
              requestBonuses={this.props.apiBonuses.fetch}
              route={match}/>
          )}/>

          <Route path="/personal/usuarios" render={(match) => (
            <UsersList
              users={staff.users}
              staffs={staff.staffs}
              api={this.props.apiUsers}
              requestStaffs={this.props.apiStaffs.fetch}
              route={match}/>
          )}/>

          <Route path="/personal/prestaciones" render={(match) => (
            <BonusesList
              bonuses={staff.bonuses}
              api={this.props.apiBonuses}
              route={match}/>
          )}/>

          {/****************
            * REGISTRY
            ****************/}

          <Route path="/registro/clientes" render={(match) => (
            <ClientsList
              clients={registry.clients}
              api={this.props.apiClients}
              route={match}/>
          )}/>

          <Route path="/registro/ventas" render={(match) => (
            <SalesList
              sales={registry.sales}
              api={this.props.apiSales}
              route={match}/>
          )}/>

          {/****************
            * REGISTRY
            ****************/}
          <Route path="/contabilidad/ganancias" render={(match) => (
            <EarningsList
              earnings={stats.earnings}
              api={this.props.apiEarnings}
              route={match}/>
          )}/>

          {/****************
            * SHOPPING CART
            ****************/}
          <Route path="/carrito" render={(match) => (
            <ShoppingCart
              cart={cart}
              clients={registry.clients}
              staffs={staff.staffs}
              api={this.props.apiCart}
              handlePost={this.props.apiSales.post}
              requestClients={this.props.apiClients.fetch}
              requestStaffs={this.props.apiStaffs.fetch}
              route={match}/>
          )}/>

        </div>
      </div>
    );
  }
}

Dashboard.propTypes = {
  doLogout: PropTypes.func.isRequired
};

const mapStateToProps = (state) => {
  return {
    catalogue: Object.assign({}, null, {products: state.products, treatments: state.treatments, services: state.services}),
    inventory: Object.assign({}, null, {supplies: state.supplies, orders: state.orders, promotions: state.promotions, providers: state.providers}),
    staff: Object.assign({}, null, {staffs: state.staffs, users: state.users, bonuses: state.bonuses}),
    registry: Object.assign({}, null, {clients: state.clients, sales: state.sales}),
    stats: Object.assign({}, null, {earnings: state.earnings}),
    cart: state.shoppingCart
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    apiProducts: Object.assign({}, {fetch: () => dispatch(doFetchProducts()), get: (id) => dispatch(doGetProduct(id)), post: (product, callback) => dispatch(doPostProduct(product, callback)), delete: (id, callback) => dispatch(doDeleteProduct(id, callback))}),
    apiTreatments: Object.assign({}, {fetch: () => dispatch(doFetchTreatments()), get: (id) => dispatch(doGetTreatment(id)), post: (treatment, callback) => dispatch(doPostTreatment(treatment, callback)), delete: (id, callback) => dispatch(doDeleteTreatment(id, callback))}),
    apiServices: Object.assign({}, {fetch: () => dispatch(doFetchServices()), get: (id) => dispatch(doGetService(id)), post: (service, callback) => dispatch(doPostService(service, callback)), delete: (id, callback) => dispatch(doDeleteService(id, callback))}),

    apiSupplies: Object.assign({}, {fetch: () => dispatch(doFetchSupplies()), get: (id) => dispatch(doGetSupply(id)), post: (supply, callback) => dispatch(doPostSupply(supply, callback)), delete: (id, callback) => dispatch(doDeleteSupply(id, callback))}),
    apiOrders: Object.assign({}, {fetch: () => dispatch(doFetchOrders()), get: (id) => dispatch(doGetOrder(id)), post: (order, callback) => dispatch(doPostOrder(order, callback)), complete: (id, callback) => dispatch(doCompleteOrder(id, callback)), delete: (id, callback) => dispatch(doDeleteOrder(id, callback))}),
    apiPromotions: Object.assign({}, {
      fetch: () => dispatch(doFetchPromotions()), fetchProductPromotions: () => dispatch(doFetchProductPromotions()), fetchTreatmentPromotions: () => dispatch(doFetchTreatmentPromotions()), fetchServicePromotions: () => dispatch(doFetchServicePromotions()),
      get: (id) => dispatch(doGetPromotion(id)), post: (promotion, callback) => dispatch(doPostPromotion(promotion, callback)), delete: (id, callback) => dispatch(doDeletePromotion(id, callback))}),
    apiProviders: Object.assign({}, {fetch: () => dispatch(doFetchProviders()), get: (id) => dispatch(doGetProvider(id)), post: (provider, callback) => dispatch(doPostProvider(provider, callback)), delete: (id, callback) => dispatch(doDeleteProvider(id, callback))}),

    apiStaffs: Object.assign({}, {fetch: () => dispatch(doFetchStaffs()), get: (id) => dispatch(doGetStaff(id)), post: (staff, callback) => dispatch(doPostStaff(staff, callback)), delete: (id, callback) => dispatch(doDeleteStaff(id, callback))}),
    apiUsers: Object.assign({}, {fetch: () => dispatch(doFetchUsers()), get: (id) => dispatch(doGetUser(id)), post: (user, callback) => dispatch(doPostUser(user, callback)), delete: (id, callback) => dispatch(doDeleteUser(id, callback))}),
    apiBonuses: Object.assign({}, {fetch: () => dispatch(doFetchBonuses()), get: (id) => dispatch(doGetBonus(id)), post: (bonus, callback) => dispatch(doPostBonus(bonus, callback)), delete: (id, callback) => dispatch(doDeleteBonus(id, callback))}),

    apiClients: Object.assign({}, {fetch: () => dispatch(doFetchClients()), get: (id) => dispatch(doGetClient(id)), post: (client, callback) => dispatch(doPostClient(client, callback)), delete: (id, callback) => dispatch(doDeleteClient(id, callback))}),
    apiSales: Object.assign({}, {fetch: (from, to) => dispatch(doFetchSales(from, to)), get: (id) => dispatch(doGetSale(id)), post: (sale, callback) => dispatch(doPostSale(sale, callback)), delete: (id, callback) => dispatch(doDeleteSale(id, callback))}),

    apiEarnings: Object.assign({}, {fetch: () => dispatch(doFetchEarnings()), post: (earnings, callback) => dispatch(doPostEarning(earnings, callback))}),

    apiCart: Object.assign({}, {
      clearCart: () => dispatch(doClearCart()),
      setClientToCart: (id) => dispatch(doSetClient(id)), setStaffToCart: (id) => dispatch(doSetStaff(id)), setProspectionToCart: (id) => dispatch(doSetProspection(id)),
      addPaymentToCart: (payment) => dispatch(doAddPayment(payment)), removePaymentFromCart: (index) => dispatch(doRemovePayment(index)),
      addProductToCart: (product) => dispatch(doAddProduct(product)), removeProductFromCart: (index) => dispatch(doRemoveProduct(index)),
      addTreatmentToCart: (treatment) => dispatch(doAddTreatment(treatment)), removeTreatmentFromCart: (index) => dispatch(doRemoveTreatment(index)),
      addServiceToCart: (service) => dispatch(doAddService(service)), removeServiceFromCart: (index) => dispatch(doRemoveService(index)),
      addPromotionToCart: (promotion) => dispatch(doAddPromotion(promotion)), removePromotionFromCart: (index) => dispatch(doRemovePromotion(index)),
      requestTotal: (sale, callback) => dispatch(doRequestTotal(sale, callback))
    })
  };
}

Dashboard = connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard);

export default withRouter(Dashboard);
