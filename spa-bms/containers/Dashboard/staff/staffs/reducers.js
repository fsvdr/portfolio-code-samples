import {
  RESOLVE_STAFFS_FETCH,
  REJECT_STAFFS_FETCH,

  RESOLVE_STAFF_GET,
  REJECT_STAFF_GET,

  RESOLVE_STAFF_POST,
  REJECT_STAFF_POST,

  RESOLVE_STAFF_DELETE,
  REJECT_STAFF_DELETE
} from './constants';
import {
  REQUEST_API,
  RESOLVE_REQUEST,
  REJECT_REQUEST,
} from '../../constants';

const initialSatate = {
  items: [],
  item: {},
  isLoading: false,
  error: false
};

export const staffs = (
  state = initialSatate,
  action
) => {
  switch (action.type) {
    case REQUEST_API:
      return Object.assign({}, state, {isLoading: true, error: false});

    case RESOLVE_REQUEST:
      return Object.assign({}, state, {isLoading: false});

    case REJECT_REQUEST:
    case REJECT_STAFFS_FETCH:
    case REJECT_STAFF_GET:
    case REJECT_STAFF_POST:
    case REJECT_STAFF_DELETE:
      return Object.assign({}, state, {isLoading: false, error: true});

    case RESOLVE_STAFFS_FETCH:
      return Object.assign({}, state, {items: action.payload, isLoading: false});

    case RESOLVE_STAFF_GET:
      return Object.assign({}, state, {item: action.payload, isLoading: false});

    case RESOLVE_STAFF_POST:
    case RESOLVE_STAFF_DELETE:
      return Object.assign({}, state, {isLoading: false});

    default:
      return state;
  }
};
