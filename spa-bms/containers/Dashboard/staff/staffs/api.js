import { API_URL } from '../../constants';

export const fetchStaffs = () => {
  return fetch(`${API_URL}/staff`, {
    method: 'GET',
    credentials: 'include',
    mode: 'cors'
  });
};

export const getStaff = (id) => {
  return fetch(`${API_URL}/staff/${id}`, {
    method: 'GET',
    credentials: 'include'
  });
};

export const postStaff = (body) => {
  return fetch(`${API_URL}/staff`, {
    method: 'POST',
    credentials: 'include',
    body
  });
};

export const deleteStaff = (id) => {
  return fetch(`${API_URL}/staff/deactivate/${id}`, {
    method: 'POST',
    credentials: 'include'
  });
};
