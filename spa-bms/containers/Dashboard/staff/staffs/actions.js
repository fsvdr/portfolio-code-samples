import {
  DO_FETCH_STAFFS,
  RESOLVE_STAFFS_FETCH,
  REJECT_STAFFS_FETCH,

  DO_GET_STAFF,
  RESOLVE_STAFF_GET,
  REJECT_STAFF_GET,

  DO_POST_STAFF,
  RESOLVE_STAFF_POST,
  REJECT_STAFF_POST,

  DO_DELETE_STAFF,
  RESOLVE_STAFF_DELETE,
  REJECT_STAFF_DELETE
} from './constants';

/**************************************
 * LIST STAFFS
 **************************************/

export const doFetchStaffs = () => {
  return {
    type: DO_FETCH_STAFFS
  };
};

export const resolveStaffsFetch = (staffs) => {
  return {
    type: RESOLVE_STAFFS_FETCH,
    payload: staffs
  };
};

export const rejectStaffsFetch = () => {
  return {
    type: REJECT_STAFFS_FETCH
  };
};

/**************************************
 * GET STAFF
 **************************************/

export const doGetStaff = (id) => {
  return {
    type: DO_GET_STAFF,
    payload: id
  };
};

export const resolveStaffGet = (staff) => {
  return {
    type: RESOLVE_STAFF_GET,
    payload: staff
  };
};

export const rejectStaffGet = () => {
  return {
    type: REJECT_STAFF_GET
  };
};

/**************************************
 * POST STAFF
 **************************************/

 export const doPostStaff = (staff, callback) => {
   return {
     type: DO_POST_STAFF,
     payload: staff,
     callback
   };
 };

 export const resolveStaffPost = () => {
   return {
     type: RESOLVE_STAFF_POST
   };
 };

 export const rejectStaffPost = () => {
   return {
     type: REJECT_STAFF_POST
   };
 };

/**************************************
 * DELETE STAFF
 **************************************/

 export const doDeleteStaff = (id, callback) => {
   return {
     type: DO_DELETE_STAFF,
     payload: id,
     callback: callback
   };
 };

 export const resolveStaffDelete = () => {
   return {
     type: RESOLVE_STAFF_DELETE
   };
 };

 export const rejectStaffDelete = () => {
   return {
     type: REJECT_STAFF_DELETE
   };
 };
