import { call, put, takeEvery } from 'redux-saga/effects'
import { doTriggerNotification, doDeAuthenticate } from '../../../../actions';
import { mapCodeToMessage } from '../../../../helpers/api-codes';
import { fetchStaffs, getStaff, postStaff, deleteStaff } from './api';
import {
  DO_FETCH_STAFFS,
  DO_GET_STAFF,
  DO_POST_STAFF,
  DO_DELETE_STAFF
} from './constants';
import {
  resolveStaffsFetch, rejectStaffsFetch,
  resolveStaffGet, rejectStaffGet,
  resolveStaffPost, rejectStaffPost,
  resolveStaffDelete, rejectStaffDelete
} from './actions';
import {
  requestApi, resolveRequest, rejectRequest
} from '../../actions';

/**************************************
 * LIST STAFFS
 **************************************/

function *handleStaffsFetch(action) {
  // Inform the UI that a request is beign handled
  yield put(requestApi());
  try {
    // Do API request
    const response = yield call(fetchStaffs);

    if (response.status === 200) {
      // Resolve successful API request
      const data = yield response.json();
      yield put(resolveStaffsFetch(data));
    } else if (response.status === 401) {
      // Resolve unsuccessful API request due to authorization
      // Trigger a notification and de authenticate
      yield put(rejectStaffsFetch());
      yield put(doTriggerNotification('Sesión terminada'));
      yield put(doDeAuthenticate());
    } else {
      // Reject unsuccessful API request and trigger a notification
      const notify = mapCodeToMessage(response.status);
      yield put(rejectStaffsFetch());
      yield put(doTriggerNotification(notify.msg, notify.state));
    }

    // Resolve correct request
    yield put(resolveRequest());
  } catch (e) {
    // Resolve unsuccessful request
    yield put(rejectRequest());
  }
}

/**************************************
 * GET STAFF
 **************************************/

function *handleStaffGet(action) {
  // Inform the UI that a request is beign handled
  yield put(requestApi());

  try {
    // Do API request
    const response = yield call(getStaff, action.payload);

    if (response.status === 200) {
      // Resolve successful API request
      const data = yield(response.json());
      yield put(resolveStaffGet(data));
    } else if (response.status === 401) {
      // Resolve unsuccessful API request due to authorization
      // Trigger a notification and de authenticate
      yield put(rejectStaffGet());
      yield put(doTriggerNotification('Sesión terminada'));
      yield put(doDeAuthenticate());
    } else {
      // Reject unsuccessful API request and trigger a notification
      const notify = mapCodeToMessage(response.status);
      yield put(rejectStaffGet());
      yield put(doTriggerNotification(notify.msg, notify.state));
    }

    // Resolve correct request
    yield put(resolveRequest());
  } catch (e) {
    // Resolve unsuccessful request
    yield put(rejectRequest());
  }
}

/**************************************
 * POST STAFF
 **************************************/

function *handleStaffPost(action) {
 // Inform the UI that a request is beign handled
 yield put(requestApi());

 // Format data for API request's body
 const data = {
   id: action.payload.id,
   name: action.payload.name,
   salary: action.payload.salary,
   address: action.payload.address,
   phone: action.payload.phone,
   cellphone: action.payload.cellphone,
   rfc: action.payload.rfc,
   imss: action.payload.imss,
   curp: action.payload.curp,
   email: action.payload.email,
   title: action.payload.title,
   objective: action.payload.objective,
   objectiveBonus: action.payload.objectiveBonus,
   bonus: action.payload.bonus
 }
 let body = new FormData();
 body.append('staff', JSON.stringify(data));

const notifyMsg = action.payload.id ? 'actualizó' : 'creó';

 try {
   // Do API request
   const response = yield call(postStaff, body);

   if (response.status === 200) {
     // Resolve successful API request
     yield put(resolveStaffPost());
     yield put(doTriggerNotification(`El empleado se ${notifyMsg} exitosamente`));
     if (action.callback) action.callback();
   } else if (response.status === 401) {
     // Resolve unsuccessful API request due to authorization
     // Trigger a notification and de authenticate
     yield put(rejectStaffPost());
     yield put(doTriggerNotification('Sesión terminada'));
     yield put(doDeAuthenticate());
   } else {
     // Reject unsuccessful API request and trigger a notification
     const notify = mapCodeToMessage(response.status);
     yield put(rejectStaffPost());
     yield put(doTriggerNotification(notify.msg, notify.state));
   }

   // Resolve correct request
   yield put(resolveRequest());
 } catch (e) {
   // Resolve unsuccessful request
   yield put(rejectRequest());
 }
}

 /**************************************
  * DELETE STAFF
  **************************************/

  function *handleStaffDelete(action) {
    // Inform the UI that a request is beign handled
    yield put(requestApi());

    try {
      // Do API request
      const response = yield call(deleteStaff, action.payload);

      if (response.status === 200) {
        // Resolve successful API request
        yield put(resolveStaffDelete());
        yield put(doTriggerNotification('El empleado se eliminó exitosamente'));
        if (action.callback) action.callback();
      } else if (response.status === 401) {
        // Resolve unsuccessful API request due to authorization
        // Trigger a notification and de authenticate
        yield put(rejectStaffDelete());
        yield put(doTriggerNotification('Sesión terminada'));
        yield put(doDeAuthenticate());
      } else {
        // Reject unsuccessful API request and trigger a notification
        const notify = mapCodeToMessage(response.status);
        yield put(rejectStaffDelete());
        yield put(doTriggerNotification(notify.msg, notify.state));
      }

      // Resolve correct request
      yield put(resolveRequest());
    } catch (e) {
      // Resolve unsuccessful request
      yield put(rejectRequest());
    }
  }





  export const staffsSaga = [
    takeEvery(DO_FETCH_STAFFS, handleStaffsFetch),
    takeEvery(DO_GET_STAFF, handleStaffGet),
    takeEvery(DO_POST_STAFF, handleStaffPost),
    takeEvery(DO_DELETE_STAFF, handleStaffDelete)
  ];
