import {
  DO_FETCH_USERS,
  RESOLVE_USERS_FETCH,
  REJECT_USERS_FETCH,

  DO_GET_USER,
  RESOLVE_USER_GET,
  REJECT_USER_GET,

  DO_POST_USER,
  RESOLVE_USER_POST,
  REJECT_USER_POST,

  DO_DELETE_USER,
  RESOLVE_USER_DELETE,
  REJECT_USER_DELETE
} from './constants';

/**************************************
 * LIST USERS
 **************************************/

export const doFetchUsers = () => {
  return {
    type: DO_FETCH_USERS
  };
};

export const resolveUsersFetch = (users) => {
  return {
    type: RESOLVE_USERS_FETCH,
    payload: users
  };
};

export const rejectUsersFetch = () => {
  return {
    type: REJECT_USERS_FETCH
  };
};

/**************************************
 * GET USER
 **************************************/

export const doGetUser = (id) => {
  return {
    type: DO_GET_USER,
    payload: id
  };
};

export const resolveUserGet = (user) => {
  return {
    type: RESOLVE_USER_GET,
    payload: user
  };
};

export const rejectUserGet = () => {
  return {
    type: REJECT_USER_GET
  };
};

/**************************************
 * POST USER
 **************************************/

 export const doPostUser = (user, callback) => {
   return {
     type: DO_POST_USER,
     payload: user,
     callback
   };
 };

 export const resolveUserPost = () => {
   return {
     type: RESOLVE_USER_POST
   };
 };

 export const rejectUserPost = () => {
   return {
     type: REJECT_USER_POST
   };
 };

/**************************************
 * DELETE USER
 **************************************/

 export const doDeleteUser = (id, callback) => {
   return {
     type: DO_DELETE_USER,
     payload: id,
     callback: callback
   };
 };

 export const resolveUserDelete = () => {
   return {
     type: RESOLVE_USER_DELETE
   };
 };

 export const rejectUserDelete = () => {
   return {
     type: REJECT_USER_DELETE
   };
 };
