import {
  RESOLVE_USERS_FETCH,
  REJECT_USERS_FETCH,

  RESOLVE_USER_GET,
  REJECT_USER_GET,

  RESOLVE_USER_POST,
  REJECT_USER_POST,

  RESOLVE_USER_DELETE,
  REJECT_USER_DELETE
} from './constants';
import {
  REQUEST_API,
  RESOLVE_REQUEST,
  REJECT_REQUEST,
} from '../../constants';

const initialSatate = {
  items: [],
  item: {},
  isLoading: false,
  error: false
};

export const users = (
  state = initialSatate,
  action
) => {
  switch (action.type) {
    case REQUEST_API:
      return Object.assign({}, state, {isLoading: true, error: false});

    case RESOLVE_REQUEST:
      return Object.assign({}, state, {isLoading: false});

    case REJECT_REQUEST:
    case REJECT_USERS_FETCH:
    case REJECT_USER_GET:
    case REJECT_USER_POST:
    case REJECT_USER_DELETE:
      return Object.assign({}, state, {isLoading: false, error: true});

    case RESOLVE_USERS_FETCH:
      return Object.assign({}, state, {items: action.payload, isLoading: false});

    case RESOLVE_USER_GET:
      return Object.assign({}, state, {item: action.payload, isLoading: false});

    case RESOLVE_USER_POST:
    case RESOLVE_USER_DELETE:
      return Object.assign({}, state, {isLoading: false});

    default:
      return state;
  }
};
