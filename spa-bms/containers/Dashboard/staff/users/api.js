import { API_URL } from '../../constants';

export const fetchUsers = () => {
  return fetch(`${API_URL}/users`, {
    method: 'GET',
    credentials: 'include',
    mode: 'cors'
  });
};

export const getUser = (id) => {
  return fetch(`${API_URL}/user/${id}`, {
    method: 'GET',
    credentials: 'include'
  });
};

export const postUser = (body) => {
  return fetch(`${API_URL}/user`, {
    method: 'POST',
    credentials: 'include',
    body
  });
};

export const deleteUser = (id) => {
  return fetch(`${API_URL}/user/delete/${id}`, {
    method: 'POST',
    credentials: 'include'
  });
};
