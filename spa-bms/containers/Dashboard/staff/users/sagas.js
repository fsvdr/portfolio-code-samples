import { call, put, takeEvery } from 'redux-saga/effects'
import { doTriggerNotification, doDeAuthenticate } from '../../../../actions';
import { mapCodeToMessage } from '../../../../helpers/api-codes';
import { fetchUsers, getUser, postUser, deleteUser } from './api';
import {
  DO_FETCH_USERS,
  DO_GET_USER,
  DO_POST_USER,
  DO_DELETE_USER
} from './constants';
import {
  resolveUsersFetch, rejectUsersFetch,
  resolveUserGet, rejectUserGet,
  resolveUserPost, rejectUserPost,
  resolveUserDelete, rejectUserDelete
} from './actions';
import {
  requestApi, resolveRequest, rejectRequest
} from '../../actions';

/**************************************
 * LIST USERS
 **************************************/

function *handleUsersFetch(action) {
  // Inform the UI that a request is beign handled
  yield put(requestApi());
  try {
    // Do API request
    const response = yield call(fetchUsers);

    if (response.status === 200) {
      // Resolve successful API request
      const data = yield response.json();
      yield put(resolveUsersFetch(data));
    } else if (response.status === 401) {
      // Resolve unsuccessful API request due to authorization
      // Trigger a notification and de authenticate
      yield put(rejectUsersFetch());
      yield put(doTriggerNotification('Sesión terminada'));
      yield put(doDeAuthenticate());
    } else {
      // Reject unsuccessful API request and trigger a notification
      const notify = mapCodeToMessage(response.status);
      yield put(rejectUsersFetch());
      yield put(doTriggerNotification(notify.msg, notify.state));
    }

    // Resolve correct request
    yield put(resolveRequest());
  } catch (e) {
    // Resolve unsuccessful request
    yield put(rejectRequest());
  }
}

/**************************************
 * GET USER
 **************************************/

function *handleUserGet(action) {
  // Inform the UI that a request is beign handled
  yield put(requestApi());

  try {
    // Do API request
    const response = yield call(getUser, action.payload);

    if (response.status === 200) {
      // Resolve successful API request
      const data = yield(response.json());
      yield put(resolveUserGet(data));
    } else if (response.status === 401) {
      // Resolve unsuccessful API request due to authorization
      // Trigger a notification and de authenticate
      yield put(rejectUserGet());
      yield put(doTriggerNotification('Sesión terminada'));
      yield put(doDeAuthenticate());
    } else {
      // Reject unsuccessful API request and trigger a notification
      const notify = mapCodeToMessage(response.status);
      yield put(rejectUserGet());
      yield put(doTriggerNotification(notify.msg, notify.state));
    }

    // Resolve correct request
    yield put(resolveRequest());
  } catch (e) {
    // Resolve unsuccessful request
    yield put(rejectRequest());
  }
}

/**************************************
 * POST USER
 **************************************/

function *handleUserPost(action) {
 // Inform the UI that a request is beign handled
 yield put(requestApi());

 // Format data for API request's body
 const data = {
   id: action.payload.id,
   email: action.payload.email,
   password: action.payload.password,
   clearance: action.payload.clearance,
   staff: action.payload.staff
 }
 let body = new FormData();
 body.append('user', JSON.stringify(data));

const notifyMsg = action.payload.id ? 'actualizó' : 'creó';

 try {
   // Do API request
   const response = yield call(postUser, body);

   if (response.status === 200) {
     // Resolve successful API request
     yield put(resolveUserPost());
     yield put(doTriggerNotification(`El usuario se ${notifyMsg} exitosamente`));
     if (action.callback) action.callback();
   } else if (response.status === 401) {
     // Resolve unsuccessful API request due to authorization
     // Trigger a notification and de authenticate
     yield put(rejectUserPost());
     yield put(doTriggerNotification('Sesión terminada'));
     yield put(doDeAuthenticate());
   } else {
     // Reject unsuccessful API request and trigger a notification
     const notify = mapCodeToMessage(response.status);
     yield put(rejectUserPost());
     yield put(doTriggerNotification(notify.msg, notify.state));
   }

   // Resolve correct request
   yield put(resolveRequest());
 } catch (e) {
   // Resolve unsuccessful request
   yield put(rejectRequest());
 }
}

 /**************************************
  * DELETE USER
  **************************************/

  function *handleUserDelete(action) {
    // Inform the UI that a request is beign handled
    yield put(requestApi());

    try {
      // Do API request
      const response = yield call(deleteUser, action.payload);

      if (response.status === 200) {
        // Resolve successful API request
        yield put(resolveUserDelete());
        yield put(doTriggerNotification('El usuario se eliminó exitosamente'));
        if (action.callback) action.callback();
      } else if (response.status === 401) {
        // Resolve unsuccessful API request due to authorization
        // Trigger a notification and de authenticate
        yield put(rejectUserDelete());
        yield put(doTriggerNotification('Sesión terminada'));
        yield put(doDeAuthenticate());
      } else {
        // Reject unsuccessful API request and trigger a notification
        const notify = mapCodeToMessage(response.status);
        yield put(rejectUserDelete());
        yield put(doTriggerNotification(notify.msg, notify.state));
      }

      // Resolve correct request
      yield put(resolveRequest());
    } catch (e) {
      // Resolve unsuccessful request
      yield put(rejectRequest());
    }
  }





  export const usersSaga = [
    takeEvery(DO_FETCH_USERS, handleUsersFetch),
    takeEvery(DO_GET_USER, handleUserGet),
    takeEvery(DO_POST_USER, handleUserPost),
    takeEvery(DO_DELETE_USER, handleUserDelete)
  ];
