import {
  RESOLVE_BONUSES_FETCH,
  REJECT_BONUSES_FETCH,

  RESOLVE_BONUS_GET,
  REJECT_BONUS_GET,

  RESOLVE_BONUS_POST,
  REJECT_BONUS_POST,

  RESOLVE_BONUS_DELETE,
  REJECT_BONUS_DELETE
} from './constants';
import {
  REQUEST_API,
  RESOLVE_REQUEST,
  REJECT_REQUEST,
} from '../../constants';

const initialSatate = {
  items: [],
  item: {},
  isLoading: false,
  error: false
};

export const bonuses = (
  state = initialSatate,
  action
) => {
  switch (action.type) {
    case REQUEST_API:
      return Object.assign({}, state, {isLoading: true, error: false});

    case RESOLVE_REQUEST:
      return Object.assign({}, state, {isLoading: false});

    case REJECT_REQUEST:
    case REJECT_BONUSES_FETCH:
    case REJECT_BONUS_GET:
    case REJECT_BONUS_POST:
    case REJECT_BONUS_DELETE:
      return Object.assign({}, state, {isLoading: false, error: true});

    case RESOLVE_BONUSES_FETCH:
      return Object.assign({}, state, {items: action.payload, isLoading: false});

    case RESOLVE_BONUS_GET:
      return Object.assign({}, state, {item: action.payload, isLoading: false});

    case RESOLVE_BONUS_POST:
    case RESOLVE_BONUS_DELETE:
      return Object.assign({}, state, {isLoading: false});

    default:
      return state;
  }
};
