import {
  DO_FETCH_BONUSES,
  RESOLVE_BONUSES_FETCH,
  REJECT_BONUSES_FETCH,

  DO_GET_BONUS,
  RESOLVE_BONUS_GET,
  REJECT_BONUS_GET,

  DO_POST_BONUS,
  RESOLVE_BONUS_POST,
  REJECT_BONUS_POST,

  DO_DELETE_BONUS,
  RESOLVE_BONUS_DELETE,
  REJECT_BONUS_DELETE
} from './constants';

/**************************************
 * LIST BONUSES
 **************************************/

export const doFetchBonuses = () => {
  return {
    type: DO_FETCH_BONUSES
  };
};

export const resolveBonusesFetch = (bonuses) => {
  return {
    type: RESOLVE_BONUSES_FETCH,
    payload: bonuses
  };
};

export const rejectBonusesFetch = () => {
  return {
    type: REJECT_BONUSES_FETCH
  };
};

/**************************************
 * GET BONUS
 **************************************/

export const doGetBonus = (id) => {
  return {
    type: DO_GET_BONUS,
    payload: id
  };
};

export const resolveBonusGet = (bonus) => {
  return {
    type: RESOLVE_BONUS_GET,
    payload: bonus
  };
};

export const rejectBonusGet = () => {
  return {
    type: REJECT_BONUS_GET
  };
};

/**************************************
 * POST BONUS
 **************************************/

 export const doPostBonus = (bonus, callback) => {
   return {
     type: DO_POST_BONUS,
     payload: bonus,
     callback
   };
 };

 export const resolveBonusPost = () => {
   return {
     type: RESOLVE_BONUS_POST
   };
 };

 export const rejectBonusPost = () => {
   return {
     type: REJECT_BONUS_POST
   };
 };

/**************************************
 * DELETE BONUS
 **************************************/

 export const doDeleteBonus = (id, callback) => {
   return {
     type: DO_DELETE_BONUS,
     payload: id,
     callback: callback
   };
 };

 export const resolveBonusDelete = () => {
   return {
     type: RESOLVE_BONUS_DELETE
   };
 };

 export const rejectBonusDelete = () => {
   return {
     type: REJECT_BONUS_DELETE
   };
 };
