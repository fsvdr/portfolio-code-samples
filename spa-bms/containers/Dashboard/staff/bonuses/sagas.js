import { call, put, takeEvery } from 'redux-saga/effects'
import { doTriggerNotification, doDeAuthenticate } from '../../../../actions';
import { mapCodeToMessage } from '../../../../helpers/api-codes';
import { fetchBonuses, getBonus, postBonus, deleteBonus } from './api';
import {
  DO_FETCH_BONUSES,
  DO_GET_BONUS,
  DO_POST_BONUS,
  DO_DELETE_BONUS
} from './constants';
import {
  resolveBonusesFetch, rejectBonusesFetch,
  resolveBonusGet, rejectBonusGet,
  resolveBonusPost, rejectBonusPost,
  resolveBonusDelete, rejectBonusDelete
} from './actions';
import {
  requestApi, resolveRequest, rejectRequest
} from '../../actions';

/**************************************
 * LIST BONUSES
 **************************************/

function *handleBonusesFetch(action) {
  // Inform the UI that a request is beign handled
  yield put(requestApi());
  try {
    // Do API request
    const response = yield call(fetchBonuses);

    if (response.status === 200) {
      // Resolve successful API request
      const data = yield response.json();
      yield put(resolveBonusesFetch(data));
    } else if (response.status === 401) {
      // Resolve unsuccessful API request due to authorization
      // Trigger a notification and de authenticate
      yield put(rejectBonusesFetch());
      yield put(doTriggerNotification('Sesión terminada'));
      yield put(doDeAuthenticate());
    } else {
      // Reject unsuccessful API request and trigger a notification
      const notify = mapCodeToMessage(response.status);
      yield put(rejectBonusesFetch());
      yield put(doTriggerNotification(notify.msg, notify.state));
    }

    // Resolve correct request
    yield put(resolveRequest());
  } catch (e) {
    // Resolve unsuccessful request
    yield put(rejectRequest());
  }
}

/**************************************
 * GET BONUS
 **************************************/

function *handleBonusGet(action) {
  // Inform the UI that a request is beign handled
  yield put(requestApi());

  try {
    // Do API request
    const response = yield call(getBonus, action.payload);

    if (response.status === 200) {
      // Resolve successful API request
      const data = yield(response.json());
      yield put(resolveBonusGet(data));
    } else if (response.status === 401) {
      // Resolve unsuccessful API request due to authorization
      // Trigger a notification and de authenticate
      yield put(rejectBonusGet());
      yield put(doTriggerNotification('Sesión terminada'));
      yield put(doDeAuthenticate());
    } else {
      // Reject unsuccessful API request and trigger a notification
      const notify = mapCodeToMessage(response.status);
      yield put(rejectBonusGet());
      yield put(doTriggerNotification(notify.msg, notify.state));
    }

    // Resolve correct request
    yield put(resolveRequest());
  } catch (e) {
    // Resolve unsuccessful request
    yield put(rejectRequest());
  }
}

/**************************************
 * POST BONUS
 **************************************/

function *handleBonusPost(action) {
 // Inform the UI that a request is beign handled
 yield put(requestApi());

 // Format data for API request's body
 const data = {
   id: action.payload.id,
   concept: action.payload.concept,
   frequency: action.payload.frequency,
   quantity: action.payload.quantity,
 }
 let body = new FormData();
 body.append('bonus', JSON.stringify(data));

const notifyMsg = action.payload.id ? 'actualizó' : 'creó';

 try {
   // Do API request
   const response = yield call(postBonus, body);

   if (response.status === 200) {
     // Resolve successful API request
     yield put(resolveBonusPost());
     yield put(doTriggerNotification(`La prestación se ${notifyMsg} exitosamente`));
     if (action.callback) action.callback();
   } else if (response.status === 401) {
     // Resolve unsuccessful API request due to authorization
     // Trigger a notification and de authenticate
     yield put(rejectBonusPost());
     yield put(doTriggerNotification('Sesión terminada'));
     yield put(doDeAuthenticate());
   } else {
     // Reject unsuccessful API request and trigger a notification
     const notify = mapCodeToMessage(response.status);
     yield put(rejectBonusPost());
     yield put(doTriggerNotification(notify.msg, notify.state));
   }

   // Resolve correct request
   yield put(resolveRequest());
 } catch (e) {
   // Resolve unsuccessful request
   yield put(rejectRequest());
 }
}

 /**************************************
  * DELETE BONUS
  **************************************/

  function *handleBonusDelete(action) {
    // Inform the UI that a request is beign handled
    yield put(requestApi());

    try {
      // Do API request
      const response = yield call(deleteBonus, action.payload);

      if (response.status === 200) {
        // Resolve successful API request
        yield put(resolveBonusDelete());
        yield put(doTriggerNotification('La prestación se eliminó exitosamente'));
        if (action.callback) action.callback();
      } else if (response.status === 401) {
        // Resolve unsuccessful API request due to authorization
        // Trigger a notification and de authenticate
        yield put(rejectBonusDelete());
        yield put(doTriggerNotification('Sesión terminada'));
        yield put(doDeAuthenticate());
      } else {
        // Reject unsuccessful API request and trigger a notification
        const notify = mapCodeToMessage(response.status);
        yield put(rejectBonusDelete());
        yield put(doTriggerNotification(notify.msg, notify.state));
      }

      // Resolve correct request
      yield put(resolveRequest());
    } catch (e) {
      // Resolve unsuccessful request
      yield put(rejectRequest());
    }
  }





  export const bonusesSaga = [
    takeEvery(DO_FETCH_BONUSES, handleBonusesFetch),
    takeEvery(DO_GET_BONUS, handleBonusGet),
    takeEvery(DO_POST_BONUS, handleBonusPost),
    takeEvery(DO_DELETE_BONUS, handleBonusDelete)
  ];
