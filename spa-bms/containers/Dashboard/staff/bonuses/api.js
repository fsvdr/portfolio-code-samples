import { API_URL } from '../../constants';

export const fetchBonuses = () => {
  return fetch(`${API_URL}/bonus`, {
    method: 'GET',
    credentials: 'include',
    mode: 'cors'
  });
};

export const getBonus = (id) => {
  return fetch(`${API_URL}/bonus/${id}`, {
    method: 'GET',
    credentials: 'include'
  });
};

export const postBonus = (body) => {
  return fetch(`${API_URL}/bonus`, {
    method: 'POST',
    credentials: 'include',
    body
  });
};

export const deleteBonus = (id) => {
  return fetch(`${API_URL}/bonus/deactivate/${id}`, {
    method: 'POST',
    credentials: 'include'
  });
};
