export const navLinks = [
  {
    id: 0,
    name: 'Catálogo',
    route: 'catalogo',
    links: [
      {id: 0, name: 'Productos', route: 'productos'},
      {id: 1, name: 'Tratamientos', route: 'tratamientos'},
      {id: 2, name: 'Paquetes', route: 'paquetes'}
    ]
  },
  {
    id: 1,
    name: 'Inventario',
    route: 'inventario',
    links: [
      {id: 0, name: 'Insumos', route: 'insumos'},
      {id: 1, name: 'Pedidos', route: 'pedidos'},
      {id: 2, name: 'Promociones', route: 'promociones'},
      {id: 3, name: 'Proveedores', route: 'proveedores'}
    ]
  },
  {
    id: 2,
    name: 'Personal',
    route: 'personal',
    links: [
      {id: 0, name: 'Empleados', route: 'empleados'},
      {id: 1, name: 'Usuarios', route: 'usuarios'},
      {id: 2, name: 'Prestaciones', route: 'prestaciones'}
    ]
  },
  {
    id: 3,
    name: 'Registro',
    route: 'registro',
    links: [
      {id: 0, name: 'Clientes', route: 'clientes'},
      {id: 1, name: 'Ventas', route: 'ventas'}
    ]
  },
  {
    id: 4,
    name: 'Contabilidad',
    route: 'contabilidad',
    links: [
      {id: 0, name: 'Ganancias', route: 'ganancias'},
    ]
  },
];
