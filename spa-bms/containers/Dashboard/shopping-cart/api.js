import { API_URL } from '../constants';

export const fetchTotal = (body) => {
  return fetch(`${API_URL}/sell/preflight`, {
    method: 'POST',
    credentials: 'include',
    body
  });
};
