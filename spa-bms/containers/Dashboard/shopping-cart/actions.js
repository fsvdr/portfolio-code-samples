import {
  DO_CLEAR_CART,

  DO_SET_CLIENT,
  DO_SET_STAFF,
  DO_SET_PROSPECTION,

  DO_ADD_PAYMENT,
  DO_REMOVE_PAYMENT,

  DO_REQUEST_TOTAL,
  RESOLVE_TOTAL_REQUEST,
  REJECT_TOTAL_REQUEST,

  DO_ADD_PRODUCT,
  RESOLVE_PRODUCT_ADD,

  DO_REMOVE_PRODUCT,
  RESOLVE_PRODUCT_REMOVE,

  DO_ADD_TREATMENT,
  RESOLVE_TREATMENT_ADD,

  DO_REMOVE_TREATMENT,
  RESOLVE_TREATMENT_REMOVE,

  DO_ADD_SERVICE,
  RESOLVE_SERVICE_ADD,

  DO_REMOVE_SERVICE,
  RESOLVE_SERVICE_REMOVE,

  DO_ADD_PROMOTION,
  RESOLVE_PROMOTION_ADD,

  DO_REMOVE_PROMOTION,
  RESOLVE_PROMOTION_REMOVE,
} from './constants';

/**************************************
 * CART LOGIC
 **************************************/

export const doClearCart = () => {
  return {
    type: DO_CLEAR_CART
  };
};

/**************************************
 * TOTAL PREFLIGHT
 **************************************/

export const doRequestTotal = (sale) => {
 return {
   type: DO_REQUEST_TOTAL,
   payload: sale
 };
};

export const resolveTotalRequest = (total) => {
 return {
   type: RESOLVE_TOTAL_REQUEST,
   payload: total
 };
};

export const rejectTotalRequest = () => {
 return {
   type: REJECT_TOTAL_REQUEST
 };
};

/**************************************
 * STATIC FIELDS
 **************************************/

export const doSetClient = (id) => {
  return {
    type: DO_SET_CLIENT,
    payload: id
  };
};

export const doSetStaff = (id) => {
  return {
    type: DO_SET_STAFF,
    payload: id
  };
};

export const doSetProspection = (id) => {
  return {
    type: DO_SET_PROSPECTION,
    payload: id
  };
};

/**************************************
 * DYNAMIC FIELDS
 **************************************/

export const doAddPayment = (payment) => {
  return {
    type: DO_ADD_PAYMENT,
    payload: payment
  };
};

export const doRemovePayment = (index) => {
  return {
    type: DO_REMOVE_PAYMENT,
    payload: index
  }
}

/**************************************
 * FIELDS THAT MODIFY THE CART TOTAL
 **************************************/

export const doAddProduct = (product) => {
  return {
    type: DO_ADD_PRODUCT,
    payload: product
  };
};

export const resolveProductAdd = (product) => {
  return {
    type: RESOLVE_PRODUCT_ADD,
    payload: product
  };
};

export const doRemoveProduct = (index) => {
  return {
    type: DO_REMOVE_PRODUCT,
    payload: index
  };
};

export const resolveProductRemove = (index) => {
  return {
    type: RESOLVE_PRODUCT_REMOVE,
    payload: index
  };
};




export const doAddTreatment = (treatment) => {
  return {
    type: DO_ADD_TREATMENT,
    payload: treatment
  };
};

export const resolveTreatmentAdd = (treatment) => {
  return {
    type: RESOLVE_TREATMENT_ADD,
    payload: treatment
  };
};

export const doRemoveTreatment = (index) => {
  return {
    type: DO_REMOVE_TREATMENT,
    payload: index
  };
};

export const resolveTreatmentRemove = (index) => {
  return {
    type: RESOLVE_TREATMENT_REMOVE,
    payload: index
  };
};





export const doAddService = (service) => {
  return {
    type: DO_ADD_SERVICE,
    payload: service
  };
};

export const resolveServiceAdd = (service) => {
  return {
    type: RESOLVE_SERVICE_ADD,
    payload: service
  };
};

export const doRemoveService = (index) => {
  return {
    type: DO_REMOVE_SERVICE,
    payload: index
  };
};

export const resolveServiceRemove = (index) => {
  return {
    type: RESOLVE_SERVICE_REMOVE,
    payload: index
  };
};




export const doAddPromotion = (promotion) => {
  return {
    type: DO_ADD_PROMOTION,
    payload: promotion
  };
};

export const resolvePromotionAdd = (promotion) => {
  return {
    type: RESOLVE_PROMOTION_ADD,
    payload: promotion
  };
};

export const doRemovePromotion = (index) => {
  return {
    type: DO_REMOVE_PROMOTION,
    payload: index
  };
};

export const resolvePromotionRemove = (index) => {
  return {
    type: RESOLVE_PROMOTION_REMOVE,
    payload: index
  };
};
