import {
  DO_CLEAR_CART,

  DO_SET_CLIENT,
  DO_SET_STAFF,
  DO_SET_PROSPECTION,

  DO_ADD_PAYMENT,
  DO_REMOVE_PAYMENT,

  RESOLVE_TOTAL_REQUEST,
  REJECT_TOTAL_REQUEST,

  RESOLVE_PRODUCT_ADD,
  RESOLVE_PRODUCT_REMOVE,
  RESOLVE_TREATMENT_ADD,
  RESOLVE_TREATMENT_REMOVE,
  RESOLVE_SERVICE_ADD,
  RESOLVE_SERVICE_REMOVE,
  RESOLVE_PROMOTION_ADD,
  RESOLVE_PROMOTION_REMOVE,
} from './constants'
import {
  REQUEST_API,
  RESOLVE_REQUEST,
  REJECT_REQUEST,
} from '../constants';

const initialSatate = {
  client: '',
  staff: 1,
  propspection: '',
  payments: [],
  products: [],
  treatments: [],
  services:[],
  promotions: [],
  total: 0,
  quantity: 0,
  isLoading: false,
  error: false
}

export const shoppingCart = (
  state = initialSatate,
  action
) => {
  let payments;
  let products;
  let treatments;
  let services;
  let promotions;

  switch (action.type) {
    case REQUEST_API:
      return Object.assign({}, state, {isLoading: true, error: false});

    case RESOLVE_REQUEST:
      return Object.assign({}, state, {isLoading: false});

    case REJECT_REQUEST:
    case REJECT_TOTAL_REQUEST:
      return Object.assign({}, state, {isLoading: false, error: true});

    case DO_CLEAR_CART:
      return Object.assign({}, state, {...initialSatate});

    case DO_SET_CLIENT:
      return Object.assign({}, state, {client: action.payload});

    case DO_SET_STAFF:
      return Object.assign({}, state, {staff: action.payload});

    case DO_SET_PROSPECTION:
      return Object.assign({}, state, {prospection: action.payload});

    case DO_ADD_PAYMENT:
      payments = [...state.payments, action.payload];
      return Object.assign({}, state, {payments});

    case DO_REMOVE_PAYMENT:
      payments = [...state.payments.slice(0, action.payload), ...state.payments.slice(action.payload + 1)]
      return Object.assign({}, state, {payments});

    case RESOLVE_TOTAL_REQUEST:
      return Object.assign({}, state, {total: action.payload});

    case RESOLVE_PRODUCT_ADD:
      products = getUpdatedItems(state.products, action.payload);
      return Object.assign({}, state, {products});

    case RESOLVE_PRODUCT_REMOVE:
      products = removeItem(state.products, action.payload);
      return Object.assign({}, state, {products});

    case RESOLVE_TREATMENT_ADD:
      treatments = getUpdatedItems(state.treatments, action.payload);
      return Object.assign({}, state, {treatments});

    case RESOLVE_TREATMENT_REMOVE:
      treatments = removeItem(state.treatments, action.payload);
      return Object.assign({}, state, {treatments});

    case RESOLVE_SERVICE_ADD:
      services = getUpdatedItems(state.services, action.payload);
      return Object.assign({}, state, {services});

    case RESOLVE_SERVICE_REMOVE:
      services = removeItem(state.services, action.payload);
      return Object.assign({}, state, {services});

    case RESOLVE_PROMOTION_ADD:
      promotions = getUpdatedItems(state.promotions, action.payload);
      return Object.assign({}, state, {promotions});

    case RESOLVE_PROMOTION_REMOVE:
      promotions = removeItem(state.promotions, action.payload);
      return Object.assign({}, state, {promotions});

    default:
      return state;
  }
}

const getUpdatedItems = (items, item) => {
  let updatedItems = [];
  const matchIndex = items.findIndex((i) => i.id == item.id); // eslint-disable-line

  if (matchIndex >= 0) {
    // Item is already in array, increase the quantity by 1
    let matchItem = items[matchIndex];
    matchItem['quantity'] = matchItem.quantity + 1;
    updatedItems = [...items.slice(0, matchIndex), matchItem, ...items.slice(matchIndex + 1)];
  } else {
    // Item is not in array, format and then include it
    const formatedItem = {
      id: item.id,
      name: item.name,
      price: item.price,
      quantity: 1,
      promotions: item.promotions
    };
    updatedItems = [...items, formatedItem];
  }

  return updatedItems;
}

const removeItem = (items, index) => {
  let updatedItems = [];

  if (index >= 0 && index < items.length ){
    let matchItem = items[index];

    if (matchItem.quantity - 1 == 0) { // eslint-disable-line
      // Updated quantity is 0, remove item from array
      updatedItems = [...items.slice(0, index), ...items.slice(index + 1)];
    } else {
      // Updated quantity is not 0, decrese quantity by 1
      matchItem['quantity'] = matchItem.quantity - 1;
      updatedItems = [...items.slice(0, index), matchItem, ...items.slice(index + 1)]
    }
  }

  return updatedItems;
}
