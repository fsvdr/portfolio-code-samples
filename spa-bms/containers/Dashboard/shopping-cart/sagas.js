import { call, put, takeEvery, select } from 'redux-saga/effects'
import { doTriggerNotification, doDeAuthenticate } from '../../../actions';
import { mapCodeToMessage } from '../../../helpers/api-codes';
import { fetchTotal } from './api';
import {
  DO_REQUEST_TOTAL,
  DO_ADD_PRODUCT,
  DO_ADD_TREATMENT,
  DO_ADD_SERVICE,
  DO_ADD_PROMOTION,
  DO_REMOVE_PRODUCT,
  DO_REMOVE_TREATMENT,
  DO_REMOVE_SERVICE,
  DO_REMOVE_PROMOTION
} from './constants';
import {
  doRequestTotal,
  resolveTotalRequest, rejectTotalRequest,
  resolveProductAdd, resolveProductRemove,
  resolveTreatmentAdd, resolveTreatmentRemove,
  resolveServiceAdd, resolveServiceRemove,
  resolvePromotionAdd, resolvePromotionRemove
} from './actions';
import {
  requestApi, resolveRequest, rejectRequest
} from '../actions';

const getCart = (state) => state.shoppingCart;

/**************************************
 * REQUEST TOTAL PREFLIGHT
 **************************************/

function *handleTotalRequest(action) {
  // Inform the UI that a request is beign handled
  yield put(requestApi());

  // Format data for API request's body
  const data = {
    id: action.payload.id,
    client: action.payload.client,
    staff: action.payload.staff,
    prospection: action.payload.prospection,
    payments: action.payload.payments,
    products: action.payload.products.map((i) => {return {id: i.id, name: i.name, quantity: i.quantity}}),
    treatments: action.payload.treatments.map((i) => {return {id: i.id, name: i.name, quantity: i.quantity}}),
    services: action.payload.services.map((i) => {return {id: i.id, name: i.name, quantity: i.quantity}}),
    promotions: action.payload.promotions.map((i) => i.id)
  }
  let body = new FormData();
  body.append('sell', JSON.stringify(data));

  try {
    // Do API request
    const response = yield call(fetchTotal, body);

    if (response.status === 200) {
      // Resolve successful API request
      const data = yield(response.json());
      yield put(resolveTotalRequest(data.total));
    } else if (response.status === 401) {
      // Resolve unsuccessful API request due to authorization
      // Trigger a notification and de authenticate
      yield put(rejectTotalRequest());
      yield put(doTriggerNotification('Sesión terminada'));
      yield put(doDeAuthenticate());
    } else {
      // Reject unsuccessful API request and trigger a notification
      const notify = mapCodeToMessage(response.status);
      yield put(rejectTotalRequest());
      yield put(doTriggerNotification(notify.msg, notify.state));
    }

    // Resolve correct request
    yield put(resolveRequest());
  } catch (e) {
    // Resolve unsuccessful request
    yield put(rejectRequest());
  }
}





/**************************************
 * INTERCEPT OPERATIONS THAT REQUIRE
 * A TOTAL RECALCULATION
 **************************************/

function *handleProductAdd(action) {
  yield put(resolveProductAdd(action.payload));
  const state = yield select(getCart);
  yield put(doRequestTotal(state))
}

function *handleProductRemove(action) {
  yield put(resolveProductRemove(action.payload));
  const state = yield select(getCart);
  yield put(doRequestTotal(state))
}

function *handleTreatmentAdd(action) {
  yield put(resolveTreatmentAdd(action.payload));
  const state = yield select(getCart);
  yield put(doRequestTotal(state))
}

function *handleTreatmentRemove(action) {
  yield put(resolveTreatmentRemove(action.payload));
  const state = yield select(getCart);
  yield put(doRequestTotal(state))
}

function *handleServiceAdd(action) {
  yield put(resolveServiceAdd(action.payload));
  const state = yield select(getCart);
  yield put(doRequestTotal(state))
}

function *handleServiceRemove(action) {
  yield put(resolveServiceRemove(action.payload));
  const state = yield select(getCart);
  yield put(doRequestTotal(state))
}

function *handlePromotionAdd(action) {
  yield put(resolvePromotionAdd(action.payload));
  const state = yield select(getCart);
  yield put(doRequestTotal(state))
}

function *handlePromotionRemove(action) {
  yield put(resolvePromotionRemove(action.payload));
  const state = yield select(getCart);
  yield put(doRequestTotal(state))
}





export const cartSaga = [
  takeEvery(DO_REQUEST_TOTAL, handleTotalRequest),
  takeEvery(DO_ADD_PRODUCT, handleProductAdd),
  takeEvery(DO_ADD_TREATMENT, handleTreatmentAdd),
  takeEvery(DO_ADD_SERVICE, handleServiceAdd),
  takeEvery(DO_ADD_PROMOTION, handlePromotionAdd),
  takeEvery(DO_REMOVE_PRODUCT, handleProductRemove),
  takeEvery(DO_REMOVE_TREATMENT, handleTreatmentRemove),
  takeEvery(DO_REMOVE_SERVICE, handleServiceRemove),
  takeEvery(DO_REMOVE_PROMOTION, handlePromotionRemove)
];
