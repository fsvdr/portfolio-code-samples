import { call, put, takeEvery } from 'redux-saga/effects'
import { doTriggerNotification, doDeAuthenticate } from '../../../../actions';
import { mapCodeToMessage } from '../../../../helpers/api-codes';
import { fetchEarnings, postEarning } from './api';
import {
  DO_FETCH_EARNINGS,
  DO_POST_EARNING,
} from './constants';
import {
  resolveEarningsFetch, rejectEarningsFetch,
  resolveEarningPost, rejectEarningPost,
} from './actions';
import {
  requestApi, resolveRequest, rejectRequest
} from '../../actions';

/**************************************
 * LIST EARNINGS
 **************************************/

function *handleEarningsFetch(action) {
  // Inform the UI that a request is beign handled
  yield put(requestApi());
  try {
    // Do API request
    const response = yield call(fetchEarnings);

    if (response.status === 200) {
      // Resolve successful API request
      const data = yield response.json();
      yield put(resolveEarningsFetch(data));
    } else if (response.status === 401) {
      // Resolve unsuccessful API request due to authorization
      // Trigger a notification and de authenticate
      yield put(rejectEarningsFetch());
      yield put(doTriggerNotification('Sesión terminada'));
      yield put(doDeAuthenticate());
    } else {
      // Reject unsuccessful API request and trigger a notification
      const notify = mapCodeToMessage(response.status);
      yield put(rejectEarningsFetch());
      yield put(doTriggerNotification(notify.msg, notify.state));
    }

    // Resolve correct request
    yield put(resolveRequest());
  } catch (e) {
    // Resolve unsuccessful request
    yield put(rejectRequest());
  }
}

/**************************************
 * POST EARNING
 **************************************/

function *handleEarningPost(action) {
 // Inform the UI that a request is beign handled
 yield put(requestApi());

 // Format data for API request's body
 const data = {
   id: action.payload.id,
   money: action.payload.money,
   real: action.payload.real,
   pending: action.payload.pending
 }
 let body = new FormData();
 body.append('status', JSON.stringify(data));

const notifyMsg = action.payload.id ? 'actualizó' : 'creó';

 try {
   // Do API request
   const response = yield call(postEarning, body);

   if (response.status === 200) {
     // Resolve successful API request
     yield put(resolveEarningPost());
     yield put(doTriggerNotification(`La contabilidad se ${notifyMsg} exitosamente`));
     if (action.callback) action.callback();
   } else if (response.status === 401) {
     // Resolve unsuccessful API request due to authorization
     // Trigger a notification and de authenticate
     yield put(rejectEarningPost());
     yield put(doTriggerNotification('Sesión terminada'));
     yield put(doDeAuthenticate());
   } else {
     // Reject unsuccessful API request and trigger a notification
     const notify = mapCodeToMessage(response.status);
     yield put(rejectEarningPost());
     yield put(doTriggerNotification(notify.msg, notify.state));
   }

   // Resolve correct request
   yield put(resolveRequest());
 } catch (e) {
   // Resolve unsuccessful request
   yield put(rejectRequest());
 }
}





export const earningsSaga = [
  takeEvery(DO_FETCH_EARNINGS, handleEarningsFetch),
  takeEvery(DO_POST_EARNING, handleEarningPost),
];
