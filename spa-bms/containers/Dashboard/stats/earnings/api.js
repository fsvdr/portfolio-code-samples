import { API_URL } from '../../constants';

export const fetchEarnings = () => {
  return fetch(`${API_URL}/status`, {
    method: 'GET',
    credentials: 'include',
    mode: 'cors'
  });
};

export const postEarning = (body) => {
  return fetch(`${API_URL}/status`, {
    method: 'POST',
    credentials: 'include',
    body
  });
};
