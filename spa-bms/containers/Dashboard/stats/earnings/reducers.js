import {
  RESOLVE_EARNINGS_FETCH,
  REJECT_EARNINGS_FETCH,

  RESOLVE_EARNING_POST,
  REJECT_EARNING_POST,
} from './constants';
import {
  REQUEST_API,
  RESOLVE_REQUEST,
  REJECT_REQUEST,
} from '../../constants';

const initialSatate = {
  item: {},
  isLoading: false,
  error: false
};

export const earnings = (
  state = initialSatate,
  action
) => {
  switch (action.type) {
    case REQUEST_API:
      return Object.assign({}, state, {isLoading: true, error: false});

    case RESOLVE_REQUEST:
      return Object.assign({}, state, {isLoading: false});

    case REJECT_REQUEST:
    case REJECT_EARNINGS_FETCH:
    case REJECT_EARNING_POST:
      return Object.assign({}, state, {isLoading: false, error: true});

    case RESOLVE_EARNINGS_FETCH:
      return Object.assign({}, state, {item: action.payload, isLoading: false});

    case RESOLVE_EARNING_POST:
      return Object.assign({}, state, {isLoading: false});

    default:
      return state;
  }
};
