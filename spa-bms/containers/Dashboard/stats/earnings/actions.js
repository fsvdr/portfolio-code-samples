import {
  DO_FETCH_EARNINGS,
  RESOLVE_EARNINGS_FETCH,
  REJECT_EARNINGS_FETCH,

  DO_POST_EARNING,
  RESOLVE_EARNING_POST,
  REJECT_EARNING_POST,
} from './constants';

/**************************************
 * LIST EARNINGS
 **************************************/

export const doFetchEarnings = () => {
  return {
    type: DO_FETCH_EARNINGS
  };
};

export const resolveEarningsFetch = (earnings) => {
  return {
    type: RESOLVE_EARNINGS_FETCH,
    payload: earnings
  };
};

export const rejectEarningsFetch = () => {
  return {
    type: REJECT_EARNINGS_FETCH
  };
};

/**************************************
 * POST EARNING
 **************************************/

 export const doPostEarning = (earning, callback) => {
   return {
     type: DO_POST_EARNING,
     payload: earning,
     callback
   };
 };

 export const resolveEarningPost = () => {
   return {
     type: RESOLVE_EARNING_POST
   };
 };

 export const rejectEarningPost = () => {
   return {
     type: REJECT_EARNING_POST
   };
 };
