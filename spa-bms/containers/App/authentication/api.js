import { API_URL } from '../../../constants';

export const login = (body) => {
  return fetch(`${API_URL}/login`, {
    method: 'POST',
    body: body,
    credentials: 'include'
  });
};

export const logout = () => {
  return fetch(`${API_URL}/logout`, {
    method: 'POST',
    credentials: 'include'
  });
};
