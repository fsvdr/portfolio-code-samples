import { call, put, takeEvery } from 'redux-saga/effects'
import { login, logout } from './api';
import {
  DO_FETCH_AUTHENTICATION,
  DO_AUTHENTICATE,
  DO_DE_AUTHENTICATE } from './constants';
import {
  requestAuthentication,
  resolveRequestSuccess,
  resolveRequestFailure,
  resolveIsAuthenticated,
  resolveIsDeAuthenticated
} from './actions';
import { doTriggerNotification } from '../../../actions';
import { mapCodeToMessage } from '../../../helpers/api-codes';

/**
 * Worker Saga: will be fired on FETCH_AUTHENTICATION actions
 */
function *handleAuthenticationFetch(action) {
  // Inform the UI that a request is beign handled
  yield put(requestAuthentication());

  // Fetch local authentication state
  const cookies = document.cookie.split(';');
  const hasSession = cookies.findIndex((c) => c.includes('PHPSESSID'));

  // Inform UI of successful request
  yield put(resolveRequestSuccess());
  if (hasSession >= 0) {
    yield put(resolveIsAuthenticated());
  } else {
    yield put(resolveIsDeAuthenticated());
  }
}





/**
 * Worker Saga: will be fired on DO_AUTHENTICATE actions
 */
function *handleLogin(action) {

  // Credentials are provided as an object but request must
  // provide form data
  let body = new FormData();
  for (let key in action.payload) {
    body.append(key, action.payload[key]);
  }

  try {
    // Inform the UI that a request is beign handled
    yield put(requestAuthentication());

    // Perfom API request
    const response = yield call(login, body);

    // Inform UI of successful request
    yield put(resolveRequestSuccess());

    if (response.status === 200) {
      // Inform UI of correct authentication
      yield put(resolveIsAuthenticated());
    } else {
      // Inform UI of incorrect authentication
      const notify = mapCodeToMessage(response.status);
      yield put(resolveIsDeAuthenticated());
      yield put(doTriggerNotification(notify.msg, notify.state));
    }

  } catch (e) {
    // Inform the UI of unsuccessful request
    yield put(resolveRequestFailure(e.message));
    yield put(doTriggerNotification('Ocurrió un error, por favor contacta al administrador.', 'ERROR'));
  }
}





/**
 * Worker Saga: will be fired on DO_DE_AUTHENTICATE actions
 */
function *handleLogout(action) {
  try {
    // Inform the UI that a request is beign handled
    yield put(requestAuthentication());

    // Perfom API request
    const response = yield call(logout);

    // Inform UI of successful request
    yield put(resolveRequestSuccess());

    if (response.status === 200) {
      // Inform UI of correct de authentication
      yield put(resolveIsDeAuthenticated());
      yield put(doTriggerNotification('Sesión terminada'));
    } else {
      // Inform UI of incorrect request
      const notify = mapCodeToMessage(response.status);
      yield put(resolveIsDeAuthenticated());
      yield put(doTriggerNotification(notify.msg, notify.state));
    }

  } catch (e) {
    // Inform the UI of unsuccessful request
    yield put(resolveRequestFailure());
    yield put(doTriggerNotification('An error occured, pleade contact the admin.', 'ERROR'));
  }
}





export const authenticationSaga = [
  takeEvery(DO_FETCH_AUTHENTICATION, handleAuthenticationFetch),
  takeEvery(DO_AUTHENTICATE, handleLogin),
  takeEvery(DO_DE_AUTHENTICATE, handleLogout)
];
