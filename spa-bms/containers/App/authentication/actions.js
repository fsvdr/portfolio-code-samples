import {
  AUTHENTICATION_REQUEST,
  RESOLVE_REQUEST_SUCCESS,
  RESOLVE_REQUEST_FAILURE,

  DO_FETCH_AUTHENTICATION,
  DO_AUTHENTICATE,
  RESOLVE_IS_AUTHENTICATED,

  DO_DE_AUTHENTICATE,
  RESOLVE_IS_DE_AUTHENTICATED
} from './constants';

/**************************************
 * AUTHENTICATION REQUESTS
 **************************************/

export const requestAuthentication = () => {
  return {
    type: AUTHENTICATION_REQUEST
  };
};

export const resolveRequestSuccess = () => {
  return {
    type: RESOLVE_REQUEST_SUCCESS
  };
};

export const resolveRequestFailure = () => {
  return {
    type: RESOLVE_REQUEST_FAILURE
  };
};





/**************************************
 * AUTHENTICATION
 **************************************/

export const doFetchAuthentication = () => {
  return {
    type: DO_FETCH_AUTHENTICATION
  }
};

export const doAuthenticate = (credentials) => {
  return {
    type: DO_AUTHENTICATE,
    payload: credentials
  };
};

export const resolveIsAuthenticated = () => {
  return {
    type: RESOLVE_IS_AUTHENTICATED
  };
};





/**************************************
 * DE AUTHENTICATION
 **************************************/

export const doDeAuthenticate = () => {
  return {
    type: DO_DE_AUTHENTICATE
  };
};

export const resolveIsDeAuthenticated = () => {
  return {
    type: RESOLVE_IS_DE_AUTHENTICATED
  };
};
