import {
  AUTHENTICATION_REQUEST,
  RESOLVE_REQUEST_SUCCESS,
  RESOLVE_REQUEST_FAILURE,

  RESOLVE_IS_AUTHENTICATED,
  RESOLVE_IS_DE_AUTHENTICATED
} from './constants';

const initialSatate = {
  isAuthenticated: false,
  isLoading: false,
  error: ''
};

export const authentication = (
  state = initialSatate,
  action
) => {
  switch (action.type) {
    case AUTHENTICATION_REQUEST:
      return Object.assign({}, state, {isLoading: true, error: ''});

    case RESOLVE_REQUEST_SUCCESS:
      return Object.assign({}, state, {isLoading: false});

    case RESOLVE_REQUEST_FAILURE:
      return Object.assign({}, state, {isLoading: false, error: action.payload});

    case RESOLVE_IS_AUTHENTICATED:
      return Object.assign({}, state, {isAuthenticated: true});

    case RESOLVE_IS_DE_AUTHENTICATED:
      return Object.assign({}, state, {isAuthenticated: false});
      
    default:
      return state;
  }
};
