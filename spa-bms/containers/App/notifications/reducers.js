import {
  DO_TRIGGER_NOTIFICATION,
  DO_SHOW_NOTIFICATION,
  DO_HIDE_NOTIFICATION
} from './constants';

const initialSatate = {
  show: false,
  msg: '',
  state: 'SUCCESS'
};

export const notification = (
  state = initialSatate,
  action
) => {
  switch (action.type) {
    case DO_TRIGGER_NOTIFICATION:
      return Object.assign({}, state, {msg: action.payload.msg, state: action.payload.state});

    case DO_SHOW_NOTIFICATION:
      return Object.assign({}, state, {show: true});

    case DO_HIDE_NOTIFICATION:
      return Object.assign({}, state, initialSatate);

    default:
      return state;
  }
};
