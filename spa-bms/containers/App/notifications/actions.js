import {
  DO_TRIGGER_NOTIFICATION,
  DO_SHOW_NOTIFICATION,
  DO_HIDE_NOTIFICATION
} from './constants';

export const doTriggerNotification = (msg, state = 'SUCCESS') => {
  return {
    type: DO_TRIGGER_NOTIFICATION,
    payload: {msg, state}
  };
};

export const doShowNotification = (msg, state) => {
  return {
    type: DO_SHOW_NOTIFICATION
  };
};

export const doHideNotification = () => {
  return {
    type: DO_HIDE_NOTIFICATION
  };
};
