import { put, takeEvery } from 'redux-saga/effects';
import { DO_TRIGGER_NOTIFICATION } from './constants';
import {
  doShowNotification,
  doHideNotification
} from './actions';

const delay = (ms) => {
  return new Promise(resolve => setTimeout(() => resolve(true), ms))
}

function *handleNotificationTrigger() {
  // Show the notification
  yield put(doShowNotification());

  // Then wait for 5 seconds
  yield delay(6000);

  // And hide it again
  yield put(doHideNotification())
}

export const notificationSaga = [
  takeEvery(DO_TRIGGER_NOTIFICATION, handleNotificationTrigger)
];
