import React, { Component } from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import './index.scss';
import { Notification } from '../../components/Notification';
import { Login } from '../../components/Login';
import Dashboard from '../Dashboard';
import {
  doFetchAuthentication,
  doAuthenticate,
  doDeAuthenticate,
  doTriggerNotification
} from './actions';

class App extends Component {

  constructor(props) {
    super(props);

    this.props.getAuthState();
  }

  render() {
    const { isAuthenticated, doLogin, doLogout } = this.props;
    const mainComponent = isAuthenticated ? <Dashboard doLogout={doLogout} /> : <Login doLogin={doLogin} />;

    return (
      <div className="App">
        { mainComponent }
        <Notification show={this.props.notification.show} msg={this.props.notification.msg} state={this.props.notification.state}/>
      </div>
    );
  }
}

App.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  isLoading: PropTypes.bool.isRequired,
  notification: PropTypes.object,
  doLogin: PropTypes.func.isRequired,
  doLogout: PropTypes.func.isRequired
};

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.authentication.isAuthenticated,
    isLoading: state.authentication.isLoading,
    notification: state.notification
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getAuthState: () => dispatch(doFetchAuthentication()),
    doLogin: (credentials) => dispatch(doAuthenticate(credentials)),
    doLogout: () => dispatch(doDeAuthenticate()),
    triggerNotification: (msg, state) => dispatch(doTriggerNotification(msg, state))
  }
}

App = connect(
  mapStateToProps,
  mapDispatchToProps
)(App);

export default App;
