import { all } from 'redux-saga/effects'
import { authenticationSaga, notificationSaga } from './containers/App/sagas';
import {
  productsSaga, treatmentsSaga, servicesSaga,
  suppliesSaga, ordersSaga, promotionsSaga, providersSaga,
  staffsSaga, usersSaga, bonusesSaga,
  clientsSaga, salesSaga,
  earningsSaga,
  cartSaga
} from './containers/Dashboard/sagas';

export function *rootSaga() {
  yield all([
    ...authenticationSaga,
    ...notificationSaga,
    ...productsSaga,
    ...treatmentsSaga,
    ...servicesSaga,
    ...suppliesSaga,
    ...ordersSaga,
    ...promotionsSaga,
    ...providersSaga,
    ...staffsSaga,
    ...usersSaga,
    ...bonusesSaga,
    ...clientsSaga,
    ...salesSaga,
    ...earningsSaga,
    ...cartSaga
  ]);
};
