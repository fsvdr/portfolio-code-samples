import {
  REQUEST_API,
  RESOLVE_REQUEST,
  REJECT_REQUEST
} from './constants';

export * from './containers/App/actions'; // Notifications and Auth

/**************************************
 * API REQUESTS
 **************************************/

export const requestApi = () => {
  return {
    type: REQUEST_API
  };
};

export const resolveRequest = () => {
  return {
    type: RESOLVE_REQUEST
  };
};

export const rejectRequest = () => {
  return {
    type: REJECT_REQUEST
  };
};
