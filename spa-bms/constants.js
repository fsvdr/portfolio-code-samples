export const API_URL = 'http://localhost:8080/API';

export const REQUEST_API = 'REQUEST_API';
export const RESOLVE_REQUEST = 'RESOLVE_REQUEST';
export const REJECT_REQUEST = 'REJECT_REQUEST';

export const PAYMENT_METHODS = [
  {id: 'Efectivo', name: 'Efectivo'},
  {id: 'Tarjeta de crédito', name: 'Tarjeta de crédito'},
  {id: 'Tarjeta de débito', name: 'Tarjeta de débito'},
];

export const ACCESS_CLEARANCE = [
  {id: 1, name: 'Empleado'},
  {id: 3, name: 'Administrativo'}
];

export const FREQUENCIES = [
  {id: 'Quincenal', name: 'Quincenal'},
  {id: 'Mensual', name: 'Mensual'},
  {id: 'Anual', name: 'Anual'}
];

export const PROMOTION_TYPES = [
  {id: '2x1', name: '2x1'},
  {id: '3x2', name: '3x2'},
  {id: 'Discount', name: 'Descuento'},
  {id: 'Percentage', name: 'Procentaje'},
];

export * from './containers/App/constants'; // Notifications and Auth
