import { combineReducers } from 'redux'
import { authentication, notification } from './containers/App/reducers';
import * as dashboard from './containers/Dashboard/reducers';

export const rootReducer = combineReducers({
  authentication,
  notification,
  ...dashboard
});
