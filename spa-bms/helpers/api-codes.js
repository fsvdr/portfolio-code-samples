/**
 * Maps API response status code's to a notification
 * data.
 * @param  {number} code Response status code
 * @return {object}      Notification data
 */
export const mapCodeToMessage = (code) => {
  switch (code) {
    case 403:
      return {msg: 'Credenciales incorrectas', state: 'WARNING'};
    case 406:
      return {msg: 'La orden aún no se puede completar', state: 'WARNING'};
    case 409:
      return {msg: 'Se econtró un duplicado', state: 'WARNING'};
    case 500:
      return {msg: 'Ocurrió un error, por favor intenta más tarde', state: 'ERROR'}
    default:
      return {msg: 'Ocurrió un error, por favor contacta al administrador', state: 'ERROR'};
  }
}
