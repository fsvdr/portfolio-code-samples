export const toMoney = (amount) => {
  let n = Number(amount).toFixed(2);
  return`$${Number(n).toLocaleString('en')}`;
}
