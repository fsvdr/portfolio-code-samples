export const lineData = {
    "1": {
        "color": "#f54a91"
    },
    "2": {
        "color": "#0164a8"
    },
    "3": {
        "color": "#9e9a36"
    },
    "4": {
        "color": "#6fb6ae"
    },
    "5": {
        "color": "#f7d417"
    },
    "6": {
        "color": "#da251c"
    },
    "7": {
        "color": "#e87b14"
    },
    "8": {
        "color": "#00923f"
    },
    "9": {
        "color": "#4f2926"
    },
    "A": {
        "color": "#991a91"
    },
    "B": {
        "color": "#018c65"
    },
    "12": {
        "color": "#b89d4e"
    }
};
