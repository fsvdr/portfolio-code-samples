export let graph = {
    vertexList: [],
    addVertex: function(vertex) {
        this.vertexList.push (vertex);
    },
    getVertex: function(id) {
        for (let i in this.vertexList) {
            if (this.vertexList[i].id == id) {
                return this.vertexList[i];
            }
        }
        return null;
    },
    add: function(destination, source) {
       source.edges.push(destination)
    },

    shortestPath: function(origin, destination) {
         let visited = [];
         let visitedObjects = [];
         let frontier = [];
         visited.push(origin.id);
         visitedObjects.push(origin);
         frontier.push(origin);

         let i = 0;

         while (frontier.length > 0) {
           let current = frontier.shift();

            if (current == destination) {
                visitedObjects.push(current);
                break;
            } else {
                for (let j in current.edges) {
                    if (visited.indexOf(current.edges[j].id) <= -1) {
                        visited.push(current.edges[j].id);
                        visitedObjects.push(current.edges[j]);
                        current.edges[j].parent = current;
                        frontier.push(current.edges[j]);
                    }

                }

            }

            i = i + 1;
         }

        let parent = visitedObjects.pop();
        let path = [];
         while (parent) {
            //console.log(parent.id);
            path.push(parent.id);
            parent = parent.parent;
         }
         return path.reverse();
    }
}
