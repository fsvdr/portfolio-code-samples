import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableWithoutFeedback
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import RouteListComponent from '../components/RouteListComponent';

import * as Stations from '../config/StationsGraphData';
import * as Subway from '../modules/SubwayModule';

/**
 * RouteView Component
 * Layout for route between origin and destination stations using Dijkstra
 * shortest path algorithm
 *
 * @prop    {Navigator} navigator   -   global navigator
 * @prop    {Object}    origin      -   origin station
 * @prop    {Object}    destination -   destination station
 *
 * @state   {Array<Object>}     route   -   stations from origin to destination
 *
 * @external {RouteListComponent}   RouteListComponent
 * @external {Object}   Stations    -   stations graph data module
 * @external {Object}   Subway      -   dijkstra algorithm module
 */
export default class RouteView extends Component {

    constructor(props){
        super(props);

        this.state = {
            route: []
        };

    }


    render(){
        let {stations, origin, destination} = this.props;
        let {route} = this.state;

        return (
            <View style={styles.container}>
                <View style={styles.navBar}>
                    <TouchableWithoutFeedback
                        onPress={() => this.props.navigator.pop()}>
                        <View>
                            <Icon name="ios-close" size={42} style={{color: '#ffffff', paddingTop: 2, paddingRight: 4}} />
                        </View>
                    </TouchableWithoutFeedback>
                </View>
                <View style={styles.header}>
                    <Text style={styles.subtitle}>Ruta más corta para</Text>
                    <Text style={styles.title}>{`${origin.name}-${destination.name}`.toUpperCase()}</Text>
                </View>
                <RouteListComponent stations={stations} route={route} />
            </View>
        );
    }

    componentDidMount(){
        let stations = Stations.data;
        let subway = Subway.graph;
        let {origin, destination} = this.props;

        for (var i in stations) {
            subway.addVertex ({
                "id": i,
                "edges": [],
                "parent": null
            });
        }

        for (var i in stations) {
            var stationDict = stations[i];

            var stationObject = subway.getVertex(i);
            for (var j in stationDict) {
                var neightborObject =  subway.getVertex(stations[i][j]);

                subway.add(neightborObject, stationObject);
                subway.add(stationObject, neightborObject);
            }
        }

        let path = subway.shortestPath(subway.getVertex(origin.name), subway.getVertex(destination.name));

        let route = path.map((stop) => {
            return this.props.stations.find((station) => {
                return station.name == stop;
            });
        });

        this.setState({route: route});
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 20,
        backgroundColor: '#000'
    },
    navBar: {
        flexDirection: 'row',
        height: 44,
        alignItems: 'center',
        justifyContent: 'flex-end',
        alignSelf: 'stretch',
        paddingHorizontal: 8,
    },
    header: {
        height: 56,
        alignSelf: 'stretch',
        paddingHorizontal: 18,
    },
    subtitle: {
        fontSize: 17,
        color: 'rgba(255, 255, 255, 0.53)',
        marginBottom: 8
    },
    title: {
        fontSize: 20,
        color: '#ffffff',
        fontWeight: 'bold'
    }
});
