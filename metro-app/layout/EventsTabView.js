import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    ListView,
    ActivityIndicator
} from 'react-native';
import RNShakeEvent from 'react-native-shake-event';

import EventPreviewComponent from '../components/EventPreviewComponent';

export default class EventsTabView extends Component{

    constructor(props){
        super(props);

        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => !Object.is(r1, r2)});

        this.state = {
            isLoading: true,
            events: [],
            dataSource: ds.cloneWithRows([])
        }

        this.fetchEvents = this.fetchEvents.bind(this);
        this._navigate = this._navigate.bind(this);
    }
    //====================================================================
    //  COMPONENT METHODS
    //====================================================================
    fetchEvents(){
        const url = 'http://metro.hyuchia.mx/events';

        fetch(url)
            .then( (response) => response.json())
            .then( (jsonData) => {
                this.setState({
                    isLoading: false,
                    dataSource: this.state.dataSource.cloneWithRows(jsonData)
                });
            })
            .catch( (error) => console.log(`Fetch error: ${error}`) );
    }

    renderRow(event, index){
        return (
            <EventPreviewComponent key={index} event={event} navigate={this._navigate}/>
        );
    }

    //====================================================================
    //  NAVIGATION
    //====================================================================
    _navigate(event){
        this.props.navigator.push({
            name: 'Event',
            passProps: {
                event: event
            },
            type: 'Modal'
        });
    }

    //====================================================================
    //  LIFECYCLE
    //====================================================================

    render(){
        let {isLoading, events, dataSource} = this.state;

        if (isLoading){
            return (
                <View style={styles.loadingContainer}>
                    <ActivityIndicator
                        animating={true}
                        color={'#fff'}
                        size={'small'}
                        style={{margin: 15}} />
                    <Text style={{color: '#fff'}}>Descargando eventos</Text>
                </View>
            );
        } else {
            return (
                <View style={styles.container}>
                    <View style={styles.eventsList}>
                        <ListView
                            style={{paddingTop: 28, paddingHorizontal: 24}}
                            enableEmptySections={true}
                            dataSource={dataSource}
                            renderRow={(rowData, sectionID, rowID) => this.renderRow(rowData, rowID)}/>
                    </View>
                </View>
            );
        }
    }

    componentDidMount(){
        this.fetchEvents();

        RNShakeEvent.addEventListener('shake', () => {
          this.setState({isLoading: true, events: []});
          this.fetchEvents();
        });
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        paddingBottom: 49
    },
    loadingContainer: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
});
