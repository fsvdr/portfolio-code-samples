import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView
} from 'react-native';

export default class InfoTabView extends Component{
    render(){
        return (
            <View style={styles.container}>
                <ScrollView>
                    <View style={styles.block}>
                        <Text style={styles.label}>Horarios de servicio</Text>
                        <View style={styles.data}>
                            <View style={styles.row}>
                                <Text style={styles.name}>Días laborales</Text>
                                <Text style={styles.detail}>5:00 a 24:00 horas</Text>
                            </View>
                            <View style={styles.row}>
                                <Text style={styles.name}>Sábados</Text>
                                <Text style={styles.detail}>6:00 a 24:00 horas</Text>
                            </View>
                            <View style={styles.row}>
                                <Text style={styles.name}>Domingos</Text>
                                <Text style={styles.detail}>7:00 a 24:00 horas</Text>
                            </View>
                            <View style={styles.row}>
                                <Text style={styles.name}>Días festivos</Text>
                                <Text style={styles.detail}>7:00 a 24:00 horas</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.block}>
                        <Text style={styles.label}>Costo del servicio</Text>
                        <View style={styles.data}>
                            <View style={styles.row}>
                                <Text style={styles.name}>Boleto normal</Text>
                                <Text style={styles.detail}>$5.00 MXN</Text>
                            </View>
                            <View style={styles.row}>
                                <Text style={styles.name}>Boleto estudiantil</Text>
                                <Text style={styles.detail}>$3.00 MXN</Text>
                            </View>
                            <View style={styles.row}>
                                <Text style={styles.name}>Tarifa diferenciada</Text>
                                <Text style={styles.detail}>$3.00 MXN</Text>
                            </View>
                            <View style={styles.row}>
                                <Text style={styles.name}>Tarjeta libre acceso</Text>
                                <Text style={styles.detail}>$3.00 MXN</Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 22,
        paddingTop: 18
    },
    block: {
        alignSelf: 'stretch',
        marginVertical: 12
    },
    label: {
        fontSize: 16,
        color: 'rgba(255, 255, 255, 0.53)'
    },
    data: {
        marginTop: 12,
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: 8
    },
    name: {
        fontSize: 17,
        color: '#ffffff',
        fontWeight: '700'
    },
    detail: {
        fontSize: 16,
        color: 'rgba(255, 255, 255, 0.53)'
    }
});
