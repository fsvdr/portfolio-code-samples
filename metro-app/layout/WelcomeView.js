import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  TouchableHighlight
} from 'react-native';

import Swiper from 'react-native-swiper';

export default class WelcomeView extends Component {

    //====================================================================
    //  NAVIGATION
    //====================================================================
    _navigate() {
        this.props.navigator.push({
            name: 'Home'
        });
    }

    //====================================================================
    //  LIFECYCLE
    //====================================================================

    render() {
        return (
            <View>
                <StatusBar
                    backgroundColor="#272327"
                    barStyle="light-content"/>
                <Swiper style={styles.wrapper}
                    dot={<View style={{backgroundColor:'rgba(255,255,255,.4)', width: 8, height: 8,borderRadius: 10, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3,}} />}
                    activeDot={<View style={{backgroundColor: '#FF3824', width: 12, height: 12, borderRadius: 7, marginLeft: 7, marginRight: 7}} />}
                    loop={false}>



                    <View style={styles.slide1}>
                        <Text style={styles.subtitle}>Bienvenido a</Text>
                        <Text style={styles.title}>METRO</Text>
                        <Text style={styles.description}>Encuentra la ruta más corta en el metro de la CDMX</Text>
                    </View>
                    <View style={styles.slide2}>
                    </View>
                    <View style={styles.slide3}>
                        <TouchableHighlight
                            style={styles.btn}
                            underlayColor={'#f01600'}
                            onPress={() => this._navigate()}>
                            <Text style={styles.btnText}>Comenzar</Text>
                        </TouchableHighlight>
                    </View>
                </Swiper>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
    },
    slide1: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'flex-start',
        backgroundColor: '#000',
        paddingLeft: 18,
        paddingBottom: 68
   },
   subtitle: {
       color: 'rgba(255, 255, 255, 0.53)',
       fontSize: 17
   },
   title: {
       color: '#ffffff',
       fontSize: 52,
       fontWeight: 'bold'
   },
   description: {
       color: '#ffffff',
       marginTop: 28
   },
   slide2: {
       flex: 1,
       justifyContent: 'center',
       alignItems: 'center',
       backgroundColor: '#000',
   },
   slide3: {
       flex: 1,
       justifyContent: 'flex-end',
       alignItems: 'center',
       backgroundColor: '#000',
       paddingBottom: 68
   },
   btn: {
       paddingVertical: 12,
       paddingHorizontal: 38,
       borderWidth: 1,
       borderRadius: 4,
       backgroundColor: 'rgba(0, 0, 0, 0)',
       borderColor: '#FF3824',
   },
   btnText: {
       color: '#ffffff'
   }
});
