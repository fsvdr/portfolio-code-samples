import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TextInput,
    TouchableWithoutFeedback
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import StationListComponent from '../components/StationListComponent';

export default class StationSelectionView extends Component {

    constructor(props){
        super(props);

        this.state = {
            search: "",
            matches: []
        }

        this.handleStationSearch = this.handleStationSearch.bind(this);
        this.handleStationSelection = this.handleStationSelection.bind(this);
    }
    //====================================================================
    //  COMPONENT METHODS
    //====================================================================
    handleStationSearch(text){
        this.setState({search: text});
        let matches = this.props.stations.filter((station) => {
            let search = text.toLowerCase().trim();
            let name = station.name.toLowerCase().trim();

            if (name.includes(search)) {
                return station;
            }
            return;
        });
        this.setState({matches: matches});
    }

    handleStationSelection(station){
        this.props.handleSelection(station);
        this.props.navigator.pop();
    }

    //====================================================================
    //  LIFECYCLE
    //====================================================================

    render() {
        let {search, matches} = this.state;
        let {handleSelection} = this.props;

        return (
            <View style={styles.container}>
                <View style={styles.navBar}>
                    <TouchableWithoutFeedback
                        onPress={() => this.props.navigator.pop()}>
                        <View>
                            <Icon name="ios-close" size={42} style={{color: '#ffffff', paddingTop: 2, paddingRight: 4}} />
                        </View>
                    </TouchableWithoutFeedback>
                </View>
                <View style={styles.inputContainer}>
                    <TextInput
                        style={styles.input}
                        autoCorrect={false}
                        autoFocus={true}
                        placeholder="Estación"
                        placeholderTextColor="rgba(255, 255, 255, 0.53)"
                        returnKeyType="default"
                        keyboardAppearance="dark"
                        clearButtonMode="while-editing"
                        value={search}
                        onChangeText={(text) => this.handleStationSearch(text)}
                         />
                </View>
                <View style={styles.results}>
                    <Text style={styles.label}>Resultados</Text>
                    <StationListComponent matches={matches} handleStationSelection={this.handleStationSelection}/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 20,
        backgroundColor: '#000'
    },
    navBar: {
        flexDirection: 'row',
        height: 44,
        alignItems: 'center',
        justifyContent: 'flex-end',
        alignSelf: 'stretch',
        paddingHorizontal: 8,
    },
    inputContainer: {
        height: 48,
        marginTop: 32,
        borderBottomWidth: 2,
        borderBottomColor: 'rgba(255, 255, 255, 0.24)',
        justifyContent: 'flex-end',
        paddingHorizontal: 16
    },
    input: {
        height: 48,
        alignSelf: 'stretch',
        fontSize: 36,
        fontWeight: 'bold',
        color: '#ffffff'
    },
    results: {
        paddingHorizontal: 18,
        marginVertical: 24
    },
    label: {
        color: 'rgba(255, 255, 255, 0.53)',
        fontSize: 17,
        marginBottom: 12
    },
});
