import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text
} from 'react-native';

import MapView from 'react-native-maps';
import StationMapMarkerComponent from '../components/StationMapMarkerComponent';

export default class MapTabView extends Component{

    constructor(props){
        super(props);

        this.state = {
            userLatitude: 19.3910038,
            userLongitude: -99.2837001,
            markers: {}
        };
    }

    //====================================================================
    //  COMPONENT METHODS
    //====================================================================

    fetchMarkers(){
        const url = "https://metro.hyuchia.mx/map";

        fetch(url)
            .then((response) => response.json())
            .then((data) => {
                this.setState({markers: data})
            })
            .catch( (error) => console.log(`Fetch error: ${error}`) );
    }

    //====================================================================
    //  NAVIGATION
    //====================================================================

    _navigate(station){
        this.props.navigator.push({
            name: 'Station',
            passProps: {
                station: station
            }
        });
    }

    //====================================================================
    //  LIFECYCLE
    //====================================================================

    render(){
        let {userLatitude, userLongitude, markers} = this.state;
        let stationsMarkers = Object.keys(markers).map((key) => {
            return (markers[key]).map((station, index) => (
                <StationMapMarkerComponent marker={station} navigate={this._navigate} key={`${key}${index}`} />
            ));
        })

        return(
            <View style={styles.container}>
                <MapView
                    style={styles.map}
                    showsUserLocation={true}
                    showsMyLocationButton={true}
                    followsUserLocation={false}
                    region={{
                      latitude: userLatitude,
                      longitude: userLongitude,
                      latitudeDelta: 0.0922,
                      longitudeDelta: 0.0421,
                    }}>
                    {stationsMarkers}
                </MapView>
            </View>
        );
    }

    componentDidMount(){
        this.props.navigator.geolocation.getCurrentPosition((position) => {
            let lat = position.coords.latitude;
            let lon = position.coords.longitude;
            console.log();
            this.setState({userLatitude: lat, userLongitude: lon});
        });
        this.fetchMarkers();
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    map: {
        flex: 1,
    }
});
