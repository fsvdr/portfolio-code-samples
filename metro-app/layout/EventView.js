import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableWithoutFeedback,
    Image,
    ScrollView
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import StationLineBadges from '../components/StationLineBadges';

export default class EventView extends Component{
    render(){
        let {event} = this.props;

        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Image
                        style={styles.heroHeader}
                        resizeMode="cover"
                        source={{uri: event.Image}} />
                    <View style={styles.overlay}>
                        <View style={styles.location}>
                            <StationLineBadges lines={[event.Line]} style={{paddingTop: 6}}/>
                            <Text style={styles.locationName}>{event.Station}</Text>
                        </View>
                        <View style={styles.info}>
                            <Text style={styles.title}>{event.Name}</Text>
                        </View>
                    </View>
                </View>

                <View style={styles.navBar}>
                    <TouchableWithoutFeedback
                        onPress={() => this.props.navigator.pop()}>
                        <View>
                            <Icon name="ios-close" size={42} style={{backgroundColor: 'rgba(0, 0, 0, 0)', color: '#ffffff', paddingTop: 2, paddingRight: 4}} />
                        </View>
                    </TouchableWithoutFeedback>
                </View>

                <View style={styles.detailsContainer}>
                    <View style={styles.detail}>
                        <View style={styles.detailBox}>
                            <Icon name="ios-keypad" size={24} style={styles.detailIcon} />
                        </View>
                        <View style={styles.detailBox}>
                            <Icon name="ios-calendar-outline" size={24} style={styles.detailIcon} />
                        </View>
                        <View style={styles.detailBox}>
                            <Icon name="ios-clock-outline" size={24} style={styles.detailIcon} />
                        </View>
                    </View>
                    <View style={styles.detail}>
                        <View style={styles.detailBox}>
                            <Text style={styles.detailName}>{event.Category}</Text>
                        </View>
                        <View style={styles.detailBox}>
                            <Text style={styles.detailName}>{event.Date}</Text>
                        </View>
                        <View style={styles.detailBox}>
                            <Text style={styles.detailName}>{`${event.StartTime} - ${event.EndTime}`}</Text>
                        </View>
                    </View>
                </View>
                <ScrollView style={{paddingHorizontal: 24}}>
                    <Text style={styles.description}>{event.Description}</Text>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000'
    },
    navBar: {
        flexDirection: 'row',
        height: 44,
        alignItems: 'center',
        justifyContent: 'flex-end',
        alignSelf: 'stretch',
        paddingHorizontal: 8,
        position: 'absolute',
        top: 20,
        left: 0,
        right: 0
    },
    header: {
        height: 220,
    },
    heroHeader: {
        alignSelf: 'stretch',
        height: 220,
    },
    overlay: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        height: 220,
        backgroundColor: 'rgba(0, 0, 0, 0.80)',
        paddingHorizontal: 24,
        paddingBottom: 8,
        justifyContent: 'flex-end'
    },
    location: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingTop: 16
    },
    locationName: {
        fontWeight: '700',
        color: '#ffffff',
        fontSize: 18,
        marginRight: 16
    },
    info: {
        alignSelf: 'stretch'
    },
    title: {
        fontSize: 24,
        color: '#ffffff',
        fontWeight: '700',
        marginBottom: 18
    },
    detailsContainer: {
        paddingVertical: 12,
        paddingHorizontal: 12,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(255, 255, 255, 0.12)',
        borderTopWidth: 1,
        borderTopColor: 'rgba(255, 255, 255, 0.12)'
    },
    detail: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    detailBox: {
        flex: 1,
        alignItems: 'center',
        marginHorizontal: 4,
    },
    detailIcon: {
        color: 'rgba(255, 255, 255, 0.53)'
    },
    detailName: {
        color: 'rgba(255, 255, 255, 0.53)',
        fontWeight: '600',
        marginTop: 3,
        fontSize: 12,
        textAlign: 'center'
    },
    description: {
        color: 'rgba(255, 255, 255, 0.87)',
        lineHeight: 22,
        fontSize: 17,
        paddingVertical: 18,
    }
});
