import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  TabBarIOS,
  Alert,
  TouchableWithoutFeedback
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import RouteTabView from './RouteTabView';
import EventsTabView from './EventsTabView';
import InfoTabView from './InfoTabView';
import MapTabView from './MapTabView';

export default class HomeView extends Component {

    constructor(props){
        super(props);

        this.state = {
            selectedTab: "route"
        };
    }

    //====================================================================
    //  COMPONENT METHODS
    //====================================================================


    //====================================================================
    //  NAVIGATION
    //====================================================================
    _renderContent(num) {
        return (
            <View style={{flex: 1}}>
                <Text>TAB #{num}</Text>
            </View>
        );
    }

    //====================================================================
    //  NAVIGATION
    //====================================================================
    _navigateTo(name, props?, type?){
        this.props.navigator.push({
            name: name,
            passProps: props || {},
            type: type || null
        });
    }

    //====================================================================
    //  LIFECYCLE
    //====================================================================

    render() {
        let { selectedTab } = this.state;
        let navBarQRButton;
        if (selectedTab == "info") {
            navBarQRButton = (
                <TouchableWithoutFeedback
                    onPress={() => this._navigateTo('QR', null, 'Modal')}>
                    <View>
                        <Icon name="ios-qr-scanner" size={32} style={styles.qrBtn} />
                    </View>
                </TouchableWithoutFeedback>
            );
        }

        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor="#272327"
                    barStyle="light-content"/>
                <View style={styles.navBar}>
                    <View style={[styles.navBarButton, {alignItems: 'flex-start'}]}>

                    </View>
                    <View style={styles.navBarButton}>
                        <Text style={styles.logo}>METRO</Text>
                    </View>
                    <View style={[styles.navBarButton, {alignItems: 'flex-end'}]}>
                        {navBarQRButton}
                    </View>
                </View>
                <TabBarIOS
                    style={styles.tabBar}
                    unselectedTintColor="rgba(255, 255, 255, 0.53)"
                    tintColor="#FF3824"
                    barTintColor="#000"
                    translucent={true}>
                    <Icon.TabBarItemIOS
                        title="Ruta"
                        iconName="ios-compass-outline"
                        iconSize={26}
                        selected={selectedTab == "route"}
                        onPress={() => this.setState({selectedTab: "route"})}>
                        <RouteTabView navigator={this.props.navigator} />
                    </Icon.TabBarItemIOS>
                    <Icon.TabBarItemIOS
                        title="Mapa"
                        iconName="ios-map-outline"
                        iconSize={26}
                        selected={selectedTab == "map"}
                        onPress={() => this.setState({selectedTab: "map"})}>
                        <MapTabView navigator={navigator} />
                    </Icon.TabBarItemIOS>
                    <Icon.TabBarItemIOS
                        title="Eventos"
                        iconName="ios-megaphone-outline"
                        iconSize={24}
                        selected={selectedTab == "events"}
                        onPress={() => this.setState({selectedTab: "events"})}>
                        <EventsTabView navigator={this.props.navigator} />
                    </Icon.TabBarItemIOS>
                    <Icon.TabBarItemIOS
                        title="Información"
                        iconName="ios-information-circle-outline"
                        iconSize={26}
                        selected={selectedTab == "info"}
                        onPress={() => this.setState({selectedTab: "info"})}>
                        <InfoTabView navigator={this.props.navigator} />
                    </Icon.TabBarItemIOS>
                </TabBarIOS>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 20,
        backgroundColor: '#000'
    },
    navBar: {
        alignSelf: 'stretch',
        flexDirection: 'row',
        height: 44,
        alignItems: 'center',
        alignSelf: 'stretch',
        paddingHorizontal: 8,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(255, 255, 255, 0.12)'
    },
    navBarButton: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    logo: {
        fontWeight: 'bold',
        color: '#ffffff',
        fontSize: 17,
    },
    qrBtn: {
        width: 48,
        color: '#ffffff',
        fontWeight: '700',
        paddingTop: 2,
        paddingLeft: 12,
    },
    tabBar: {
        height: 49
    }
});
