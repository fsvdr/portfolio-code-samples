import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  TabBarIOS,
  TouchableHighlight,
  TouchableWithoutFeedback,
  Alert,
  ScrollView,
  AsyncStorage
} from 'react-native';
import RNShakeEvent from 'react-native-shake-event';

import TravelComponent from '../components/TravelComponent';
import HistoryListComponent from '../components/HistoryListComponent';

import {stations} from '../config/StationsData';

// const history = [
//     {origin: "Observatorio", destination: "Tacubaya"},
//     {origin: "Balbuena", destination: "Boulevard Pto. Aéreo"}
// ];

export default class RouteTabView extends Component {

    constructor(props){
        super(props);

        this.state = {
            history: [],
            origin: {},
            destination: {}
        };

        this.loadHistory = this.loadHistory.bind(this);
        this.saveHistoryToStorage = this.saveHistoryToStorage.bind(this);
        this.saveTravelToHistory = this.saveTravelToHistory.bind(this);
        this.handleOriginChange = this.handleOriginChange.bind(this);
        this.handleDestinationChange = this.handleDestinationChange.bind(this);
        this.handleTravelValidation = this.handleTravelValidation.bind(this);
        this.handleTravelSelection = this.handleTravelSelection.bind(this);
    }

    //====================================================================
    //  COMPONENT METHODS
    //====================================================================

    async loadHistory(){
        let history;

        try {
            //await AsyncStorage.removeItem("TravelHistory");
            history = await AsyncStorage.getItem('TravelHistory');
        } catch (error) {
            console.log(`ERROR FETCHING ASYNCSTORAGE: ${error}`);
        }

        if (history){
            console.log(JSON.parse(history));
            this.setState({history: JSON.parse(history)});
        }
    }

    async saveHistoryToStorage(newHistory){
        try {
            await AsyncStorage.setItem('TravelHistory', JSON.stringify(newHistory));
        } catch (error) {
        }
    }

    saveTravelToHistory(origin, destination){
        let {history} = this.state;

        let newHistory = history;

        let travel = [{origin: origin, destination: destination}];
        let travelIsInHistory = false;

        if (history) {
            let foundTravel = history.filter((t) => {
                return t.origin == origin && t.destination == destination;
            });

            travelIsInHistory = foundTravel.length ? true : false;

            if (!travelIsInHistory) {
                newHistory = travel.concat(history).slice(0, 2);
                this.saveHistoryToStorage(newHistory);
            }
        } else {
            newHistory = travel;
            this.saveHistoryToStorage(newHistory);
        }

        this.setState({history: newHistory});
    }

    handleOriginChange(origin) {
        this.setState({origin: origin});
    }

    handleDestinationChange(destination) {
        this.setState({destination: destination});
    }

    handleTravelValidation(){
        let {origin, destination} = this.state;

        if (origin.name && destination.name){
            this._navigateTo('Route', {stations: stations, origin: origin, destination: destination}, 'Modal');
            this.saveTravelToHistory(origin.name, destination.name);
        } else {
            Alert.alert('Campos vacios', 'Por favor selecciona tu estación origen y destino para generar una ruta');
        }
    }

    handleTravelSelection(origin, destination){
        let newOrigin = stations.find((station) => {
            return station.name == origin;
        });
        let newDestination = stations.find((station) => {
            return station.name == destination;
        });

        this.handleOriginChange(newOrigin);
        this.handleDestinationChange(newDestination);
    }

    //====================================================================
    //  NAVIGATION
    //====================================================================
    _navigateTo = (name, props?, type?) => {
        this.props.navigator.push({
            name: name,
            passProps: props || {},
            type: type || null
        });
    }

    //====================================================================
    //  LIFECYCLE
    //====================================================================

    render() {
        let {history, origin, destination} = this.state;
        return (
            <View style={styles.container}>
                <ScrollView>
                    <TravelComponent
                        stations={stations}
                        origin={this.state.origin}
                        destination={this.state.destination}
                        _navigate={this._navigateTo}
                        handleOriginChange={this.handleOriginChange}
                        handleDestinationChange={this.handleDestinationChange} />
                    <View style={styles.actionsContainer}>
                        <View style={styles.action}>
                            <TouchableHighlight
                                style={[styles.btn, styles.ghost]}
                                underlayColor={'#f01600'}
                                onPress={() => this.handleTravelValidation()}>
                                <Text style={styles.btnText}>Buscar ruta</Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                    <HistoryListComponent history={history} handleTravelSelection={this.handleTravelSelection}/>
                </ScrollView>
            </View>
        );
    }

    componentDidMount(){

        this.loadHistory();

        RNShakeEvent.addEventListener('shake', () => {
          console.log("shake");
          this.setState({origin: {}, destination: {}});
        });
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        paddingHorizontal: 24,
        paddingTop: 28,
        paddingBottom: 49
    },
    actionsContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 36,
        marginBottom: 36
    },
    action: {
        flex: 1,
        marginHorizontal: 4,
        alignSelf: 'center',
    },
    btn: {
        paddingVertical: 12,
        paddingHorizontal: 38,
        borderWidth: 1,
        backgroundColor: '#FF3824',
        borderRadius: 4
    },
    ghost: {
        backgroundColor: 'rgba(0, 0, 0, 0)',
        borderColor: '#FF3824',
    },
    btnText: {
        color: '#fff',
        alignSelf: 'stretch',
        textAlign: 'center'
    }
});
