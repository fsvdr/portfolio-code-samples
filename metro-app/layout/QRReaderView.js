import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableWithoutFeedback,
    VibrationIOS,
    Alert
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import Camera from 'react-native-camera';

export default class QRReaderView extends Component{

    constructor(props){
        super(props);

        this.state = {
            hasReadQR: false
        };

        this.handleQRRead = this.handleQRRead.bind(this);
    }

    //====================================================================
    //  COMPONENT METHODS
    //====================================================================

    handleQRRead(result){
        let {hasReadQR} = this.state;

        if (!hasReadQR) {
            this.setState({hasReadQR: true});

            setTimeout(() => {
                VibrationIOS.vibrate();
                Alert.alert("Información del código", result.data);
                this.props.navigator.pop();
            }, 1000);

        }
    }

    //====================================================================
    //  LIFECYCLE
    //====================================================================

    render(){
        let {hasReadQR} = this.state;
        let scannerColor = 'rgba(255, 255, 255, 0.53)';

        if (hasReadQR) {
            scannerColor = '#A6ED8E';
        }

        return (
            <View style={styles.container}>
                <View style={styles.navBar}>
                    <View style={[styles.navBarButton, {alignItems: 'flex-start'}]}>

                    </View>
                    <View style={styles.navBarButton}>
                        <Text style={styles.logo}>METRO</Text>
                    </View>
                    <View style={[styles.navBarButton, {alignItems: 'flex-end'}]}>
                        <TouchableWithoutFeedback
                            onPress={() => this.props.navigator.pop()}>
                            <View>
                                <Icon name="ios-close" size={42} style={{backgroundColor: 'rgba(0, 0, 0, 0)', color: '#ffffff', paddingTop: 2, paddingRight: 4}} />
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </View>
                <Camera
                  ref={(cam) => {
                    this.camera = cam;
                  }}
                  style={styles.preview}
                  onBarCodeRead={(data) => this.handleQRRead(data)}
                  aspect={Camera.constants.Aspect.fill}>
                  <View>
                    <Icon name="ios-qr-scanner" size={320} style={[styles.scanner, {color: scannerColor}]} />
                  </View>
                </Camera>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 20,
        backgroundColor: '#000'
    },
    navBar: {
        alignSelf: 'stretch',
        flexDirection: 'row',
        height: 44,
        alignItems: 'center',
        alignSelf: 'stretch',
        paddingHorizontal: 8,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(255, 255, 255, 0.12)'
    },
    navBarButton: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    logo: {
        fontWeight: 'bold',
        color: '#ffffff',
        fontSize: 17,
    },
    preview: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    scanner: {
        fontSize: 320,
        backgroundColor: 'rgba(0, 0, 0, 0)'
    },
    toolBar: {
        height: 56
    }
});
