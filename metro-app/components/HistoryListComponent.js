import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableWithoutFeedback
} from 'react-native';

export default class HistoryListComponent extends Component {

    constructor(props) {
        super(props);

    }

    //====================================================================
    //  LIFECYCLE
    //====================================================================

    render() {
        let {history, handleTravelSelection} = this.props;
        let travels = history ? history.map((travel, index) => {
            return (
                <TouchableWithoutFeedback
                    onPress={() => handleTravelSelection(travel.origin, travel.destination)}
                    key={index}>
                    <View style={styles.travel}>
                        <Text style={styles.travelLabel}>{`${travel.origin} - ${travel.destination}`}</Text>
                    </View>
                </TouchableWithoutFeedback>
            );
        }) : [];

        return (
            <View style={styles.history}>
                <Text style={styles.inputLabel}>Viajes previos</Text>
                <View style={styles.historyList}>
                    {travels}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    history: {
        marginTop: 0
    },
    inputLabel: {
        color: 'rgba(255, 255, 255, 0.53)',
        fontSize: 17,
        marginBottom: 12
    },
    travel: {
        height: 44,
        justifyContent: 'center',
        borderRadius: 4,
        backgroundColor: 'rgba(255, 255, 255, 0.06)',
        marginBottom: 16
    },
    travelLabel: {
        alignSelf: 'center',
        color: 'rgba(255, 255, 255, 0.87)'
    },
});
