import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import StationLineBadges from '../components/StationLineBadges';

export default class EventPreviewComponent extends Component{

    render(){
        let {event} = this.props;
        return (
            <TouchableOpacity
                onPress={() => this.props.navigate(event)} >
                <View style={styles.event} >
                    <View style={styles.details}>
                        <View style={styles.location}>
                            <StationLineBadges lines={[event.Line]} style={{paddingTop: 6}}/>
                            <Text style={styles.locationName}>{event.Station}</Text>
                        </View>
                        <View style={styles.info}>
                            <Text style={styles.title}>{event.Name}</Text>
                            <Text
                                style={styles.description}
                                numberOfLines={3} >
                                {event.Description}
                            </Text>
                            <View style={styles.dateContainer}>
                                <Icon name="ios-calendar-outline" size={20} style={{color: 'rgba(255, 255, 255, 0.53)'}} />
                                <Text style={styles.date}>{event.Date}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.action}>
                        <Icon name="ios-arrow-forward" size={42} style={{color: 'rgba(255, 255, 255, 0.24)'}} />
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    event: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 12
    },
    details: {
        alignSelf: 'stretch',
        flex: 4
    },
    location: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingTop: 16
    },
    locationName: {
        fontWeight: '700',
        color: '#ffffff',
        fontSize: 18,
        marginRight: 16
    },
    info: {
        alignSelf: 'stretch'
    },
    title: {
        fontSize: 24,
        color: '#ffffff',
        fontWeight: '700',
        marginBottom: 18
    },
    description: {
        fontSize: 14,
        color: 'rgba(255, 255, 255, 0.87)'
    },
    action: {
        flex: 1,
        alignItems: 'flex-end'
    },
    dateContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 16
    },
    date: {
        color: 'rgba(255, 255, 255, 0.53)',
        marginLeft: 16,
        fontStyle: 'italic'
    }
});
