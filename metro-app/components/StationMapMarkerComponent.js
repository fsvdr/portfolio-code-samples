import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text
} from 'react-native';

import {lineData} from '../config/LineData';
import MapView from 'react-native-maps';

export default class StationMapMarkerComponent extends Component{

    constructor(props){
        super(props);

        this.state = {
            hasPressed: false
        }

        this.handleStationSelection = this.handleStationSelection.bind(this);
    }

    //====================================================================
    //  COMPONENT METHODS
    //====================================================================

    handleStationSelection(){
        if (this.state.hasPressed) {
            //this.props.navigate(this.props.marker.title);
            console.log("NAVIGATE!");
        } else {
            this.setState({hasPressed: true});
        }
    }

    //====================================================================
    //  LIFECYCLE
    //====================================================================

    render(){
        let {marker} = this.props;

        return (
            <View>
                <MapView.Marker
                    coordinate={marker.latlng}
                    title={marker.title}
                    description={`Línea ${marker.line}`}
                    pinColor={lineData[marker.line].color}
                    onPress={() => this.handleStationSelection()}
                    onSelect={() => console.log("That was a selection")}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({

});
