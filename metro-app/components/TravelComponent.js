import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import TravelInputComponent from './TravelInputComponent';

export default class TravelComponent extends Component {

    render() {
        let {origin, destination, handleOriginChange, handleDestinationChange} = this.props;

        return (
            <View style={styles.container}>
                <TravelInputComponent
                    stations={this.props.stations}
                    isOrigin={true}
                    station={origin}
                    _navigate={this.props._navigate}
                    handleSelection={handleOriginChange} />
                <TravelInputComponent
                    stations={this.props.stations}
                    isOrigin={false}
                    station={destination}
                    _navigate={this.props._navigate}
                    handleSelection={handleDestinationChange} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});
