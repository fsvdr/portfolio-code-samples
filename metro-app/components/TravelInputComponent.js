import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  TabBarIOS,
  TouchableHighlight,
  TouchableWithoutFeedback
} from 'react-native';

import StationLineBadges from './StationLineBadges';

export default class TravelInputComponent extends Component {

    constructor(props){
        super(props);
    }

    render() {
        let {stations, isOrigin, station, handleSelection} = this.props;
        let label = isOrigin ? 'origen' : 'destino';
        let lines = (<View></View>);
        let input = (<Text style={styles.placeholder}>Seleccionar</Text>);
        let points = (
            <View style={[styles.decoration, {justifyContent: 'flex-start'}]}>
                <View style={styles.mainPoint}></View>
                <View style={styles.point}></View>
                <View style={styles.point}></View>
                <View style={styles.point}></View>
                <View style={styles.point}></View>
                <View style={styles.point}></View>
            </View>
        );

        if (!isOrigin) {
            points = (
                <View style={[styles.decoration, {justifyContent: 'space-between'}]}>
                    <View style={styles.point}></View>
                    <View style={styles.point}></View>
                    <View style={styles.point}></View>
                    <View style={styles.point}></View>
                    <View style={styles.mainPoint}></View>
                </View>
            );
        }

        if (Object.keys(station).length != 0) {
            lines = (<StationLineBadges lines={station.lines} />);
            input = (<Text style={styles.stationName} numberOfLines={1}>{station.name.toUpperCase()}</Text>);

        }

        if (!isOrigin && Object.keys(station).length == 0) {
            points = (
                <View style={[styles.decoration, {justifyContent: 'flex-end'}]}>
                    <View style={styles.point}></View>
                    <View style={styles.point}></View>
                    <View style={styles.mainPoint}></View>
                </View>
            );
        }

        return (
            <View style={styles.stationInput}>
                <TouchableWithoutFeedback
                    onPress={() => this.props._navigate('Stations', {
                        stations: stations,
                        handleSelection: handleSelection}, 'Modal')} >
                    <View style={styles.container}>
                        {points}
                        <View style={styles.input}>
                            <Text style={styles.inputLabel}>Estación {label}</Text>
                            <View style={styles.stationLines}>
                                { lines }
                            </View>
                            { input }
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    stationInput: {
        alignSelf: 'stretch'
    },
    container: {
        flexDirection: 'row'
    },
    decoration: {
        flex: 1,
        marginRight: 24,
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    mainPoint: {
        width: 20,
        height: 20,
        borderRadius: 10,
        backgroundColor: '#FF3824',
        marginTop: 0,
        marginBottom: 12
    },
    point: {
        width: 10,
        height: 10,
        borderRadius: 5,
        backgroundColor: '#ffffff',
        marginBottom: 10
    },
    input: {
        flex: 10
    },
    inputLabel: {
        color: 'rgba(255, 255, 255, 0.53)',
        fontSize: 17,
        marginBottom: 12
    },
    placeholder: {
        color: 'rgba(255, 255, 255, 0.12)',
        fontSize: 32,
        fontWeight: 'bold'
    },
    stationLines: {
        flexDirection: 'row',
        marginBottom: 8
    },
    lineBadge: {
        backgroundColor: '#FF3824',
        width: 26,
        borderRadius: 13,
        height: 26,
        justifyContent: 'center',
        marginRight: 8
    },
    badgeText: {
        color: '#ffffff',
        alignSelf: 'center',
        fontWeight: 'bold',
        fontSize: 12
    },
    stationName: {
        color: '#ffffff',
        fontSize: 32,
        fontWeight: 'bold',
        alignSelf: 'stretch'
    },
});
