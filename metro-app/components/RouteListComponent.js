import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    ListView
} from 'react-native';

import StationLineBadges from './StationLineBadges';

/**
 * RouteListComponent Component
 * Represents a station from the route
 *
 * @prop    {Array<Object>}     stations  -   stations data
 * @prop    {Array<String>}    route      -   stations from origin to destination
 */
export default class RouteListComponent extends Component{

    constructor(props){
        super(props);

        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => !Object.is(r1, r2)});

        this.state = {
            dataSource: ds.cloneWithRows([])
        };
    }

    //====================================================================
    //  COMPONENT METHODS
    //====================================================================

    renderRouteStop(station, index){
        let middlePoints = null;

        if (index != this.props.route.length - 1) {
            middlePoints = (
                <View>
                <View style={styles.point}></View>
                <View style={styles.point}></View>
                </View>
            );
        }

        return (
            <View style={styles.stop}>
                <View style={styles.decoration}>
                    <View style={styles.mainPoint}></View>
                    {middlePoints}
                </View>
                <View style={styles.station}>
                    <Text style={styles.stationName}>{station.name}</Text>
                    <View style={styles.stationLines}>
                        <StationLineBadges lines={station.lines} />
                    </View>
                </View>
            </View>
        );
    }

    //====================================================================
    //  LIFECYCLE
    //====================================================================

    render(){
        return (
            <View style={styles.container}>
            <ListView
                enableEmptySections={true}
                dataSource={this.state.dataSource}
                renderRow={(rowData, sectionID, rowID, highlightRow) => this.renderRouteStop(rowData, rowID)} />
            </View>
        );
    }

    componentWillReceiveProps(nextProps){
        this.setState(() => {
            if (!Object.is(this.props, nextProps)) {
                return {dataSource: this.state.dataSource.cloneWithRows(nextProps.route)};
            }
        });
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 20,
        paddingHorizontal: 18
    },
    stop: {
        alignSelf: 'stretch',
        flexDirection: 'row',
        flexWrap: 'nowrap',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        marginBottom: 8
    },
    decoration: {
        marginRight: 36,
        marginLeft: 20,
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    mainPoint: {
        width: 20,
        height: 20,
        borderRadius: 10,
        backgroundColor: '#FF3824',
        marginTop: 8,
        marginBottom: 12
    },
    point: {
        width: 10,
        height: 10,
        borderRadius: 5,
        backgroundColor: '#ffffff',
        marginBottom: 12
    },
    stationLines: {
        flexDirection: 'row',
        marginTop: 12
    },
    lineBadge: {
        backgroundColor: '#FF3824',
        width: 26,
        borderRadius: 13,
        height: 26,
        justifyContent: 'center',
        marginRight: 8
    },
    badgeText: {
        color: '#ffffff',
        alignSelf: 'center',
        fontWeight: 'bold',
        fontSize: 12
    },
    stationName: {
        color: '#ffffff',
        fontSize: 26,
        fontWeight: 'bold'
    },
});
