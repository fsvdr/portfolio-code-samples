import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableWithoutFeedback,
    ListView
} from 'react-native';

import StationLineBadges from './StationLineBadges';

export default class StationListComponent extends Component {

    constructor(props){
        super(props);

        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => !Object.is(r1, r2)});

        this.state = {
            dataSource: ds.cloneWithRows([])
        };
    }

    //====================================================================
    //  COMPONENT METHODS
    //====================================================================

    renderRow(station){
        let {handleStationSelection} = this.props;
        return (
            <TouchableWithoutFeedback
                underlayColor="rgba(0, 0, 0, 0.87)"
                onPress={() => handleStationSelection(station)}
                key={station.name} >
                <View style={styles.station}>
                    <Text style={styles.name}>{station.name}</Text>
                    <View style={styles.stationLines}>
                        <StationLineBadges lines={station.lines} />
                    </View>
                </View>
            </TouchableWithoutFeedback>
        );
    }

    //====================================================================
    //  LIFECYCLE
    //====================================================================
    render() {
        return (
            <View>
                <ListView
                    enableEmptySections={true}
                    dataSource={this.state.dataSource}
                    renderRow={(rowData) => this.renderRow(rowData)} />
            </View>
        );
    }

    componentWillReceiveProps(nextProps){
        this.setState(() => {
            if (!Object.is(this.props, nextProps)) {
                return {dataSource: this.state.dataSource.cloneWithRows(nextProps.matches)};
            }
        });
    }
}

const styles = StyleSheet.create({
    station: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 8,
        paddingBottom: 8,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(255, 255, 255, 0.06)'
    },
    name: {
        fontSize: 24,
        color: '#ffffff'
    },
});
