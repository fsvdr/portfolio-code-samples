import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
} from 'react-native';

import {lineData} from '../config/LineData';

export default class StationLineBadges extends Component {
    render() {
        let {lines} = this.props;
        return (
            <View style={styles.badges}>
                {lines.map((line) => {
                    return (
                        <View style={[styles.lineBadge, {backgroundColor: lineData[line].color}]} key={line}>
                            <Text style={styles.badgeText}>{`L${line}`}</Text>
                        </View>
                    );
                })}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    badges: {
        flexDirection: 'row',
        marginBottom: 8
    },
    lineBadge: {
        backgroundColor: '#ffffff',
        width: 26,
        borderRadius: 13,
        height: 26,
        justifyContent: 'center',
        marginRight: 8
    },
    badgeText: {
        color: '#ffffff',
        alignSelf: 'center',
        fontWeight: 'bold',
        fontSize: 12
    },
});
